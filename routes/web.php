<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return redirect('/');
});

//Route::get('/test', 'social\InstagramController@test');
Route::get('/verify-email/{token}', 'Auth\RegisterController@verifyEmail')->name('verifyEmail');
Route::get('/terms-and-conditions', function () {
    return view('user.terms_and_conditions');
})->name('terms_and_conditions');

Route::get('/development-environment/login', 'GuestController@showLoginFormDevelopment')->name('development.show.login');
Route::post('development-environment/login', 'GuestController@developmentLogin')->name('development.login');

Route::get("/photo/{token}", "GalleryController@paymentPhoto")->name("payment-photo");

Route::group(['middleware' => 'development-environment'], function () {

    Route::get('/', 'GuestController@first_page')->name('first_page');
    Route::get('/tag/{tag}', 'TagController@tag')->name('tag');
    Route::get('/check_age', 'GuestController@check_age')->name('check_age');

    Route::get('/business', 'GuestController@business')->name('business');
    Route::get('/professionals', 'GuestController@professionals')->name('professionals');

    Route::group(['middleware' => 'check-age'], function () {

        Route::get('/feed', 'GuestController@index')->name('home');
        //Route::get('/testtesttest', 'GuestController@test')->name('home');
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
        Route::get('/create-account', 'Auth\RegisterController@showRegistrationForm')->name('create-account');

        Route::group(['prefix' => 'geo'], function () {
            Route::get('/countries', 'GeoController@GetAllCountry');
            Route::get('/cities', 'GeoController@GetCities');
            Route::get('/states', 'GeoController@GetStates');
            Route::get('/cities_state', 'GeoController@GetCitiesByState');
        });

        Route::get('/search', 'SearchController@index')->name('search');


        Route::post('/post/get_page', 'PostController@get_page');

        Route::group(['middleware' => 'custom-auth'], function () {

            Route::group(['prefix' => 'stream'], function () {
                Route::get('/{id}', 'Stream\StreamController@connect_stream')->name('connect_stream');
                Route::get('/start/{id}', 'Stream\StreamController@start_stream')->name('start_stream');
            });

            Route::group(['prefix' => 'live'], function () {
                Route::get('/', 'Stream\LiveController@index')->name('index-live');
                Route::get('/start', 'Stream\LiveController@start_live')->name('start-live');
                Route::get('/view/{user}', 'Stream\LiveController@view')->name('view-stream');
            });

            Route::group(['prefix' => 'profile'], function () {
                Route::get('/balance_log', 'UserController@balance_log')->name('balance_log');
                Route::get('/withdraw-money', 'UserController@withdraw_money_page')->name('withdraw_money');
                Route::post('/withdraw-money', 'UserController@withdraw_money');

                Route::get('/subscription', 'UserController@subscription')->name('subscription');
                Route::get('/payment', 'PaymentController@payment_page')->name('payment');
                Route::get('/payment/success', 'PaymentController@payment_success')->name('payment-success');
                Route::get('/payment/error', 'PaymentController@payment_error')->name('payment-error');
                Route::post('/payment', 'PaymentController@payment');

                Route::get('/', 'UserController@my_profile')->name('profile');
                Route::get('/id/{id}', 'UserController@profile')->name('id_profile');

                Route::get('/notifications', 'UserController@notifications')->name('notifications');
                Route::get('/setting-notifications', 'UserController@setting_notifications')->name('setting_notifications');
                Route::post('/save_notifications_setting', 'UserController@save_notifications_setting')->name('save_notifications_setting');

                Route::get('/news-and-events', 'UserController@news_and_events')->name('news-and-events');
                Route::get('/draft', 'UserController@draft');
                Route::get('/id/{id}/news-and-events', 'UserController@news_and_events')->name('news-and-events-id');

                Route::get('/id/{id}/social', 'UserController@social')->name('social');

                Route::get('/videos', 'UserController@profile_videos')->name('my_profile_videos');
                Route::get('/purchased_videos', 'UserController@purchased_videos')->name('purchased_videos');
                Route::get('/id/{id}/videos', 'UserController@profile_videos')->name('profile_videos');
                Route::group(['prefix' => 'video'], function () {
                    Route::get('/{id}', 'UserController@profile_video')->name('profile_video');
                    Route::post('/add', 'UserController@profile_video_add');
                    Route::post('/delete', 'UserController@profile_video_delete');
                    Route::post('/search', 'UserController@profile_video_search');
                    Route::post('/like', 'UserController@like_video');
                });

                Route::get('/photos', 'UserController@profile_photos')->name('my_profile_photos');
                Route::get('/photo/{pid}', 'UserController@profile_photo');
                Route::get('/id/{id}/photos', 'UserController@profile_photos')->name('profile_photos');
                Route::get('/id/{id}/{pid}', 'UserController@profile_photo')->name('profile_photo');

                Route::get('/profile-editing', 'UserController@profile_editing')->name('profile-editing');

                Route::post('/update', 'UserController@update');
                Route::post('/update/business', 'UserController@update_business');
                Route::post('/add_category', 'UserController@add_category');
                Route::post('/add_tweet', 'UserController@add_tweet');
                Route::post('/add_insta_post', 'UserController@add_insta_post');
                Route::post('/update_profile', 'UserController@update_profile');
                Route::post('/update_avaliable', 'UserController@update_avaliable');
                Route::post('/upload-files', 'UserController@uploadFiles');
                Route::post('/update_category', 'UserController@update_category');
            });

            Route::group(['prefix' => 'social'], function () {
                Route::get('/instagram', 'social\InstagramController@auth_redirect')->name('login_instagram');
                Route::get('/instagram/logout', 'social\InstagramController@logout')->name('logout_instagram');
                Route::get('/instagram/callback', 'social\InstagramController@callback_instagram')->name('callback_instagram');
            });

            Route::group(['prefix' => 'post'], function () {
                Route::get('/going', 'PostController@going_post')->name('going_post');
                Route::get('/interested', 'PostController@interested_post')->name('interested_post');
                Route::get('/{post}', 'PostController@single_post')->name('single_post');

                Route::post('/new_post', 'PostController@new_post');
                Route::post('/like', 'PostController@like');
                Route::post('/search', 'PostController@search');
                Route::post('/delete', 'PostController@delete');
                Route::post('/publish', 'PostController@publish');
                Route::post('/get', 'PostController@get');

                Route::post('/interested', 'InterestedController@interested');
                Route::post('/going', 'InterestedController@going');
            });

            Route::group(['prefix' => 'gallery'], function () {
                Route::post('/add', 'GalleryController@add');
                Route::post('/like', 'GalleryController@like');
                Route::post('/delete', 'GalleryController@delete');
                Route::post('/search', 'GalleryController@profile_photo_search');
            });

            Route::group(['prefix' => 'video-gallery'], function () {
                Route::post('/add', 'VideoGalleryController@add');
                Route::post('/like', 'VideoGalleryController@like');
                Route::post('/delete', 'VideoGalleryController@delete');
            });

            Route::group(['prefix' => 'job-alerts'], function () {
                Route::get('/', 'JobController@index')->name('job-alerts');
                Route::post('/get', 'JobController@get');
                Route::post('/add', 'JobController@add');
                Route::post('/update', 'JobController@update');
                Route::post('/delete', 'JobController@delete');
                Route::post('/request', 'JobController@request');
                Route::post('/request_approve', 'JobController@request_approve');
                Route::post('/request_cancel', 'JobController@request_cancel');
            });

            Route::get('/groups', 'GroupController@groups')->name('groups');
            Route::get('/my-groups', 'GroupController@my_groups')->name('my-groups');
            Route::get('/subscribed-groups', 'GroupController@subscribed_groups')->name('subscribed-groups');

            Route::group(['prefix' => 'group'], function () {
                Route::get('/{id}', 'GroupController@group')->name('group');
                Route::get('/{id}/followers', 'GroupController@followers_group')->name('followers_group');
                Route::get('/{id}/gallery', 'GroupController@gallery_group')->name('gallery_group');
                Route::get('/{id}/gallery/{pid}', 'GroupController@photo_group')->name('photo_group');
                Route::get('/{id}/settings', 'GroupController@settings_group')->name('settings_group');
                Route::get('/{id}/videos', 'GroupController@videos_group')->name('videos_group');
                Route::get('/{id}/notifications', 'GroupController@notifications_group')->name('notifications_group');
                Route::get('/{id}/video/{vid}', 'GroupController@video_group')->name('video_group');

                Route::post('/approve', 'GroupController@approve');
                Route::post('/cancel', 'GroupController@cancel');
                Route::post('/update', 'GroupController@update');
                Route::post('/new_group', 'GroupController@new_group');
                Route::post('/leave', 'GroupController@leave');
                Route::post('/join', 'GroupController@join');
            });

            Route::get('/worked-with/{id}', 'UserController@worked_with')->name('worked_with');
            Route::get('/works-with/{id}', 'UserController@works_with')->name('works_with');
            Route::get('/connection-requests', 'UserController@connection_requests')->name('connection-requests');

            Route::group(['prefix' => 'connections'], function () {
                Route::get('/', 'UserController@connections')->name('connections');
                Route::get('/{id}', 'UserController@connections')->name('connections_id');

                Route::post('/request', 'ConnectionsController@request');
                Route::post('/approved', 'ConnectionsController@approved');
                Route::post('/remove', 'ConnectionsController@remove');
            });

            Route::group(['prefix' => 'calendar'], function () {
                Route::get('/{id}', 'CalendarController@index')->name('calendar');
            });

            Route::group(['prefix' => 'comment'], function () {
                Route::post('/add', 'CommentController@add');
                Route::post('/like', 'CommentController@like');
                Route::post('/update', 'CommentController@update');
            });

            Route::group(['prefix' => 'chat'], function () {
                Route::post('/send_message', 'ChatController@send_message');
                Route::post('/get_dialog', 'ChatController@get_dialog');
                Route::post('/get_dialogs', 'ChatController@get_dialogs');
                Route::post('/checkCountUnread', 'ChatController@checkCountUnread');
            });

            Route::group(['prefix' => 'audio'], function () {
                Route::post('/call', 'AudioChatController@call');
                Route::post('/cancel_call', 'AudioChatController@cancel_call');
                Route::post('/update_active_call', 'AudioChatController@update_active_call');
                Route::post('/check_call', 'AudioChatController@check_call');
                Route::post('/take_call', 'AudioChatController@take_call');
                Route::post('/check_incoming', 'AudioChatController@check_incoming');
            });

            Route::group(['prefix' => 'video'], function () {
                Route::post('/call', 'VideoChatController@call');
                Route::post('/cancel_call', 'VideoChatController@cancel_call');
                Route::post('/update_active_call', 'VideoChatController@update_active_call');
                Route::post('/check_call', 'VideoChatController@check_call');
                Route::post('/take_call', 'VideoChatController@take_call');
                Route::post('/check_incoming', 'VideoChatController@check_incoming');
            });

            Route::post('/upload', 'UploadController@upload');
            Route::post('/cropp', 'UploadController@cropp');

            //Route::get('/calendar-page', 'UserController@calendar_page');
            Route::get('/videos', 'GuestController@videos');
            Route::get('/gallery', 'GuestController@gallery');
            //Route::get('/calendar-business', 'GuestController@calendar_business');
            Route::get('/profile-business', 'GuestController@profile_business');
            Route::get('/summit-page', 'GuestController@summit_page');
        });
    });
    Route::prefix('admin')->group(function () {
        Route::get('/login', 'Admin\AdminController@login')->name('admin.login'); //Auth\LoginController@showLoginForm
        Route::post('/login', 'Auth\LoginController@login');

        Route::middleware(['admin'])->group(function () {
            Route::get('/', 'Admin\AdminController@home')->name('admin-dashboard');

            Route::get('/braintree-settings', 'Admin\PaymentController@braintree_settings');

            Route::get('/payments', 'Admin\PaymentController@payments');

            Route::group(['prefix' => 'withdraw-money'], function () {
                Route::get('/', 'Admin\WithdrawController@withdraw');
                Route::post('/set_status', 'Admin\WithdrawController@set_status');
            });

            Route::group(['prefix' => 'option'], function () {
                Route::post('/update', 'Admin\OptionController@update');
            });

            Route::get('/users', 'Admin\UserController@index')->name('admin-users');
            Route::group(['prefix' => 'user'], function () {
                Route::get('/export', 'Admin\UserController@export')->name('export');
                Route::get('/{id}', 'Admin\UserController@user')->name('admin-user');
                Route::get('/{id}/login', 'Admin\UserController@login')->name('admin-login');
                Route::post('/basic_info', 'Admin\UserController@basic_info');
                Route::post('/avatar', 'Admin\UserController@avatar');
                Route::post('/block', 'Admin\UserController@block');
                Route::post('/unblock', 'Admin\UserController@unblock');
                Route::post('/import', 'Admin\UserController@import');
            });
            Route::get('/user-categories', 'Admin\UserController@user_categories')->name('admin-user-categories');
            Route::group(['prefix' => 'user-category'], function () {
                Route::get('/{id}', 'Admin\UserController@user_category')->name('admin-user-category');
                Route::post('/update', 'Admin\UserController@update_user_category');
            });

            Route::get('/groups', 'Admin\GroupController@index')->name('admin-groups');

            Route::group(['prefix' => 'group'], function () {
                Route::get('/{id}', 'Admin\GroupController@group')->name('admin-group');
                Route::post('/basic_info', 'Admin\GroupController@basic_info');
                Route::post('/avatar', 'Admin\GroupController@avatar');
                Route::post('/block', 'Admin\GroupController@block');
                Route::post('/unblock', 'Admin\GroupController@unblock');
            });

            Route::get('/posts', 'Admin\PostController@index')->name('admin-posts');

            Route::group(['prefix' => 'post'], function () {
                Route::get('/{id}', 'Admin\PostController@post')->name('admin-post');
                Route::post('/basic_info', 'Admin\PostController@basic_info');
                Route::post('/delete', 'Admin\PostController@delete');
                Route::post('/delete_comment', 'Admin\PostController@delete_comment');
                Route::post('/image', 'Admin\PostController@image');
                Route::post('/video', 'Admin\PostController@video');
            });

            Route::get('/jobs', 'Admin\JobController@index')->name('admin-jobs');

            Route::group(['prefix' => 'job'], function () {
                Route::get('/{id}', 'Admin\JobController@job')->name('admin-job');
                Route::post('/basic_info', 'Admin\JobController@basic_info');
                Route::post('/delete', 'Admin\JobController@delete');
                Route::post('/image', 'Admin\JobController@image');
                Route::post('/video', 'Admin\JobController@video');
            });

            Route::get('/videos', 'Admin\VideoController@index')->name('admin-videos');

            Route::group(['prefix' => 'video'], function () {
                Route::get('/{id}', 'Admin\VideoController@video')->name('admin-video');
                Route::post('/basic_info', 'Admin\VideoController@basic_info');
                Route::post('/delete', 'Admin\VideoController@delete');
                Route::post('/delete_comment', 'Admin\VideoController@delete_comment');
            });

            Route::get('/photos', 'Admin\PhotoController@index')->name('admin-photos');

            Route::group(['prefix' => 'photo'], function () {
                Route::get('/{id}', 'Admin\PhotoController@photo')->name('admin-photo');
                Route::post('/basic_info', 'Admin\PhotoController@basic_info');
                Route::post('/delete', 'Admin\PhotoController@delete');
                Route::post('/delete_comment', 'Admin\PhotoController@delete_comment');
            });

            Route::get('/group-videos', 'Admin\GroupVideoController@index')->name('admin-group-videos');

            Route::group(['prefix' => 'group-video'], function () {
                Route::get('/{id}', 'Admin\GroupVideoController@group_video')->name('admin-group-video');
                Route::post('/basic_info', 'Admin\GroupVideoController@basic_info');
                Route::post('/delete', 'Admin\GroupVideoController@delete');
                Route::post('/delete_comment', 'Admin\GroupVideoController@delete_comment');
            });

            Route::get('/chats', 'Admin\ChatController@index')->name('admin-chats');

            Route::group(['prefix' => 'chat'], function () {
                Route::get('/{id}', 'Admin\ChatController@chat')->name('admin-chat');
                Route::post('/delete', 'Admin\ChatController@chat_delete');
                Route::post('/delete_message', 'Admin\ChatController@delete_message');
            });

            Route::get('/auth-logs', 'Admin\LogController@auth_logs')->name('admin-auth-logs');

            Route::get('/admin-settings', 'Admin\AdminController@admin_settings')->name('admin-settings');
            Route::post('/change_password', 'Admin\AdminController@change_password');
            Route::post('/update_general_settings', 'Admin\OptionController@update');

        });
    });

    Auth::routes();
    Route::get('/verify-email/{token}', 'Auth\RegisterController@verifyEmail')->name('verifyEmail');
    Route::get('/terms-and-conditions', function () {
        return view('user.terms_and_conditions');
    })->name('terms_and_conditions');
});
