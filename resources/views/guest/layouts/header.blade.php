<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AEN - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="assets/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="css/reset.css" type="text/css" rel="stylesheet"/>
    <link href="css/header.css" type="text/css" rel="stylesheet"/>
    <link href="css/StyleSheet.css" rel="stylesheet" />
    <link href="css/footer.css" type="text/css" rel="stylesheet"/>
    <link href="css/fonts.css" type="text/css" rel="stylesheet"/>
    <link href="css/owl.carousel.min.css" rel="stylesheet" >
    <link href="css/main.css" rel="stylesheet" >
    @if(isset($css))
        @foreach($css as $src)
            <link rel="stylesheet" href="{{$src}}" />
        @endforeach
    @endif
    <!--
        <link href="css/youtuber.css" type="text/css" rel="stylesheet"/>

    -->
    <link href="css/ruslan.css" type="text/css" rel="stylesheet"/>
    <link href="css/max.css" type="text/css" rel="stylesheet"/>
</head>
<body>

<!--header -->
<header class="header-main">
    <div class="top-header-menu">
        <div class="container-fluid container-nav">

            <div class="content-part">
                <div class="post-bar">
                    <div class="post-bar-heading">
                        <select class="post-bar-select">
                            <option value="">Post Type</option>
                            <option value="">News / Update</option>
                            <option value="">Events</option>
                        </select>
                        <a href="#" class="photo-album">Photo Album</a>
                        <a href="#" class="video-album">Video</a>
                    </div>
                    <div class="post-bar-content">
                        <h3>Basic info</h3>
                        <div class="create-line">
                            <b class="label">Event Photo or Video</b>
                            <div class="upload-section">
                                <img src="img/upload-bg.jpg" alt="" id="upload-bg">
                                <input type="file" id="upload-file">
                            </div>
                        </div>
                        <div class="create-line">
                            <b class="label">Event Name</b>
                            <input type="text">
                        </div>
                        <div class="create-line">
                            <b class="label">Location</b>
                            <input type="text">
                        </div>
                        <div class="create-line">
                            <b class="label">Frequency</b>
                            <select name="" id="">
                                <option value="">Occurs Once</option>
                                <option value="">Occurs Twice</option>
                                <option value="">Occurs 3-5 times</option>
                                <option value="">More...</option>
                            </select>
                        </div>
                        <div class="create-line">
                            <b class="label">Starts</b>
                            <input type="date">
                        </div>
                        <div class="create-line">
                            <b class="label">Ends</b>
                            <input type="date">
                        </div>
                        <h3>Details</h3>
                        <p>Let people know what type of event you are hosting and what to expect</p>
                        <div class="create-line">
                            <b class="label">Category</b>
                            <select name="" id="">
                                <option value="">Category One</option>
                                <option value="">Category Two</option>
                                <option value="">Category Three</option>
                                <option value="">Category Four</option>
                            </select>
                        </div>
                        <div class="create-line">
                            <b class="label">Description</b>
                            <textarea name="" id="" cols="30" rows="5"></textarea>
                        </div>
                        <div class="create-line">
                            <b class="label">Keywords</b>
                            <input type="text">
                        </div>
                    </div>
                    <div class="post-bar-footer">
                        <a href="#" class="post-button">Post</a>
                    </div>
                </div>
            </div>


            {{--@yield('user.create-nav')--}}

            <div class="item-nav-menu">
                <div class="item-nav-logo">
                    <a class="brand" href="/">
                        <img src="img/logo.svg" alt="" class="logo">
                    </a>
                </div>
                <div class="search-mob">
                    <input text="text" placeholder="SEARCH" />
                </div>
                <nav class="navbar navbar-expand-lg navbar-full navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="main-nav navbar-nav mr-auto">
                            <li class="nav-item"><a href="#">news</a><br>
                                <span>what’s going on in the<br>Network Users</span>
                            </li>
                            <li class="nav-item">
                                <a href="#">events</a><br>
                                <span>from the Adult Enterainment<br>Adult Entertainment World</span>
                            </li>
                            <li class="nav-item">
                                <a href="#">businesses</a><br>
                                <span>in the Adult Entertainment <br>Industry - updates - Jobs</span>
                            </li>
                            <li class="nav-item">
                                <a href="#">professionals</a><br>
                                <span>Actors, Bar Staff, Models, Dancers<br>& Film Crew in Adult Entertainment</span>
                            </li>
                            <li class="nav-item">
                                <div class="search-menu">
                                    <input text="text" placeholder="SEARCH" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="search">
                    <input text="text" placeholder="SEARCH" />
                </div>
            </div>
        </div>
    </div>
    <span class="border"></span>
    <div class="bottom-header-menu">
        <div class="container-fluid">
            <ul class="bottom-menu">
                @if (Auth::check())
                    <a href="/custom-logout">{{ Auth::user()->name }}</a>
                @else
                    <li><a href="#">Hi Guest</a></li>
                @endif


                <li><a href="/connections">Connection Request<span>999</span> </a></li>
                <li class="dialog-messages-link"><a href="#">Messages</a></li>
                <li><a href="#">Job Alerts </a></li>
                <li class="post-bar-link"><a href="#" >Post News, Updates & Events</a></li>
            </ul>
            <a href="#" class="mobile-button"><span></span></a>
        </div>
    </div>
</header>
<!-- header end -->