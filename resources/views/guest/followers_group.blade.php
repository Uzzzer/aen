@extends('layouts.layout-full')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">
                    <div class="gallary_top">
                        <div class="gal_top_left"><span>Group Members</span></div>
                    </div>
                    <div class="grops-grid">
                        @forelse ($Followers as $Follower)
                            <div class="groups-item follow-item" data-id="{{ $Follower->user_id }}">
                                <a href="{{ route('id_profile', ['id' => $Follower->user_id]) }}" class="groups-item-title">
                                    <img src="{{ asset($Follower->avatar) }}" alt="">
                                </a>
                                <div class="groups-item-content">
                                    <a href="{{ route('id_profile', ['id' => $Follower->user_id]) }}" class="groups-item-title">
                                        <b>{{ $Follower->name }}</b>
                                        <span>{{ \App\ConnectionRequest::countConnection($Follower->user_id, true) }}</span>
                                        @if ((Auth::check() and $Follower->user_id != Auth::user()->id) or !Auth::check())
                                            <a href="#" class="groups-item-wheel groups_follow btn_follow connection_request @if(\App\ConnectionRequest::CheckConnection($Follower->user_id)) btn_disabled @endif" data-id="{{ $Follower->user_id }}">Follow</a>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        @empty
                            <p>No members</p>
                        @endforelse
                    </div>
                </div>
                @include('includes.right-sidebar-group')
            </div>


        </div>
        </div>
    </section>
@endsection