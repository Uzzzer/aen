@extends('layouts.layout-full')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">

                    @include('includes.new-group')

                    <div class="gallary_top">
                        <div class="gal_top_left">
                            <span>Groups</span>
                        </div>
                        <a href="{{ route('subscribed-groups') }}" class="groups-tab-link">Subscribed groups</a>
                        <a href="{{ route('my-groups') }}" class="groups-tab-link">My groups</a>
                        @if (!in_array($UserType, ['user', 'public_user', false]))
                            <a href="#" class="create-group-button group-bar-link">Create Group +</a>
                        @endif
                    </div>
                    <div class="grops-grid groups_list">
                        @foreach ($Groups as $Group)
                            <div class="groups-item" data-id="{{ $Group->id }}">
                                <img src="{{ asset($Group->avatar) }}" alt="">
                                <div class="groups-item-content">
                                    <a href="{{ route('group', ['id' => $Group->id]) }}" class="groups-item-title {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : '' }}">
                                        <b>{{ $Group->name }}</b>
                                        <span>{{ \App\Post::countUnreadGroupPosts($Group->id) }}</span>
                                    </a>
                                    <a href="{{ route('group', ['id' => $Group->id]) }}" class="groups-item-wheel {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : '' }}"></a>
                                    <ul class="groups-item-options">
                                        <li>
                                            <a href="{{ route('notifications_group', ['id' => $Group->id]) }}">Edit notification settings</a>
                                        </li>
                                        @if (\App\GroupFollower::CheckFollowGroup($Group->id))
                                            <li>
                                                <a href="#" class="leave_group" data-id='{{ $Group->id }}'
                                                   @if($Group->type == 'private') data-private="1" @endif>Leave group
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="#"
                                                   class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'join_group' }}"
                                                   data-id='{{ $Group->id }}'
                                                   @if($Group->type == 'private') data-private="1" @endif>@if($Group->type != 'private' or (\Auth::check() and $Group->user_id == \Auth::user()->id)){{ 'Join group' }}@else{{ 'Send request' }}@endif</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="#">Pin to shortcuts</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @include('includes.right-sidebar')
            </div>


        </div>
    </section>
@endsection