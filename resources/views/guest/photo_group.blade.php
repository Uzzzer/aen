@extends('layouts.layout-full')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">
                    <div class="gallary_top">
                        <div class="gal_top_left"><span>Group Gallery</span></div>
                    </div>
                    <div class="gallary_container">
                        <input type="hidden" name="photo_update_id" value="{{ $Gallery->id }}">
                        <ul id="imageGallery" class="gallery_group photos-box">
                            @php
                                $count_likes = \App\PhotoLike::countLike($Gallery->id);
                            @endphp
                            <li class="item big_photo_box" data-id="{{ $Gallery->id }}" style="width: 100% !important;">
                                <img src="{{ asset($Gallery->file_name) }}">
                                <div class="photo-bottom-footer">
                                    <a href="#" class="like_photo @if (\App\PhotoLike::checkLike($Gallery->id)) liked @endif" data-id="{{ $Gallery->id }}"><i class="v-icon like"></i>Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></a>
                                    <a href="#"><i class="v-icon share"></i>Share</a>
                                </div>
                                <div class="img_Gallerry_text ">
                                    <div class="gallary_tittle" style="font-size: 28px; color: #ffffff;">{{ $Gallery->title }}</div>
                                    <p style="font-style: normal; font-weight: 300; line-height: normal; font-size: 18px;">{{ \App\MediaView::countViewsPhoto($Gallery->id) }}</p>
                                    <div class="gallary_text video-description">
                                        {{ $Gallery->description }}
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="coment_container">
                            <div class="hide_comment_block" data-id="{{ $Gallery->id }}">
                                @php
                                    $Comments = \App\MediaComment::where('media_comments.photo_id', $Gallery->id)->whereNull('media_comments.reply_id')->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                @endphp
                                @foreach ($Comments as $Comment)
                                    <div class="coment_block" data-id="{{ $Comment->id }}">
                                        <div class="coment_photo_person">
                                            <a href="{{ url('/profile/id/'.$Comment->user_id) }}"><img src="{{ asset($Comment->user_avatar) }}" alt="{{ $Comment->user_name }}" title="{{ $Comment->user_name }}"/></a>
                                        </div>
                                        <div class="coment_right">
                                            <div class="coment_text">
                                                {{ $Comment->comment }}
                                                @if ($Comment->image != NULL)
                                                    <a href="{{ asset($Comment->image) }}" data-lightbox="image-{{ $Comment->id }}"><img src="{{ asset($Comment->image) }}"/></a>
                                                @endif
                                            </div>
                                            <ul class="coment_data">
                                                @php
                                                    $MediaCommentLike = NULL;
                                                    if (Auth::check())
                                                        $MediaCommentLike = \App\MediaCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                                                    $count_likes = \App\MediaCommentLike::where('comment_id', $Comment->id)->count();
                                                @endphp
                                                <li data-media="1" class="like_comment @if($MediaCommentLike != NULL) liked @endif" @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>Like <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></li>
                                                <li class="reply_comment" @if(\Auth::check()) data-id="{{ $Comment->id }}" data-photo-id="{{ $Gallery->id }}" @endif>Reply</li>
                                                <li>{{ \App\DateConvert::Convert($Comment->created_at) }}</li>
                                            </ul>
                                            @php
                                                $ReplyComments = \App\MediaComment::where('media_comments.photo_id', $Gallery->id)->where('media_comments.reply_id', $Comment->id)->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                            @endphp
                                            @foreach ($ReplyComments as $ReplyComment)
                                                <div class="coment_block" data-id="{{ $ReplyComment->id }}" data-reply-id="{{ $ReplyComment->reply_id }}">
                                                    <div class="coment_photo_person">
                                                        <a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}"><img src="{{ asset($ReplyComment->user_avatar) }}" alt="{{ $ReplyComment->user_name }}" title="{{ $ReplyComment->user_name }}"/></a>
                                                    </div>
                                                    <div class="coment_right">
                                                        <div class="coment_text">
                                                            {{ $ReplyComment->comment }}
                                                            @if ($ReplyComment->image != NULL)
                                                                <a href="{{ asset($ReplyComment->image) }}" data-lightbox="image-{{ $ReplyComment->id }}"><img src="{{ asset($ReplyComment->image) }}"/></a>
                                                            @endif
                                                        </div>
                                                        <ul class="coment_data">
                                                            @php
                                                                $MediaCommentLike = NULL;
                                                                if (Auth::check())
                                                                    $MediaCommentLike = \App\MediaCommentLike::where('comment_id', $ReplyComment->id)->where('user_id', Auth::user()->id)->first();
                                                                $count_likes = \App\MediaCommentLike::where('comment_id', $ReplyComment->id)->count();
                                                            @endphp
                                                            <li data-media="1" class="like_comment @if($MediaCommentLike != NULL) liked @endif" @if(\Auth::check()) data-id="{{ $ReplyComment->id }}" @endif>Like <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></li>
                                                            <li>{{ \App\DateConvert::Convert($ReplyComment->created_at) }}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                                @if (Auth::check())
                                    <form class="add_comment comment_before" data-id="{{ $Gallery->id }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="photo_id" value="{{ $Gallery->id }}">
                                        <input type="hidden" name="image" value="">
                                        <div class="coment_write">
                                            <div class="writer_left">
                                                <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                                            </div>
                                            <div class="writer_right">
                                                <div class="comment_image"></div>
                                                <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                                            </div>
                                        </div>
                                        <img class="comment_img">
                                        <input type="file" style="display: none;">
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @include('includes.right-sidebar-group')
            </div>


        </div>
        </div>
    </section>

    @if (Auth::check())
        <form class="add_comment reply_comment_form copy_form" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" name="photo_id" value="">
            <input type="hidden" name="comment_id" value="">
            <input type="hidden" name="image" value="">
            <div class="coment_write">
                <div class="writer_left">
                    <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                </div>
                <div class="writer_right">
                    <div class="comment_image"></div>
                    <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                </div>
            </div>
            <img class="comment_img">
            <input type="file" style="display: none;">
        </form>
    @endif
@endsection