@extends('layouts.layout')

@section('content')
    @foreach ($Jobs as $Job)
        <div class="right_container post_box job_list" data-id="{{ $Job->id }}">
            <div class="m_r_top">
                <div class="m_rl_top">
                    <div class="ph_person"><a href="{{ route('id_profile', ['id' => $Job->user_id]) }}"><img src="{{ asset($Job->user_avatar) }}" /></a></div>
                    <div class="person_text">
                        <p>
                            <span><a href="{{ route('id_profile', ['id' => $Job->user_id]) }}" class="user-name-link">{{ $Job->user_name }}</a>,</span> {{ $Job->title }} - {{ \App\DateConvert::Convert($Job->created_at) }}
                        </p>
                        <p class="fon-f-light">
                            <!-- You, Ashley Murray, Dirk Diggler & 12 others Commented -->
                        </p>
                    </div>
                </div>
            </div>
            @if ($Job->file_type == 'image')
                <div class="m_bg_container" style="background-image: url({{ asset($Job->file_name) }});">
                    <img src="{{ asset($Job->file_name) }}" alt="">
                </div>
            @endif
            @if ($Job->file_type == 'video')
                <video width="100%" height="400px" controls>
                    <source src="{{ asset($Job->file_name) }}">
                    Your browser does not support the video tag.
                </video>
            @endif
            <div class="m_bot_container">
                <div class="m_data">
                    <img src="{{ asset('img/cal_ic.svg') }}"/>
                    {{ $Job->title }}
                </div>
                <div class="m-bot-text job_box">
                        <p>
                             {{ $Job->text }}
                        </p>
                        <p class="open_job_profile">View Details</p>
                        <div class="tab_job_block_bottom" style="display: none;">
                            @php
                                $fields = json_decode($Job->fields, true);
                            @endphp
                            @if ($Job->responsibilities != NULL)
                                @php
                                    $responsibilities = json_decode($Job->responsibilities, true);
                                @endphp
                                @if (is_array($responsibilities))
                                    <h6>Responsibilities</h6>
                                    <ul>
                                        @foreach ($responsibilities as $responsibility)
                                        <li>{{ $responsibility }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            @endif
                            @if (is_array($fields))
                                @foreach ($fields as $key => $value)
                                    <div class="job_info"><span>{{ $key }}: </span>{{ $value }}</div>
                                @endforeach
                            @endif
                            <div class="Apply_for_job">
                                <a href="#" class="apply_for_job" data-id="{{ $Job->id }}">Apply for job</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection