@extends('layouts.layout')

@section('content')
    <input type="hidden" name="load_page_type" value="load_search">
    <input type="hidden" name="load_search" value="{{ $search }}">
    <div class="tab-wrapper profile-tab">
        <div class="tabs">
            <span class="tab active" data-load="posts">Posts</span>
            <span class="tab" data-load="photos">Photos</span>
            <span class="tab" data-load="videos">Videos</span>
            <span class="tab" data-load="business">Business</span>
            <span class="tab" data-load="professionals">Professionals</span>
        </div>
        <div class="tab_content">
            <div class="tab_item">

                <div class="post_load_list">
                    @forelse ($Posts as $Post)
                        <div class="right_container post_box" data-id="{{ $Post->id }}">
                            <div class="m_r_top">
                                <div class="m_rl_top">
                                    <div class="ph_person">
                                        <a href="{{ url('/profile/id/'.$Post->user_id) }}">
                                            <img src="{{ asset($Post->user_avatar) }}" alt="Alternate Text"/>
                                        </a>
                                    </div>
                                    <div class="person_text">
                                        <p>
                                <span><a href="{{ url('/profile/id/'.$Post->user_id) }}"
                                         class="user-name-link">{{ $Post->user_name }}</a>,</span> {{ $Post->title }} - {{ \App\DateConvert::Convert($Post->created_at) }}
                                        </p>
                                        <p class="fon-f-light">
                                            <!-- You, Ashley Murray, Dirk Diggler & 12 others Commented -->
                                        </p>
                                    </div>
                                </div>
                                <div class="m_rr_top">
                                    @php
                                        $PostLike = NULL;
                                        if (Auth::check())
                                            $PostLike = \App\PostLike::where('post_id', $Post->id)->where('user_id', Auth::user()->id)->first();
                                    @endphp
                                    <a href="#" class="like_post @if ($PostLike != NULL) liked @endif"
                                       data-id="{{ $Post->id }}">
                                        <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                                        <span>Like</span>
                                    </a>
                                    <a href="#" class="comments_post" data-id="{{ $Post->id }}">
                                        <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                                        <span>Comment</span>
                                    </a>
                                    <a href="#" class="share_post"
                                       data-text="{{ $Post->title }}, {{ date('jS F Y', strtotime($Post->start)) }}"
                                       data-img="@if ($Post->file_type == 'image'){{ asset($Post->file_name) }}@endif"
                                       data-url=" {{ url('/') }}" data-id="{{ $Post->id }}">
                                        <img src="{{ asset('img/r_ic_3.svg') }}" alt="Alternate Text"/>
                                        <span>Share</span>
                                    </a>
                                    @if(\Auth::user() != null)
                                        @if (\Auth::user()->id == $Post->user_id)
                                            <a href="#" class="delete_post"
                                               data-id="{{ $Post->id }}">
                                                <span>Delete</span>
                                            </a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            @if ($Post->file_type == 'image')
                                <div class="m_bg_container"
                                     style="background-image: url({{ asset($Post->file_name) }});">
                                    <img src="{{ asset($Post->file_name) }}" alt="">
                                </div>
                            @endif
                            @if ($Post->file_type == 'video')
                                <video width="100%" height="400px"
                                       controls {!!  $Post->video_preview ? 'poster="' . asset($Post->video_preview) . '"' : '' !!}>
                                    <source src="{{ asset($Post->file_name) }}">
                                    Your browser does not support the video tag.
                                </video>
                            @endif
                            <div class="m_bot_container">
                                <div class="m_data">
                                    @if ($Post->post_type == 'news')
                                        <i class="fa fa-newspaper-o" style="font-size: 24px; margin-right: 15px;"
                                           aria-hidden="true"></i>
                                    @elseif ($Post->post_type == 'online')
                                        <i class="fa fa-video-camera" aria-hidden="true"
                                           style="font-size: 24px; margin-right: 15px;"></i>
                                    @else
                                        <i class="fa fa-calendar" style="font-size: 24px; margin-right: 15px;"
                                           aria-hidden="true"></i>
                                    @endif
                                    {{ $Post->title }}
                                    @if ($Post->post_type == 'event')
                                        <span>{{ date('jS F Y', strtotime($Post->start)) }} Local Time, {{ $Post->location }}</span>
                                    @endif
                                    @if ($Post->post_type == 'online')
                                        <span>{{ date('jS F Y', strtotime($Post->start)) }}, {{ date('H:i', strtotime($Post->start_time)) }} - {{ $Post->start == $Post->end ? '' : date('jS F Y', strtotime($Post->end)) . ',' }} {{ date('H:i', strtotime($Post->end_time)) }}</span>
                                    @endif
                                </div>
                                <div class="m-bot-text">
                                    {!! $Post->description !!}
                                    <div class="coment_container" style=" border: 0px; ">
                                        <div class="hide_comment_block" data-id="{{ $Post->id }}"
                                             style="display: none;">
                                            @php
                                                $Comments = \App\PostComment::where('post_comments.post_id', $Post->id)->whereNull('post_comments.reply_id')->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                            @endphp
                                            @foreach ($Comments as $Comment)
                                                <div class="coment_block" data-id="{{ $Comment->id }}">
                                                    <div class="coment_photo_person">
                                                        <a href="{{ url('/profile/id/'.$Comment->user_id) }}">
                                                            <img src="{{ asset($Comment->user_avatar) }}"
                                                                 alt="{{ $Comment->user_name }}"
                                                                 title="{{ $Comment->user_name }}"/>
                                                        </a>
                                                    </div>
                                                    <div class="coment_right">
                                                        <div class="coment_text">
                                                            {{ $Comment->comment }}
                                                            @if ($Comment->image != NULL)
                                                                <a href="{{ asset($Comment->image) }}"
                                                                   data-lightbox="image-{{ $Comment->id }}">
                                                                    <img src="{{ asset($Comment->image) }}"/>
                                                                </a>
                                                            @endif
                                                        </div>
                                                        <ul class="coment_data">
                                                            @php
                                                                $PostCommentLike = NULL;
                                                                if (Auth::check())
                                                                    $PostCommentLike = \App\PostCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                                                                $count_likes = \App\PostCommentLike::where('comment_id', $Comment->id)->count();
                                                            @endphp
                                                            <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                                                @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>Like
                                                                <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                                            </li>
                                                            <li class="reply_comment"
                                                                @if(\Auth::check()) data-id="{{ $Comment->id }}"
                                                                data-post-id="{{ $Post->id }}" @endif>Reply
                                                            </li>
                                                            <li>{{ \App\DateConvert::Convert($Comment->created_at) }}</li>
                                                        </ul>
                                                        @php
                                                            $ReplyComments = \App\PostComment::where('post_comments.post_id', $Post->id)->where('post_comments.reply_id', $Comment->id)->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                                        @endphp
                                                        @foreach ($ReplyComments as $ReplyComment)
                                                            <div class="coment_block" data-id="{{ $ReplyComment->id }}"
                                                                 data-reply-id="{{ $ReplyComment->reply_id }}">
                                                                <div class="coment_photo_person">
                                                                    <a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}">
                                                                        <img src="{{ asset($ReplyComment->user_avatar) }}"
                                                                             alt="{{ $ReplyComment->user_name }}"
                                                                             title="{{ $ReplyComment->user_name }}"/>
                                                                    </a>
                                                                </div>
                                                                <div class="coment_right">
                                                                    <div class="coment_text">
                                                                        {{ $ReplyComment->comment }}
                                                                        @if ($ReplyComment->image != NULL)
                                                                            <a href="{{ asset($ReplyComment->image) }}"
                                                                               data-lightbox="image-{{ $ReplyComment->id }}">
                                                                                <img src="{{ asset($ReplyComment->image) }}"/>
                                                                            </a>
                                                                        @endif
                                                                    </div>
                                                                    <ul class="coment_data">
                                                                        @php
                                                                            $PostCommentLike = NULL;
                                                                            if (Auth::check())
                                                                                $PostCommentLike = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->where('user_id', Auth::user()->id)->first();
                                                                            $count_likes = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->count();
                                                                        @endphp
                                                                        <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                                                            @if(\Auth::check()) data-id="{{ $ReplyComment->id }}" @endif>Like
                                                                            <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                                                        </li>
                                                                        <li>{{ \App\DateConvert::Convert($ReplyComment->created_at) }}</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach
                                            @if (Auth::check())
                                                <form class="add_comment" data-id="{{ $Post->id }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="post_id" value="{{ $Post->id }}">
                                                    <input type="hidden" name="image" value="">
                                                    <div class="coment_write">
                                                        <div class="writer_left">
                                                            <img src="{{ asset(Auth::user()->avatar) }}"
                                                                 alt="Alternate Text"/>
                                                        </div>
                                                        <div class="writer_right">
                                                            <div class="comment_image"></div>
                                                            <input type="text" name="comment" value=""
                                                                   placeholder="Write Comments..."/>
                                                        </div>
                                                    </div>
                                                    <img class="comment_img">
                                                    <input type="file" style="display: none;">
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="coment_container">
                                    <div class="like_conent">
                                        @php
                                            $count_likes = \App\PostLike::where('post_id', $Post->id)->count();
                                            $count_comments = \App\PostComment::where('post_id', $Post->id)->count();
                                        @endphp
                                        <div class="like_left {{ !$count_likes ? 'hidden-post-block' : '' }}">
                                            <div class="like">
                                                <img src="{{ asset('img/like_c.svg') }}" alt="Alternate Text"/>
                                            </div>
                                            <div class="like_text count_like">
                                                <span>{{ $count_likes }}</span>
                                                likes
                                            </div>
                                        </div>
                                        <div data-id="{{ $Post->id }}" style=" cursor: pointer; "
                                             class="like_right {{ !$count_comments ? 'hidden-post-block' : '' }} {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'comments_post' }}">
                                            <span>{{ $count_comments }}</span>
                                            Comments
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p class="no-result">No results</p>
                    @endforelse
                </div>
                @if (Auth::check())
                    <form class="add_comment reply_comment_form copy_form" style="display: none;">
                        {{ csrf_field() }}
                        <input type="hidden" name="post_id" value="">
                        <input type="hidden" name="comment_id" value="">
                        <input type="hidden" name="image" value="">
                        <div class="coment_write">
                            <div class="writer_left">
                                <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                            </div>
                            <div class="writer_right">
                                <div class="comment_image"></div>
                                <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                            </div>
                        </div>
                        <img class="comment_img">
                        <input type="file" style="display: none;">
                    </form>
                @endif

            </div>
            <div class="tab_item" style="display: none">

                <div class="photo_load_list">
                    @forelse ($Photos as $Photo)
                        <div class="right_container photo_box" data-id="{{ $Photo->id }}">
                            <div class="m_r_top">
                                <div class="m_rl_top">
                                    <div class="ph_person">
                                        <a href="{{ url('/profile/id/'.$Photo->user_id) }}">
                                            <img src="{{ asset($Photo->user_avatar) }}" alt="Alternate Text"/>
                                        </a>
                                    </div>
                                    <div class="person_text">
                                        <p>
                                            <span>
                                                <a href="{{ url('/profile/id/'.$Photo->user_id) }}"
                                                   class="user-name-link">{{ $Photo->user_name }}</a>,
                                            </span>
                                            {{ $Photo->title }} - {{ \App\DateConvert::Convert($Photo->created_at) }}
                                        </p>
                                    </div>
                                </div>
                                <div class="m_rr_top">
                                    @php
                                        $PhotoLike = NULL;
                                        if (Auth::check())
                                            $PhotoLike = \App\PhotoLike::where('photo_id', $Photo->id)->where('user_id', Auth::user()->id)->first();
                                    @endphp
                                    <a href="#"
                                       class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_photo' }} @if ($PhotoLike != NULL) liked @endif"
                                       data-id="{{ $Photo->id }}">
                                        <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                                        <span>Like</span>
                                    </a>
                                    <a href="{{ route('profile_photo', ['id' => $Photo->user_id, 'pid' => $Photo->id]) }}"
                                       data-id="{{ $Photo->id }}">
                                        <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                                        <span>Comment</span>
                                    </a>
                                </div>
                            </div>
                            <div class="m_bg_container"
                                 style="background-image: url({{ asset($Photo->file_name) }});">
                                <img src="{{ asset($Photo->file_name) }}" alt="">
                            </div>
                            <div class="m_bot_container">
                                <div class="m_data">
                                    <i class="fa fa-picture-o" aria-hidden="true"
                                       style="font-size: 24px; margin-right: 15px;"></i>
                                    {{ $Photo->title }}
                                </div>
                                <div class="m-bot-text"
                                     @if (empty($Photo->description)) style=" border-top: 0px; padding: 0px; " @endif>
                                    {!! $Photo->description !!}
                                </div>
                                <div class="coment_container">
                                    <div class="like_conent">
                                        @php
                                            $count_likes = \App\PhotoLike::where('photo_id', $Photo->id)->count();
                                            $count_comments = \App\MediaComment::where('photo_id', $Photo->id)->count();
                                        @endphp
                                        <div class="like_left {{ !$count_likes ? 'hidden-post-block' : '' }}">
                                            <div class="like">
                                                <img src="{{ asset('img/like_c.svg') }}" alt="Alternate Text"/>
                                            </div>
                                            <div class="like_text count_like">
                                                <span>{{ $count_likes }}</span>
                                                likes
                                            </div>
                                        </div>
                                        <a href="{{ route('profile_photo', ['id' => $Photo->user_id, 'pid' => $Photo->id]) }}">
                                            <div data-id="{{ $Photo->id }}" style=" cursor: pointer; "
                                                 class="like_right {{ !$count_comments ? 'hidden-post-block' : '' }}">
                                                <span>{{ $count_comments }}</span>
                                                Comments
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p class="no-result">No results</p>
                    @endforelse
                </div>

            </div>
            <div class="tab_item" style="display: none">

                <div class="video_load_list">
                    @forelse ($Videos as $Video)
                        <div class="right_container video_box" data-id="{{ $Video->id }}">
                            <div class="m_r_top">
                                <div class="m_rl_top">
                                    <div class="ph_person">
                                        <a href="{{ url('/profile/id/'.$Video->user_id) }}">
                                            <img src="{{ asset($Video->user_avatar) }}" alt="Alternate Text"/>
                                        </a>
                                    </div>
                                    <div class="person_text">
                                        <p>
                                            <span>
                                                <a href="{{ url('/profile/id/'.$Video->user_id) }}"
                                                   class="user-name-link">{{ $Video->user_name }}</a>,
                                            </span>
                                            {{ $Video->title }} - {{ \App\DateConvert::Convert($Video->created_at) }}
                                        </p>
                                    </div>
                                </div>
                                <div class="m_rr_top">
                                    @php
                                        $VideoLike = NULL;
                                        if (Auth::check())
                                            $VideoLike = \App\ProfileVideoLike::where('video_id', $Video->id)->where('user_id', Auth::user()->id)->first();
                                    @endphp
                                    <a href="#"
                                       class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_video' }} @if ($VideoLike != NULL) liked @endif"
                                       data-id="{{ $Video->id }}">
                                        <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                                        <span>Like</span>
                                    </a>
                                    <a href="{{ route('profile_video', ['id' => $Video->user_id]) }}"
                                       data-id="{{ $Video->id }}"
                                       class="@if(!\App\PurchasedVideo::checkPurchase($Video->id)){{ 'must_buy_video' }}@endif">
                                        <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                                        <span>Comment</span>
                                    </a>
                                </div>
                            </div>
                            <div class="m_bg_container">
                                <div class="video-wraper">
                                    @if (\App\PurchasedVideo::checkPurchase($Video->id))
                                        <video width="100%" height="190px"
                                               controls {!!  $Video->preview_image ? 'poster="' . asset($Video->preview_image) . '"' : '' !!}>
                                            <source src="{{ asset($Video->file_name) }}">
                                            Your browser does not support the video tag.
                                        </video>
                                    @else
                                        <img src="{{ asset($Video->preview_image) }}" alt="{{ $Video->title }}">
                                    @endif
                                    <div class="play-btn-wrapper">
                                        <a href="#">
                                            <i class="v-icon play-red"></i>
                                        </a>
                                    </div>
                                    <div class="video-bottom-block">
                                        <div class="left-box">
                                            <i class="v-icon play"></i>
                                            <div class="video-bottom-text">
                                                <p class="lite-text">Run Time: {{ \App\Video::convertTime($Video->time) }}</p>
                                            </div>
                                        </div>
                                        <a href="@if(\App\PurchasedVideo::checkPurchase($Video->id)){{ route('profile_video', ['id' => $Video->id]) }}@else{{ route('payment', ['video' => $Video->id]) }}@endif"
                                           class="video-btn"
                                           style="animation-name: fadeInRight;">@if(\App\PurchasedVideo::checkPurchase($Video->id)){{ 'Watch NOW' }}@else{{ 'Buy NOW' }}@endif</a>
                                    </div>
                                    <div class="overlay-block">
                                        <div class="price-box">
                                            <span class="price">{{ \App\Option::option('currency') }}{{ $Video->price }}</span>
                                            <span></span>
                                        </div>
                                        <div class="circle">
                                            <img class="check-mark-icon" src="{{ asset('img/check-mark-icon.svg') }}"
                                                 alt="Icon">
                                            PURCHASE
                                            <br>
                                            CONFIRMED
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m_bot_container">
                                <div class="m_data">
                                    <i class="fa fa-video-camera" aria-hidden="true"
                                       style="font-size: 24px; margin-right: 15px;"></i>
                                    {{ $Video->title }}
                                </div>
                                <div class="coment_container">
                                    <div class="like_conent">
                                        @php
                                            $count_likes = \App\ProfileVideoLike::where('video_id', $Video->id)->count();
                                            $count_comments = \App\ProfileVideoComment::where('video_id', $Video->id)->count();
                                        @endphp
                                        <div class="like_left {{ !$count_likes ? 'hidden-post-block' : '' }}">
                                            <div class="like">
                                                <img src="{{ asset('img/like_c.svg') }}" alt="Alternate Text"/>
                                            </div>
                                            <div class="like_text count_like">
                                                <span>{{ $count_likes }}</span>
                                                likes
                                            </div>
                                        </div>
                                        <a href="{{ route('profile_video', ['id' => $Video->id]) }}"
                                           class="@if(!\App\PurchasedVideo::checkPurchase($Video->id)){{ 'must_buy_video' }}@endif">
                                            <div data-id="{{ $Video->id }}" style=" cursor: pointer; "
                                                 class="like_right {{ !$count_comments ? 'hidden-post-block' : '' }}">
                                                <span>{{ $count_comments }}</span>
                                                Comments
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p class="no-result">No results</p>
                    @endforelse
                </div>

            </div>
            <div class="tab_item" style="display: none">

                <div class="grops-grid">
                    <div class="business_load_list">
                        @forelse ($Business as $business_user)
                            <div class="groups-item follow-item">
                                <a href="{{ route('id_profile', ['id' => $business_user->id] ) }}"
                                   class="groups-item-title">
                                    <img src="{{ asset($business_user->avatar) }}" alt="">
                                </a>
                                <div class="groups-item-content">
                                    <a href="{{ route('id_profile', ['id' => $business_user->id] ) }}"
                                       class="groups-item-title">
                                        <b>{{ $business_user->name }}</b>
                                        <span>{{ \App\ConnectionRequest::countConnection($business_user->id, true) }}</span>
                                    </a>
                                </div>
                            </div>
                        @empty
                            <p class="no-result">No results</p>
                        @endforelse
                    </div>
                </div>

            </div>
            <div class="tab_item" style="display: none">

                <div class="grops-grid">
                    <div class="professionals_load_list">
                        @forelse ($Professionals as $business_user)
                            <div class="groups-item follow-item">
                                <a href="{{ route('id_profile', ['id' => $business_user->id] ) }}"
                                   class="groups-item-title">
                                    <img src="{{ asset($business_user->avatar) }}" alt="">
                                </a>
                                <div class="groups-item-content">
                                    <a href="{{ route('id_profile', ['id' => $business_user->id] ) }}"
                                       class="groups-item-title">
                                        <b>{{ $business_user->name }}</b>
                                        <span>{{ \App\ConnectionRequest::countConnection($business_user->id, true) }}</span>
                                    </a>
                                </div>
                            </div>
                        @empty
                            <p class="no-result">No results</p>
                        @endforelse
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '177979539749540',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v3.1'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

@endsection