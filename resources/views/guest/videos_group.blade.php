@extends('layouts.layout-full')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary overflow_block">
                    @foreach ($VideoGallery as $video)
                        <div class="gallery-video">
                            <video width="100%" height="400px" controls>
                                <source src="{{ asset($video->file_name) }}">
                                Your browser does not support the video tag.
                            </video>
                            @php
                                $count_likes = \App\VideoLike::countLike($video->id);
                                $count_comments = \App\MediaComment::where('video_id', $video->id)->count();
                            @endphp
                            <div class="photo-bottom-footer" data-id="{{ $video->id }}">
                                <a href="#" class="like_video @if (\App\VideoLike::checkLike($video->id)) liked @endif" data-id="{{ $video->id }}"><i class="v-icon like"></i>Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></a>
                                <a href="{{ route('video_group', ['id' => $Group->id, 'vid' => $video->id]) }}"><i class="v-icon comment"></i>Comment @if($count_comments > 0){{ '('.$count_comments.')' }}@endif</a>
                                <a href="#"><i class="v-icon share"></i>Share</a>
                            </div>
                        </div>
                        <div class="gallary_top video-top">
                            <div class="gal_top_left">
                                <span>{{ $video->title }}</span>
                                @php \App\MediaView::viewVideo($video->id); @endphp
                                <p>{{ \App\MediaView::videoViewsVideo($video->id) }}</p>
                                <div class="video-description">
                                    {{ $video->description }}
                                </div>
                            </div>
                        </div>
                        @php
                            break;
                        @endphp
                    @endforeach
                    @foreach ($VideoGallery as $key => $video)
                        @php
                            if (!$key)
                                continue;
                        @endphp
                        <div class="video-grid">
                            <a href="{{ route('video_group', ['id' => $Group->id, 'vid' => $video->id]) }}" class="video-item">
                                <img src="{{ $video->preview }}" alt="">
                                <div class="video-title">{{ $video->title }}</div>
                                <div class="video-description">
                                    {{ $video->description }}
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                @include('includes.right-sidebar-group')
            </div>


        </div>
        </div>
    </section>
@endsection