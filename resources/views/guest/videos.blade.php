@include('guest.layouts.header')
<div class="modal_upload">
    <div class="close_modal_up">×</div>
    <div class="upload_block">
        <label for="file" class="label-file-img">
            <img class="upload-icon" src="https://image.flaticon.com/icons/svg/149/149185.svg">
            <div class="image-wrapper">
                <img src>
            </div>
        </label>
        <h1>Upload file (jpeg, < 4Mo)</h1>
        <p class="file-name"></p>
        <input id="file" type="file" class="input-file">
        <div class="horizontal-line"></div>
        <div class="under-block">
            <input type="submit" value="Upload" class="input-submit">
        </div>
    </div>
</div>
<section id="profile" class="profile main_wrap">
    <div class="container-fluid">
        <div class="main-container">
            <div class="gallary overflow_block">
                <div class="gallery-video">
                    <iframe class="main-video" width="1024" height="600" src="https://www.youtube.com/embed/6lt2JfJdGSY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="gallary_top video-top">
                    <div class="gal_top_left">
                        <span>Video Title in here</span>
                        <p>5,333,333 views</p>
                    </div>
                    <a class="Upl_new_ph">Upload New Video</a>
                </div>
                <div class="video-grid">
                    <?php
                    for ($i = 0; $i < 6; $i++) {
                    ?>
                    <a href="#" class="video-item">
                        <img src="../img/video-prev.png" alt="">
                        <div class="video-title">Another video title here</div>
                        <div class="video-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae tortor vitae ipsum scelerisque malesuada eu non dui. Fusce eu gravida mauris, a feugiat sapien…
                        </div>
                    </a>
                    <?php } ?>
                </div>
            </div>
            <div class="sidebar">
                <div class="m_l_top">
                    <div class="bl_person_photo">
                        <img src="../img/avatar-profile.png" alt="Alternate Text">
                    </div>
                    <div class="bl_person_inf">
                        <div>Location: Houston Texas</div>
                        <div class="data-dob">DOB: 03/07/78</div>
                    </div>
                </div>
                <div class="m_container_inf">
                    <div class="b_block_inf">Connections - <a href="#">14,657</a></div>
                    <div class="b_block_inf">Companies - <a href="#">435</a></div>
                    <div class="b_block_inf">Groups - <a href="#">1,200</a></div>
                    <div class="b_block_inf">Your Posts - <a href="#">743</a></div>
                </div>
                <div class="m_container_soc">
                    <div class="m_block_soc"><img src="../img/inf_ic_1.jpg" alt="Alternate Text">Photos - <a href="#">221</a></div>
                    <div class="m_block_soc"><img src="../img/inf_ic_2.jpg" alt="Alternate Text">Videos - <a href="#">119</a></div>
                    <div class="m_soc_ic">
                        <a href="#"><img src="../img/soc_ic_1.jpg" alt="Alternate Text"></a>
                        <a href="#"><img src="../img/soc_ic_2.jpg" alt="Alternate Text"></a>
                        <a href="#"><img src="../img/soc_ic_3.jpg" alt="Alternate Text"></a>
                    </div>
                </div>
                <a href="#" class="logout-link">Logout of Account</a>
            </div>
        </div>


    </div>
    </div>
</section>
@include('guest.layouts.footer')