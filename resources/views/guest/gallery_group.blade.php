@extends('layouts.layout-full')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">
                    <div class="gallary_top">
                        <div class="gal_top_left"><span>Group Gallery</span></div>
                    </div>
                    <div class="gallary_container">
                        @if (count($Gallery))
                        <ul id="imageGallery" class="gallery_group photos-box">
                            @foreach ($Gallery as $photo)
                                @php
                                    $count_likes = \App\PhotoLike::countLike($photo->id);
                                    $count_comments = \App\MediaComment::where('photo_id', $photo->id)->count();
                                @endphp
                                <li class="item" data-id="{{ $photo->id }}">
                                    <a href="{{ asset($photo->file_name) }}" class="open_photo"><div class="photo_box" style="background-image: url({{ asset($photo->file_name) }});"></div></a>
                                    <div class="photo-bottom-footer">
                                        <a href="#" class="like_photo @if (\App\PhotoLike::checkLike($photo->id)) liked @endif" data-id="{{ $photo->id }}"><i class="v-icon like"></i>Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></a>
                                        <a href="{{ route('photo_group', ['id' => $Group->id, 'pid' => $photo->id]) }}"><i class="v-icon comment"></i>Comment @if($count_comments > 0){{ '('.$count_comments.')' }}@endif</a>
                                        <a href="#"><i class="v-icon share"></i>Share</a>
                                    </div>
                                    <div class="img_Gallerry_text ">
                                        <div class="gallary_tittle">{{ $photo->title }}</div>
                                        <div class="gallary_text">
                                            {{ $photo->description }}
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        @else
                            <p>No photos</p>
                        @endif
                    </div>
                </div>
                @include('includes.right-sidebar-group')
            </div>


        </div>
        </div>
    </section>
@endsection