@extends('layouts.layout')

@section('content')
    <h3 style="color: #fff; margin-bottom: 25px;">{{ $title }}</h3>
    <div class="grops-grid">
        @forelse ($business_users as $business_user)
            <div class="groups-item follow-item">
                <a href="{{ route('id_profile', ['id' => $business_user->id] ) }}" class="groups-item-title">
                    <img src="{{ asset($business_user->avatar) }}" alt="">
                </a>
                <div class="groups-item-content">
                    <a href="{{ route('id_profile', ['id' => $business_user->id] ) }}" class="groups-item-title">
                        <b>{{ $business_user->name }}</b>
                        <span>{{ \App\ConnectionRequest::countConnection($business_user->id, true) }}</span>
                    </a>
                </div>
            </div>
        @empty
            <p>No business accounts</p>
        @endforelse
    </div>
@endsection