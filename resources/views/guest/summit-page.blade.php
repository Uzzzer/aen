@include('guest.layouts.header')
<div class="main_wrap">
    <div class="container-fluid">
        <div class="main_container">
            <div class="m_left_block">
                <div class="left-sidebar">
                    <div class="l_close"><img src="../img/setting_ic.jpg" alt="Alternate Text"></div>
                    <div class="m_l_top">
                        <div class="bl_person_photo">
                            <img src="../img/photo_ic.jpg" alt="Alternate Text" />
                        </div>
                        <div class="bl_person_inf">
                            <a class="bck_to_prf" href="#">Back to Profile</a>
                            <div class="ab_me">About Me</div>
                        </div>
                    </div>
                    <div class="m_container_inf">
                        <div class="b_block_inf">Connections - <a href="#">14,657</a></div>
                        <div class="b_block_inf">Companies - <a href="#">435</a></div>
                        <div class="b_block_inf">Groups - <a href="#">1,200</a></div>
                        <div class="b_block_inf">Your Posts - <a href="#">743</a></div>
                    </div>
                    <div class="m_container_soc">
                        <div class="m_block_soc"><img src="../img/inf_ic_1.jpg" alt="Alternate Text">Photos - <a href="#">221</a></div>
                        <div class="m_block_soc"><img src="../img/inf_ic_2.jpg" alt="Alternate Text">Videos - <a href="#">119</a></div>
                        <div class="m_soc_ic">
                            <a href="#"><img src="../img/soc_ic_1.svg" alt="Alternate Text"></a>
                            <a href="#"><img src="../img/soc_ic_2.svg" alt="Alternate Text"></a>
                            <a href="#"><img src="../img/soc_ic_3.svg" alt="Alternate Text"></a>
                        </div>
                    </div>
                    <a href="#" class="logout-link">Logout of Account</a>
                </div>
            </div>
            <div class="m_right_block">
                <div class="right_container">
                    <div class="m_r_top">
                        <div class="m_rl_top">
                            <div class="ph_person"><img src="../img/photo_ic.jpg" alt="Alternate Text" /></div>
                            <div class="person_text">
                                <p>
                                    <span>Luka Ross, </span>Pole Dancer - <span>24</span> minutes ago via mobile
                                </p>
                                <p class="fon-f-light">
                                    You, Ashley Murray, Dirk Diggler & 12 others Commented
                                </p>
                            </div>
                        </div>
                        <div class="m_rr_top">
                            <a href="#"><img src="../img/r_ic_1.svg" alt="Alternate Text" /> <span>Like</span></a>
                            <a href="#"><img src="../img/r_ic_2.svg" alt="Alternate Text" /> <span>Comment</span></a>
                            <a href="#"><img src="../img/r_ic_3.svg" alt="Alternate Text" /> <span>Share</span></a>
                        </div>
                    </div>
                    <div class="m_bg_container">
                        <img src="../img/summit.png" alt="" class="post-mini-icon">
                        <img src="../img/post-image.png" alt="" class="post-featured-image">
                    </div>
                    <div class="m_bot_container summit_container">
                        <div class="sum_top">
                            <div class="sum_top_data">
                                <p>Mar</p>
                                <span>24</span>
                            </div>
                            <div class="sum_top_text">
                                <h6>The European Summit</h6>
                                <p>Public - Hosted By The European Summit</p>
                            </div>
                            <a href="#" class="sum_top_calendar"><img src="../img/Icon_calendar.svg" alt="Alternate Text" /></a>
                        </div>
                        <div class="summit_info_container">
                            <div class="summit_btns">
                                <a href="#" class="btn_interested">
                                <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19" viewBox="0 0 21 19" fill="none" class="star-icon">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.5 15.75L4.32826 18.9947L5.50696 12.1223L0.513916 7.25532L7.41414 6.25266L10.5 0L13.5859 6.25266L20.4861 7.25532L15.4931 12.1223L16.6718 18.9947L10.5 15.75Z" fill="#4A4A4A"/>
                                </svg>    
                                Interested</a>
                                <a class="btn_going" href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="19" viewBox="0 0 25 19" fill="none" class="check-icon">
                                        <path d="M2.30769 10.3077L9 17" stroke="#4A4A4A" stroke-width="2" stroke-linecap="square"/>
                                        <path d="M22.72 2.28571L9 17" stroke="#4A4A4A" stroke-width="2" stroke-linecap="square"/>
                                    </svg>
                                Going</a>
                            </div>
                            <div class="data_time">
                                <p><img src="../img/watch_icc.svg" alt="Alternate Text" />Friday, March 24 at 1pm - 10pm</p>
                            </div>
                            <div class="coordinates">
                                <div>
                                    <img src="../img/coordinates_ic.svg" alt="Alternate Text" />
                                </div>
                                <div>
                                    <h6>The Oitavos</h6>
                                    <p>R. de Oitavos, Cascais, Portugal</p>
                                </div>
                            </div>
                            <div class="show_map">
                                <a href="#" id="show_map_btn">Show Map</a>
                            </div>
                        </div>
                        <div id="summit_map" class="summit_map" data-address="The Oitavos R. de Oitavos, Cascais, Portugal" data-coords="38.703691, -9.467296"></div>
                        <div class="summit_tab">
                            <ul class="summit_tab_btns">
                                <li  class="summ_btn sum_li_active">About</li>
                                <li class="summ_btn">Discussion</li>
                            </ul>
                            <div class="summit_tab_block summ_active">
                                <div class="sum_text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec <span>magna est,</span> viverra eget porttitor id, varius at metus.
                                    Phasellus non tortor vitae felis varius pellentesque sed quis urna. Etiam eu justo est. In iaculis et urna sit amet vulputate.
                                    Integer pellentesque volutpat est, at lacinia ipsum mattis sed. <span>Pellentesque</span> sed sodales magna, a volutpat metus.
                                    Pellentesque dignissim eu ipsum sed scelerisque.
                                </div>
                                <div class="people_container">
                                    <div class="going_peope padding_people">
                                        16 Going - 39 Interested
                                    </div>
                                    <div class="padding_people person_block">
                                        <div class="person_info">
                                            <div class="girls_photo">
                                                <img class="photo_girl" src="../img/photo_girl_1.jpg" alt="Alternate Text" />
                                                <img class="ok_photo" src="../img/ok_photo.png" alt="Alternate Text" />
                                            </div>
                                            <div class="girls_name">
                                                Luka Ross is going
                                            </div>
                                        </div>
                                        <div class="people_btn">
                                            <a href="#">Message</a>
                                        </div>
                                    </div>
                                    <div class="sugguested_friends padding_people">
                                        <h6>Sugguested Friends</h6>
                                        <div class="person_block">
                                            <div class="person_info">
                                                <div class="girls_photo">
                                                    <img class="photo_girl" src="../img/photo_girl_2.jpg" alt="Alternate Text" />
                                                </div>
                                                <div class="girls_name">
                                                    Jayden James
                                                </div>
                                                <!-- <div class="person_info_hover">
                                                    <a href="#">Invite Friends</a>
                                                    <a href="#">Share in Messenger</a>
                                                    <a href="#">Share as post</a>
                                                </div> -->
                                            </div>
                                            <div class="people_btn">
                                                <a href="#">Invite</a>
                                            </div>
                                        </div>
                                        <div class="person_block">
                                            <div class="person_info">
                                                <div class="girls_photo">
                                                    <img class="photo_girl" src="../img/photo_girl_3.jpg" alt="Alternate Text" />
                                                </div>
                                                <div class="girls_name">
                                                    Pheonix Marie
                                                </div>
                                                <!-- <div class="person_info_hover">
                                                    <a href="#">Invite Friends</a>
                                                    <a href="#">Share in Messenger</a>
                                                    <a href="#">Share as post</a>
                                                </div> -->
                                            </div>
                                            <div class="people_btn">
                                                <a href="#">Invite</a>
                                            </div>
                                        </div>
                                        <div class="person_block">
                                            <div class="person_info">
                                                <div class="girls_photo">
                                                    <img class="photo_girl" src="../img/photo_girl_4.jpg" alt="Alternate Text" />
                                                </div>
                                                <div class="girls_name">
                                                    Gemma Atkinson
                                                </div>
                                                <!-- <div class="person_info_hover">
                                                    <a href="#">Invite Friends</a>
                                                    <a href="#">Share in Messenger</a>
                                                    <a href="#">Share as post</a>
                                                </div> -->
                                            </div>
                                            <div class="people_btn">
                                                <a href="#">Invite</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="people_share padding_people">
                                        <a href="#"><img src="../img/share_icc.svg" alt="Alternate Text" />Share</a>
                                        <div class="person_info_hover">
                                            <a href="#">Invite Friends</a>
                                            <a href="#">Share in Messenger</a>
                                            <a href="#">Share as post</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="people_container">
                                    <div class="recent_posts padding_people">Recent Posts</div>
                                    <div class="recent_block padding_people">
                                        <div class="person_block">
                                            <div class="person_info">
                                                <div class="girls_photo">
                                                    <img class="photo_girl" src="../img/photo_girl_2.jpg" alt="Alternate Text" />
                                                </div>
                                                <div class="girls_name">
                                                    <span>Gemma Atkinson -</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec magna est, viverra eget porttitor id,
                                                    varius at metus. Phasellus non tortor vitae felis varius pellentesque sed quis urna. Etiam eu justo est. In iaculis et urna sit amet vulputate.
                                                    <p>September 4, 2018 at 7:42PM</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="recent_block padding_people">
                                        <div class="person_block">
                                            <div class="person_info">
                                                <div class="girls_photo">
                                                    <img class="photo_girl" src="../img/photo_girl_3.jpg" alt="Alternate Text" />
                                                </div>
                                                <div class="girls_name">
                                                    <span>Gemma Atkinson -</span> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec magna est, viverra eget porttitor id, varius at metus.
                                                    Phasellus non tortor vitae felis varius pellentesque sed quis urna. Etiam eu justo est. In iaculis et urna sit amet vulputate.
                                                    <p>September 4, 2018 at 7:42PM</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" class="see_all_post padding_people">See All Posts</a>
                                </div>
                            </div>
                            <div class="summit_tab_block">
                                <div class="write_post">
                                    <div class="post_menu">
                                        <a href="#">Write Post</a>
                                        <a href="#"><img src="../img/Icon_img.svg" alt="Alternate Text" />Photo Album</a>
                                    </div>
                                    <div class="white_block">
                                        <div class="post_photo">
                                            <img src="../img/photo_girl_1.jpg" alt="Alternate Text" />
                                        </div>
                                        <p>
                                            Write Post here… Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla rhoncus ante, vitae pharetra neque auctor in.
                                            In et placerat dolor, in fringilla tortor. Suspendisse vitae odio aliquam, pulvinar nisi sed, fringilla leo. Quisque nec libero sit amet
                                            neque lobortis mattis eget vel eros. Duis euismod arcu sed elit accumsan, ut fringilla mi maximus. Nulla massa leo, pharetra sit amet bibendum
                                        </p>
                                    </div>
                                </div>
                                <div class="discuss_block">
                                    <div class="discuss_top">
                                        <div class="discuss_photo">
                                            <img src="../img/photo_girl_5.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="discuss_text">
                                            <h6>Spearmint Rhino, London - 55 minutes ago</h6>
                                            <p>You, Ashley Murray, Dirk Diggler & 12 others Commented</p>
                                        </div>
                                    </div>
                                    <div class="discuss_cont">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec <span> magna est</span>, viverra eget porttitor id,
                                        varius at metus. Phasellus non tortor vitae felis varius pellentesque sed quis urna.
                                    </div>
                                    <div class="discuss_bott">
                                        <a href="#"><img src="../img/soc_bot_icc_1.svg" alt="Alternate Text" />Like</a>
                                        <a href="#"><img src="../img/soc_bot_icc_2.svg" alt="Alternate Text" />Comment</a>
                                        <a href="#"><img src="../img/soc_bot_icc_3.svg" alt="Alternate Text" />Share</a>
                                    </div>
                                </div>
                                <div class="discuss_block">
                                    <div class="discuss_top">
                                        <div class="discuss_photo">
                                            <img src="../img/photo_girl_2.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="discuss_text">
                                            <h6>Spearmint Rhino, London - 55 minutes ago</h6>
                                            <p>You, Ashley Murray, Dirk Diggler & 12 others Commented</p>
                                        </div>
                                    </div>
                                    <div class="discuss_cont">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec <span> magna est</span>, viverra eget porttitor id,
                                        varius at metus. Phasellus non tortor vitae felis varius pellentesque sed quis urna.
                                    </div>
                                    <div class="discuss_bott">
                                        <a href="#"><img src="../img/soc_bot_icc_1.svg" alt="Alternate Text" />Like</a>
                                        <a href="#"><img src="../img/soc_bot_icc_2.svg" alt="Alternate Text" />Comment</a>
                                        <a href="#"><img src="../img/soc_bot_icc_3.svg" alt="Alternate Text" />Share</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('guest.layouts.footer')