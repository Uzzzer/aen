@extends('layouts.layout-full')

@section('content')
    @php
        $private = \App\Group::checkPrivateForUser($Group->id);
    @endphp
    <div class="main_wrap">
        <div class="container-fluid">
            @if ($Group->status == 'blocked')
                <h1 style="color: #fff;">The group is blocked.</h1>
            @else
                @if (Auth::check() and Auth::user()->id == $Group->user_id)
                    @include('includes.new-post-group')
                    <div class="buttons-group">
                        <a href="{{ route('settings_group', ['id' => $Group->id]) }}"
                           class="groups-tab-link">Settings Group
                        </a>
                        <a href="#" class="group-bar-link">Add Post</a>
                    </div>
                @endif
                <div class="main_container">
                    <div class="mebmers_group_left">
                        <h6>Members
                            <sup style="font-size: 50%">{{ \App\GroupFollower::getCountFollowersGroup($Group->id) }}</sup>
                        </h6>
                        @if (!$private)
                            <div class="members_profile">
                                @php
                                    $RandomMembers = \App\GroupFollower::getRandomMembers($Group->id, 6);
                                @endphp
                                @foreach ($RandomMembers as $RandomMember)
                                    <a href="{{ route('id_profile', ['id' => $RandomMember->id]) }}">
                                        <img src="{{ asset($RandomMember->avatar) }}" alt="Alternate Text">
                                    </a>
                                @endforeach
                            </div>
                            @if (\App\GroupFollower::getCountFollowersGroup($Group->id))
                                <a class="view_all"
                                   href="{{ route('followers_group', ['id' => $Group->id]) }}">View All
                                </a>
                            @endif
                        @endif
                        <h6 class="mar_top_35">Photos
                            <sup style="font-size: 50%">{{ \App\Gallery::getCountPhotosGroup($Group->id) }}</sup>
                        </h6>
                        @if (!$private)
                            <div class="members_profile_photo">
                                @php
                                    $RandomPhotos = \App\Gallery::getRandomPhotoGroup($Group->id, 4);
                                @endphp
                                @foreach ($RandomPhotos as $RandomPhoto)
                                    <a href="{{ route('photo_group', ['id' => $Group->id, 'pid' => $RandomPhoto->id]) }}"
                                       class="group_preview">
                                        <div style="background-image: url({{ asset($RandomPhoto->file_name) }})"></div>
                                    </a>
                                @endforeach
                            </div>
                            @if (\App\Gallery::getCountPhotosGroup($Group->id))
                                <a class="view_all" href="{{ route('gallery_group', ['id' => $Group->id]) }}">View All
                                </a>
                            @endif
                        @endif
                        <h6 class="mar_top_35">Videos
                            <sup style="font-size: 50%">{{ \App\VideoGallery::getCountVideoGroup($Group->id) }}</sup>
                        </h6>
                        @if (!$private)
                            <div class="group_videos">
                                @php
                                    $RandomVideos = \App\VideoGallery::getRandomVideoGroup($Group->id, 4);
                                @endphp
                                @foreach ($RandomVideos as $RandomVideo)
                                    <a href="{{ route('video_group', ['id' => $Group->id, 'vid' => $RandomVideo->id]) }}"
                                       class="group_preview">
                                        <div style="background-image: url({{ asset($RandomVideo->preview) }})"></div>
                                    </a>
                                @endforeach
                            </div>
                            @if (\App\VideoGallery::getCountVideoGroup($Group->id))
                                <a class="view_all" href="{{ route('videos_group', ['id' => $Group->id]) }}">View All
                                </a>
                            @endif
                        @endif
                    </div>
                    <div class="m_right_block">

                        <div class="mebmers_group_right">
                            <div class="group_photo">
                                <img src="{{ asset($Group->avatar) }}" alt="Alternate Text">
                            </div>
                            <div class="group_decription">
                                <h6>{{ $Group->name }}</h6>
                                <p>
                                    {{ $Group->description }}
                                </p>
                                @if (\App\GroupFollower::CheckFollowGroup($Group->id))
                                    <a href="#" class="leave_group" data-id='{{ $Group->id }}'
                                       @if($Group->type == 'private') data-private="1" @endif>Leave group
                                    </a>
                                @else
                                    <a href="#" class="join_group" data-id='{{ $Group->id }}'
                                       @if($Group->type == 'private') data-private="1" @endif>@if($Group->type != 'private' or (\Auth::check() and $Group->user_id == \Auth::user()->id)){{ 'Join group' }}@else{{ 'Send request' }}@endif</a>
                                @endif
                            </div>
                        </div>
                        @if (!$private)
                            <div class="post_load_list">
                                <input type="hidden" name="load_page_type" value="load_group">
                                <input type="hidden" name="load_page_group" value="{{ $Group->id }}">
                                @foreach ($Posts as $Post)
                                    <div class="right_container post_box" data-id="{{ $Post->id }}">
                                        <div class="m_r_top">
                                            <div class="m_rl_top">
                                                <div class="ph_person">
                                                    <a href="{{ route('group', ['id' => $Post->group_id]) }}">
                                                        <img src="{{ asset($Post->group_avatar) }}"
                                                             alt="Alternate Text"/>
                                                    </a>
                                                </div>
                                                <div class="person_text">
                                                    <p>
                                                        <span><a href="{{ route('group', ['id' => $Post->group_id]) }}"
                                                                 class="user-name-link">{{ $Post->group_name }}</a>,</span> {{ $Post->title }} - {{ \App\DateConvert::Convert($Post->created_at) }}
                                                    </p>
                                                    <p class="fon-f-light">
                                                        <!-- You, Ashley Murray, Dirk Diggler & 12 others Commented -->
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="m_rr_top">
                                                @php
                                                    $PostLike = NULL;
                                                    if (Auth::check())
                                                        $PostLike = \App\PostLike::where('post_id', $Post->id)->where('user_id', Auth::user()->id)->first();
                                                @endphp
                                                <a href="#" class="like_post @if ($PostLike != NULL) liked @endif"
                                                   data-id="{{ $Post->id }}">
                                                    <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                                                    <span>Like{{ $Post->like_count()  ? ' ('.$Post->like_count().')' : '' }}</span>
                                                </a><!--class="comments_post"-->
                                                <a href="{{ route('single_post', $Post->id) }}" class="" data-id="{{ $Post->id }}">
                                                    <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                                                    <span>Comment{{ $Post->comment_count()  ? ' ('.$Post->comment_count().')' : '' }}</span>
                                                </a>
                                                <a href="#" class="share_post"
                                                   data-text="{{ $Post->title }}, {{ date('jS F Y', strtotime($Post->start)) }}"
                                                   data-img="@if ($Post->file_type == 'image'){{ asset($Post->file_name) }}@endif"
                                                   data-url=" {{ url('/') }}" data-id="{{ $Post->id }}">
                                                    <img src="{{ asset('img/r_ic_3.svg') }}" alt="Alternate Text"/>
                                                    <span>Share</span>
                                                </a>
                                                @if (\Auth::check() and \Auth::user()->id == $Post->user_id)
                                                    <a href="#" class="delete_post"
                                                       data-id="{{ $Post->id }}">
                                                        <span>Delete</span>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                        @if ($Post->file_type == 'image')
                                            <div class="m_bg_container"
                                                 style="background-image: url({{ asset($Post->file_name) }});">
                                                <img src="{{ asset($Post->file_name) }}" alt="">
                                            </div>
                                        @endif
                                        @if ($Post->file_type == 'video')
                                            <video width="100%" height="400px"
                                                   controls {!!  $Post->video_preview ? 'poster="' . asset($Post->video_preview) . '"' : '' !!}>
                                                <source src="{{ asset($Post->file_name) }}">
                                                Your browser does not support the video tag.
                                            </video>
                                        @endif
                                        <div class="m_bot_container">
                                            <div class="m_data">
                                                <i class="fa fa-paper-plane-o" style="font-size: 24px; margin-right: 15px;" aria-hidden="true"></i>

                                                {{ $Post->title }}
                                            </div>
                                            <div class="m-bot-text">
                                                {!! $Post->description !!}
                                                <div class="coment_container" style=" border: 0px; ">
                                                    <div class="hide_comment_block" data-id="{{ $Post->id }}"
                                                         style="display: none;">
                                                        @php
                                                            $Comments = \App\PostComment::where('post_comments.post_id', $Post->id)->whereNull('post_comments.reply_id')->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                                        @endphp
                                                        @foreach ($Comments as $Comment)
                                                            <div class="coment_block" data-id="{{ $Comment->id }}">
                                                                <div class="coment_photo_person">
                                                                    <a href="{{ url('/profile/id/'.$Comment->user_id) }}">
                                                                        <img src="{{ asset($Comment->user_avatar) }}"
                                                                             alt="{{ $Comment->user_name }}"
                                                                             title="{{ $Comment->user_name }}"/>
                                                                    </a>
                                                                </div>
                                                                <div class="coment_right">
                                                                    <div class="coment_text">
                                                                        {{ $Comment->comment }}
                                                                        @if ($Comment->image != NULL)
                                                                            <a href="{{ asset($Comment->image) }}"
                                                                               data-lightbox="image-{{ $Comment->id }}">
                                                                                <img src="{{ asset($Comment->image) }}"/>
                                                                            </a>
                                                                        @endif
                                                                    </div>
                                                                    <ul class="coment_data">
                                                                        @php
                                                                            $PostCommentLike = NULL;
                                                                            if (Auth::check())
                                                                                $PostCommentLike = \App\PostCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                                                                            $count_likes = \App\PostCommentLike::where('comment_id', $Comment->id)->count();
                                                                        @endphp
                                                                        <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                                                            @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>Like
                                                                            <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                                                        </li>
                                                                        <li class="reply_comment"
                                                                            @if(\Auth::check()) data-id="{{ $Comment->id }}"
                                                                            data-post-id="{{ $Post->id }}" @endif>Reply
                                                                        </li>
                                                                        <li>{{ \App\DateConvert::Convert($Comment->created_at) }}</li>
                                                                    </ul>
                                                                    @php
                                                                        $ReplyComments = \App\PostComment::where('post_comments.post_id', $Post->id)->where('post_comments.reply_id', $Comment->id)->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                                                    @endphp
                                                                    @foreach ($ReplyComments as $ReplyComment)
                                                                        <div class="coment_block"
                                                                             data-id="{{ $ReplyComment->id }}"
                                                                             data-reply-id="{{ $ReplyComment->reply_id }}">
                                                                            <div class="coment_photo_person">
                                                                                <a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}">
                                                                                    <img src="{{ asset($ReplyComment->user_avatar) }}"
                                                                                         alt="{{ $ReplyComment->user_name }}"
                                                                                         title="{{ $ReplyComment->user_name }}"/>
                                                                                </a>
                                                                            </div>
                                                                            <div class="coment_right">
                                                                                <div class="coment_text">
                                                                                    {{ $ReplyComment->comment }}
                                                                                    @if ($ReplyComment->image != NULL)
                                                                                        <a href="{{ asset($ReplyComment->image) }}"
                                                                                           data-lightbox="image-{{ $ReplyComment->id }}">
                                                                                            <img src="{{ asset($ReplyComment->image) }}"/>
                                                                                        </a>
                                                                                    @endif
                                                                                </div>
                                                                                <ul class="coment_data">
                                                                                    @php
                                                                                        $PostCommentLike = NULL;
                                                                                        if (Auth::check())
                                                                                            $PostCommentLike = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->where('user_id', Auth::user()->id)->first();
                                                                                        $count_likes = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->count();
                                                                                    @endphp
                                                                                    <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                                                                        @if(\Auth::check()) data-id="{{ $ReplyComment->id }}" @endif>Like
                                                                                        <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                                                                    </li>
                                                                                    <li>{{ \App\DateConvert::Convert($ReplyComment->created_at) }}</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @if (Auth::check())
                                                            <form class="add_comment" data-id="{{ $Post->id }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="post_id" value="{{ $Post->id }}">
                                                                <input type="hidden" name="image" value="">
                                                                <div class="coment_write">
                                                                    <div class="writer_left">
                                                                        <img src="{{ asset(Auth::user()->avatar) }}"
                                                                             alt="Alternate Text"/>
                                                                    </div>
                                                                    <div class="writer_right">
                                                                        <div class="comment_image"></div>
                                                                        <input type="text" name="comment" value=""
                                                                               placeholder="Write Comments..."/>
                                                                    </div>
                                                                </div>
                                                                <img class="comment_img">
                                                                <input type="file" style="display: none;">
                                                            </form>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="coment_container">
                                                <div class="like_conent">
                                                    @php
                                                        $count_likes = \App\PostLike::where('post_id', $Post->id)->count();
                                                        $count_comments = \App\PostComment::where('post_id', $Post->id)->count();
                                                    @endphp
                                                    <div class="like_left {{ !$count_likes ? 'hidden-post-block' : '' }}">
                                                        <div class="like">
                                                            <img src="{{ asset('img/like_c.svg') }}"
                                                                 alt="Alternate Text"/>
                                                        </div>
                                                        <div class="like_text count_like">
                                                            <span>{{ $count_likes }}</span>
                                                            likes
                                                        </div>
                                                    </div>
                                                    <div style=" cursor: pointer; " class="like_right {{ !$count_comments ? 'hidden-post-block' : '' }}">
                                                        <span>{{ $count_comments }}</span>
                                                        Comments
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
    @if (Auth::check())
        <form class="add_comment reply_comment_form copy_form" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" name="post_id" value="">
            <input type="hidden" name="comment_id" value="">
            <input type="hidden" name="image" value="">
            <div class="coment_write">
                <div class="writer_left">
                    <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                </div>
                <div class="writer_right">
                    <div class="comment_image"></div>
                    <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                </div>
            </div>
            <img class="comment_img">
            <input type="file" style="display: none;">
        </form>
    @endif
@endsection