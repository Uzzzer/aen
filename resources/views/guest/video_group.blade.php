@extends('layouts.layout-full')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary overflow_block">
                    <div class="gallery-video">
                        <input type="hidden" name="video_update_id" value="{{ $VideoGallery->id }}">
                        <video width="100%" height="400px" controls>
                            <source src="{{ asset($VideoGallery->file_name) }}">
                            Your browser does not support the video tag.
                        </video>
                        @php
                            $count_likes = \App\VideoLike::countLike($VideoGallery->id);
                            $count_comments = \App\MediaComment::where('video_id', $VideoGallery->id)->count();
                        @endphp
                        <div class="photo-bottom-footer" data-id="{{ $VideoGallery->id }}">
                            <a href="#" class="like_video @if (\App\VideoLike::checkLike($VideoGallery->id)) liked @endif" data-id="{{ $VideoGallery->id }}"><i class="v-icon like"></i>Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></a>
                            <a href="{{ route('video_group', ['id' => $Group->id, 'vid' => $VideoGallery->id]) }}"><i class="v-icon comment"></i>Comment @if($count_comments > 0){{ '('.$count_comments.')' }}@endif</a>
                            <a href="#"><i class="v-icon share"></i>Share</a>
                        </div>
                    </div>
                    <div class="gallary_top video-top">
                        <div class="gal_top_left">
                            <span>{{ $VideoGallery->title }}</span>
                            <p>{{ \App\MediaView::videoViewsVideo($VideoGallery->id) }}</p>
                            <div class="video-description">
                                {{ $VideoGallery->description }}
                            </div>
                        </div>
                    </div>
                    <div class="coment_container">
                        <div class="hide_comment_block" data-id="{{ $VideoGallery->id }}">
                            @php
                                $Comments = \App\MediaComment::where('media_comments.video_id', $VideoGallery->id)->whereNull('media_comments.reply_id')->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                            @endphp
                            @foreach ($Comments as $Comment)
                                <div class="coment_block" data-id="{{ $Comment->id }}">
                                    <div class="coment_photo_person">
                                        <a href="{{ url('/profile/id/'.$Comment->user_id) }}"><img src="{{ asset($Comment->user_avatar) }}" alt="{{ $Comment->user_name }}" title="{{ $Comment->user_name }}"/></a>
                                    </div>
                                    <div class="coment_right">
                                        <div class="coment_text">
                                            {{ $Comment->comment }}
                                            @if ($Comment->image != NULL)
                                                <a href="{{ asset($Comment->image) }}" data-lightbox="image-{{ $Comment->id }}"><img src="{{ asset($Comment->image) }}"/></a>
                                            @endif
                                        </div>
                                        <ul class="coment_data">
                                            @php
                                                $MediaCommentLike = NULL;
                                                if (Auth::check())
                                                    $MediaCommentLike = \App\MediaCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                                                $count_likes = \App\MediaCommentLike::where('comment_id', $Comment->id)->count();
                                            @endphp
                                            <li data-media="1" class="like_comment @if($MediaCommentLike != NULL) liked @endif" @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>Like <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></li>
                                            <li class="reply_comment" @if(\Auth::check()) data-id="{{ $Comment->id }}" data-video-id="{{ $VideoGallery->id }}" @endif>Reply</li>
                                            <li>{{ \App\DateConvert::Convert($Comment->created_at) }}</li>
                                        </ul>
                                        @php
                                            $ReplyComments = \App\MediaComment::where('media_comments.video_id', $VideoGallery->id)->where('media_comments.reply_id', $Comment->id)->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                        @endphp
                                        @foreach ($ReplyComments as $ReplyComment)
                                            <div class="coment_block" data-id="{{ $ReplyComment->id }}" data-reply-id="{{ $ReplyComment->reply_id }}">
                                                <div class="coment_photo_person">
                                                    <a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}"><img src="{{ asset($ReplyComment->user_avatar) }}" alt="{{ $ReplyComment->user_name }}" title="{{ $ReplyComment->user_name }}"/></a>
                                                </div>
                                                <div class="coment_right">
                                                    <div class="coment_text">
                                                        {{ $ReplyComment->comment }}
                                                        @if ($ReplyComment->image != NULL)
                                                            <a href="{{ asset($ReplyComment->image) }}" data-lightbox="image-{{ $ReplyComment->id }}"><img src="{{ asset($ReplyComment->image) }}"/></a>
                                                        @endif
                                                    </div>
                                                    <ul class="coment_data">
                                                        @php
                                                            $MediaCommentLike = NULL;
                                                            if (Auth::check())
                                                                $MediaCommentLike = \App\MediaCommentLike::where('comment_id', $ReplyComment->id)->where('user_id', Auth::user()->id)->first();
                                                            $count_likes = \App\MediaCommentLike::where('comment_id', $ReplyComment->id)->count();
                                                        @endphp
                                                        <li data-media="1" class="like_comment @if($MediaCommentLike != NULL) liked @endif" @if(\Auth::check()) data-id="{{ $ReplyComment->id }}" @endif>Like <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></li>
                                                        <li>{{ \App\DateConvert::Convert($ReplyComment->created_at) }}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                            @if (Auth::check())
                                <form class="add_comment comment_before" data-id="{{ $VideoGallery->id }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="video_id" value="{{ $VideoGallery->id }}">
                                    <input type="hidden" name="image" value="">
                                    <div class="coment_write">
                                        <div class="writer_left">
                                            <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                                        </div>
                                        <div class="writer_right">
                                            <div class="comment_image"></div>
                                            <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                                        </div>
                                    </div>
                                    <img class="comment_img">
                                    <input type="file" style="display: none;">
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
                @include('includes.right-sidebar-group')
            </div>


        </div>
        </div>
    </section>

    @if (Auth::check())
        <form class="add_comment reply_comment_form copy_form" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" name="video_id" value="">
            <input type="hidden" name="comment_id" value="">
            <input type="hidden" name="image" value="">
            <div class="coment_write">
                <div class="writer_left">
                    <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                </div>
                <div class="writer_right">
                    <div class="comment_image"></div>
                    <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                </div>
            </div>
            <img class="comment_img">
            <input type="file" style="display: none;">
        </form>
    @endif
@endsection