<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AEN - Enter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/reset.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/header.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/StyleSheet.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fonts.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ruslan.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/max.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/additional.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/lightbox.css') }}" type="text/css" rel="stylesheet">
    @if(isset($css))
        @foreach($css as $src)
            <link rel="stylesheet" href="{{$src}}" />
        @endforeach
    @endif
</head>
<body>

    <div class="main_wrap check-age-wrap">
        <div class="container-fluid">
			<div class="check-age-block">
				<div class="check-age-img">
					<img src="img/girls-check-age.png" alt="Alternate Text" />
				</div>
				<div class="check-age-text">
					 <h2 style=" color: #fff; text-align: center; ">If you are 18 years old</h2>
					 <a href="{{ route('check_age') }}"><div class="btn_follow">Enter AEN</div></a>
				</div>
			</div>
        </div>
    </div>

    <script src="{{ asset('js/jquery.3.3.1.js') }}"></script>
    @if(isset($js))
        @foreach($js as $src)
            <script src="{{$src}}"></script>
        @endforeach
    @endif
</body>
</html>