<div class="post-bar job-post-add">
    <form id="NewJob">
        <div class="post-bar-content">
            <h3>Basic info</h3>
            <div class="create-line">
                <b class="label">Photo or Video</b>
                <div class="upload-section">
                    <img src="{{ asset('img/upload-bg.jpg') }}" alt="" id="upload-bg" class="image-job">
                    <input type="file" id="upload-file" class="image-job">
                    <input type="hidden" name="file_type" class="image-job">
                    <input type="hidden" name="file_name" class="image-job">
                    <input type="hidden" name="file_preview" class="image-job">
                </div>
            </div>
            <div class="create-line">
                <b class="label">Title</b>
                <input type="text" name="JobTitle">
            </div>
            <h3>Details</h3>
            <div class="create-line">
                <b class="label">Description</b>
                <textarea name="Description" cols="30" rows="5"></textarea>
            </div>
            <div class="create-line responsibilities">
                <b class="label">Responsibilities</b>
                <a href="#" class="add_responsibilities">+</a>
                <input type="text" name="responsibilities[]" class="copy_row">
            </div>
            <div class="create-line">
                <b class="label">Job Type</b>
                <input type="text" name="fields[Job Type]">
            </div>
            <div class="create-line">
                <b class="label">Salary</b>
                <input type="text" name="fields[Salary]">
            </div>
            <div class="create-line">
                <b class="label">Experience</b>
                <input type="text" name="fields[Experience]">
            </div>
            <div class="create-line">
                <b class="label">Location</b>
                <input type="text" name="fields[Location]">
            </div>
            <div class="create-line">
                <b class="label">Licence</b>
                <input type="text" name="fields[Licence]">
            </div>
            <div class="create-line">
                <b class="label">Language</b>
                <input type="text" name="fields[Language]">
            </div>
        </div>
        <div class="post-bar-footer">
            <input type="submit" class="post-button" value="Add job">
        </div>
        {{ csrf_field() }}
    </form>
</div>