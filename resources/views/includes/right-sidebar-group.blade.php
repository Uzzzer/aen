<div class="sidebar">
    <div class="m_l_top">
        <div class="bl_person_photo">
            <a href="{{ route('group', ['id' => $Group->id]) }}"><img src="{{ asset($Group->avatar) }}" alt="Alternate Text"></a>
        </div>
        @if (\App\GroupFollower::CheckFollowGroup($Group->id))
            <a href="#" class="btn_follow leave_group" data-id='{{ $Group->id }}'>Leave group</a>
        @else
            <a href="#" class="btn_follow join_group" data-id='{{ $Group->id }}'>Join group</a>
        @endif
        <div class="bl_person_inf">
            <div><a href="{{ route('group', ['id' => $Group->id]) }}">{{ $Group->name }}</a></div>
        </div>
    </div>
    <div class="m_container_inf">
        <div class="b_block_inf {{ \Request::route()->getName() == 'followers_group' ? 'menu-sidebar-active' : '' }}"><a href="{{ route('followers_group', ['id' => $Group->id]) }}"><b>Members - </b> {{ \App\GroupFollower::getCountFollowersGroup($Group->id) }}</a></div>
        <div class="b_block_inf"><a href="{{ route('group', ['id' => $Group->id]) }}"><b>Posts - </b> {{ \App\Post::getCountPostsGroup($Group->id) }}</a></div>
    </div>
    <div class="m_container_soc">
        <div class="m_block_soc {{ \Request::route()->getName() == 'gallery_group' ? 'menu-sidebar-active' : '' }}"><img src="{{ asset('img/inf_ic_1.jpg') }}" alt="Alternate Text"><a href="{{ route('gallery_group', ['id' => $Group->id]) }}"><b>Photos - </b> {{ \App\Gallery::getCountPhotosGroup($Group->id) }}</a></div>
        <div class="m_block_soc {{ \Request::route()->getName() == 'videos_group' ? 'menu-sidebar-active' : '' }}"><img src="{{ asset('img/inf_ic_2.jpg') }}" alt="Alternate Text"><a href="{{ route('videos_group', ['id' => $Group->id]) }}"><b>Videos - </b> {{ \App\VideoGallery::getCountVideoGroup($Group->id) }}</a></div>
    </div>

    <a href="{{ route('logout') }}" class="logout-link">Logout of Account</a>
</div>