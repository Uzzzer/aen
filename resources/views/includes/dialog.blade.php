{{ csrf_field() }}
@if (Auth::check())
    <section id="dialog" class="dialog window-chat">
        <div class="messages-list" id="chat-list">
            <div class="dialog-header">
                <div class="dialog-header-title">
                    <h3>Your Messages</h3>
                    <span></span>
                </div>
                <div class="dialog-header-search">
                    <input type="text" id="SearchBox" placeholder="Search" class="placeholder-shown">
                </div>
                <a class="dialog-header-message" href="#"></a>
                <a class="dialog-closed-window" href="#" style=" margin-top: -30px; margin-right: -10px; margin-left: 10px; opacity: 0.9; "><i class="fa fa-times" aria-hidden="true" style=" color: #fff; font-size: 15px; "></i></a>
            </div>
            <div class="dialog-body">

            </div>
        </div>
        <div class="chat-window" id="chat-window">
            <div class="dialog-header">
                <div class="chat-back"></div>
                <div class="chat-person">
                    <p></p>
                    <span>Active in ago</span>
                </div>
                <div class="chat-call" id="Call"></div>
                <div class="chat-video-call" id="VideoCall"></div>
            </div>
            <div class="dialog-body">

            </div>
            <div class="dialog-footer">
                <input type="text" id="MessageBox">
                <input type="submit" id="MessageSend">
            </div>
        </div>
    </section>
    <div style="display: none;" id="music_play"></div>
    <script>
        var user_id = {{ Auth::user()->id }};
    </script>
@endif