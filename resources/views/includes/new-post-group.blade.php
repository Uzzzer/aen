<div class="post-bar group-post-add">
    <form id="NewPostGroup">
        <input type="hidden" name="PostType" value="group_post">
        <input type="hidden" name="groupId" value="{{ $Group->id }}">
        <div class="post-bar-content">
            <h3>Basic info</h3>
            <div class="create-line" style=" display: block; ">
                <b class="label" style=" display: block; ">Photo or Video</b>
                <div class="upload-section">
                    <img src="{{ asset('img/upload-bg.jpg') }}" alt="" id="upload-bg" class="image-post-group">
                    <input type="file" id="upload-file" class="image-post-group">
                    <input type="hidden" name="file_type" class="image-post-group">
                    <input type="hidden" name="file_name" class="image-post-group">
                    <input type="hidden" name="preview_video" class="image-post">
                </div>
            </div>
            <div class="create-line" style=" display: block; ">
                <b class="label" style=" display: block; ">Title</b>
                <input type="text" name="PostTitle">
            </div>
            <div class="create-line" style=" display: block; ">
                <b class="label">Subtitle</b>
                <input type="text" name="PostSubtitle">
            </div>
            <h3>Details</h3>
            <div class="create-line" style=" display: block; ">
                <b class="label" style=" display: block; ">Description</b>
                <textarea name="Description" cols="30" rows="5"></textarea>
            </div>
            <div class="create-line" style=" display: block; ">
                <b class="label" style=" display: block; ">Keywords</b>
                <input type="text" name="Keywords">
            </div>
        </div>
        <div class="post-bar-footer">
            <input type="submit" class="post-button" value="Post">
        </div>
        {{ csrf_field() }}
    </form>
</div>