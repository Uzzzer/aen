<style>
.checkout_outer{
	position: absolute;
	top: 50%;
	left: calc(50% - 114px);
	text-align: center;
}
.photo_price{
	font-size: 25px;
	background: #80808045;
	margin: 0 auto;
	margin-top: -10px;
	margin-bottom: 10px;
	border-radius: 5px;
	color: white;
}
</style>

<div class="slider">
    <div class="carousel">
        <div id="firstCar" class="owl-carousel">
            @php
               if (!$UserType or $UserType == 'public_user'){
	               $GalleryPhotos = [];
               }else{
			   	if(isset($newGallery)){
			   		$GalleryPhotos = $newGallery;
			   	}else{
			   		$GalleryPhotos = \App\Gallery::where('user_id', $user->id)->whereNull('group_id')->get();
			   	}
               }
            @endphp
            
            @forelse($GalleryPhotos as $photo)
                <div class="item">
                    <img class="owl-lazy" alt="">
                    @if($photo->need_payment)
	                    <img src="{{ asset($photo->blured) }}"/>
                    	<div class="checkout_outer">
                    		<div class="photo_price">
                                £{{$photo->price}}
                    		</div>
                    		<br>
                    		<div>
                                <a href="/profile/payment?photo={{$photo->id}}">
								    <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png" alt="Check out with PayPal" />
                                </a>
                            </div>
                    	</div>
                    @else
	                    <img src="{{ asset($photo->file_name) }}"/>
                    @endif
                </div>
            @empty
                @if (\Auth::user()->id == $user->id)
                    <a href="{{ url('/profile/profile-editing?photo=1') }}">
                        <div class="add_photo_container">
                            <div class="photo_edit_block">
                                <img src="{{ asset('/img/camera_upload.svg') }}" alt="Alternate Text"
                                     style="width: auto;">
                                <div class="upl_gall_img">Upload Gallery
                                    <br>
                                    Images
                                </div>
                                <div class="sell_your_img">You can sell your images?</div>
                            </div>
                        </div>
                    </a>
                @endif
            @endforelse
        </div>
    </div>
</div>
