@php
    $GroupCategories = \App\GroupCategory::get();
@endphp
<div class="post-bar new-group-bar">
    <form id="NewGroup">
        <div class="post-bar-content">
            <h3>New Group</h3>
            <div class="create-line">
                <b class="label">Photo</b>
                <div class="upload-section">
                    <img src="{{ asset('img/upload-bg.jpg') }}" alt="" id="upload-bg" class="image-group">
                    <input type="file" id="upload-file" class="image-group">
                    <input type="hidden" name="file_type" class="image-group">
                    <input type="hidden" name="file_name" class="image-group">
                </div>
            </div>
            <div class="create-line">
                <b class="label">Name</b>
                <input type="text" name="GroupName">
            </div>
            <div class="create-line">
                <b class="label">Category</b>
                <select name="GroupCategories">
                    @foreach ($GroupCategories as $GroupCategory)
                    <option value="{{ $GroupCategory->id }}">{{ $GroupCategory->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="create-line">
                <b class="label">Group type</b>
                <select name="GroupType" class="edit_input main_input">
                    <option value="public">Public</option>
                    <option value="private">Private</option>
                </select>
            </div>
            <div class="create-line">
                <b class="label">Description</b>
                <textarea name="Description" cols="30" rows="5"></textarea>
            </div>
            <div class="create-line">
                <b class="label">Keywords</b>
                <input type="text" name="Keywords">
            </div>
        </div>
        <div class="post-bar-footer">
            <input type="submit" class="post-button" value="Create">
        </div>
        {{ csrf_field() }}
    </form>
</div>