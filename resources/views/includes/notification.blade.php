@php
    $GroupNotice = null;
    $UserNotice = null;

    if (!is_null($Notification->from_group_id)) {
        $type = 'group';
        $GroupNotice = \App\Group::find($Notification->from_group_id);
    } else {
        $type = 'user';
        $UserNotice = \App\User::find($Notification->from_user_id);
    }

@endphp
@if (!is_null($UserNotice) or !is_null($GroupNotice))
<div class="right_container post_box">
    <div class="m_r_top">
        <div class="m_rl_top">
            <div class="ph_person"><a href="@if($type == 'user'){{ route('id_profile', ['id' => $UserNotice->id]) }}@else{{ route('group', ['id' => $GroupNotice->user_id]) }}@endif"><img src="@if($type == 'user'){{ asset($UserNotice->avatar) }}@else{{ asset($GroupNotice->avatar) }}@endif" alt="Alternate Text"/></a></div>
            <div class="person_text">
                <p>
                    <span>@if($type == 'group')Group @endif<a href="@if($type == 'user'){{ route('id_profile', ['id' => $UserNotice->id]) }}@else{{ route('group', ['id' => $GroupNotice->user_id]) }}@endif" class="user-name-link">@if($type == 'user'){{ $UserNotice->name }}@else{{ $GroupNotice->name }}@endif</a>,</span> - {{ \App\DateConvert::Convert($Notification->created_at) }}
                </p>
            </div>
        </div>
    </div>
    <div class="m_bot_container">
        <div class="m_data">
            @php echo $Notification->text; @endphp
        </div>
    </div>
</div>
@endif