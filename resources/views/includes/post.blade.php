<div class="right_container post_box" data-id="{{ $Post->id }}">
    <div class="m_r_top">
        <div class="m_rl_top">
            <div class="ph_person">
                @if (\Auth::check())
                    <a href="{{ url('/profile/id/'.$Post->user_id) }}">
                        <img src="{{ asset($Post->user_avatar) }}" alt="Alternate Text"/>
                    </a>
                @else
                    <img src="{{ asset($Post->user_avatar) }}" alt="Alternate Text"/>
                @endif
            </div>
            <div class="person_text">
                <p>
                    @if (\Auth::check())
                        <span><a href="{{ url('/profile/id/'.$Post->user_id) }}"
                                 class="user-name-link">{{ $Post->user_name }}</a>,</span>
                    @else
                        <span>{{ $Post->user_name }},</span>
                    @endif
                    {{ $Post->title }} - {{ \App\DateConvert::Convert($Post->created_at) }}
                </p>
                <p class="fon-f-light">
                    <!-- You, Ashley Murray, Dirk Diggler & 12 others Commented -->
                </p>
            </div>
        </div>
        <div class="m_rr_top">
            @php
                $PostLike = NULL;
                if (Auth::check())
                    $PostLike = \App\PostLike::where('post_id', $Post->id)->where('user_id', Auth::user()->id)->first();
            @endphp
            <a href="#" class="like_post @if ($PostLike != NULL) liked @endif" data-id="{{ $Post->id }}">
                <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                <span>Like{{ $Post->like_count()  ? ' ('.$Post->like_count().')' : '' }}</span>
            </a>
            <a href="{{ route('single_post', $Post->id) }}" class="" data-id="{{ $Post->id }}">
                <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                <span>Comment{{ $Post->comment_count()  ? ' ('.$Post->comment_count().')' : '' }}</span>
            </a>
            <a href="#" class="share_post" data-text="{{ $Post->title }}, {{ date('jS F Y', strtotime($Post->start)) }}"
               data-img="@if ($Post->file_type == 'image'){{ asset($Post->file_name) }}@endif"
               data-url=" {{ url('/') }}" data-id="{{ $Post->id }}">
                <img src="{{ asset('img/r_ic_3.svg') }}" alt="Alternate Text"/>
                <span>Share</span>
            </a>
            @if (\Auth::check() and \Auth::user()->id == $Post->user_id)
                <div class="drop_tp-post_box">
                    <div class="dot_down-btn">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Capa_1" x="0px" y="0px"
                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"
                             width="20px" height="30px">
                                        <g>
                                            <g>
                                                <g>
                                                    <g>
                                                        <circle cx="256" cy="256" r="64" data-original="#000000"
                                                                class="active-path" data-old_color="#ffffff"
                                                                fill="#ffffff"/>
                                                        <circle cx="256" cy="448" r="64" data-original="#000000"
                                                                class="active-path" data-old_color="#ffffff"
                                                                fill="#ffffff"/>
                                                        <circle cx="256" cy="64" r="64" data-original="#000000"
                                                                class="active-path" data-old_color="#ffffff"
                                                                fill="#ffffff"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                    </div>
                    <div class="drop_content_inner_list">
                        <a href="#" class="edit_post"
                           data-id="{{ $Post->id }}">
                            <span>Edit</span>
                        </a>
                        <a href="#" class="delete_post"
                           data-id="{{ $Post->id }}">
                            <span>Delete</span>
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @if ($Post->file_type == 'image')
        <div class="m_bg_container" style="background-image: url({{ asset($Post->file_name) }});">
            <img src="{{ asset($Post->file_name) }}" alt="">
        </div>
    @endif
    @if ($Post->file_type == 'video')
        <video width="100%" height="400px"
               controls {!!  $Post->video_preview ? 'poster="' . asset($Post->video_preview) . '"' : '' !!}>
            <source src="{{ asset($Post->file_name) }}">
            Your browser does not support the video tag.
        </video>
    @endif
    <div class="m_bot_container">
        <div class="m_data">
            @if ($Post->post_type == 'news')
                <i class="fa fa-newspaper-o" style="font-size: 24px; margin-right: 15px;" aria-hidden="true"></i>
            @elseif ($Post->post_type == 'online')
                <i class="fa fa-video-camera" aria-hidden="true" style="font-size: 24px; margin-right: 15px;"></i>
            @else
                <i class="fa fa-calendar" style="font-size: 24px; margin-right: 15px;" aria-hidden="true"></i>
            @endif
            {{ $Post->title }}
            @if ($Post->post_type == 'event')
                <span>{{ date('jS F Y', strtotime($Post->start)) }} Local Time, {{ $Post->location }}</span>
            @endif
            @if ($Post->post_type == 'online')
                <span>{{ date('jS F Y', strtotime($Post->start)) }}, {{ date('H:i', strtotime($Post->start_time)) }} - {{ $Post->start == $Post->end ? '' : date('jS F Y', strtotime($Post->end)) . ',' }} {{ date('H:i', strtotime($Post->end_time)) }}</span>
            @endif
        </div>
        <div class="m-bot-text">
            {!! $Post->description !!}
            <div class="coment_container" style=" border: 0px; ">
                <div class="hide_comment_block" data-id="{{ $Post->id }}" style="display: none;">
                    @php
                        $Comments = \App\PostComment::where('post_comments.post_id', $Post->id)->whereNull('post_comments.reply_id')->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                    @endphp
                    @foreach ($Comments as $Comment)
                        <div class="coment_block" data-id="{{ $Comment->id }}">
                            <div class="coment_photo_person">
                                <a href="{{ url('/profile/id/'.$Comment->user_id) }}">
                                    <img src="{{ asset($Comment->user_avatar) }}" alt="{{ $Comment->user_name }}"
                                         title="{{ $Comment->user_name }}"/>
                                </a>
                            </div>
                            <div class="coment_right">
                                <div class="coment_text">
                                    {{ $Comment->comment }}
                                    @if ($Comment->image != NULL)
                                        <a href="{{ asset($Comment->image) }}" data-lightbox="image-{{ $Comment->id }}">
                                            <img src="{{ asset($Comment->image) }}"/>
                                        </a>
                                    @endif
                                </div>
                                <ul class="coment_data">
                                    @php
                                        $PostCommentLike = NULL;
                                        if (Auth::check())
                                            $PostCommentLike = \App\PostCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                                        $count_likes = \App\PostCommentLike::where('comment_id', $Comment->id)->count();
                                    @endphp
                                    <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                        @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>Like
                                        <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                    </li>
                                    <li class="reply_comment" @if(\Auth::check()) data-id="{{ $Comment->id }}"
                                        data-post-id="{{ $Post->id }}" @endif>Reply
                                    </li>
                                    <li>{{ \App\DateConvert::Convert($Comment->created_at) }}</li>
                                </ul>
                                @php
                                    $ReplyComments = \App\PostComment::where('post_comments.post_id', $Post->id)->where('post_comments.reply_id', $Comment->id)->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                @endphp
                                @foreach ($ReplyComments as $ReplyComment)
                                    <div class="coment_block" data-id="{{ $ReplyComment->id }}"
                                         data-reply-id="{{ $ReplyComment->reply_id }}">
                                        <div class="coment_photo_person">
                                            <a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}">
                                                <img src="{{ asset($ReplyComment->user_avatar) }}"
                                                     alt="{{ $ReplyComment->user_name }}"
                                                     title="{{ $ReplyComment->user_name }}"/>
                                            </a>
                                        </div>
                                        <div class="coment_right">
                                            <div class="coment_text">
                                                {{ $ReplyComment->comment }}
                                                @if ($ReplyComment->image != NULL)
                                                    <a href="{{ asset($ReplyComment->image) }}"
                                                       data-lightbox="image-{{ $ReplyComment->id }}">
                                                        <img src="{{ asset($ReplyComment->image) }}"/>
                                                    </a>
                                                @endif
                                            </div>
                                            <ul class="coment_data">
                                                @php
                                                    $PostCommentLike = NULL;
                                                    if (Auth::check())
                                                        $PostCommentLike = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->where('user_id', Auth::user()->id)->first();
                                                    $count_likes = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->count();
                                                @endphp
                                                <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                                    @if(\Auth::check()) data-id="{{ $ReplyComment->id }}" @endif>Like
                                                    <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                                </li>
                                                <li>{{ \App\DateConvert::Convert($ReplyComment->created_at) }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                    @if (Auth::check())
                        <form class="add_comment" data-id="{{ $Post->id }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="post_id" value="{{ $Post->id }}">
                            <input type="hidden" name="image" value="">
                            <div class="coment_write">
                                <div class="writer_left">
                                    <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                                </div>
                                <div class="writer_right">
                                    <div class="comment_image"></div>
                                    <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                                </div>
                            </div>
                            <img class="comment_img">
                            <input type="file" style="display: none;">
                        </form>
                    @endif
                </div>
            </div>
        </div>
        <div class="coment_container">
            <div class="like_conent">
                @php
                    $count_likes = \App\PostLike::where('post_id', $Post->id)->count();
                    $count_comments = \App\PostComment::where('post_id', $Post->id)->count();
                @endphp
                <div class="like_left {{ !$count_likes ? 'hidden-post-block' : '' }}">
                    <div class="like">
                        <img src="{{ asset('img/like_c.svg') }}" alt="Alternate Text"/>
                    </div>
                    <div class="like_text count_like">
                        <span>{{ $count_likes }}</span>
                        likes
                    </div>
                </div>
                <div data-id="{{ $Post->id }}" style=" cursor: pointer; "
                     class="like_right {{ !$count_comments ? 'hidden-post-block' : '' }} {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'comments_post' }}">
                    <span>{{ $count_comments }}</span>
                    Comments
                </div>
            </div>
        </div>
    </div>
</div>