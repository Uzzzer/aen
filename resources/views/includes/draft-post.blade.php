<div class="right_container post_box" data-id="{{ $Post->id }}">
    <div class="m_r_top">
        <div class="m_rl_top">
            <div class="ph_person">
                <a href="{{ url('/profile/id/'.\Auth::user()->id) }}">
                    <img src="{{ asset(\Auth::user()->avatar) }}" alt="Alternate Text"/>
                </a>
            </div>
            <div class="person_text">
                <p>
                                <span><a href="{{ url('/profile/id/'.\Auth::user()->id) }}"
                                         class="user-name-link">{{ \Auth::user()->name }}</a>,</span> {{ $Post->title }} - {{ \App\DateConvert::Convert($Post->created_at) }}
                </p>
                <p class="fon-f-light">
                    <!-- You, Ashley Murray, Dirk Diggler & 12 others Commented -->
                </p>
            </div>
        </div>
        <div class="m_rr_top">
            <a href="#"
               class="publish_draft_post"
               data-id="{{ $Post->id }}">
                <i class="fa fa-paper-plane-o" aria-hidden="true" style=" font-size: 20px; margin-top: -4px; "></i>
                <span>Publish</span>
            </a>
            <a href="#"
               class="edit_draft_post"
               data-id="{{ $Post->id }}">
                <i class="fa fa-pencil-square-o" aria-hidden="true" style=" font-size: 20px; margin-top: -4px; "></i>
                <span>Edit</span>
            </a>
            <a href="#" class="delete_post"
               data-id="{{ $Post->id }}">
                <i class="fa fa-trash" aria-hidden="true" style=" font-size: 20px; margin-top: -4px; "></i>
                <span>Delete</span>
            </a>
        </div>
    </div>
    @if ($Post->file_type == 'image')
        <div class="m_bg_container" style="background-image: url({{ asset($Post->file_name) }});">
            <img src="{{ asset($Post->file_name) }}" alt="">
        </div>
    @endif
    @if ($Post->file_type == 'video')
        <video width="100%" height="400px"
               controls {!!  $Post->video_preview ? 'poster="' . asset($Post->video_preview) . '"' : '' !!}>
            <source src="{{ asset($Post->file_name) }}">
            Your browser does not support the video tag.
        </video>
    @endif
    <div class="m_bot_container">
        <div class="m_data">
            @if ($Post->post_type == 'news')
                <i class="fa fa-newspaper-o" style="font-size: 24px; margin-right: 15px;"
                   aria-hidden="true"></i>
            @elseif ($Post->post_type == 'online')
                <i class="fa fa-video-camera" aria-hidden="true"
                   style="font-size: 24px; margin-right: 15px;"></i>
            @else
                <i class="fa fa-calendar" style="font-size: 24px; margin-right: 15px;"
                   aria-hidden="true"></i>
            @endif

            {{ $Post->title }}
            @if ($Post->post_type == 'event')
                <span>{{ date('jS F Y', strtotime($Post->start)) }} Local Time, {{ $Post->location }}</span>
            @endif
            @if ($Post->post_type == 'online')
                <span>{{ date('jS F Y', strtotime($Post->start)) }}, {{ date('H:i', strtotime($Post->start_time)) }} - {{ $Post->start == $Post->end ? '' : date('jS F Y', strtotime($Post->end)) . ',' }} {{ date('H:i', strtotime($Post->end_time)) }}</span>
            @endif
        </div>
        <div class="m-bot-text">
            {!! $Post->description !!}
        </div>
    </div>
</div>