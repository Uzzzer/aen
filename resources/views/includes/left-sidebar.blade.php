<div class="left-sidebar">
    @if(Auth::check())
        <div class="l_close">
            <img src="{{ asset('img/setting_ic.jpg') }}" alt="Alternate Text">
        </div>
        <div class="m_l_top">
            <div class="bl_person_photo">
                <img src="{{ asset(\Auth::user()->avatar) }}" alt="Alternate Text"/>
            </div>
            <div class="bl_person_inf">
                <a class="bck_to_prf" href="{{ route('profile') }}">Back to Profile</a>
                <div class="ab_me">About Me</div>
            </div>
        </div>
        <div class="m_container_inf">

            @if ($user->business)
                <div class="b_block_inf">
                    <a href="@if(Auth::check() and $user->id != Auth::user()->id){{ route('connections_id', ['id' => $user->id]) }}@else{{ route('connections') }}@endif"><b>Fans -</b> {{ \App\ConnectionRequest::countConnection($user->id) }}</a>
                </div>
                {{--<div class="b_block_inf">
                    <a href="#"><b>Associates -</b> 14,657</a>
                </div>--}}
                <div class="b_block_inf">
                    <a href="{{ route('worked_with', ['id' => $user->id]) }}"><b>Worked with -</b> {{ \App\WorksWith::countWorked($user->id) }}</a>
                </div>
                <div class="b_block_inf">
                    <a href="{{ route('subscribed-groups') }}"><b>Groups -</b> {{ \App\GroupFollower::getCountFollow($user->id) }}</a>
                </div>
                @if (Auth::check() and $user->id == Auth::user()->id)
                    <div class="b_block_inf">
                        <a href="{{ url('/profile/news-and-events') }}"><b>Your Posts -</b> {{ \App\Post::countPosts($user->id) }}</a>
                    </div>
                    <div class="b_block_inf">
                        <a href="{{ url('/profile/draft') }}"><b>Your Draft -</b> {{ \App\Post::countPosts($user->id, true) }}</a>
                    </div>
                    @if (\App\PurchasedVideo::countPurchase())
                        <div class="b_block_inf">
                            <a href="{{ route('purchased_videos') }}"><b>Purchased Videos -</b> {{ \App\PurchasedVideo::countPurchase() }}</a>
                        </div>
                    @endif
                @else
                    <div class="b_block_inf">
                        <a href="{{ url('/profile/id/'.$user->id.'/news-and-events') }}"><b>Posts -</b> {{ \App\Post::countPosts($user->id) }}</a>
                    </div>
                @endif
            @else
                <div class="b_block_inf">
                    <a href="@if(Auth::check() and $user->id != Auth::user()->id){{ route('connections_id', ['id' => $user->id]) }}@else{{ route('connections') }}@endif"><b>Connections -</b> {{ \App\ConnectionRequest::countConnection() }}</a>
                </div>
                <div class="b_block_inf">
                    <a href="{{ route('subscribed-groups') }}"><b>Groups -</b> {{ \App\GroupFollower::getCountFollow() }}</a>
                </div>
            @endif
            @if (Auth::check() and $user->id == Auth::user()->id)
                <div class="b_block_inf {{ \Request::route()->getName() == 'going_post' ? 'menu-sidebar-active' : '' }}">
                    <a href="{{ route('going_post') }}"><b>Going -</b> {{ $user->going_count() }}</a>
                </div>
                <div class="b_block_inf {{ \Request::route()->getName() == 'interested_post' ? 'menu-sidebar-active' : '' }}">
                    <a href="{{ route('interested_post') }}"><b>Interested -</b> {{ $user->interested_count() }}</a>
                </div>
            @endif
        </div>
        @if ($user->business)
            <div class="m_container_soc">
                <div class="m_block_soc">
                    <img src="{{ asset('img/inf_ic_1.jpg') }}" alt="Alternate Text">
                    <a href="{{ route('profile_photos', ['id' => $user->id]) }}"><b>Photos -</b> {{ \App\Gallery::getCountPhotosUser($user->id) }}</a>
                </div>
                <div class="m_block_soc">
                    <img src="{{ asset('img/inf_ic_2.jpg') }}" alt="Alternate Text">
                    <a href="{{ route('profile_videos', ['id' => $user->id]) }}"><b>Videos -</b> {{ \App\Video::getCountVideos($user->id) }}</a>
                </div>
                <div class="m_soc_ic">
                    @if (!is_null($user->facebook_link))
                        <a href="{{ $user->facebook_link }}" target="_blank">
                            <img src="{{ asset('img/soc_ic_1.svg') }}" alt="Alternate Text">
                        </a>
                    @endif
                    @if (!is_null($user->twitter_link))
                        <a href="{{ $user->twitter_link }}" target="_blank">
                            <img src="{{ asset('img/soc_ic_2.svg') }}" alt="Alternate Text">
                        </a>
                    @endif
                    @if (!is_null($user->instagram_link))
                        <a href="{{ $user->instagram_link }}" target="_blank">
                            <img src="{{ asset('img/soc_ic_3.svg') }}" alt="Alternate Text">
                        </a>
                    @endif
                </div>
            </div>
        @endif
        <a href="{{ route('logout') }}" class="logout-link">Logout of Account</a>
    @else
        <div class="m_l_top">
            <form class="login-form" method="post" action="{{ route('login') }}">
                {{ csrf_field() }}
                <input type="email" placeholder="E-mail" required name="email">
                <input type="password" placeholder="Password" required name="password">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                @if (!is_null(\Request::input('error')) and \Request::input('error') == 'blocked')
                    <span class="help-block">
                        <strong>Your account has been blocked.</strong>
                    </span>
                @endif
                <button type="submit" class="btn btn-primary">Login</button>
                <a href="{{ route('create-account') }}">Create account</a>
                <a href="#" class="login_mobile_popup_close">Close</a>

                @if(session('success'))
                    <span class="help-block">
                        <strong>{{session('success')}}</strong>
                    </span>
                @else
                    <span class="help-block">
                            <strong>You need to register to see the content on this website.</strong>
                    </span>
                @endif
            </form>
        </div>
    @endif
</div>