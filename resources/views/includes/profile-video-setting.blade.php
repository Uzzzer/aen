<div class="item" data-id="{{ $Video->id }}">
    <div class="video-wraper">
        <img src="{{ asset($Video->preview_image) }}" alt="{{ $Video->title }}">
        <div class="video-bottom-block">
            <div class="left-box">
                <i class="v-icon play"></i>
                <div class="video-bottom-text">
                    <span class="title">{{ $Video->title }}</span>
                    <p class="lite-text">Run Time: {{ \App\Video::convertTime($Video->time) }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="video-information-wrapper" style="display: none;">
        <div class="video-bottom-footer">
            <a>Likes: &nbsp;<span>{{ \App\ProfileVideoLike::countLike($Video->id) }}</a>
            <a href="#" class="delete_video" data-id="{{ $Video->id }}">Delete</a>
        </div>
    </div>
</div>