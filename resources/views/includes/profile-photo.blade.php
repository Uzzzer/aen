<div class="item" data-id="{{ $photo->id }}">
    <div class="photo-wraper">
        <img src="{{ asset($photo->file_name) }}" alt="Alternate Text">
        <div class="photo-bottom-block">
            <span class="title">{{ $photo->title }}</span>
        </div>
    </div>
    <div class="photo-information-wrapper">
        @php
            $count_likes = \App\PhotoLike::countLike($photo->id);
            $count_comments = \App\MediaComment::where('photo_id', $photo->id)->count();
        @endphp
        <div class="photo-bottom-footer">
            <a href="#" class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_photo' }} @if (\App\PhotoLike::checkLike($photo->id)) liked @endif" data-id="{{ $photo->id }}"><i class="v-icon like"></i>Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></a>
            <a href="{{ route('profile_photo', ['id' => $user->id, 'pid' => $photo->id]) }}" class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_photo' }}"><i class="v-icon comment"></i>Comment @if($count_comments > 0){{ '('.$count_comments.')' }}@endif</a>
            <a href="#"><i class="v-icon share"></i>Share</a>
        </div>
        <div class="video-item-information">
            <div class="views">{{ \App\MediaView::countViewsPhoto($photo->id) }}</div>
            <div class="video-item-information-box">
                <p>From: <a href="{{ route('id_profile', ['id' => $photo->user_id]) }}">{{ $user->name }}</a></p>
                <p>Added on: {{ date('jS M Y', strtotime($photo->created_at)) }}</p>
            </div>
        </div>
    </div>
</div>