<div class="sidebar sidebar_bisiness">
    <div class="m_l_top">
        <a href="{{ route('id_profile', ['id' => $user->id]) }}" class="bl_person_name">
            {{ $user->name }}
        </a>
        <div class="bl_person_photo">
            <img src="{{ asset($user->avatar) }}" alt="Alternate Text">
        </div>
        @if ((Auth::check() and $user->id != Auth::user()->id) or !Auth::check())
            <a class="btn_follow {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'connection_request' }} @if(\App\ConnectionRequest::CheckConnection($user->id)) btn_disabled @endif"
               data-id="{{ $user->id }}" href="#">Follow
            </a>
        @endif
        <div class="bl_person_inf">
            <div>Location: {{ $user->location }}</div>
            <div>Operating since: 03/07/78</div>
        </div>
    </div>
    <div class="m_container_inf">
        <div class="b_block_inf">
            <a href="@if(Auth::check() and $user->id != Auth::user()->id){{ route('connections_id', ['id' => $user->id]) }}@else{{ route('connections') }}@endif"><b>Fans -</b> {{ \App\ConnectionRequest::countConnection($user->id) }}</a>
        </div>
        {{--<div class="b_block_inf">
            <a href="#"><b>Associates -</b> 14,657</a>
        </div>--}}
        <div class="b_block_inf">
            <a href="{{ route('worked_with', ['id' => $user->id]) }}"><b>Worked with -</b> {{ \App\WorksWith::countWorked($user->id) }}</a>
        </div>
        <div class="b_block_inf">
            <a href="{{ route('subscribed-groups') }}"><b>Groups -</b> {{ \App\GroupFollower::getCountFollow($user->id) }}</a>
        </div>
        @if (Auth::check() and $user->id == Auth::user()->id)
            <div class="b_block_inf">
                <a href="{{ url('/profile/news-and-events') }}"><b>Your Posts -</b> {{ \App\Post::countPosts($user->id) }}</a>
            </div>

            <div class="b_block_inf">
                <a href="{{ url('/profile/draft') }}"><b>Your Draft -</b> {{ \App\Post::countPosts($user->id, true) }}</a>
            </div>
        @else
            <div class="b_block_inf">
                <a href="{{ url('/profile/id/'.$user->id.'/news-and-events') }}"><b>Posts -</b> {{ \App\Post::countPosts($user->id) }}</a>
            </div>
        @endif
    </div>
    <div class="m_container_soc">
        @if (Auth::check() and $user->id == Auth::user()->id and $user->business == 1 and in_array($user->user_category_id, [8,9,10]))
            <a href="{{ route('profile-editing', ['tab' => 'job-alerts']) }}" class="m_block_soc">
                <img src="{{ asset('img/right_bar_ic_1.svg') }}" alt="Alternate Text">
                Post a job
            </a>
        @endif
        @if ($user->id != \Auth::user()->id)
            @php
                $conversation = Chat::conversations()->between(Auth::user()->id, $user->id);
            @endphp
        @endif
        <a href="#" class="m_block_soc OpenChat" @if (Auth::user()->id != $user->id) data-id="{{ $user->id }}"
           data-name="{{ $user->name }}"
           @endif @if (isset($conversation) and $conversation != NULL) data-conversation_id="{{ $conversation->id }}" @endif>
            <img src="{{ asset('img/right_bar_ic_2.svg') }}" alt="Alternate Text">
            Message
        </a>
        <a href="{{ route('calendar', ['id' => $user->id]) }}" class="m_block_soc">
            <img src="{{ asset('img/right_bar_ic_3.svg') }}" alt="Alternate Text">
            Calendar
        </a>
        <div class="m_soc_ic">
            @if (!is_null($user->facebook_link))
                <a href="{{ $user->facebook_link }}" target="_blank">
                    <img src="{{ asset('img/soc_ic_1.svg') }}" alt="Alternate Text">
                </a>
            @endif
            @if (!is_null($user->twitter_link))
                <a href="{{ $user->twitter_link }}" target="_blank">
                    <img src="{{ asset('img/soc_ic_2.svg') }}" alt="Alternate Text">
                </a>
            @endif
            @if (!is_null($user->instagram_link))
                <a href="{{ $user->instagram_link }}" target="_blank">
                    <img src="{{ asset('img/soc_ic_3.svg') }}" alt="Alternate Text">
                </a>
            @endif
        </div>
    </div>
    <a href="{{ route('logout') }}" class="logout-link">Logout of Account</a>
</div>