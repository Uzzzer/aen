<div class="right_container photo_box" data-id="{{ $Photo->id }}">
    <div class="m_r_top">
        <div class="m_rl_top">
            <div class="ph_person">
                <a href="{{ url('/profile/id/'.$Photo->user_id) }}">
                    <img src="{{ asset($Photo->user_avatar) }}" alt="Alternate Text"/>
                </a>
            </div>
            <div class="person_text">
                <p>
                                            <span>
                                                <a href="{{ url('/profile/id/'.$Photo->user_id) }}"
                                                   class="user-name-link">{{ $Photo->user_name }}</a>,
                                            </span>
                    {{ $Photo->title }} - {{ \App\DateConvert::Convert($Photo->created_at) }}
                </p>
            </div>
        </div>
        <div class="m_rr_top">
            @php
                $PhotoLike = NULL;
                if (Auth::check())
                    $PhotoLike = \App\PhotoLike::where('photo_id', $Photo->id)->where('user_id', Auth::user()->id)->first();
            @endphp
            <a href="#"
               class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_photo' }} @if ($PhotoLike != NULL) liked @endif"
               data-id="{{ $Photo->id }}">
                <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                <span>Like</span>
            </a>
            <a href="{{ route('profile_photo', ['id' => $Photo->user_id, 'pid' => $Photo->id]) }}"
               data-id="{{ $Photo->id }}">
                <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                <span>Comment</span>
            </a>
        </div>
    </div>
    <div class="m_bg_container"
         style="background-image: url({{ asset($Photo->file_name) }});">
        <img src="{{ asset($Photo->file_name) }}" alt="">
    </div>
    <div class="m_bot_container">
        <div class="m_data">
            <i class="fa fa-picture-o" aria-hidden="true"
               style="font-size: 24px; margin-right: 15px;"></i>
            {{ $Photo->title }}
        </div>
        <div class="m-bot-text" @if (empty($Photo->description)) style=" border-top: 0px; padding: 0px; " @endif>
            {!! $Photo->description !!}
        </div>
        <div class="coment_container">
            <div class="like_conent">
                @php
                    $count_likes = \App\PhotoLike::where('photo_id', $Photo->id)->count();
                    $count_comments = \App\MediaComment::where('photo_id', $Photo->id)->count();
                @endphp
                <div class="like_left {{ !$count_likes ? 'hidden-post-block' : '' }}">
                    <div class="like">
                        <img src="{{ asset('img/like_c.svg') }}" alt="Alternate Text"/>
                    </div>
                    <div class="like_text count_like">
                        <span>{{ $count_likes }}</span>
                        likes
                    </div>
                </div>
                <a href="{{ route('profile_photo', ['id' => $Photo->user_id, 'pid' => $Photo->id]) }}">
                    <div data-id="{{ $Photo->id }}" style=" cursor: pointer; "
                         class="like_right {{ !$count_comments ? 'hidden-post-block' : '' }}">
                        <span>{{ $count_comments }}</span>
                        Comments
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>