@php
    $PostCategories = \App\PostCategory::get();
@endphp

<div id="UploadImageBox">
    <img src=""/>
    <input type="button" id="CancelCropp" value="Cancel">
    <input type="button" id="ApplyCropp" value="Apply">
</div>

<div class="post-bar new-post-bar" style="margin-top: 0px;">
    <form id="NewPost">
        <div class="post-bar-heading">
            <select class="post-bar-select" name="PostType">
                <option value="News" selected>News / Update</option>
                <option value="Event">Event</option>
                <option value="Online">Schedule Online</option>
            </select>
            <a href="#" class="photo-album">Photo Album</a>
            <a href="#" class="video-album">Video</a>
        </div>
        <div class="post-bar-content">
            <h3>Basic info</h3>
            <div class="create-line" style=" display: block; ">
                <b class="label">Photo or Video</b>
                <div class="upload-section">
                    <img src="{{ asset('img/upload-bg.jpg') }}" alt="" id="upload-bg" class="image-post">
                    <input type="file" id="upload-file" class="image-post">
                    <input type="hidden" name="file_type" class="image-post">
                    <input type="hidden" name="file_name" class="image-post">
                    <input type="hidden" name="preview_video" class="image-post">
                </div>
            </div>
            <div class="create-line" style=" display: block; ">
                <b class="label">Title</b>
                <input type="text" name="PostTitle">
            </div>
            <div class="create-line" style=" display: block; ">
                <b class="label">Subtitle</b>
                <input type="text" name="PostSubtitle">
            </div>
            <div class="create-line event-type-hidden online-type-hidden" style=" display: none; ">
                <b class="label">Location</b>
                <input type="text" name="Location">
            </div>
            <div class="create-line event-type-hidden online-type-hidden" style=" display: none; ">
                <b class="label">Frequency</b>
                <select name="Frequency">
                    <option value="Occurs Once">Occurs Once</option>
                    <option value="Occurs Twice">Occurs Twice</option>
                    <option value="Occurs 3-5 times">Occurs 3-5 times</option>
                    <option value="More...">More...</option>
                </select>
            </div>
            <div class="create-line event-type-hidden online-type-hidden" style=" display: none; ">
                <b class="label">Start</b>
                <input type="date" name="StartEvent">
            </div>
            <div class="create-line event-type-hidden online-type-hidden" style=" display: none; ">
                <b class="label">End</b>
                <input type="date" name="EndEvent">
            </div>
            <div class="create-line online-type-visible" style=" display: none; ">
                <b class="label" style=" display: block; ">Start</b>
                <input type="time" name="StartTime" style=" width: 25%; display: inline-block; ">
                <input type="date" name="Start" style=" width: 25%; display: inline-block; ">
            </div>
            <div class="create-line online-type-visible" style=" display: none; ">
                <b class="label" style=" display: block; ">End</b>
                <input type="time" name="EndTime" style=" width: 25%; display: inline-block; ">
                <input type="date" name="End" style=" width: 25%; display: inline-block; ">
            </div>
            <h3>Details</h3>
            <p class="online-type-hidden">Let people know what type of event you are hosting and what to expect</p>
            <div class="create-line online-type-hidden" style=" display: none; ">
                <b class="label">Category</b>
                <select name="PostCategory">
                    @foreach ($PostCategories as $PostCategory)
                    <option value="{{ $PostCategory->id }}">{{ $PostCategory->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="create-line" style=" display: block; ">
                <b class="label">Description</b>
                <textarea name="Description" cols="30" rows="5"></textarea>
            </div>
            <div class="create-line" style=" display: block; ">
                <b class="label">Keywords</b>
                <input type="text" name="Keywords">
            </div>
        </div>
        <div class="post-bar-footer">
            <input type="button" class="post-button" id="save_draft" value="Save draft" style="margin-right: 10px;">
            <input type="submit" class="post-button" value="Post">
        </div>
        {{ csrf_field() }}
    </form>
</div>

<style>
    .online-type-visible {
        display: none;
    }
</style>