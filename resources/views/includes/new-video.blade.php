<div class="post-bar video-post-add">
    <form id="NewVideo">
        <div class="post-bar-content">
            <h3>Basic info</h3>
            <div class="create-line">
                <b class="label">Video</b>
                <div class="upload-section">
                    <img src="{{ asset('img/upload-bg.jpg') }}" alt="" class="image-video">
                    <input type="file" class="image-video">
                    <input type="hidden" name="file_type" class="image-video">
                    <input type="hidden" name="file_name" class="image-video">
                </div>
            </div>
            <div class="create-line">
                <b class="label">Title</b>
                <input type="text" name="Title">
            </div>
            <h3>Details</h3>
            <div class="create-line">
                <b class="label">Content</b>
                @php
                    $CategoriesAvailable = \App\CategoriesAvailable::get();
                @endphp
                <div class="avaliable_checkbox" style="display: block;">
                    @foreach ($CategoriesAvailable as $CategoryAvailable)
                        <label class="avaliable_label">
    						<span class="switch">
    							  <input type="checkbox" name="Content[]" value="{{ $CategoryAvailable->id }}">
    						   	  <span class="checkbox_block"></span>
    						</span>
                            <span class="checkbox_name">{{ $CategoryAvailable->name }}</span>
                        </label>
                    @endforeach
                </div>
            </div>
            <div class="create-line">
                <b class="label">Tags</b>
                <input type="text" name="Tags">
            </div>
            <div class="create-line">
                <b class="label">Price</b>
                <input type="number" style="display: block; width: 50%; border: 1px solid #fff; color: #fff; font-size: 14px; padding: 18px 14px;" step="0.01" name="Price">
            </div>
        </div>
        <div class="post-bar-footer">
            <input type="submit" class="post-button" value="Add video">
        </div>
        {{ csrf_field() }}
    </form>
</div>