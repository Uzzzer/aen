<div class="item{{ \App\PurchasedVideo::checkPurchase($Video->id) ? ' paid' : '' }}" data-id="{{ $Video->id }}">
    <div class="video-wraper">
        <img src="{{ asset($Video->preview_image) }}" alt="{{ $Video->title }}">
        <div class="play-btn-wrapper">
            <a href="#"><i class="v-icon play-red"></i></a>
        </div>
        <div class="video-bottom-block">
            <div class="left-box">
                <i class="v-icon play"></i>
                <div class="video-bottom-text">
                    <span class="title">{{ $Video->title }}</span>
                    <p class="lite-text">Run Time: {{ \App\Video::convertTime($Video->time) }}</p>
                </div>
            </div>
            <a href="@if(\App\PurchasedVideo::checkPurchase($Video->id)){{ route('profile_video', ['id' => $Video->id]) }}@else{{ route('payment', ['video' => $Video->id]) }}@endif" class="video-btn" style="animation-name: fadeInRight;">@if(\App\PurchasedVideo::checkPurchase($Video->id)){{ 'Watch NOW' }}@else{{ 'Buy NOW' }}@endif</a>
        </div>
        <div class="overlay-block">
            <div class="price-box">
                <span class="price">{{ \App\Option::option('currency') }}{{ $Video->price }}</span>
                <span></span>
            </div>
            <div class="circle">
                <img class="check-mark-icon" src="{{ asset('img/check-mark-icon.svg') }}" alt="Icon">
                PURCHASE<br>CONFIRMED
            </div>
        </div>
    </div>
    <div class="video-information-wrapper" style="display: none;">
        @php
            $count_likes = \App\ProfileVideoLike::countLike($Video->id);
            $count_comments = \App\ProfileVideoComment::where('video_id', $Video->id)->count();
        @endphp
        <div class="video-bottom-footer">
            <a href="#" class="like_video @if (\App\ProfileVideoLike::checkLike($Video->id)) liked @endif" data-id="{{ $Video->id }}"><i class="v-icon like"></i>Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></a>
            <a href="{{ route('profile_video', ['id' => $Video->id]) }}" class="@if(!\App\PurchasedVideo::checkPurchase($Video->id)){{ 'must_buy_video' }}@endif"><i class="v-icon comment"></i>Comment ({{ $count_comments }})</a>
            <a href="#"><i class="v-icon share"></i>Share</a>
        </div>
        <div class="video-item-information">
            <div class="views">{{ \App\ProfileVideoView::videoViewsVideo($Video->id) }}</div>
            <div class="video-item-information-box">
                <p>From: <a href="{{ route('id_profile', ['id' => $user->id]) }}">{{ $user->name }}</a> - {{ \App\Video::getCountVideos($user->id) }} videos</p>
                <p>
                    @php
                        $Contents = json_decode($Video->content);
                    @endphp
                    Content:
                    @foreach ($Contents as $key => $content)
                        @php
                            $content = \App\CategoriesAvailable::find($content);
                        @endphp
                        @if (!is_null($content))
                            @if($key), @endif<a href="#">{{ $content->name }}</a>
                        @endif
                    @endforeach
                </p>
                <p>
                    @php
                        $Tags = \App\VideoTag::where('video_id', $Video->id)->get();
                    @endphp
                    @if (count($Tags))
                        Tags:
                        @foreach ($Tags as $key => $tag)
                            @if($key), @endif<a href="#">{{ $tag->tag }}</a>
                        @endforeach
                    @endif
                </p>
                <p>Added on: {{ date('jS M Y', strtotime($Video->created_at)) }}</p>
            </div>
        </div>
    </div>
</div>