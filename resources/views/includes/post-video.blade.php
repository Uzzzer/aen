<div class="right_container video_box" data-id="{{ $Video->id }}">
    <div class="m_r_top">
        <div class="m_rl_top">
            <div class="ph_person">
                <a href="{{ url('/profile/id/'.$Video->user_id) }}">
                    <img src="{{ asset($Video->user_avatar) }}" alt="Alternate Text"/>
                </a>
            </div>
            <div class="person_text">
                <p>
                                            <span>
                                                <a href="{{ url('/profile/id/'.$Video->user_id) }}"
                                                   class="user-name-link">{{ $Video->user_name }}</a>,
                                            </span>
                    {{ $Video->title }} - {{ \App\DateConvert::Convert($Video->created_at) }}
                </p>
            </div>
        </div>
        <div class="m_rr_top">
            @php
                $VideoLike = NULL;
                if (Auth::check())
                    $VideoLike = \App\ProfileVideoLike::where('video_id', $Video->id)->where('user_id', Auth::user()->id)->first();
            @endphp
            <a href="#" class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_video' }} @if ($VideoLike != NULL) liked @endif"
               data-id="{{ $Video->id }}">
                <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                <span>Like</span>
            </a>
            <a href="{{ route('profile_video', ['id' => $Video->user_id]) }}"
               data-id="{{ $Video->id }}" class="@if(!\App\PurchasedVideo::checkPurchase($Video->id)){{ 'must_buy_video' }}@endif">
                <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                <span>Comment</span>
            </a>
        </div>
    </div>
    <div class="m_bg_container">
        <div class="video-wraper">
            @if (\App\PurchasedVideo::checkPurchase($Video->id))
                <video width="100%" height="190px" controls {!!  $Video->preview_image ? 'poster="' . asset($Video->preview_image) . '"' : '' !!}>
                    <source src="{{ asset($Video->file_name) }}">
                    Your browser does not support the video tag.
                </video>
            @else
                <img src="{{ asset($Video->preview_image) }}" alt="{{ $Video->title }}">
            @endif
            <div class="play-btn-wrapper">
                <a href="#">
                    <i class="v-icon play-red"></i>
                </a>
            </div>
            <div class="video-bottom-block">
                <div class="left-box">
                    <i class="v-icon play"></i>
                    <div class="video-bottom-text">
                        <p class="lite-text">Run Time: {{ \App\Video::convertTime($Video->time) }}</p>
                    </div>
                </div>
                <a href="@if(\App\PurchasedVideo::checkPurchase($Video->id)){{ route('profile_video', ['id' => $Video->id]) }}@else{{ route('payment', ['video' => $Video->id]) }}@endif"
                   class="video-btn"
                   style="animation-name: fadeInRight;">@if(\App\PurchasedVideo::checkPurchase($Video->id)){{ 'Watch NOW' }}@else{{ 'Buy NOW' }}@endif</a>
            </div>
            <div class="overlay-block">
                <div class="price-box">
                    <span class="price">{{ \App\Option::option('currency') }}{{ $Video->price }}</span>
                    <span></span>
                </div>
                <div class="circle">
                    <img class="check-mark-icon" src="{{ asset('img/check-mark-icon.svg') }}"
                         alt="Icon">
                    PURCHASE
                    <br>
                    CONFIRMED
                </div>
            </div>
        </div>
    </div>
    <div class="m_bot_container">
        <div class="m_data">
            <i class="fa fa-video-camera" aria-hidden="true"
               style="font-size: 24px; margin-right: 15px;"></i>
            {{ $Video->title }}
        </div>
        <div class="coment_container">
            <div class="like_conent">
                @php
                    $count_likes = \App\ProfileVideoLike::where('video_id', $Video->id)->count();
                    $count_comments = \App\ProfileVideoComment::where('video_id', $Video->id)->count();
                @endphp
                <div class="like_left {{ !$count_likes ? 'hidden-post-block' : '' }}">
                    <div class="like">
                        <img src="{{ asset('img/like_c.svg') }}" alt="Alternate Text"/>
                    </div>
                    <div class="like_text count_like">
                        <span>{{ $count_likes }}</span>
                        likes
                    </div>
                </div>
                <a href="{{ route('profile_video', ['id' => $Video->id]) }}"
                   class="@if(!\App\PurchasedVideo::checkPurchase($Video->id)){{ 'must_buy_video' }}@endif">
                    <div data-id="{{ $Video->id }}" style=" cursor: pointer; "
                         class="like_right {{ !$count_comments ? 'hidden-post-block' : '' }}">
                        <span>{{ $count_comments }}</span>
                        Comments
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>