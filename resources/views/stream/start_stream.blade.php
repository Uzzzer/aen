@extends('layouts.stream')

@section('content')
    <section id="stream" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="stream_box">
                    {{ csrf_field() }}
                    <div id="stream_video"></div>

                    <button id="StartStream" type="button">Start stream</button>
                </div>
                @include('includes.right-sidebar-business')
            </div>
        </div>
    </section>

    <script>
        var user_id = {{ \Auth::user()->id }};
    </script>
@endsection