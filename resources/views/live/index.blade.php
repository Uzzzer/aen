@extends('layouts.layout', ['left_sidebar' => false])

@section('content')
    <style>
        /* .stream-items {
            background: #fff;
            padding: 10px;
        }

        .stream-item-box {
            border: 1px solid gray;
            display: inline-block;
            width: 25%;
        }

        .stream-item-box span {
            padding: 5px 10px;
            display: block;
            border-bottom: 1px solid gray;
            margin-bottom: 10px;
        } */
    </style>
    @if (Auth::user()->business == 1)
        <a href="{{ route('start-live') }}" class="start-streem-btn">Start stream</a>
    @endif
    <div class="stream-items">
        @foreach($streams as $stream)
            <div class="stream-item-box">
                <a href="{{ route('view-stream', $stream->user_id) }}">
                    <img src="{{ $stream->user->avatar }}" class="stream-promo-image">
                    <div class="stream-item-info">
                        <div class="stream-item-info-line">
                            <div class="stream-user-name">
                                {{ $stream->user->name }}
                            </div>
                            <div class="stream-user-origin">
                                <span>{{ $stream->user->location }}</span>
                                <i class="stream-user-icon stream-user-icon-{{strtolower($stream->user->sex)}}" title="{{ $stream->user->sex }}"></i>
                            </div>
                        </div>
                        <div class="stream-item-info-line bottom-line">
                            <div class="stream-type party-chat">Party chat</div>
                            <div class="bottom-label hd-label">HD</div>
                        </div>
                    </div> 
                </a> 
            </div>
        @endforeach
    </div>
@endsection

@section('custom_js')
    <script>
        var UserID = {{ auth()->user()->id }};
        var apiToken = '{{ \Auth::user()->api_token }}';
    </script>
@endsection