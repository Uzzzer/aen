@extends('layouts.layout', ['left_sidebar' => false])

@section('content')
    <div class="post_load_list">
        <div class="right_container post_box">
            <div class="m_r_top">
                <div class="m_rl_top">
                    <div class="ph_person">
                        <a href="{{ route('id_profile', ['id' => $user->id]) }}">
                            <img src="{{ asset($user->avatar) }}">
                        </a>
                    </div>
                    <div class="person_text">
                        <p>
                            {{ $user->name }}
                        </p>
                    </div>
                </div>
                <div class="m_rr_top">

                </div>
            </div>
            <div class="m_bot_container stream-container">
                <div class="live-box">
                    {{ csrf_field() }}
                    <div class="stream-box">
                        <div id="stream_video"></div>
                        <div class="stream-live-info">
                            <div id="stream-info" class="stream-viewer">Viewer: <span id="here-now">0</span></div>
                            <input type="button" id="stopStream" value="Stop Stream" class="stop-stream-btn">
                            <input type="button" id="muteStream" value="Mute microphone" class="mute-strea-btn">
                        </div>
                    </div>
                    <div class="chat-box">
                        <div id="chatBox">

                        </div>
                        <input type="text" id="chatInput" placeholder="Write a message">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        var UserID = {{ auth()->user()->id }};
        var apiToken = '{{ \Auth::user()->api_token }}';
    </script>
    <script src="{{ asset('/js/live.js') }}"></script>
    <script src="{{ asset('/js/rtc-controller.js') }}"></script>
@endsection