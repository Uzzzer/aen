<div class="dis-post-item coment_block {{ !is_null($Comment->reply_id) ? 'reply-comment-block' : '' }}"
     data-id="{{ $Comment->id }}">
    <div class="dis-top-panel">
        <div class="img-wrap">
            <a href="{{ url('/profile/id/'.$Comment->user_id) }}">
                <img src="{{ asset($Comment->user()->avatar) }}"
                     alt="{{ $Comment->user()->name }}"
                     title="{{ $Comment->user()->name }}">
            </a>
        </div>
        <div class="dis-text-box">
            <span><a href="{{ url('/profile/id/'.$Comment->user_id) }}">{{ $Comment->user()->name }}</a> - {{ \App\DateConvert::Convert($Comment->created_at) }}</span>
            <!-- <span>You, Ashley Murray, Dirk Diggler &amp; 12 others Commented</span> -->
        </div>
    </div>
    <div class="dis-content">
        <p style="{{ empty($Comment->comment) ? 'margin-bottom: 0px;' : '' }}">
            {{ $Comment->comment }}
        </p>
        @if ($Comment->image != NULL)
            <a href="{{ asset($Comment->image) }}"
               data-lightbox="image-{{ $Comment->id }}">
                <img src="{{ asset($Comment->image) }}"/>
            </a>
        @endif
    </div>
    <div class="dis-bottom-panel">
        @php
            $PostCommentLike = NULL;
            if (Auth::check())
                $PostCommentLike = \App\PostCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
            $count_likes = \App\PostCommentLike::where('comment_id', $Comment->id)->count();
        @endphp
        <a href="#"
           class="dis-action like_comment @if($PostCommentLike != NULL) liked @endif"
           @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>
            <img src="{{ is_null($PostCommentLike) ? '/img/dis-like-icon.svg' : '/img/full-like-icon-like.svg' }}"
                 alt="">
            <span style=" padding-right: 5px; ">Like</span>
            <span>@if($count_likes > 0){{ $count_likes }}@endif</span>
        </a>
        @if (is_null($Comment->reply_id))
            <a href="#" class="dis-action" class="reply_comment"
               @if(\Auth::check()) data-id="{{ $Comment->id }}"
               data-post-id="{{ $Post->id }}" @endif>
                <img src="/img/dis-com-icon.svg" alt="">
                <span>Reply</span>
            </a>
        @endif
    </div>
</div>