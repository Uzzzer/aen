@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="profile-content" style="width: calc(82% - 30px);">
                    <h2>Payment <span style=" color: #d0021b; ">{{ $price.' '.\App\Option::option('currency') }}</span></h2>
                    <h3 style=" color: #fff; font-size: 16px; ">{{ $description }}</h3>
                    <form id="PaymentForm">
                        {{ csrf_field() }}
                        <input type="hidden" name="product" value="{{ $product }}">
                        <div class="payment box-card">
                            <div class="form-group-card">
                                <label form="card_number">Card Number</label>
                                <input type="text" id="card_number" readonly onfocus="this.removeAttribute('readonly');" name="card_number" class="number_card" placeholder="4149 4393 9672 9216">
                                <img id="cardlabel" src="{{ asset('img/visa.svg') }}" alt="visa" class="cardlabel">
                            </div>
                            <div class="form-group-card">
                                <label for="card_exp">Exp. Date</label>
                                <input type="text" id="card_exp" readonly onfocus="this.removeAttribute('readonly');" name="card_exp" class="date_card" placeholder="09/21">
                            </div>
                            <div class="form-group-card">
                                <label for="card_cvv">CVV</label>
                                <input type="password" id="card_cvv" readonly onfocus="this.removeAttribute('readonly');" name="card_cvv" class="cvv_card" placeholder="***">
                            </div>
                            <div class="form-group-card">
                                <label for="cardholderName">Holder Name</label>
                                <input type="text" id="cardholderName" readonly onfocus="this.removeAttribute('readonly');" name="cardholderName" class="cardholderName" placeholder="">
                            </div>
                            <div class="form-group-card">
                                <a href="{{ $paypal_link }}"><button type="button" class="btn white_grey_btn pay_pal_btn">@php(include("img/paypal_logo.svg")) Pay by PayPal</button></a>
                                <div class="btn_follow" id="PaymentSubmit" style="margin-top: 0px">Pay</div>
                            </div>
                        </div>
                    </form>
                </div>
                @include('includes.right-sidebar')
            </div>
        </div>
    </section>

    <script src="https://js.braintreegateway.com/web/dropin/1.15.0/js/dropin.min.js"></script>

@endsection