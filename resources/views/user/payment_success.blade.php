@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="profile-content" style="width: calc(82% - 30px);">
                    <h2>Payment Success</h2>
                </div>
                @include('includes.right-sidebar')
            </div>
        </div>
    </section>

    <script>
        setTimeout(function () {
            window.location.href = '{{ $redirect_url }}'
        }, 2000)
    </script>
    <script src="https://js.braintreegateway.com/web/dropin/1.15.0/js/dropin.min.js"></script>
@endsection