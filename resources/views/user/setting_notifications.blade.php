@extends('layouts.layout')

@section('content')

<div class="avaliable_checkbox" style="display: block;">
    <form id="NotificationSettings">
        {{ csrf_field() }}
        @foreach ($NotificationTypes as $NotificationType)
            <label class="avaliable_label">
                <span class="switch">
                    <input type="checkbox" value="{{ $NotificationType->id }}" name="NotificationType[]" @if (in_array($NotificationType->id, $NotificationOn)) checked @endif>
                    <span class="checkbox_block"></span>
                </span>
                <span class="checkbox_name">{{ $NotificationType->name_view }}</span>
            </label>
        @endforeach
        <div class="btn_follow" id='NotificationSettingsSubmit' style=" margin-bottom: 15px; ">Save changes</div>
    </form>
</div>
@endsection