@extends('layouts.profile')

@section('content')
    <div class="modal_upload">
        <div class="close_modal_up">×</div>
        <div class="upload_block">
            <label for="file" class="label-file-img">
                <img class="upload-icon" src="https://image.flaticon.com/icons/svg/149/149185.svg">
                <div class="image-wrapper">
                    <img src>
                </div>
            </label>
            <h1>Upload file (jpeg, < 4Mo)</h1>
            <p class="file-name"></p>
            <input id="file" type="file" class="input-file">
            <div class="horizontal-line"></div>
            <div class="under-block">
                <input type="submit" value="Upload" class="input-submit">
            </div>
        </div>
    </div>
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">
                    <div class="gallary_top">
                        <div class="gal_top_left"><span>@if(!$user->business){{ 'Connections'}}@else{{ 'Fans' }}@endif</span></div>
                    </div>
                    <div class="grops-grid">

                        @forelse ($Connections as $Connection)
                            <div class="groups-item follow-item" data-id="{{ $Connection->id }}">
                                <a href="{{ route('id_profile', ['id' => $Connection->user_id] ) }}" class="groups-item-title">
                                    <img src="{{ asset($Connection->user_avatar) }}" alt="">
                                </a>
                                <div class="groups-item-content">
                                    <a href="{{ route('id_profile', ['id' => $Connection->user_id] ) }}" class="groups-item-title">
                                        <b>{{ $Connection->user_name }}</b>
                                        <span>{{ \App\ConnectionRequest::countConnection($Connection->user_id, true) }}</span>
                                        @if (Auth::check() and $user->id == Auth::user()->id)
                                            <a href="#" class="groups-item-wheel groups_follow">Actions</a>
                                            <ul class="groups-item-options">
                                                <li><a href="#">Get Notifications</a></li>
                                                <li><a href="#">Suggest Connections</a></li>
                                                <li><a href="#" class="remove_connection" data-id="{{ $Connection->id }}">Remove Connection</a></li>
                                            </ul>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        @empty
                            <p>No connections</p>
                        @endforelse
                    </div>
                </div>
                @include('includes.right-sidebar')
            </div>


        </div>
        </div>
    </section>
@endsection