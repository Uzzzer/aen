@extends('layouts.profile')

@section('content')
    <div class="modal_upload">
        <div class="close_modal_up">×</div>
        <div class="upload_block">
            <label for="file" class="label-file-img">
                <img class="upload-icon" src="https://image.flaticon.com/icons/svg/149/149185.svg">
                <div class="image-wrapper">
                    <img src>
                </div>
            </label>
            <h1>Upload file (jpeg, < 4Mo)</h1>
            <p class="file-name"></p>
            <input id="file" type="file" class="input-file">
            <div class="horizontal-line"></div>
            <div class="under-block">
                <input type="submit" value="Upload" class="input-submit">
            </div>
        </div>
    </div>
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">
                    <div class="gallary_top">
                        <div class="gal_top_left"><span>Connection requests</span></div>
                    </div>
                    <div class="grops-grid">

                        @forelse ($ConnectionRequests as $ConnectionRequest)
                            <div class="groups-item follow-item" data-id="{{ $ConnectionRequest->id }}">
                                <a href="{{ url('/profile/id/'.$ConnectionRequest->user_id_1) }}" class="groups-item-title">
                                    <img src="{{ asset($ConnectionRequest->user_avatar) }}" alt="">
                                </a>
                                <div class="groups-item-content">
                                    <a href="{{ url('/profile/id/'.$ConnectionRequest->user_id_1) }}" class="groups-item-title">
                                        <b>{{ $ConnectionRequest->user_name }}</b>
                                        <span>{{ \App\ConnectionRequest::countConnection($ConnectionRequest->user_id_1, true) }}</span>
                                    </a>
                                    <a href="#" class="groups-item-wheel groups_follow approved_request" data-id="{{ $ConnectionRequest->id }}">Approved</a>
                                </div>
                            </div>
                        @empty
                            <p>No requests</p>
                        @endforelse
                    </div>
                </div>
                @include('includes.right-sidebar')
            </div>


        </div>
        </div>
    </section>
@endsection