@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap profile-price-container balance-log">
        <div class="container-fluid">
            <div class="main-container">
                <div class="profile-content" style="width: calc(82% - 30px);">
                    <h2>Withdraw money</h2>
                    <form id="WithdrawMoney" style=" color: #fff; font-size: 22px; margin-top: 25px; margin-bottom: 50px; ">
                        {{ csrf_field() }}
                        Sum: <input type="number" step="0.01" name="sum" class="edit_input" value="{{ $user->balance }}">
                        <div class="btn_follow" id="WithdrawMoneySubmit" style=" display: inline-block; padding: 11px; height: 44px; vertical-align: top; margin: 0px; margin-left: 10px; ">Withdraw</div>
                    </form>
                    <h2>Withdraw log</h2>
                    <table id="BalanceLogs" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sum</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($WithdrawLogs as $WithdrawLog)
                                @php
                                    switch ($WithdrawLog->status) {
                                        case "approved":
                                            $color = '#1bbf11';
                                            $tr_color = '#e2efe1';
                                            break;
                                        case "canceled":
                                            $color = '#D0021B';
                                            $tr_color = '#f1d2d5';
                                            break;
                                        case "pending":
                                            $color = '#ec8908';
                                            $tr_color = '#efdfca';
                                            break;
                                        default:
                                            $color = '#333333';
                                            $tr_color = '';
                                            break;
                                    }
                                @endphp
                                <tr style="background: {{ $tr_color }} !important">
                                    <td>{{ $WithdrawLog->sum }} {{ $WithdrawLog->currency }}</td>
                                    <td style="font-weight: bold; color: {{ $color }} !important">{{ ucfirst($WithdrawLog->status) }}</td>
                                    <td>{{ \App\DateConvert::Convert($WithdrawLog->created_at, false, false) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Sum</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @include('includes.right-sidebar')
            </div>
        </div>
    </section>
@endsection