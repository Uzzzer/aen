@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                @if ($user->status == 'blocked')
                    <h1 style="color: #fff;">The user is blocked.</h1>
                @else
                    @include('includes.photo-carousel')
                    <div class="profile-content overflow_block">
                        <h2><span>About</span> {{ $user->name }}</h2>
                        <div class="lady_location"><span>Location: </span> {{ $user->location }}</div>
                        <div class="tab-wrapper profile-tab">
                            <div class="tab_content">
                                <div class="tab_item">
                                    <div class="post-item">
                                        <span class="post-title">Basic Info</span>
                                        <div class="table-grup">
                                            <div class="left-table">
                                                <table class="table-1">
                                                    <tr>
                                                        <td>Nationality:</td>
                                                        <td>{{ $user->nationality }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Birthday:</td>
                                                        <td>{{ date('F jS, Y', strtotime($user->birthday)) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Profession:</td>
                                                        <td>{{ $user->profession }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Experience:</td>
                                                        <td>{{ $user->experience }} @if ( $user->experience == 1 ) years @else year @endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sex:</td>
                                                        <td>{{ $user->sex }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @include('includes.right-sidebar')
                @endif
            </div>
        </div>
    </section>
@endsection