@extends('layouts.profile')

@section('content')
    <div id="UploadAvatarBox">
        <img src=""/>
        <input type="button" id="CancelCropp" value="Cancel">
        <input type="button" id="ApplyCropp" value="Apply">
    </div>
    <section id="profile" class="profile main_wrap profile_edit">
        <div class="container-fluid">
            <div class="main-container">
                <div class="slider">
                    <div class="add_photo_container">
                        <div class="photo_edit_block">
                            <img src="{{ asset('img/camera_upload.svg') }}" alt="Alternate Text">
                            <div class="upl_gall_img">Upload Gallery </br> Images</div>
                            <div class="sell_your_img">You can sell your images?</div>
                        </div>
                        <div class="add_to_gallary">
                            <div class="add_photo_name">Add to Gallery</div>
                            <div class="add_to_gallary_container">
                                @for ($i = 0; $i < 6; $i++)
                                    <div class="add_to_gallary_block" data-id="{{ $i }}">
                                        <div class="add_input_block">
                                            <form class="imageUploadForm" data-id="{{ $i }}">
                                                <span class="helpText">Add Photos/Videos</span>
                                                <input type="hidden" name="photo" value="">
                                                <input type='file' class="uploadButton file" accept="image/*"/>
                                                <div class="uploadedImg">
                                                    <span class="unveil"></span>
                                                </div>
                                            </form>
                                        </div>
                                        <input class="upload_input_text" type="text" name="title" value=""
                                           placeholder="Title Image"/>
                                        <textarea name="description" class="upload_textarea"></textarea>
                                        <input type="number" step="0.01" class="price input1" placeholder="Price $">

                                    </div>
                                @endfor
                                <div class="upload_to_gallery_btn">
                                    <span class="btn_follow">Upload to Gallery</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-content overflow_block">
                    <h2 class="edit_iccon">
                        <span>About</span>
                        <input class="edit_input" type="text" name="name" value="{{ $user->name }}" style=" width: calc(100% - 111px); "/>
                    </h2>
                    <div class="lady_location edit_iccon">
                        <span>Location: </span>
                        <input class="edit_input" type="text" name="location" value="{{ $user->location }}" style=" width: calc(100% - 111px); "/>
                    </div>
                    <div class="tab-wrapper profile-tab">
                        <div class="tabs">
                            <span class="tab @if(!isset($_REQUEST['tab']) or $_REQUEST['tab'] == 'basic-info'){{ 'active' }}@endif">Basic info:</span>
                            <span class="tab @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'profile'){{ 'active' }}@endif">Profile:</span>
                            <span class="tab @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'categories'){{ 'active' }}@endif">Available for:</span>
                        </div>
                        <div class="tab_content">
                            <div class="tab_item"
                                 @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] != 'basic-nfo') style="display: none;" @endif>
                                <form id="ProfileInfoEdit">
                                    <input type="hidden" name="name" value="{{ $user->name }}">
                                    <input type="hidden" name="location" value="{{ $user->location }}">
                                    {{ csrf_field() }}
                                    <div class="post-item">
                                        <span class="post-title edit_iccon">Subscription</span>
                                        @if (is_null($user->subscription_end) or date('Y-m-d H:i:s') >= $user->subscription_end)
                                            <a href="{{ route('subscription') }}">
                                                <div class="btn_follow">Subscribe</div>
                                            </a>
                                        @else
                                            <h3 style=" font-size: 16px; color: #fff; ">Subscription end:
                                                <span style=" color: #D0021B; ">{{ date('d M Y H:i', strtotime($user->subscription_end)) }}</span>
                                            </h3>
                                            <a href="{{ route('subscription', ['extend' => 'y']) }}">
                                                <div class="btn_follow">To extend</div>
                                            </a>
                                        @endif
                                    </div>
                                    <div class="post-item basic_info_edit">
                                        <span class="post-title edit_iccon basic_inf_tittle">Basic Info</span>
                                        <div class="table-grup">
                                            <div class="left-table">
                                                <table class="table-1">
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Nationality:</td>
                                                        <td>
                                                            <input class="edit_input" type="text" name="nationality"
                                                                   value="{{ $user->nationality }}"/>
                                                        </td>
                                                        <td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">American</td>
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Birthday:</td>
                                                        <td>
                                                            <input class="edit_input birthday_input" type="text" name="birthday"
                                                                   value="{{ $user->birthday }}"/>
                                                        </td>
                                                        <td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">30/01/1990</td>
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Profession:</td>
                                                        <td>
                                                            <input class="edit_input" type="text" name="profession"
                                                                   value="{{ $user->profession }}"/>
                                                        </td>
                                                        <td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">Actor</td>
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Experience:</td>
                                                        <td>
                                                            <input class="edit_input" type="number" name="experience"
                                                                   value="{{ $user->experience }}"/>
                                                        </td>
                                                        <td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">3 (years)</td>
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Sex:</td>
                                                        <td>
                                                            <select name="sex" class="edit_input">
                                                                <option @if ($user->sex == 'Female') selected
                                                                        @endif value="Female">Female
                                                                </option>
                                                                <option @if ($user->sex == 'Male') selected
                                                                        @endif value="Male">Male
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="post-item basic_info_edit">
                                        <span class="post-title edit_iccon basic_inf_tittle">Avatar</span>
                                        <span class="upload-button-wrapper">
                                            Upload Avatar
                                            <input type="file" id="AvatarUpload">
                                        </span>
                                        <input type="hidden" name="avatar" value="{{ $user->avatar }}">
                                    </div>

                                    <div class="btn_follow ProfileInfoEdit_save">Save changes</div>

                                </form>
                            </div>
                            <div class="tab_item"
                                 @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'profile') style="display: block;" @endif>
                                <div class="post-item basic_info_edit">
                                    <span class="post-title edit_iccon basic_inf_tittle">Account details</span>
                                    <div class="table-grup">
                                        <form id="ProfileEdit">
                                            {{ csrf_field() }}
                                            <table class="table-1">
                                                <tr>
                                                    <td>E-mail (required):</td>
                                                    <td>
                                                        <input class="edit_input" type="text" name="email"
                                                               value="{{ $user->email }}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Your Password (required):</td>
                                                    <td>
                                                        <input class="edit_input" type="password" name="your_password"
                                                               value=""/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>New Password:</td>
                                                    <td>
                                                        <input class="edit_input" type="password" name="password"
                                                               value=""/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Password confirmation:</td>
                                                    <td>
                                                        <input class="edit_input" type="password"
                                                               name="password_confirmation" value=""/>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="btn_follow ProfileEdit_save">Save changes</div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_item"
                                 @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'categories') style="display: block;" @endif>
                                <form id="CategoryEdit">
                                    <input type="hidden" name="name" value="{{ $user->name }}">
                                    <input type="hidden" name="location" value="{{ $user->location }}">
                                    {{ csrf_field() }}

                                    <p class="avaliable_for">
                                        <span>Categories</span>
                                    </p>
                                    <div class="avaliable_checkbox" style="display: block;">
                                        @php
                                            $avaliable = json_decode($user->avaliable, true);

                                            if (!is_array($avaliable))
                                                $avaliable = [];

                                            $CategoriesAvailable = \App\CategoriesAvailable::get();
                                        @endphp
                                        @foreach ($CategoriesAvailable as $CategoryAvailable)
                                            <label class="avaliable_label">
                        						<span class="switch">
                        							  <input type="checkbox" data-val="{{ $CategoryAvailable->name }}"
                                                             @if (in_array($CategoryAvailable->name, $avaliable)) checked @endif>
                        						   	  <span class="checkbox_block"></span>
                        						</span>
                                                <span class="checkbox_name">{{ $CategoryAvailable->name }}</span>
                                            </label>
                                        @endforeach
                                        @foreach (\App\UsersCategoriesAvailable::getUserCategories() as $category)
                                            <label class="avaliable_label">
                        						<span class="switch">
                        							  <input type="checkbox" data-val="{{ $category->name }}"
                                                             @if (in_array($category->name, $avaliable)) checked @endif>
                        						   	  <span class="checkbox_block"></span>
                        						</span>
                                                <span class="checkbox_name">{{ $category->name }}</span>
                                            </label>
                                        @endforeach

                                    </div>
                                    <a href="#" id="AddCategory"
                                       style=" color: #D0021B; margin-top: 10px; display: inline-block; ">Add Category
                                    </a>

                                    <div class="btn_follow CategoryEdit_save">Save changes</div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>

                @include('includes.right-sidebar')

            </div>
        </div>
    </section>


<style>

.input1 {
	background: inherit;
	border: 0.5px solid 
	#979797;
	width: 100%;
	height: 20px;
	padding-left: 7px;
	font-family: 'Poppins', sans-serif;
	font-weight: 300;
	line-height: 23px;
	font-size: 11px;
	margin: 0 0 7px;
	color:
	#4A4A4A;
  -webkit-appearance: none;
  -moz-appearance:textfield;

}

</style>




@endsection

@section('custom_js')
    @if(!is_null(\Request::input('photo')))
        <script>
            $(document).ready(function () {
                $('.photo_edit_block').click();
            });
        </script>
    @endif
@endsection


