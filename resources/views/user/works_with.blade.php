@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">
                    <div class="gallary_top">
                        <div class="gal_top_left"><span>Companies</span></div>
                    </div>
                    <div class="grops-grid">
                        @forelse ($WorksWith as $User)
                            <div class="groups-item follow-item">
                                <a href="{{ route('id_profile', ['id' => $User->id]) }}" class="groups-item-title">
                                    <img src="{{ asset($User->avatar) }}" alt="">
                                </a>
                                <div class="groups-item-content">
                                    <a href="{{ url('/profile/id/'.$User->id) }}" class="groups-item-title">
                                        <b>{{ $User->name }}</b>
                                        <span>{{ \App\ConnectionRequest::countConnection($User->id, true) }}</span>
                                    </a>
                                </div>
                            </div>
                        @empty
                            <p>No companies</p>
                        @endforelse
                    </div>
                </div>
                @include('includes.right-sidebar')
            </div>


        </div>
        </div>
    </section>
@endsection