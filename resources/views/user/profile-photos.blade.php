@extends('layouts.profile')

@section('content')

    <style>
        /* Gallery fix */

		.checkout_outer{
			display: flex;
			justify-content: center;
			flex-direction: column;
			text-align: center;
		}
		.checkout_outer .photo_price{
				font-size: 25px;
				background: #80808045;
				width: 20%;
				margin: 0 auto;
				margin-top: -10px;
				margin-bottom: 10px;
				border-radius: 5px;
				color: white;
		}

        .photo-bottom-block {
            display: none !important;
        }

        .slider {
            display: none;
        }

        .profile-content {
            width: calc(83% - 30px);
        }

        .profile-content {
            width: calc(83% - 30px);
        }

        .profile-content .photos-box{
            justify-content: flex-start;
        }

        .photos-box .item {
            width: 25%;
            height: auto;
            padding: 0px !important;
            margin: 0px !important;
        }

        .item .hover-block {
            background: #0000009e;
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: 2;
            display: none;
        }

        .item:hover .hover-block {
            display: block;
        }

        .item .hover-block .views {
            color: #fff;
            text-align: center;
            padding-top: 16px;
            font-size: 20px;
        }

        .item .hover-block .title {
            color: #fff;
            text-align: center;
            padding-top: 20px;
            font-size: 20px;
            height: 130px;
        }

        .item .hover-block ul {
            color: #fff;
            font-size: 15px;
            text-align: center;
        }

        .item .hover-block ul li {
            display: inline-block;
            margin: 5px;
            width: 33%;
        }

        .item .hover-block ul li i {
            margin-right: 5px;
        }
        .photos-box .item .photo-wraper img{
            object-position: top;
        }
        .photos-box .item .photo-wraper {
            height: 272px !important;
        }
        @media(max-width:991px){
            .profile-content{
                width: 100%;
            }
        }
        @media(max-width:767px){
            .photos-box .item{
                width: 33.3333%;
            }
        }
        @media(max-width:576px){
            .photos-box .item{
                width: 50%;
            }
        }
        @media(max-width:400px){
            .photos-box .item{
                width: 100%;
            }
            .photos-box .item .photo-wraper{
                height: auto;
            }
        }
    </style>
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                @include('includes.photo-carousel')
                <div class="profile-content overflow_block">
                    {{-- <form class="search-line" id="SearchPhotos">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <input type="text" name="search">
                        <input type="submit" value="">
                    </form> --}}
                    <div class="photos-box photos-box-list">
                        @foreach ($Gallery as $photo)
                            @php
                                $count_likes = \App\PhotoLike::countLike($photo->id);
                                $count_comments = \App\MediaComment::where('photo_id', $photo->id)->count();
                            @endphp
                            <div class="item" data-id="{{ $photo->id }}">
                                <a href="{{ route('profile_photo', ['id' => $user->id, 'pid' => $photo->id]) }}">
                                    <div class="hover-block return-true">
                                        <div class="views">{{ \App\MediaView::countViewsPhoto($photo->id) }}</div>
                                        <div class="title">{{ $photo->title }}</div>
                                        @if($photo->need_payment)
                                        	<div class="checkout_outer">
                                        		<div class="photo_price">
                                                    £{{$photo->price}}
                                        		</div>
                                        		<br>
                                        		<div>
													<a href="/profile/payment?photo={{$photo->id}}"> <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png" alt="Check out with PayPal" /> </a>
                                        		</div>
                                        	</div>
                                        @else
		                                    <ul>
		                                        <li>
		                                            <div class="btn_follow {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_photo' }}"
		                                                 style="margin: 0px;padding: 0px 10px;" data-id="{{ $photo->id }}">
		                                                @if (!\App\PhotoLike::checkLike($photo->id))
		                                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
		                                                @else
		                                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
		                                                @endif
		                                                {{ $count_likes }}
		                                            </div>
		                                        </li>
		                                        <li>
		                                            <div class="btn_follow" style="margin: 0px;padding: 0px 10px;">
		                                                <i class="fa fa-comment-o"
		                                                   aria-hidden="true"></i> {{ $count_comments }}
		                                            </div>
		                                        </li>
		                                    </ul>
                                        @endif
                                    </div>
                                </a>
                                <div class="photo-wraper">
                                	@if($photo->need_payment)
		                                <img src="{{$photo->blured}}"
		                                     alt="Alternate Text">                                	
                                	@else
		                                <img src="{{ (!is_null($photo->preview) and !empty($photo->preview)) ? asset($photo->preview) : asset($photo->file_name) }}"
		                                     alt="Alternate Text">
                                     @endif
                                    <div class="photo-bottom-block">
                                        <span class="title">{{ $photo->title }}</span>
                                    </div>
                                </div>
                                <div class="photo-information-wrapper">
                                    <div class="photo-bottom-footer">
                                        <a href="#"
                                           class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_photo' }} @if (\App\PhotoLike::checkLike($photo->id)) liked @endif"
                                           data-id="{{ $photo->id }}">
                                            <i class="v-icon like"></i>
                                            Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                        </a>
                                        <a href="{{ route('profile_photo', ['id' => $user->id, 'pid' => $photo->id]) }}"
                                           class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : '' }}">
                                            <i class="v-icon comment"></i>
                                            Comment @if($count_comments > 0){{ '('.$count_comments.')' }}@endif</a>
                                        <a href="#">
                                            <i class="v-icon share"></i>
                                            Share
                                        </a>
                                    </div>
                                    <div class="video-item-information">
                                        <div class="views">{{ \App\MediaView::countViewsPhoto($photo->id) }}</div>
                                        <div class="video-item-information-box">
                                            <p>From:
                                                <a href="{{ route('id_profile', ['id' => $photo->user_id]) }}">{{ $user->name }}</a>
                                            </p>
                                            <p>Added on: {{ date('jS M Y', strtotime($photo->created_at)) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                @include('includes.right-sidebar')

            </div>
        </div>
    </section>

@endsection
