@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap profile-price-container balance-log">
        <div class="container-fluid">
            <div class="main-container">
                <div class="profile-content" style="width: calc(82% - 30px);">
                    <h2>Balance log</h2>
                    <a href="{{ route('withdraw_money') }}" style=" display: inline-block; "><div class="btn_follow" style=" width: 200px; ">Withdraw money</div></a>
                    <div style=" display: inline-block; color: #fff; padding-left: 10px; ">
                        Total: <span style="color: #D0021B;font-size: 16px;padding-right: 3px;padding-left: 3px;">{{ number_format($total, 2, '.', ' ') }}</span>{{ \App\Option::option('currency') }}
                    </div>
                    <table id="BalanceLogs" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Operation</th>
                                <th>Cost</th>
                                <th>Sum</th>
                                {{-- <th>Commission</th> --}}
                                <th>User</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($BalanceLogs as $BalanceLog)
                                <tr>
                                    <td>@php echo $BalanceLog->description; @endphp</td>
                                    <td>{{ $BalanceLog->cost }} {{ $BalanceLog->currency }}</td>
                                    <td><span style="{{ $BalanceLog->sum < 0 ? 'color: #D0021B;' : 'color: #1bbf11;' }}">{{ $BalanceLog->sum > 0 ? '+' : '' }}{{ $BalanceLog->currency }}{{ $BalanceLog->sum }}</span></td>
                                    {{-- <td><span style="color: #D0021B;">-{{ $BalanceLog->commission }}</span> {{ $BalanceLog->currency }}</td> --}}
                                    <td><a href="{{ route('id_profile', ['id' => $BalanceLog->customer_id]) }}">{{ $BalanceLog->user_name }}</a></td>
                                    <td>{{ \App\DateConvert::Convert($BalanceLog->created_at, false, false) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Operation</th>
                                <th>Cost</th>
                                <th>Sum</th>
                                {{-- <th>Commission</th> --}}
                                <th>User</th>
                                <th>Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @include('includes.right-sidebar')
            </div>
        </div>
    </section>
@endsection