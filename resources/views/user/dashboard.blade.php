@include('layouts.header')
<section id="profile" class="profile main_wrap">
    <div class="container-fluid">
        <div class="main-container">
            <div class="slider">
                <div class="carousel">
                    <div id="firstCar" class="owl-carousel">
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                        <div class="item"> <img class="owl-lazy" alt=""> <img src="img/img-1.png" /> </div>
                    </div>
                </div>
            </div>
            <div class="about overflow_block">
                <h2>About Luka Ross</h2>
                <p><span>Basic Info</span></p>
                <div class="table-grup">
                    <div class="left-table">
                        <table class="table-1">
                            <tr>
                                <td>Nationality:</td>
                                <td>American</td>
                            </tr>
                            <tr>
                                <td>Birthday:</td>
                                <td>December 20th, 1978</td>
                            </tr>
                            <tr>
                                <td>Profession:</td>
                                <td>Porn Star</td>
                            </tr>
                            <tr>
                                <td>Experience:</td>
                                <td>10 Years</td>
                            </tr>
                            <tr>
                                <td>Sex:</td>
                                <td>Female</td>
                            </tr>
                        </table>
                    </div>
                    <div class="right-table">
                        <table class="table-2">
                            <tr>
                                <td>Height:</td>
                                <td>5ft 6inchs</td>
                            </tr>
                            <tr>
                                <td>Weight:</td>
                                <td>65kg</td>
                            </tr>
                            <tr>
                                <td>Chest:</td>
                                <td>34 EE</td>
                            </tr>
                            <tr>
                                <td>Waist:</td>
                                <td>26</td>
                            </tr>
                            <tr>
                                <td>Hips:</td>
                                <td>36</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla rhoncus
                    ante, vitae pharetra neque auctor in. In et placerat dolor, in fringilla tortor.
                    Suspendisse vitae odio aliquam, pulvinar nisi sed, fringilla leo. Quisque nec libero
                    sit amet neque lobortis mattis eget vel eros. Duis euismod arcu sed elit
                    accumsan, ut fringilla mi maximus. Nulla massa leo, pharetra sit amet
                    bibendum sit amet, consequat id nulla. Sed pulvinar ante in tellus interdum, sit
                    amet rutrum metus facilisis. Nunc venenatis facilisis augue, et suscipit nisi
                    ultricies vel. Nunc orci dolor, sagittis sit amet sapien vitae, hendrerit rutrum
                    lorem. Praesent id convallis sapien. Cras sed massa at ipsum congue maximus
                    non at justo. Curabitur vehicula turpis quis ante condimentum semper vel eu
                    dolor. Phasellus pharetra, odio ac fermentum sollicitudin, urna tortor consectetur
                    lorem, a blandit urna nisl eget erat. Sed augue massa, eleifend ac nisi in,
                    consectetur condimentum odio.</p>
                <p><span>Special Features</span></p>
                <p>Nulla luctus mollis risus iaculis pellentesque. Aenean pretium consectetur tincidunt. Cras venenatis imperdiet velit, vitae rhoncus turpis rhoncus quis. Quisque molestie in nisi at pulvinar.</p>
            </div>
            <div class="sidebar">
                <div class="m_l_top">
                    <div class="bl_person_photo">
                        <img src="img/avatar-profile.png" alt="Alternate Text">
                    </div>
                    <div class="bl_person_inf">
                        <div>Location: Houston Texas</div>
                        <div class="data-dob">DOB: 03/07/78</div>
                    </div>
                </div>
                <div class="m_container_inf">
                    <div class="b_block_inf">Connections - <a href="#">14,657</a></div>
                    <div class="b_block_inf">Companies - <a href="#">435</a></div>
                    <div class="b_block_inf">Groups - <a href="#">1,200</a></div>
                    <div class="b_block_inf">Your Posts - <a href="#">743</a></div>
                </div>
                <div class="m_container_soc">
                    <div class="m_block_soc"><img src="img/inf_ic_1.jpg" alt="Alternate Text">Photos - <a href="#">221</a></div>
                    <div class="m_block_soc"><img src="img/inf_ic_2.jpg" alt="Alternate Text">Videos - <a href="#">119</a></div>
                    <div class="m_soc_ic">
                        <a href="#"><img src="img/soc_ic_1.jpg" alt="Alternate Text"></a>
                        <a href="#"><img src="img/soc_ic_2.jpg" alt="Alternate Text"></a>
                        <a href="#"><img src="img/soc_ic_3.jpg" alt="Alternate Text"></a>
                    </div>
                </div>
                <a href="#" class="logout-link">Logout of Account</a>
            </div>

        </div>
    </div>
</section>
@include('layouts.footer')
