@extends('layouts.layout-full-calendar')

@section('content')
    <script>
        var events = [];

        @foreach ($Events as $event)
        events.push({
            id: '{{ $event->id }}',
            title: '{{ addslashes($event->title) }}',
            start: '{{ date("m/d/Y g:i A", strtotime($event->start." 00:00:01")) }}',
            end: '{{ date("m/d/Y g:i A", strtotime($event->end." 23:59:59")) }}',
            className: '{{ $event->post_type == 'online' ? 'webchat' : 'event' }}', // bday, event, webcam, webchat
            editable: false
        });
        @endforeach
        @if (!is_null($user->birthday))
        @php
            $birthday = explode('/', $user->birthday);
            $birthday = date('Y') . '-' . $birthday[1] . '-' . $birthday[0];
        @endphp
        events.push({
            id: '0',
            title: 'Birthday',
            start: '{{ date("m/d/Y g:i A", strtotime($birthday . " 00:00:01")) }}',
            end: '{{ date("m/d/Y g:i A", strtotime($birthday . " 23:59:59")) }}',
            className: 'bday',
            editable: false
        });
        @endif
    </script>
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">
                    <div class="grops-grid groups_list">
                        <div id="calendar"></div>
                        <div class="upcoming-events" style=" margin-bottom: 25px; ">
                            @php
                                $blocks = 0;
                            @endphp
                            @foreach ($Events as $event)
                                @php
                                    if ($event->start < date('Y-m-d'))
                                        continue;
                                @endphp
                                @if ($event->post_type == 'online')
                                    <div class="item">
                                        <span class="day">{{ date("M", strtotime($event->start." 00:00:01")) }}</span>
                                        <div class="item-box red">
                                            <div class="top-row">
                                                <span>{{ date("jS", strtotime($event->start." 00:00:01")) }}</span>
                                                <i class="fa fa-comments-o" aria-hidden="true"></i>
                                            </div>
                                            <h4>LIVE WEB CHAT</h4>
                                            <p>{{ date("H:i", strtotime($event->start_time)) }} - {{ date("H:i", strtotime($event->end_time)) }}</p>
                                        </div>
                                    </div>
                                @else
                                    <div class="item">
                                        <span class="day">{{ date("M", strtotime($event->start." 00:00:01")) }}</span>
                                        <div class="item-box red">
                                            <div class="top-row">
                                                <span>{{ date("jS", strtotime($event->start." 00:00:01")) }}</span>
                                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                            </div>
                                            <h4>{{ $event->title }}</h4>
                                            <div class="event-row" style=" height: 29px; ">
                                                <img src="{{ asset($user->avatar) }}"
                                                     alt="Alternate Text" style=" margin-top: -3px; ">
                                                <p>
                                                    <b>{{ $user->name }}</b>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @php
                                    $blocks++;

                                    if ($blocks == 6)
                                        break;
                                @endphp
                            @endforeach
                            {{--<div class="item">
                                <span class="day">DayCal</span>
                                <div class="item-box">
                                    <div class="top-row">
                                        <span>1st</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <span class="day">DayCal-bday</span>
                                <div class="item-box">
                                    <div class="top-row">
                                        <span>1st</span>
                                    </div>
                                    <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                                    <h4>Lisa Annes</h4>
                                    <p>birthday</p>
                                </div>
                            </div>
                            <div class="item">
                                <span class="day">DayCal-event</span>
                                <div class="item-box red">
                                    <div class="top-row">
                                        <span>1st</span>
                                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                    </div>
                                    <h4>The European Summit</h4>
                                    <p>Loremipsum ipsum dolor</p>
                                    <div class="event-row">
                                        <img src="https://ladies.cgp.systems/storage/uploads/avatars/CETspLlUAWL0owVDWIL7dbmLHvJSiHUwNcRoeOVC.jpeg"
                                             alt="Alternate Text">
                                        <p>
                                            <b>Lisa Annes</b>
                                            test
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <span class="day">DayCal-webcam</span>
                                <div class="item-box red">
                                    <div class="top-row">
                                        <span>1st</span>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </div>
                                    <h4>LIVE WEB CAM</h4>
                                    <p>Loremipsum ipsum dolor</p>
                                </div>
                            </div>
                            <div class="item">
                                <span class="day">DayCal-webchat</span>
                                <div class="item-box red">
                                    <div class="top-row">
                                        <span>1st</span>
                                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                                    </div>
                                    <h4>LIVE WEB CHAT</h4>
                                    <p>Loremipsum ipsum dolor</p>
                                </div>
                            </div>
                            <div class="item">
                                <span class="day">Next Month Dates</span>
                                <div class="item-box grey">
                                    <div class="top-row">
                                        <span>1st</span>
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                </div>
                @include('includes.right-sidebar')
            </div>


        </div>
    </section>
@endsection