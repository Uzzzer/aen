@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                @include('includes.photo-carousel')
                <div class="profile-content overflow_block">
                    <form class="search-line" id="SearchNewsAndEvents">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <input type="text" name="search">
                        <input type="submit" value="">
                    </form>
                    <div class="news-box detail-list post_load_list">
                        <input type="hidden" name="load_page_type" value="load_user">
                        <input type="hidden" name="load_page_user" value="{{ $user->id }}">
                        @foreach ($Posts as $Post)
                            <div class="item event-item post_box" data-id="{{ $Post->id }}">
                                <a href="{{ route('single_post', $Post->id) }}">
                                    <div class="top-block">
                                        <div class="hover-block return-true">Show</div>
                                        @if ($Post->file_type == 'image')
                                            <img src="{{ asset($Post->file_name) }}" alt="Alternate Text">
                                        @endif
                                        @if ($Post->file_type == 'video')
                                            <video width="100%" height="233px"
                                                   controls {!!  $Post->video_preview ? 'poster="' . asset($Post->video_preview) . '"' : '' !!}>
                                                <source src="{{ asset($Post->file_name) }}">
                                                Your browser does not support the video tag.
                                            </video>
                                        @endif
                                    </div>
                                </a>
                                <div class="detail-bottom-block">
                                    <div class="detail-bottom-head">
                                        <img class="photo" src="{{ asset($user->avatar) }}" alt="Alternate Text">
                                        <div class="person-text">
                                            <p>
                                                <a href="{{ url('/profile/id/'.$Post->user_id) }}">{{ $user->name }}</a>
                                                ,</span> {{ $Post->title }} - {{ \App\DateConvert::Convert($Post->created_at) }}
                                            </p>
                                            <!-- <p class="fon-f-light">You, Ashley Murray, Dirk Diggler &amp; 12 others
                                                Commented</p> -->
                                        </div>
                                    </div>
                                    <div class="detail-bottom-body">
                                        <div class="title">
                                            @if ($Post->post_type == 'news')
                                                <i class="fa fa-newspaper-o" style="font-size: 15px; margin-right: 5px;"
                                                   aria-hidden="true"></i>
                                            @elseif ($Post->post_type == 'online')
                                                <i class="fa fa-video-camera" aria-hidden="true"
                                                   style="font-size: 15px; margin-right: 5px;"></i>
                                            @else
                                                <i class="fa fa-calendar" style="font-size: 15px; margin-right: 5px;"
                                                   aria-hidden="true"></i>
                                            @endif
                                            {{ $Post->title }}
                                            @if ($Post->post_type == 'event')
                                                <span class="c-red">{{ date('jS F Y', strtotime($Post->start)) }} Local Time, {{ $Post->location }}</span>
                                            @endif
                                            @if ($Post->post_type == 'online')
                                                <span class="c-red">{{ date('jS F Y', strtotime($Post->start)) }}, {{ date('H:i', strtotime($Post->start_time)) }} - {{ $Post->start == $Post->end ? '' : date('jS F Y', strtotime($Post->end)) . ',' }} {{ date('H:i', strtotime($Post->end_time)) }}</span>
                                            @endif
                                        </div>
                                        <div class="text">
                                            {!! $Post->description !!}
                                        </div>
                                    </div>
                                    <div class="detail-bottom-footer">
                                        @php
                                            $count_likes = \App\PostLike::where('post_id', $Post->id)->count();
                                            $count_comments = \App\PostComment::where('post_id', $Post->id)->count();

                                            $PostLike = NULL;
                                            if (Auth::check())
                                                $PostLike = \App\PostLike::where('post_id', $Post->id)->where('user_id', Auth::user()->id)->first();
                                        @endphp
                                        <a href="#" class="like_post @if ($PostLike != NULL) liked @endif"
                                           data-id="{{ $Post->id }}">
                                            <i class="v-icon like"></i>
                                            <span>Like @if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                        </a>
                                        <a href="#" class="comments_post" data-id="{{ $Post->id }}">
                                            <i class="v-icon comment"></i>
                                            Comment
                                            <span>@if($count_comments > 0){{ '('.$count_comments.')' }}@endif</span>
                                        </a>
                                        <a href="#" class="share_post"
                                           data-text="{{ $Post->title }}@if($Post->post_type == 'event'){{ ', '.date('jS F Y', strtotime($Post->start)) }}@endif"
                                           data-img="@if ($Post->file_type == 'image'){{ asset($Post->file_name) }}@endif"
                                           data-url=" {{ url('/') }}" data-id="{{ $Post->id }}">
                                            <i class="v-icon share"></i>
                                            Share
                                        </a>

                                        @if (\Auth::check() and \Auth::user()->id == $Post->user_id)
                                            <a href="#" class="delete_post"
                                               data-id="{{ $Post->id }}">
                                                <span>Delete</span>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="coment_container">
                                    <div class="hide_comment_block" data-id="{{ $Post->id }}" style="display: none;">
                                        @php
                                            $Comments = \App\PostComment::where('post_comments.post_id', $Post->id)->whereNull('post_comments.reply_id')->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                        @endphp
                                        @foreach ($Comments as $Comment)
                                            <div class="coment_block" data-id="{{ $Comment->id }}">
                                                <div class="coment_photo_person">
                                                    <a href="{{ url('/profile/id/'.$Comment->user_id) }}">
                                                        <img src="{{ asset($Comment->user_avatar) }}"
                                                             alt="{{ $Comment->user_name }}"
                                                             title="{{ $Comment->user_name }}"/>
                                                    </a>
                                                </div>
                                                <div class="coment_right">
                                                    <div class="coment_text">
                                                        {{ $Comment->comment }}
                                                        @if ($Comment->image != NULL)
                                                            <a href="{{ asset($Comment->image) }}"
                                                               data-lightbox="image-{{ $Comment->id }}">
                                                                <img src="{{ asset($Comment->image) }}"/>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <ul class="coment_data">
                                                        @php
                                                            $PostCommentLike = NULL;
                                                            if (Auth::check())
                                                                $PostCommentLike = \App\PostCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                                                            $count_likes = \App\PostCommentLike::where('comment_id', $Comment->id)->count();
                                                        @endphp
                                                        <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                                            @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>Like
                                                            <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                                        </li>
                                                        <li class="reply_comment"
                                                            @if(\Auth::check()) data-id="{{ $Comment->id }}"
                                                            data-post-id="{{ $Post->id }}" @endif>Reply
                                                        </li>
                                                        <li>{{ \App\DateConvert::Convert($Comment->created_at) }}</li>
                                                    </ul>
                                                    @php
                                                        $ReplyComments = \App\PostComment::where('post_comments.post_id', $Post->id)->where('post_comments.reply_id', $Comment->id)->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                                    @endphp
                                                    @foreach ($ReplyComments as $ReplyComment)
                                                        <div class="coment_block" data-id="{{ $ReplyComment->id }}"
                                                             data-reply-id="{{ $ReplyComment->reply_id }}">
                                                            <div class="coment_photo_person">
                                                                <a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}">
                                                                    <img src="{{ asset($ReplyComment->user_avatar) }}"
                                                                         alt="{{ $ReplyComment->user_name }}"
                                                                         title="{{ $ReplyComment->user_name }}"/>
                                                                </a>
                                                            </div>
                                                            <div class="coment_right">
                                                                <div class="coment_text">
                                                                    {{ $ReplyComment->comment }}
                                                                    @if ($ReplyComment->image != NULL)
                                                                        <a href="{{ asset($ReplyComment->image) }}"
                                                                           data-lightbox="image-{{ $ReplyComment->id }}">
                                                                            <img src="{{ asset($ReplyComment->image) }}"/>
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                                <ul class="coment_data">
                                                                    @php
                                                                        $PostCommentLike = NULL;
                                                                        if (Auth::check())
                                                                            $PostCommentLike = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->where('user_id', Auth::user()->id)->first();
                                                                        $count_likes = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->count();
                                                                    @endphp
                                                                    <li class="like_comment @if($PostCommentLike != NULL) liked @endif"
                                                                        @if(\Auth::check()) data-id="{{ $ReplyComment->id }}" @endif>Like
                                                                        <span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span>
                                                                    </li>
                                                                    <li>{{ \App\DateConvert::Convert($ReplyComment->created_at) }}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                        @if (Auth::check())
                                            <form class="add_comment" data-id="{{ $Post->id }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="post_id" value="{{ $Post->id }}">
                                                <input type="hidden" name="image" value="">
                                                <div class="coment_write">
                                                    <div class="writer_left">
                                                        <img src="{{ asset(Auth::user()->avatar) }}"
                                                             alt="Alternate Text"/>
                                                    </div>
                                                    <div class="writer_right">
                                                        <div class="comment_image"></div>
                                                        <input type="text" name="comment" value=""
                                                               placeholder="Write Comments..."/>
                                                    </div>
                                                </div>
                                                <img class="comment_img">
                                                <input type="file" style="display: none;">
                                            </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                @include('includes.right-sidebar')

            </div>
        </div>
    </section>
    @if (Auth::check())
        <form class="add_comment reply_comment_form copy_form" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" name="post_id" value="">
            <input type="hidden" name="comment_id" value="">
            <input type="hidden" name="image" value="">
            <div class="coment_write">
                <div class="writer_left">
                    <img src="{{ asset(Auth::user()->avatar) }}" alt="Alternate Text"/>
                </div>
                <div class="writer_right">
                    <div class="comment_image"></div>
                    <input type="text" name="comment" value="" placeholder="Write Comments..."/>
                </div>
            </div>
            <img class="comment_img">
            <input type="file" style="display: none;">
        </form>
    @endif
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '177979539749540',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v3.1'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection