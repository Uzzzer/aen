@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap profile-price-container">
        <div class="container-fluid">
            <div class="main-container">
                <div class="profile-content" style="width: calc(82% - 30px);">
                    <h2>Subscription</h2>
                    @if (!is_null(\Request::input('extend')))
                        <h3 style="color: #D0021B; font-size: 18px;">Current subscription will be canceled.</h3>
                    @endif
                    <table id="PriceTab">
                        <thead>
                        <tr>
                            <th style="width: 25%;">12 month</th>
                            <th style="width: 25%;">3 month</th>
                            <th style="width: 25%;">1 month</th>
                            @if ($UserCategory->trial_period)
                                <th style="width: 25%;">{{ \App\Option::option('trial_days') }} {{ intval(\App\Option::option('trial_days')) > 1 ? 'days' : 'day' }} trial</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <h4 class="title-girls-mobile">12 month</h4>
                                <img style="width: 100%;" class="girls-month" src="../img/sexi-girls-4.jpg" alt="Alternate Text"/>
                                {{ \App\Option::option('currency') }}{{ isset(json_decode($UserCategory->prices, true)[12]) ? json_decode($UserCategory->prices, true)[12] : '0' }}
                                <a href="{{ route('payment', ['subscription' => 12]) }}">
                                    <div class="btn_follow">Buy now</div>
                                </a>
                            </td>
                            <td>
                                <h4 class="title-girls-mobile">3 month</h4>
                                <img style="width: 100%;" class="girls-month" src="../img/sexi-girls-3.jpg" alt="Alternate Text"/>
                                {{ \App\Option::option('currency') }}{{ isset(json_decode($UserCategory->prices, true)[3]) ? json_decode($UserCategory->prices, true)[3] : '0' }}
                                <a href="{{ route('payment', ['subscription' => 3]) }}">
                                    <div class="btn_follow">Buy now</div>
                                </a>
                            </td>
                            <td>
                                <h4 class="title-girls-mobile">1 month</h4>
                                <img style="width: 100%;" class="girls-month" src="../img/sexi-girls-2.jpg" alt="Alternate Text"/>
                                {{ \App\Option::option('currency') }}{{ isset(json_decode($UserCategory->prices, true)[1]) ? json_decode($UserCategory->prices, true)[1] : '0' }}
                                <a href="{{ route('payment', ['subscription' => 1]) }}">
                                    <div class="btn_follow">Buy now</div>
                                </a>
                            </td>
                            @if ($UserCategory->trial_period)
                                <td>
                                    <h4 class="title-girls-mobile">2 days trial</h4>
                                    <img class="girls-month" src="../img/sexi-girls-1.png" alt="Alternate Text"/>
                                    {{ \App\Option::option('currency') }}{{ isset(json_decode($UserCategory->prices, true)['trial']) ? json_decode($UserCategory->prices, true)['trial'] : '0' }}
                                    <a href="{{ route('payment', ['subscription' => 'trial']) }}">
                                        <div class="btn_follow">Buy now</div>
                                    </a>
                                </td>
                            @endif
                        </tr>
                        </tbody>
                    </table>
                </div>
                @include('includes.right-sidebar')
            </div>
        </div>
    </section>
@endsection