<section id="dialog" class="dialog">
    <div class="messages-list">
        <div class="dialog-header">
            <div class="dialog-header-title">
                <h3>Your Messages</h3>
                <span><b class="red">20</b> New Messages</span>
            </div>
            <div class="dialog-header-search">
                <input type="text">
            </div>
            <a class="dialog-header-message" href="#"></a>
        </div>
        <div class="dialog-body">
            <div class="dialog-message-box">
                <div class="message-author">
                    <img src="/img/author1.jpg" alt="">
                </div>

                <div class="message-content">
                    <div class="message-top-line">
                        <div class="author-name">Kendra Lust</div>
                        <div class="message-date">18.49</div>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    </p>
                </div>
            </div>
            <div class="dialog-message-box">
                <div class="message-author">
                    <img src="/img/author2.jpg" alt="">
                </div>
                <div class="message-content">
                    <div class="message-top-line">
                        <div class="author-name">Pheonix Marie</div>
                        <div class="message-date">Fri</div>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    </p>
                </div>
            </div>
            <div class="dialog-message-box">
                <div class="message-author">
                    <img src="/img/author3.jpg" alt="">
                </div>
                <div class="message-content">
                    <div class="message-top-line">
                        <div class="author-name">Jayden James</div>
                        <div class="message-date">Tue</div>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    </p>
                </div>
            </div>
            <div class="dialog-message-box readed">
                <div class="message-author">
                    <img src="/img/author4.jpg" alt="">
                </div>
                <div class="message-content">
                    <div class="message-top-line">
                        <div class="author-name">Laura Jones</div>
                        <div class="message-date">Tue</div>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    </p>
                </div>
            </div>
            <div class="dialog-message-box readed">
                <div class="message-author">
                    <img src="/img/author5.jpg" alt="">
                </div>
                <div class="message-content">
                    <div class="message-top-line">
                        <div class="author-name">Sarah Porter</div>
                        <div class="message-date">Mon</div>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="chat-window">
        <div class="dialog-header">
            <div class="chat-back"></div>
            <div class="chat-person">
                <p>Phoenix Marie</p>
                <span>Active in ago</span>
            </div>
            <div class="chat-call"></div>
            <div class="chat-video-call"></div>
        </div>
        <div class="dialog-body">
            <div class="date-line">Fri 21:02</div>
            <div class="friend-message-box">
                <div class="message-author">
                    <img src="/img/author5.jpg" alt="">
                </div>
                <div class="friend-message">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores autem facere ex quaerat vel atque ipsam laborum, nam nobis possimus earum nulla, amet qui blanditiis cupiditate pariatur eveniet est! Dolore.
                    </p>
                </div>
            </div>
            <div class="date-line">Fri 21:02</div>
            <div class="your-message-box">
                <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et modi dicta veritatis velit recusandae temporibus. Ut, repudiandae praesentium. Odio, impedit eos aspernatur tempora laudantium consequatur et rerum necessitatibus aliquam animi!
                </p>
            </div>
        </div>
        <div class="dialog-footer">
            <input type="text"> <input type="submit">
        </div>
    </div>

    @yield('create-event')
</section>


<script src="../js/jquery.3.3.1.js"></script>
<script src="../js/owl.carousel.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/wow.min.js"></script>
<script src="../js/app.js"></script>
<script src="../js/JavaScript.js"></script>
<script src="../js/max.js"></script>
</body>
</html>