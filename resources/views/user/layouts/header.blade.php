<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AEN - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="../assets/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="../assets/animate.css" type="text/css" rel="stylesheet"/>
    <link href="../css/reset.css" type="text/css" rel="stylesheet"/>
    <link href="../css/header.css" type="text/css" rel="stylesheet"/>
    <link href="../css/footer.css" type="text/css" rel="stylesheet"/>
    <link href="../css/fonts.css" type="text/css" rel="stylesheet"/>
    <link href="../css/owl.carousel.min.css" rel="stylesheet" >
    <link href="../css/main.css" rel="stylesheet" >
    @if(isset($css))
        @foreach($css as $src)
            <link rel="stylesheet" href="{{$src}}" />
        @endforeach
    @endif

    {{ csrf_field() }}

    {{--{{dd($js)}}--}}
    <!--
        <link href="../css/youtuber.css" type="text/css" rel="stylesheet"/>

    -->
    <link href="../css/StyleSheet.css" rel="stylesheet" />
    <link href="../css/ruslan.css" type="text/css" rel="stylesheet"/>
    <link href="../css/max.css" type="text/css" rel="stylesheet"/>
    <link href="../css/style.css" type="text/css" rel="stylesheet"/>

</head>
<body>

<!--header -->
<header class="heaedr-main heaedr-main_2">
    <div class="top-header-menu">
        <div class="container-fluid container-nav">
            <div class="item-nav-menu">
                <div class="item-nav-logo">
                    <a class="brand" href="/">
                        <img src="../img/logo.svg" alt="" class="logo">
                    </a>
                </div>
                <div class="search-mob">
                    <input text="text" placeholder="SEARCH" />
                </div>
                <nav class="navbar navbar-expand-lg navbar-full navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="main-nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#">from the Adult Enterainment <br/>Network Users</a>
                            </li>
                            <li class="nav-item">
                                <a href="#">what’s going on in the <br/>Adult Entertainment World</a>
                            </li>
                            <li class="nav-item">
                                <a href="#">in the Adult Entertainment Industry - updates - Jobs</a>
                            </li>
                            <li class="nav-item">
                                <a href="#">Actors, Bar Staff, Models, Dancers <br/>  & Film Crew in Adult Entertainment</a>
                            </li>
                            <li class="nav-item">
                                <div class="search-menu">
                                    <input text="text" placeholder="SEARCH" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="search">
                    <input text="text" placeholder="" />
                </div>
            </div>
        </div>
    </div>
    <span class="border"></span>

    <div class="profile-header-menu">
        <div class="container-fluid">
            {{--<form class="" action="{{ url('/logout') }}" method="post">--}}
                {{--{{ csrf_field() }}--}}
                {{--<button class="back-link" type="submit" name="logout">{{ Auth::user()->name }}</button>--}}
            {{--</form>--}}
            @if (Auth::check())
                <a href="/custom-logout" class="back-link">{{ Auth::user()->name }}</a>
            @endif
            <div class="right-block">
                <ul class="profile-header-nav">
                    <li class="active"><a href="/profile">Profile</a></li>
                    <li><a href="/profile/news-and-events">News/Events</a></li>
                    <li><a href="/profile/videos">Videos</a></li>
                    <li><a href="/profile/photos">Photos</a></li>
                    <li><a href="/">Website</a></li>
                    <li><a href="/social">Social</a></li>
                </ul>
                <ul class="rofile-header-btns">
                    <li class="user">
                        <a href="#"><i class="v-icon user"></i></a>
                        <div class="star">889</div>
                    </li>
                    <li><a href="#"><i class="v-icon message"></i></a></li>
                    <li><a href="#"><i class="v-icon jobs"></i></a></li>
                    <li class="vertical-line"></li>
                    <li class="post"><a href="/profile/profile-editing"><i class="v-icon post"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- header end -->