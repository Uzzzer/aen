@extends('layouts.layout-full')

@section('content')
    @php
        $private = \App\Group::checkPrivateForUser($Group->id);
    @endphp
    <div class="main_wrap">
        <div class="container-fluid">
            <div class="main_container">
                <div class="mebmers_group_left">
                    <h6>Members <sup style="font-size: 50%">{{ \App\GroupFollower::getCountFollowersGroup($Group->id) }}</sup></h6>
                    @if (!$private)
                        <div class="members_profile">
                            @php
                                $RandomMembers = \App\GroupFollower::getRandomMembers($Group->id, 6);
                            @endphp
                            @foreach ($RandomMembers as $RandomMember)
                                <a href="{{ route('id_profile', ['id' => $RandomMember->id]) }}"><img src="{{ asset($RandomMember->avatar) }}" alt="Alternate Text"></a>
                            @endforeach
                        </div>
                        @if (\App\GroupFollower::getCountFollowersGroup($Group->id))
                            <a class="view_all" href="{{ route('followers_group', ['id' => $Group->id]) }}">View All</a>
                        @endif
                    @endif
                    <h6 class="mar_top_35">Photos <sup style="font-size: 50%">{{ \App\Gallery::getCountPhotosGroup($Group->id) }}</sup></h6>
                    @if (!$private)
                        <div class="members_profile_photo">
                            @php
                                $RandomPhotos = \App\Gallery::getRandomPhotoGroup($Group->id, 4);
                            @endphp
                            @foreach ($RandomPhotos as $RandomPhoto)
                                <a href="{{ route('photo_group', ['id' => $Group->id, 'pid' => $RandomPhoto->id]) }}" class="group_preview">
                                    <div style="background-image: url({{ asset($RandomPhoto->file_name) }})"></div>
                                </a>
                            @endforeach
                        </div>
                        @if (\App\Gallery::getCountPhotosGroup($Group->id))
                            <a class="view_all" href="{{ route('gallery_group', ['id' => $Group->id]) }}">View All</a>
                        @endif
                    @endif
                    <h6 class="mar_top_35">Videos <sup style="font-size: 50%">{{ \App\VideoGallery::getCountVideoGroup($Group->id) }}</sup></h6>
                    @if (!$private)
                        <div class="group_videos">
                            @php
                                $RandomVideos = \App\VideoGallery::getRandomVideoGroup($Group->id, 4);
                            @endphp
                            @foreach ($RandomVideos as $RandomVideo)
                                <a href="{{ route('video_group', ['id' => $Group->id, 'vid' => $RandomVideo->id]) }}" class="group_preview">
                                    <div style="background-image: url({{ asset($RandomVideo->preview) }})"></div>
                                </a>
                            @endforeach
                        </div>
                        @if (\App\VideoGallery::getCountVideoGroup($Group->id))
                            <a class="view_all" href="{{ route('videos_group', ['id' => $Group->id]) }}">View All</a>
                        @endif
                    @endif
                </div>
                <div class="m_right_block">
                    <form id="NotificationSettings">
                        {{ csrf_field() }}
                        <input type="hidden" name="group_id" value="{{ $Group->id }}">
                        @foreach ($NotificationTypes as $NotificationType)
                            <label class="avaliable_label">
                                <span class="switch">
                                    <input type="checkbox" value="{{ $NotificationType->id }}" name="NotificationOff[]" @if (!in_array($NotificationType->id, $NotificationOff)) checked @endif>
                                    <span class="checkbox_block"></span>
                                </span>
                                <span class="checkbox_name">{{ $NotificationType->name_view }}</span>
                            </label>
                        @endforeach
                        <div class="btn_follow" id='NotificationSettingsSubmit' style=" margin-bottom: 15px; ">Save changes</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection