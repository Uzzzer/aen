@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="profile-content" style="width: calc(82% - 30px);">
                    <h2>Purchased Videos</h2>
                    <div class="videos-box videos-box-list coll-two" style=" margin-top: 25px; ">

                         @foreach ($Videos as $Video)
                            <div class="item" data-id="{{ $Video->video_id }}" style=" width: calc(50% - 10px); ">
                                <div class="video-wraper">
                                    <img src="{{ asset($Video->preview_image) }}" alt="{{ $Video->title }}">
                                    <div class="video-bottom-block">
                                        <div class="left-box">
                                            <i class="v-icon play"></i>
                                            <div class="video-bottom-text">
                                                <span class="title">{{ $Video->title }}</span>
                                                <p class="lite-text">Run Time: {{ \App\Video::convertTime($Video->time) }}</p>
                                            </div>
                                        </div>
                                        <a href="{{ route('profile_video', ['id' => $Video->video_id]) }}" class="video-btn" style="animation-name: fadeInRight;">Watch NOW</a>
                                    </div>
                                </div>
                                <div class="video-information-wrapper" style="display: none;">
                                    @php
                                        $count_likes = \App\ProfileVideoLike::countLike($Video->video_id);
                                        $count_comments = \App\ProfileVideoComment::where('video_id', $Video->video_id)->count();
                                    @endphp
                                    <div class="video-bottom-footer">
                                        <a href="#" class="like_video @if (\App\ProfileVideoLike::checkLike($Video->video_id)) liked @endif" data-id="{{ $Video->video_id }}"><i class="v-icon like"></i>Like&nbsp;<span>@if($count_likes > 0){{ '('.$count_likes.')' }}@endif</span></a>
                                        <a href="{{ route('profile_video', ['id' => $Video->video_id]) }}" class="@if(!\App\PurchasedVideo::checkPurchase($Video->video_id)){{ 'must_buy_video' }}@endif"><i class="v-icon comment"></i>Comment ({{ $count_comments }})</a>
                                        <a href="#"><i class="v-icon share"></i>Share</a>
                                    </div>
                                    <div class="video-item-information">
                                        <div class="views">{{ \App\ProfileVideoView::videoViewsVideo($Video->video_id) }}</div>
                                        <div class="video-item-information-box">
                                            <p>From: <a href="{{ route('id_profile', ['id' => $Video->video_user_id]) }}">{{ $Video->video_user_name }}</a> - {{ \App\Video::getCountVideos($Video->video_user_id) }} videos</p>
                                            <p>
                                                @php
                                                    $Contents = json_decode($Video->content);
                                                @endphp
                                                Content:
                                                @foreach ($Contents as $key => $content)
                                                    @php
                                                        $content = \App\CategoriesAvailable::find($content);
                                                    @endphp
                                                    @if (!is_null($content))
                                                        @if($key), @endif<a href="#">{{ $content->name }}</a>
                                                    @endif
                                                @endforeach
                                            </p>
                                            <p>
                                                @php
                                                    $Tags = \App\VideoTag::where('video_id', $Video->video_id)->get();
                                                @endphp
                                                @if (count($Tags))
                                                    Tags:
                                                    @foreach ($Tags as $key => $tag)
                                                        @if($key), @endif<a href="#">{{ $tag->tag }}</a>
                                                    @endforeach
                                                @endif
                                            </p>
                                            <p>Added on: {{ date('jS M Y', strtotime($Video->video_created_at)) }}</p>
                                            <p>Purchase date: {{ date('jS M Y', strtotime($Video->created_at)) }}</p>
                                            <p>Cost of: {{ $Video->currency }}{{ $Video->cost }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @include('includes.right-sidebar')
            </div>
        </div>
    </section>
@endsection