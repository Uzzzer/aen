@section('create-event')
    <div class="content-part">
        <div class="post-bar">
            <div class="post-bar-heading">
                <select class="post-bar-select">
                    <option value="">Post Type</option>
                    <option value="">News / Update</option>
                    <option value="">Events</option>
                </select>
                <a href="#" class="photo-album">Photo Album</a>
                <a href="#" class="video-album">Video</a>
            </div>
            <div class="post-bar-content">
                <h3>Basic info</h3>
                <div class="create-line">
                    <b class="label">Event Photo or Video</b>
                    <div class="upload-section">
                        <img src="img/upload-bg.jpg" alt="" id="upload-bg">
                        <input type="file" id="upload-file">
                    </div>
                </div>
                <div class="create-line">
                    <b class="label">Event Name</b>
                    <input type="text">
                </div>
                <div class="create-line">
                    <b class="label">Location</b>
                    <input type="text">
                </div>
                <div class="create-line">
                    <b class="label">Frequency</b>
                    <select name="" id="">
                        <option value="">Occurs Once</option>
                        <option value="">Occurs Twice</option>
                        <option value="">Occurs 3-5 times</option>
                        <option value="">More...</option>
                    </select>
                </div>
                <div class="create-line">
                    <b class="label">Starts</b>
                    <input type="date">
                </div>
                <div class="create-line">
                    <b class="label">Ends</b>
                    <input type="date">
                </div>
                <h3>Details</h3>
                <p>Let people know what type of event you are hosting and what to expect</p>
                <div class="create-line">
                    <b class="label">Category</b>
                    <select name="" id="">
                        <option value="">Category One</option>
                        <option value="">Category Two</option>
                        <option value="">Category Three</option>
                        <option value="">Category Four</option>
                    </select>
                </div>
                <div class="create-line">
                    <b class="label">Description</b>
                    <textarea name="" id="" cols="30" rows="5"></textarea>
                </div>
                <div class="create-line">
                    <b class="label">Keywords</b>
                    <input type="text">
                </div>
            </div>
            <div class="post-bar-footer">
                <a href="#" class="post-button">Post</a>
            </div>
        </div>
    </div>
@stop