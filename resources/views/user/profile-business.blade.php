@extends('layouts.profile')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                @if ($user->status == 'blocked')
                    <h1 style="color: #fff;">The user is blocked.</h1>
                @elseif ($user->id != \Auth::user()->id and !\App\ConnectionRequest::CheckConnectionApprove($user->id) and in_array(\App\User::isType($user->id), ['business']))
                    <h1 style="color: #fff; width: calc(82% + 30px);">To view the profile you need to follow the user.</h1>
                    @include('includes.right-sidebar-business')
                @else
                    @include('includes.photo-carousel')
                    <div class="profile-content overflow_block ">
                        <h2>About {{ $user->name }}</h2>

                        <div class="tab-wrapper profile-tab">
                            <div class="tabs">
                                <span class="tab active">Basic info:</span>
                                <span class="tab">Works @:</span>
                                <span class="tab">Worked with:</span>
                                @if (in_array(\App\User::isType($user->id), ['business']))
                                    <span class="tab">Jobs @:</span>
                                @endif
                            </div>
                            <div class="tab_content">
                                <div class="tab_item" style="display: block;">
                                    <div class="post-item">
                                        <span class="post-title">Basic Info</span>
                                        <div class="table-grup">
                                            <div class="left-table">
                                                <table class="table-1">
                                                    <tr>
                                                        <td>Nationality:</td>
                                                        <td>{{ $user->nationality }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Birthday:</td>
                                                        <td>{{ date('F jS, Y', strtotime($user->birthday)) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Profession:</td>
                                                        <td>{{ $user->profession }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Experience:</td>
                                                        <td>{{ $user->experience }} @if ( $user->experience == 1 ) years @else year @endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sex:</td>
                                                        <td>{{ $user->sex }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            @if (!in_array($user->user_category_id, [8,9,10]))
                                                <div class="right-table">
                                                    <table class="table-2">
                                                        <tbody>
                                                        @if (!empty($user->height))
                                                            <tr>
                                                                <td>Height:</td>
                                                                <td>{{ $user->height }}</td>
                                                            </tr>
                                                        @endif
                                                        @if (!empty($user->weight))
                                                            <tr>
                                                                <td>Weight:</td>
                                                                <td>{{ $user->weight }}</td>
                                                            </tr>
                                                        @endif
                                                        @if (!empty($user->chest))
                                                            <tr>
                                                                <td>Chest:</td>
                                                                <td>{{ $user->chest }}</td>
                                                            </tr>
                                                        @endif
                                                        @if (!empty($user->waist))
                                                            <tr>
                                                                <td>Waist:</td>
                                                                <td>{{ $user->waist }}</td>
                                                            </tr>
                                                        @endif
                                                        @if (!empty($user->hips))
                                                            <tr>
                                                                <td>Hips:</td>
                                                                <td>{{ $user->hips }}</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="post-item">
                                        <span class="post-title">Categories</span>
                                        <p class="profile-avaliable-txt">
                                            @php
                                                $avaliables = json_decode($user->avaliable, true);

                                                if (!is_array($avaliables))
                                                    $avaliables = [];
                                            @endphp
                                            @foreach ($avaliables as $key => $avaliable)
                                                <span>{{ $avaliable }}</span>
                                                @if (($key+1) % 3 == 0 and ($key+1) >= 3)
                                                    <br>
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                    <div class="post-item">
                                        <span class="post-title">Blog</span>
                                        <p>{{ $user->about_me }}</p>
                                    </div>
                                </div>
                                <div class="tab_item" style="display: none;">
                                    <div class="works-box">
                                        @php
                                            $Works = \App\Job::where('user_id', $user->id)->whereNotNull('user_job')->join('users', 'jobs.user_job', '=', 'users.id')->get();
                                        @endphp
                                        @foreach ($Works as $Work)
                                            <div class="item">
                                                <a href="{{ route('id_profile', ['id' => $Work->user_job]) }}">
                                                    <img class="persone-photo" src="{{ asset($Work->avatar) }}"
                                                         alt="Image">
                                                </a>
                                                <div class="persone-info">
                                                    <div class="persone-info-text">
                                                        <a href="{{ route('id_profile', ['id' => $Work->user_job]) }}">
                                                            <span class="title">{{ $Work->name }}</span>
                                                        </a>
                                                        <span class="sub-title">{{ $Work->title }}</span>
                                                        <!-- <span class="period">3 years 1 month</span> -->
                                                    </div>
                                                    <div class="persone-info-btns">
                                                        @if ($Work->user_job != \Auth::user()->id)
                                                            @php
                                                                $conversation = Chat::conversations()->between(Auth::user()->id, $Work->user_job);
                                                            @endphp
                                                        @endif
                                                        @if ((Auth::check() and $Work->user_job != Auth::user()->id) or !Auth::check())
                                                            <a href="#"
                                                               class="persone-btn {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'connection_request' }} @if(\App\ConnectionRequest::CheckConnection($Work->user_job)) btn_disabled @endif"
                                                               data-id="{{ $Work->user_job }}">Follow
                                                            </a>
                                                            <a href="#" class="persone-btn OpenChat"
                                                               @if (Auth::user()->id != $Work->user_job) data-id="{{ $Work->user_job }}"
                                                               data-name="{{ $Work->name }}"
                                                               @endif @if (isset($conversation) and $conversation != null) data-conversation_id="{{ $conversation->id }}" @endif>Message
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab_item" style="display: none;">
                                    <div class="busines-worked-box">
                                        @php
                                            $WorksWith = \App\WorksWith::where('business_id', $user->id)->join('users', 'works_with.user_id', '=', 'users.id')->get();
                                        @endphp
                                        @foreach ($WorksWith as $WorkWith)
                                            <div class="item">
                                                <div class="photo-wrapper">
                                                    <img src="{{ asset($WorkWith->avatar) }}"
                                                         alt="{{ $WorkWith->name }}">
                                                    <span class="name">{{ $WorkWith->name }}</span>
                                                    <div class="hover-block">
                                                        <a href="{{ url('/profile/id/'.$WorkWith->user_id) }}">View Profile</a>
                                                    </div>
                                                </div>
                                                <div class="button btn_follow {{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'connection_request' }} @if(\App\ConnectionRequest::CheckConnection($WorkWith->user_id)) btn_disabled @endif"
                                                     data-id="{{ $WorkWith->user_id }}">Follow
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                @if (in_array(\App\User::isType($user->id), ['business']))
                                    <div class="tab_item" style="display: none;">
                                        @php
                                            $Jobs = \App\Job::where('status', 'active')->where('user_id', $user->id)->get();
                                        @endphp
                                        @foreach ($Jobs as $Job)
                                            @php
                                                $fields = json_decode($Job->fields, true);
                                            @endphp
                                            <div class="tab_job_block job_box">
                                                <div class="tab_job_block_top">
                                                    <div class="tab_job_left">
                                                        <div class="job_tittle">{{ $Job->title }}</div>
                                                        @if(isset($fields['Location']))
                                                            <div class="job_location">Location: {{ $fields['Location'] }}</div>@endif
                                                        <div class="job_data">2 weeks ago</div>
                                                    </div>
                                                    <div class="tab_job_right ">
                                                        <div class="job_right_text job_right_textDetails">
                                                            <p class="open_job_profile">View Details</p>   <!-- Close Details -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab_job_block_bottom" style="display: none;">
                                                    <div class="tab_job_block_wrap">
                                                        <p>
                                                            {{ $Job->text }}
                                                        </p>
                                                        @if ($Job->responsibilities != null)
                                                            @php
                                                                $responsibilities = json_decode($Job->responsibilities, true);
                                                            @endphp
                                                            @if (is_array($responsibilities))
                                                                <h6>Responsibilities</h6>
                                                                <ul>
                                                                    @foreach ($responsibilities as $responsibility)
                                                                        <li>{{ $responsibility }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif
                                                        @endif
                                                        @if (is_array($fields))
                                                            @foreach ($fields as $key => $value)
                                                                <div class="job_info">
                                                                    <span>{{ $key }}: </span>{{ $value }}</div>
                                                            @endforeach
                                                        @endif
                                                        @if (!in_array($UserType, ['public_user', 'user', false]))
                                                            <div class="Apply_for_job">
                                                                <a href="#" class="apply_for_job"
                                                                   data-id="{{ $Job->id }}">Apply for job
                                                                </a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                    @include('includes.right-sidebar-business')
                @endif
            </div>
        </div>
    </section>
@endsection