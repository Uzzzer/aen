@extends('layouts.layout')

@section('content')

    <div class="post_load_list">
        <span><a href="#" onclick="window.history.back();" class="user-name-link">Back</a></span>

        <div class="right_container post_box" data-id="{{ $Post->id }}">
            <div class="m_r_top">
                <div class="m_rl_top">
                    <div class="ph_person">

                        <a href="{{ is_null($Group) ? url('/profile/id/'.$Post->user_id) : route('group', $Post->group_id) }}">
                            <img src="{{ is_null($Group) ? asset($Post->user()->avatar) : asset($Post->group()->avatar) }}" alt="Alternate Text"/>
                        </a>
                    </div>
                    <div class="person_text">
                        <p>
                                <span><a href="{{ is_null($Group) ? url('/profile/id/'.$Post->user_id) : route('group', $Post->group_id) }}"
                                         class="user-name-link">{{ is_null($Group) ? $Post->user()->name : $Post->group()->name }}</a>,</span> {{ $Post->title }} - {{ \App\DateConvert::Convert($Post->created_at) }}
                        </p>
                        <p class="fon-f-light">
                            <!-- You, Ashley Murray, Dirk Diggler & 12 others Commented -->
                        </p>
                    </div>
                </div>
                <div class="m_rr_top">
                    @php
                        $PostLike = NULL;
                        if (Auth::check())
                            $PostLike = \App\PostLike::where('post_id', $Post->id)->where('user_id', Auth::user()->id)->first();
                    @endphp
                    <a href="#"
                       class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'like_post' }} @if ($PostLike != NULL) liked @endif"
                       data-id="{{ $Post->id }}">
                        <img src="{{ asset('img/r_ic_1.svg') }}" alt="Alternate Text"/>
                        <span>Like{{ $Post->like_count()  ? ' ('.$Post->like_count().')' : '' }}</span>
                    </a>
                    <a href="#"
                       class="{{ (!$UserType or $UserType == 'public_user') ? 'message_no_subscription' : 'comments_post' }}"
                       data-id="{{ $Post->id }}">
                        <img src="{{ asset('img/r_ic_2.svg') }}" alt="Alternate Text"/>
                        <span>Comment{{ $Post->comment_count()  ? ' ('.$Post->comment_count().')' : '' }}</span>
                    </a>
                    <a href="#" class="share_post"
                       data-text="{{ $Post->title }}, {{ date('jS F Y', strtotime($Post->start)) }}"
                       data-img="@if ($Post->file_type == 'image'){{ asset($Post->file_name) }}@endif"
                       data-url=" {{ url('/') }}" data-id="{{ $Post->id }}">
                        <img src="{{ asset('img/r_ic_3.svg') }}" alt="Alternate Text"/>
                        <span>Share</span>
                    </a>
                    @if (\Auth::check() and \Auth::user()->id == $Post->user_id)
                        <div class="drop_tp-post_box">
                            <div class="dot_down-btn">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Capa_1" x="0px" y="0px"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                                     xml:space="preserve" width="20px" height="30px">
                                        <g>
                                            <g>
                                                <g>
                                                    <g>
                                                        <circle cx="256" cy="256" r="64" data-original="#000000"
                                                                class="active-path" data-old_color="#ffffff"
                                                                fill="#ffffff"/>
                                                        <circle cx="256" cy="448" r="64" data-original="#000000"
                                                                class="active-path" data-old_color="#ffffff"
                                                                fill="#ffffff"/>
                                                        <circle cx="256" cy="64" r="64" data-original="#000000"
                                                                class="active-path" data-old_color="#ffffff"
                                                                fill="#ffffff"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                            </div>
                            <div class="drop_content_inner_list">
                                <a href="#" class="edit_post"
                                   data-id="{{ $Post->id }}">
                                    <span>Edit</span>
                                </a>
                                <a href="#" class="delete_post"
                                   data-id="{{ $Post->id }}">
                                    <span>Delete</span>
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @if ($Post->file_type == 'image')
                <div class="m_bg_container" style="background-image: url({{ asset($Post->file_name) }});">
                    <img src="{{ asset($Post->file_name) }}" alt="">
                </div>
            @endif
            @if ($Post->file_type == 'video')
                <video width="100%" height="400px"
                       controls {!!  $Post->video_preview ? 'poster="' . asset($Post->video_preview) . '"' : '' !!}>
                    <source src="{{ asset($Post->file_name) }}">
                    Your browser does not support the video tag.
                </video>
            @endif
            <div class="m_bot_container">

                <section class="comment-inner-wrap">
                    <div class="comment-container">
                        <div class="cnt-top-panel">
                            <div class="cnt-top-info">
                                <div class="left">
                                    <div class="cnt-date">
                                        <span class="month">{{ date('M', strtotime($Post->created_at)) }}</span>
                                        <span class="date">{{ date('j', strtotime($Post->created_at)) }}</span>
                                    </div>
                                    <div class="title-group">
                                        <h3>{{ $Post->title }}</h3>
                                        <p>{{ $Post->subtitle }}</p>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="cnt-calendar-icon">
                                        @if ($Post->post_type == 'news')
                                            <i class="fa fa-newspaper-o" style="font-size: 24px; margin-right: 15px;"
                                               aria-hidden="true"></i>
                                        @elseif ($Post->post_type == 'online')
                                            <i class="fa fa-video-camera" aria-hidden="true"
                                               style="font-size: 24px; margin-right: 15px;"></i>
                                        @else
                                            <a href="{{ route('calendar', $Post->user_id) }}" style="color: #000;">
                                                <i class="fa fa-calendar" style="font-size: 24px; margin-right: 15px;"
                                                   aria-hidden="true"></i>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="cnt-post-data">
                                <div class="left">
                                    <div class="cnt-btn-group">
                                        <button class="btn interested_post {{ !is_null($Post->interested()) ? 'active' : '' }}"
                                                data-id="{{ $Post->id }}">
                                            <img src="/img/cnt-btn-star.svg" alt="">
                                            Interested
                                        </button>
                                        <button class="btn going_post {{ !is_null($Post->going()) ? 'active' : '' }}"
                                                data-id="{{ $Post->id }}">
                                            <img src="/img/cnt-btn-check.svg" alt="">
                                            Going
                                        </button>
                                    </div>
                                    @if ($Post->post_type == 'event' or $Post->post_type == 'online')
                                        <div class="cnt-full-time">
                                            <div class="icon">
                                                <img src="/img/cnt-watch.svg" alt="">
                                            </div>
                                            <div class="full-time-text">{{ date('l, M j', strtotime($Post->start)) }} at {{ date('ga', strtotime($Post->start . ' ' . $Post->start_time)) }} - @if($Post->start == $Post->end){{ date('ga', strtotime($Post->end . ' ' . $Post->end_time)) }}@else{{ date('M j, ga', strtotime($Post->end . ' ' . $Post->end_time)) }}@endif</div>
                                        </div>
                                    @endif
                                </div>
                                <div class="right">
                                    @if ($Post->post_type == 'event' and !empty($Post->location))
                                        <div class="cnt-place">
                                            <div class="icon">
                                                <img src="/img/cnt-map-pin.svg" alt="">
                                            </div>
                                            <div class="cnt-place-text">
                                                <span>{{ $Post->location }}</span>
                                            </div>
                                        </div>
                                        <div class="cnt-show-map" style="min-width: 55px;">
                                            <a href="https://www.google.com/maps/place/{{ urldecode($Post->location) }}"
                                               target="_blank">
                                                <span>Show Map</span>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <ul class="tabs-item">
                            <li class="">About</li>
                            <li class="active">Discussion</li>
                        </ul>
                        <div class="tabs-content-wrap">
                            <div class="tab-content-item" style="display: none;">
                                <div class="cnt-about">
                                    <p class="top-text">{!! $Post->description !!}</p>
                                    <div class="cnt-about-top">
                                        <div class="top-info">
                                            <span><font class="going_count">{{ $Post->going_count() }}</font>  Going</span>
                                        </div>
                                        @if ($Post->going_count())
                                            @foreach ($Post->going_rand() as $going)
                                                <ul class="following-people-list">
                                                    <li>
                                                        <div class="left">
                                                            <div class="img-wrap">
                                                                <img src="{{ asset($going->user()->avatar) }}" alt=""
                                                                     style="max-width: 50px">
                                                            </div>
                                                            <span><a href="{{ url('/profile/id/'.$going->user()->id) }}"
                                                                     style=" color: #000; ">{{ $going->user()->name }}</a> is going</span>
                                                        </div>
                                                        <div class="right">
                                                            @if (\Auth::check() and $going->user()->id != \Auth::user()->id)
                                                                <a href="#" class="cnt-btn OpenChat"
                                                                   data-name="{{ $going->user()->name }}"
                                                                   data-id="{{ $going->user()->id }}">Message
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </li>
                                                </ul>
                                            @endforeach
                                        @endif
                                        <div class="top-info">
                                            <span><font class="interested_count">{{ $Post->interested_count() }}</font> Interested</span>
                                        </div>
                                        @if ($Post->interested_count())
                                            @foreach ($Post->interested_rand() as $going)
                                                @if(!empty($going->user()))
                                                <ul class="sugguestion-friends-list">
                                                    <li>
                                                        <div class="left">
                                                            <div class="img-wrap">
                                                                <img src="{{asset($going->user()->avatar)}}" alt=""
                                                                     style="max-width: 50px">
                                                            </div>
                                                            <span><a href="{{ url('/profile/id/'.$going->user()->id) }}"
                                                                     style=" color: #000; ">{{ $going->user()->name }}</a> is going</span>
                                                        </div>
                                                        <div class="right">
                                                            @if (\Auth::check() and $going->user()->id != \Auth::user()->id)
                                                                <a href="#" class="cnt-btn OpenChat"
                                                                   data-name="{{ $going->user()->name }}"
                                                                   data-id="{{ $going->user()->id }}">Message
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </li>
                                                </ul>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="cnt-about-share-box">
                                            <div class="cnt-about-share">
                                                <div class="icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16.592"
                                                         height="15.926" viewBox="0 0 16.592 15.926">
                                                        <g id="Icon" transform="translate(15.781 1) rotate(90)"
                                                           opacity="0.485">
                                                            <path id="Stroke_218" data-name="Stroke 218"
                                                                  d="M0,0V3.79H13.926V0" transform="translate(0 10.991)"
                                                                  fill="none" stroke="#fff" stroke-linecap="round"
                                                                  stroke-linejoin="round" stroke-miterlimit="10"
                                                                  stroke-width="2"></path>
                                                            <path id="Stroke_220" data-name="Stroke 220"
                                                                  d="M.036,10.612,0,0,5.266,4.942"
                                                                  transform="translate(6.919 0.19)" fill="none"
                                                                  stroke="#fff" stroke-linecap="round"
                                                                  stroke-linejoin="round" stroke-miterlimit="10"
                                                                  stroke-width="2"></path>
                                                            <path id="Stroke_222" data-name="Stroke 222"
                                                                  d="M5.178,0,0,4.932" transform="translate(1.741 0.19)"
                                                                  fill="none" stroke="#fff" stroke-linecap="round"
                                                                  stroke-linejoin="round" stroke-miterlimit="10"
                                                                  stroke-width="2"></path>
                                                        </g>
                                                    </svg>
                                                </div>
                                                <a href="#" class="share_post"
                                                   style=" color: #000; "
                                                   data-text="{{ $Post->title }}, {{ date('jS F Y', strtotime($Post->start)) }}"
                                                   data-img="@if ($Post->file_type == 'image'){{ asset($Post->file_name) }}@endif"
                                                   data-url=" {{ url('/') }}" data-id="{{ $Post->id }}">
                                                    <span>Share</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    @if (count($RecentPosts))
                                        <div class="cnt-about-top">
                                            <div class="top-info">
                                                <span>Recent Posts</span>
                                            </div>
                                            <ul class="cnt-about-recent-posts">
                                                @foreach ($RecentPosts as $recentPost)
                                                    <li>
                                                        <div class="img-wrap">
                                                            <a href="{{ route('single_post', $recentPost->id) }}">
                                                                <img src="{{ asset($recentPost->user()->avatar) }}"
                                                                     alt=""
                                                                     style="max-width: 50px">
                                                            </a>
                                                        </div>
                                                        <div class="text-box">
                                                            <p>
                                                                <a href="{{ route('single_post', $recentPost->id) }}"
                                                                   style="color: #000; font-size: 16px;">{{ $recentPost->title }}</a>
                                                                @if (!empty($recentPost->subtitle))
                                                                    <br>
                                                                    {{ $recentPost->subtitle }}
                                                                @endif
                                                            </p>
                                                            <div class="data">{{ \App\DateConvert::Convert($recentPost->created_at) }}</div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <div class="cnt-about-see-all-post">
                                                <a href="{{ is_null($Group) ? route('news-and-events-id', $Post->user()->id) : route('group', $Post->group_id) }}"
                                                   style=" color: #000; ">See All Posts
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="tab-content-item" style="display: block;">
                                @if (Auth::check())
                                    <script>
                                        let comments_full_update_default = [];
                                    </script>
                                    <div class="top-post-box">
                                        <div class="top-post-options">
                                            <ul>
                                                <li>
                                                    <a class="comment_form_status">Write Comment</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="comment_image">
                                                        <img src="/img/photo-placeholder.svg" alt="">
                                                        Photo
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="post-content">
                                            <div class="img-wrap">
                                                <img src="{{ asset(Auth::user()->avatar) }}" style=" max-width: 50px; ">
                                            </div>
                                            <div class="text-wrap">
                                                <form class="add_comment_single" data-id="{{ $Post->id }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="post_id" value="{{ $Post->id }}">
                                                    <input type="hidden" name="image" value="">
                                                    <textarea placeholder="Write a comment for post"
                                                              name="comment"></textarea>
                                                    <img class="comment_img"
                                                         style="max-width: 100px; max-height: 300px; margin-top: 0px;">
                                                    <input type="file" style="display: none;">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @php
                                    $Comments = \App\PostComment::where('post_comments.post_id', $Post->id)->whereNull('post_comments.reply_id')->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->orderBy('post_comments.id', 'DESC')->get();
                                @endphp
                                @foreach ($Comments as $Comment)
                                    <script>
                                        comments_full_update_default.push({{ $Comment->id }});
                                    </script>
                                    <div class="dis-post-item coment_block" data-id="{{ $Comment->id }}">
                                        <div class="dis-top-panel">
                                            <div class="img-wrap">
                                                <a href="{{ url('/profile/id/'.$Comment->user_id) }}">
                                                    <img src="{{ asset($Comment->user_avatar) }}"
                                                         alt="{{ $Comment->user_name }}"
                                                         title="{{ $Comment->user_name }}">
                                                </a>
                                            </div>
                                            <div class="dis-text-box">
                                                <span><a href="{{ url('/profile/id/'.$Comment->user_id) }}">{{ $Comment->user_name }}</a> - {{ \App\DateConvert::Convert($Comment->created_at) }}</span>
                                                <!-- <span>You, Ashley Murray, Dirk Diggler &amp; 12 others Commented</span> -->
                                            </div>
                                        </div>
                                        <div class="dis-content">
                                            <p style="{{ empty($Comment->comment) ? 'margin-bottom: 0px;' : '' }}">
                                                {{ $Comment->comment }}
                                            </p>
                                            @if ($Comment->image != NULL)
                                                <a href="{{ asset($Comment->image) }}"
                                                   data-lightbox="image-{{ $Comment->id }}">
                                                    <img src="{{ asset($Comment->image) }}"/>
                                                </a>
                                            @endif
                                        </div>
                                        <div class="dis-bottom-panel">
                                            @php
                                                $PostCommentLike = NULL;
                                                if (Auth::check())
                                                    $PostCommentLike = \App\PostCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                                                $count_likes = \App\PostCommentLike::where('comment_id', $Comment->id)->count();
                                            @endphp
                                            <a href="#"
                                               class="dis-action like_comment @if($PostCommentLike != NULL) liked @endif"
                                               @if(\Auth::check()) data-id="{{ $Comment->id }}" @endif>
                                                <img src="{{ is_null($PostCommentLike) ? '/img/dis-like-icon.svg' : '/img/full-like-icon-like.svg' }}"
                                                     alt="">
                                                <span style=" padding-right: 5px; ">Like</span>
                                                <span>@if($count_likes > 0){{ $count_likes }}@endif</span>
                                            </a>
                                            <a href="#" class="dis-action reply_comment_single"
                                               @if(\Auth::check()) data-id="{{ $Comment->id }}"
                                               data-post-id="{{ $Post->id }}" @endif>
                                                <img src="/img/dis-com-icon.svg" alt="">
                                                <span>Reply</span>
                                            </a>
                                        </div>
                                    </div>
                                    @php
                                        $ReplyComments = \App\PostComment::where('post_comments.post_id', $Post->id)->where('post_comments.reply_id', $Comment->id)->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->orderBy('post_comments.id', 'DESC')->get();
                                    @endphp
                                    @foreach ($ReplyComments as $ReplyComment)
                                        <script>
                                            comments_full_update_default.push({{ $ReplyComment->id }});
                                        </script>
                                        <div class="dis-post-item coment_block reply-comment-block"
                                             data-id="{{ $ReplyComment->id }}"
                                             data-reply-id="{{ $ReplyComment->reply_id }}">
                                            <div class="dis-top-panel">
                                                <div class="img-wrap">
                                                    <a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}">
                                                        <img src="{{ asset($ReplyComment->user_avatar) }}"
                                                             alt="{{ $ReplyComment->user_name }}"
                                                             title="{{ $ReplyComment->user_name }}">
                                                    </a>
                                                </div>
                                                <div class="dis-text-box">
                                                    <span><a href="{{ url('/profile/id/'.$ReplyComment->user_id) }}">{{ $ReplyComment->user_name }}</a> - {{ \App\DateConvert::Convert($ReplyComment->created_at) }}</span>
                                                    <!-- <span>You, Ashley Murray, Dirk Diggler &amp; 12 others Commented</span> -->
                                                </div>
                                            </div>
                                            <div class="dis-content">
                                                <p>
                                                    {{ $ReplyComment->comment }}
                                                    @if ($ReplyComment->image != NULL)
                                                        <a href="{{ asset($ReplyComment->image) }}"
                                                           data-lightbox="image-{{ $ReplyComment->id }}">
                                                            <img src="{{ asset($ReplyComment->image) }}"/>
                                                        </a>
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="dis-bottom-panel">
                                                @php
                                                    $PostCommentLike = NULL;
                                                    if (Auth::check())
                                                        $PostCommentLike = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->where('user_id', Auth::user()->id)->first();
                                                    $count_likes = \App\PostCommentLike::where('comment_id', $ReplyComment->id)->count();
                                                @endphp
                                                <a href="#"
                                                   class="dis-action like_comment @if($PostCommentLike != NULL) liked @endif"
                                                   @if(\Auth::check()) data-id="{{ $ReplyComment->id }}" @endif>
                                                    <img src="/img/dis-like-icon.svg" alt="">
                                                    <span style=" padding-right: 5px; ">Like</span>
                                                    <span>@if($count_likes > 0){{ $count_likes }}@endif</span>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '177979539749540',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v3.1'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function () {

            $("html, body").animate({scrollTop: $('.top-post-box').offset().top - 206}, "fast");

        });
    </script>
@endsection