@include('user.layouts.header')
<section id="profile" class="profile main_wrap">
    <div class="container-fluid">
        <div class="main-container">
            <div class="left_big_container">
                <div class="calendar_navigation">
                    <a href="#">
                        <img src="../img/arrow_calendar_left.svg" alt="Alternate Text">
                    </a>
                    <span>September 2018</span>
                    <a href="#">
                        <img src="../img/arrow_calendar_right.svg" alt="Alternate Text">
                    </a>
                </div>
                <table class="calendar">
                    <thead>
                    <tr>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                        <th>Sunday</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal-bday">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block name_whose_birthday">Lisa Annes</div>
                                <p>Birthday</p>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                    </tr>
                    <tr>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal-event">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block caption_calendar">The European Summit</div>
                                <p>Bada Bing Club Copenhagen</p>
                                <div class="calendar_going">
                                    <div class="going_photo"><img src="../img/going_icc_person.jpg" alt="Alternate Text"></div>
                                    <div class="going_text"><div class="going_name">Luka Ross <span>going </span></div></div>
                                </div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal-webcam">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block DayCal-webcam_tittle">LIVE WEB CAM</div>
                                <p>11pm EST <br/> Don't Miss it</p>
                            </a>
                        </th>
                        <th class="DayCal-bday">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block name_whose_birthday">Lisa Annes</div>
                                <p>Birthday</p>
                            </a>
                        </th>
                    </tr>
                    <tr>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal-webchat">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block webchat_tittle">LIVE WEB CHAT</div>
                                <p>11pm EST <br/>Don’t Miss it </p>
                            </a>
                        </th>
                        <th class="DayCal-bday">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block name_whose_birthday">Lisa Annes</div>
                                <p>Birthday</p>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                    </tr>
                    <tr>
                        <th class="DayCal-bday">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block name_whose_birthday">Lisa Annes</div>
                                <p>Birthday</p>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal-event">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block caption_calendar">The European Summit</div>
                                <p>Bada Bing Club Copenhagen</p>
                                <div class="calendar_going">
                                    <div class="going_photo"><img src="../img/going_icc_person.jpg" alt="Alternate Text"></div>
                                    <div class="going_text"><div class="going_name">Luka Ross <span>going </span></div></div>
                                </div>
                            </a>
                        </th>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                    </tr>
                    <tr>
                        <th class="DayCal">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="DayCal-event">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block caption_calendar">The European Summit</div>
                                <p>Bada Bing Club Copenhagen</p>
                                <div class="calendar_going">
                                    <div class="going_photo"><img src="../img/going_icc_person.jpg" alt="Alternate Text"></div>
                                    <div class="going_text"><div class="going_name">Luka Ross <span>going </span></div></div>
                                </div>
                            </a>
                        </th>
                        <th class="DayCal-bday">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                                <div class="calendar_tittle_block name_whose_birthday">Lisa Annes</div>
                                <p>Birthday</p>
                            </a>
                        </th>
                        <th class="NextMonthDates">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="NextMonthDates">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="NextMonthDates">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                        <th class="NextMonthDates">
                            <a href="#">
                                <div class="calendar_data">1st</div>
                            </a>
                        </th>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="sidebar">
                <div class="m_l_top">
                    <div class="bl_person_photo">
                        <img src="../img/avatar-profile.png" alt="Alternate Text">
                    </div>
                    <div class="bl_person_inf">
                        <div>Location: Houston Texas</div>
                        <div class="data-dob">DOB: 03/07/78</div>
                    </div>
                </div>
                <div class="m_container_inf">
                    <div class="b_block_inf">Connections - <a href="#">14,657</a></div>
                    <div class="b_block_inf">Companies - <a href="#">435</a></div>
                    <div class="b_block_inf">Groups - <a href="#">1,200</a></div>
                    <div class="b_block_inf">Your Posts - <a href="#">743</a></div>
                </div>
                <div class="m_container_soc">
                    <div class="message_calendar_block">
                        <div><img src="../img/message_iccon.svg" alt="Alternate Text"></div>
                        <a href="#">Message</a>
                    </div>
                    <div class="message_calendar_block">
                        <div><img src="../img/calendar_icc.svg" alt="Alternate Text"></div>
                        <a href="#">Calendar</a>
                    </div>
                    <div class="m_soc_ic">
                        <a href="#"><img src="../img/soc_ic_1.jpg" alt="Alternate Text"></a>
                        <a href="#"><img src="../img/soc_ic_2.jpg" alt="Alternate Text"></a>
                        <a href="#"><img src="../img/soc_ic_3.jpg" alt="Alternate Text"></a>
                    </div>
                </div>
                <a href="#" class="logout-link">Logout of Account</a>
            </div>

        </div>
    </div>
</section>
@include('user.layouts.footer')