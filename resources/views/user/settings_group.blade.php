@extends('layouts.layout-full')

@section('content')
    <div id="UploadAvatarBox">
        <img src="" />
        <input type="button" id="CancelCropp" value="Cancel">
        <input type="button" id="ApplyCropp" value="Apply">
    </div>
    <section id="profile" class="profile main_wrap profile_edit">
        <div class="container-fluid">
            <div class="main-container">

                <div class="profile-content overflow_block" style="width: calc(84% - 50px);">
                    <h2 class="edit_iccon">
                        <span>Group Name</span>
                        <input class="edit_input" type="text" name="GroupName" value="{{ $Group->name }}"/>
                    </h2>
                    <div class="tab-wrapper profile-tab">
                        <div class="tabs">
                            <span class="tab active">Basic info:</span>
                            <span class="tab">Gallery:</span>
                            <span class="tab">Videos:</span>
                            @if (count($GroupRequest))
                                <span class="tab">Requests ({{ count($GroupRequest) }}):</span>
                            @endif
                        </div>
                        <div class="tab_content">
                            <div class="tab_item">
                                <form id="GroupInfoEdit">
                                    <input type="hidden" name="GroupName" value="{{ $Group->name }}">
                                    <input type="hidden" name="group_id" value="{{ $Group->id }}">
                                    {{ csrf_field() }}

                                    <div class="post-item edit_iccon blog_edit">
                                        <span class="post-title edit_iccon basic_inf_tittle">Basic Info</span>
                                        <div class="table-grup">
                                            <div class="left-table">
                                                <table class="table-1">
                                                    <tr>
                                                        <td>Category:</td>
                                                        <td>
                                                            <select name="GroupCategories" class="edit_input main_input">
                                                                @php
                                                                    $GroupCategories = \App\GroupCategory::get();
                                                                @endphp
                                                                @foreach ($GroupCategories as $GroupCategory)
                                                                <option value="{{ $GroupCategory->id }}" @if($GroupCategory->id == $Group->category_id) selected @endif>{{ $GroupCategory->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Group type:</td>
                                                        <td>
                                                            <select name="GroupType" class="edit_input main_input">
                                                                <option value="public" @if($Group->type == 'public') selected @endif>Public</option>
                                                                <option value="private" @if($Group->type == 'private') selected @endif>Private</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-table">
                                                <table class="table-2">
                                                    <tr>
                                                        <td>Keywords:</td>
                                                        <td>
                                                            <input class="edit_input main_input" type="text" name="Keywords" value="{{ $Group->keywords }}"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="post-item basic_info_edit">
                                        <span class="post-title edit_iccon basic_inf_tittle">Avatar</span>
                                         <span class="upload-button-wrapper">
                                            Upload Avatar
                                            <input type="file" id="AvatarUpload">
                                        </span>
                                        <input type="hidden" name="avatar" value="{{ $Group->avatar }}">
                                    </div>

                                    <div class="post-item edit_iccon blog_edit">
                                        <span class="post-title">Description <span>Limited Characters: 1000</span></span>
                                        <textarea class="textarea_edit" name="Description">{{ $Group->description }}</textarea>
                                    </div>

                                    <div class="btn_follow GroupInfoEdit_save">Save changes</div>

                                </form>
                            </div>
                            <div class="tab_item">
                                <div class="slider" style='width: 50%; display: inline-block;'>
                                    <div class="add_photo_container">
                                        <div class="photo_edit_block">
                                            <img src="{{ asset('img/camera_upload.svg') }}" alt="Alternate Text">
                                            <div class="upl_gall_img">Upload Gallery </br> Images</div>
                                            <div class="sell_your_img">You can sell your images?</div>
                                        </div>
                                        <div class="add_to_gallary">
                                            <div class="add_photo_name">Add to Gallery</div>
                                            <div class="add_to_gallary_container">
                                                @for ($i = 0; $i < 6; $i++)
                                                    <div class="add_to_gallary_block" data-id="{{ $i }}">
                                                        <div class="add_input_block">
                                                            <form class="imageUploadForm" data-id="{{ $i }}">
                                                                <span class="helpText">Add Photo</span>
                                                                <input type="hidden" name="photo" value="">
                                                                <input type="hidden" name="preview" value="">
                                                                <input type='file' class="uploadButton file" accept="image/*"/>
                                                                <div class="uploadedImg">
                                                                    <span class="unveil"></span>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <input class="upload_input_text" type="text" name="title" value="" placeholder="Title Image"/>

                                                        <textarea name="description" class="upload_textarea"></textarea>

                                                    </div>
                                                @endfor
                                                <div class="upload_to_gallery_btn"><span class="btn_follow">Upload to Gallery</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="photos-box main-box">
                                    @foreach ($Gallery as $photo)
                                        <div class="item" data-id="{{ $photo->id }}">
                                            <div class="photo-wraper">
                                                <img src="{{ asset($photo->file_name) }}" alt="Alternate Text">
                                                <div class="photo-bottom-block">
                                                    <a href="{{ route('photo_group', ['id' => $Group->id, 'pid' => $photo->id]) }}"><span class="title">{{ $photo->title }}</span></a>
                                                </div>
                                            </div>
                                            <div class="photo-information-wrapper">
                                                <div class="photo-bottom-footer">
                                                    <a>Likes: &nbsp;<span>{{ \App\PhotoLike::countLike($photo->id) }}</a>
                                                    <a href="#" class="delete_photo" data-id="{{ $photo->id }}">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab_item">
                                <div class="slider" style='width: 50%; display: inline-block;'>
                                    <div class="add_photo_container">
                                        <div class="photo_edit_block">
                                            <img src="{{ asset('img/camera_upload.svg') }}" alt="Alternate Text">
                                            <div class="upl_gall_img">Upload Gallery </br> Videos</div>
                                        </div>
                                        <div class="add_to_gallary">
                                            <div class="add_photo_name">Add to Gallery</div>
                                            <div class="add_to_gallary_container">
                                                @for ($i = 0; $i < 6; $i++)
                                                    <div class="add_to_gallary_block" data-id="{{ $i }}">
                                                        <div class="add_input_block">
                                                            <form class="imageUploadForm" data-id="{{ $i }}">
                                                                <span class="helpText">Add Video</span>
                                                                <input type="hidden" name="photo" value="">
                                                                <input type='file' class="uploadButton file" accept="video/*"/>
                                                                <div class="uploadedImg">
                                                                    <span class="unveil"></span>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <input class="upload_input_text" type="text" name="title" value="" placeholder="Title Image"/>
                                                        <textarea name="description" class="upload_textarea"></textarea>
                                                    </div>
                                                @endfor
                                                <div class="upload_to_gallery_btn video_gallery"><span class="btn_follow">Upload to Gallery</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="photos-box main-box">
                                    @foreach ($VideoGallery as $video)
                                        <div class="item" data-id="{{ $video->id }}">
                                            <div class="photo-wraper">
                                                <img src="{{ asset($video->preview) }}" alt="Alternate Text">
                                                <div class="photo-bottom-block">
                                                    <a href="{{ route('video_group', ['id' => $Group->id, 'vid' => $video->id]) }}"><span class="title">{{ $video->title }}</span></a>
                                                </div>
                                            </div>
                                            <div class="photo-information-wrapper">
                                                <div class="photo-bottom-footer">
                                                    <a>Likes: &nbsp;<span>{{ \App\VideoLike::countLike($video->id) }}</a>
                                                    <a href="#" class="delete_video" data-id="{{ $video->id }}">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @if (count($GroupRequest))
                                <div class="tab_item">
                                    @foreach ($GroupRequest as $req)
                                        <div class="groups-item follow-item" data-id="{{ $req->user_id }}">
                                            <a href="{{ route('id_profile', ['id' => $req->user_id]) }}" class="groups-item-title">
                                                <img src="{{ asset($req->avatar) }}" alt="">
                                            </a>
                                            <div class="groups-item-content">
                                                <a href="{{ route('id_profile', ['id' => $req->user_id]) }}" class="groups-item-title">
                                                    <b>{{ $req->name }}</b>
                                                </a>
                                                <a href="#" class="groups-item-wheel groups_follow btn_follow button_request group_request_approve" data-id="{{ $req->id }}">Approve</a>
                                                <a href="#" class="groups-item-wheel groups_follow btn_follow button_request group_request_cancel" data-id="{{ $req->id }}">Cancel</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>

                </div>

                @include('includes.right-sidebar-group')
            </div>


        </div>
        </div>
    </section>
@endsection