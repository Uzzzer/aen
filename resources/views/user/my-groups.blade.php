@extends('layouts.layout-full')

@section('content')
    <section id="profile" class="profile main_wrap">
        <div class="container-fluid">
            <div class="main-container">
                <div class="gallary">

                    @include('includes.new-group')

                    <div class="gallary_top">
                        <div class="gal_top_left">
                            <span>My Groups</span>
                        </div>
                        <a href="{{ route('groups') }}" class="groups-tab-link">All groups</a>
                        <a href="{{ route('subscribed-groups') }}" class="groups-tab-link">Subscribed groups</a>
                        @if (!in_array($UserType, ['user', 'public_user', false]))
                            <a href="#" class="create-group-button group-bar-link">Create Group +</a>
                        @endif
                    </div>
                    <div class="grops-grid groups_list">
                        @foreach ($Groups as $Group)
                            <div class="groups-item" data-id="{{ $Group->id }}">
                                <img src="{{ asset($Group->avatar) }}" alt="">
                                <div class="groups-item-content">
                                    <a href="{{ route('group', ['id' => $Group->id]) }}" class="groups-item-title">
                                        <b>{{ $Group->name }}</b>
                                        <span>{{ \App\Post::countUnreadGroupPosts($Group->id) }}</span>
                                    </a>
                                    <a href="#" class="groups-item-wheel"></a>
                                    <ul class="groups-item-options">
                                        <li>
                                            <a href="{{ route('notifications_group', ['id' => $Group->id]) }}">Edit notification settings</a>
                                        </li>
                                        @if (\App\GroupFollower::CheckFollowGroup($Group->id))
                                            <li>
                                                <a href="#" class="leave_group" data-id='{{ $Group->id }}'>Leave group
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="#" class="join_group" data-id='{{ $Group->id }}'>Join group</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="#">Pin to shortcuts</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @include('includes.right-sidebar')
            </div>


        </div>
    </section>
@endsection