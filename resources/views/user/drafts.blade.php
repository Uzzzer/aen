@extends('layouts.layout')

@section('content')
    <input type="hidden" name="load_page_type" value="load_draft">
    <div class="post_load_list">
        @foreach ($Drafts as $Draft)
            <div class="right_container post_box" data-id="{{ $Draft->id }}">
                <div class="m_r_top">
                    <div class="m_rl_top">
                        <div class="ph_person">
                            <a href="{{ url('/profile/id/'.$user->id) }}">
                                <img src="{{ asset($user->avatar) }}" alt="Alternate Text"/>
                            </a>
                        </div>
                        <div class="person_text">
                            <p>
                                <span><a href="{{ url('/profile/id/'.$user->id) }}"
                                         class="user-name-link">{{ $user->name }}</a>,</span> {{ $Draft->title }} - {{ \App\DateConvert::Convert($Draft->created_at) }}
                            </p>
                            <p class="fon-f-light">
                                <!-- You, Ashley Murray, Dirk Diggler & 12 others Commented -->
                            </p>
                        </div>
                    </div>
                    <div class="m_rr_top">
                        <a href="#"
                           class="publish_draft_post"
                           data-id="{{ $Draft->id }}">
                            <i class="fa fa-paper-plane-o" aria-hidden="true" style=" font-size: 20px; margin-top: -4px; "></i>
                            <span>Publish</span>
                        </a>
                        <a href="#"
                           class="edit_draft_post"
                           data-id="{{ $Draft->id }}">
                            <i class="fa fa-pencil-square-o" aria-hidden="true" style=" font-size: 20px; margin-top: -4px; "></i>
                            <span>Edit</span>
                        </a>
                        <a href="#" class="delete_post"
                           data-id="{{ $Draft->id }}">
                            <i class="fa fa-trash" aria-hidden="true" style=" font-size: 20px; margin-top: -4px; "></i>
                            <span>Delete</span>
                        </a>
                    </div>
                </div>
                @if ($Draft->file_type == 'image')
                    <div class="m_bg_container" style="background-image: url({{ asset($Draft->file_name) }});">
                        <img src="{{ asset($Draft->file_name) }}" alt="">
                    </div>
                @endif
                @if ($Draft->file_type == 'video')
                    <video width="100%" height="400px"
                           controls {!!  $Draft->video_preview ? 'poster="' . asset($Draft->video_preview) . '"' : '' !!}>
                        <source src="{{ asset($Draft->file_name) }}">
                        Your browser does not support the video tag.
                    </video>
                @endif
                <div class="m_bot_container">
                    <div class="m_data">
                        @if ($Draft->post_type == 'news')
                            <i class="fa fa-newspaper-o" style="font-size: 24px; margin-right: 15px;"
                               aria-hidden="true"></i>
                        @elseif ($Draft->post_type == 'online')
                            <i class="fa fa-video-camera" aria-hidden="true"
                               style="font-size: 24px; margin-right: 15px;"></i>
                        @else
                            <i class="fa fa-calendar" style="font-size: 24px; margin-right: 15px;"
                               aria-hidden="true"></i>
                        @endif

                        {{ $Draft->title }}
                        @if ($Draft->post_type == 'event')
                            <span>{{ date('jS F Y', strtotime($Draft->start)) }} Local Time, {{ $Draft->location }}</span>
                        @endif
                        @if ($Draft->post_type == 'online')
                            <span>{{ date('jS F Y', strtotime($Draft->start)) }}, {{ date('H:i', strtotime($Draft->start_time)) }} - {{ $Draft->start == $Draft->end ? '' : date('jS F Y', strtotime($Draft->end)) . ',' }} {{ date('H:i', strtotime($Draft->end_time)) }}</span>
                        @endif
                    </div>
                    <div class="m-bot-text">
                        {!! $Draft->description !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection