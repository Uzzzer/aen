 @extends('layouts.profile')

@section('content')
<style>

.upload_progress{
	position: absolute;
	top: calc(50% - 10px);
	width: 100%;
	border: 1px solid;
	text-align: center;
}
.upload_progress .percent{
	background: gray;
	height: 20px;
	position: absolute;
	top: 0;
	left: 0;
}
.upload_progress .percent_text{
	color: white;
	z-index: 100000;
	position: absolute;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
}



/* Firefox */
input[type=number] {
  -moz-appearance:textfield;
	background: inherit;
	border: 0.5px solid
	#979797;
	width: 100%;
	height: 20px;
	padding-left: 7px;
	font-family: 'Poppins', sans-serif;
	font-weight: 300;
	line-height: 23px;
	font-size: 11px;
	margin: 0 0 7px;
	color: #4A4A4A;
}


</style>
    <div id="UploadAvatarBox">
        <img src=""/>
        <input type="button" id="CancelCropp" value="Cancel">
        <input type="button" id="ApplyCropp" value="Apply">
    </div>
    <section id="profile" class="profile main_wrap profile_edit">
        <div class="container-fluid">
            <div class="main-container">
                <div class="slider">
                    <div class="add_photo_container">
                        <div class="photo_edit_block">
                            <img src="{{ asset('img/camera_upload.svg') }}" alt="Alternate Text">
                            <div class="upl_gall_img">Upload Gallery </br> Images</div>
                            <div class="sell_your_img">You can sell your images?</div>
                        </div>
                        <div class="add_to_gallary">
                            <div class="add_photo_name">Add to Gallery</div>
                            <div class="add_to_gallary_container">
                                @for ($i = 0; $i < 6; $i++)
                                    <div class="add_to_gallary_block" data-id="{{ $i }}">
                                        <div class="add_input_block">
                                            <form class="imageUploadForm" data-id="{{ $i }}">
                                                <span class="helpText">Add Photos/Videos</span>
                                                <input type="hidden" name="photo" value="">
                                                <input type="hidden" name="preview" value="">
                                                <input type='file' class="uploadButton file" accept="image/*"/>
                                                <div class="uploadedImg">
                                                    <span class="unveil"></span>
                                                </div>
                                            </form>
                                        </div>
                                        <input class="upload_input_text" type="text" name="title" value=""
                                               placeholder="Title Image"/>

                                        <textarea name="description" class="upload_textarea" placeholder="Description"></textarea>
                                        <input class="price" type="number" step="0.01" name="title" value=""
                                               placeholder="Price" />

                                    </div>
                                @endfor
                                <div class="upload_to_gallery_btn">
                                    <span class="btn_follow">Upload to Gallery</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-content overflow_block">
                    <h2 class="edit_iccon">
                        <span>About</span>
                        <input class="edit_input" type="text" name="name" value="{{ $user->name }}"
                               style=" width: calc(100% - 111px); "/>
                    </h2>
                    <div class="lady_location edit_iccon">
                        <span>Location: </span>
                        <input class="edit_input" type="text" name="location" value="{{ $user->location }}"
                               style=" width: calc(100% - 111px); "/>
                    </div>
                    <div class="tab-wrapper profile-tab">
                        <div class="tabs">
                            <span class="tab @if(!isset($_REQUEST['tab']) or $_REQUEST['tab'] == 'basic-info'){{ 'active' }}@endif">Basic info:</span>
                            @if (in_array(\App\User::isType($user->id), ['business']))
                                <span class="tab @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'job-alerts'){{ 'active' }}@endif">Job Alerts:</span>
                            @endif
                            <span class="tab @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'videos'){{ 'active' }}@endif">Videos:</span>
                            <span class="tab @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'profile'){{ 'active' }}@endif">Profile:</span>
                            <span class="tab @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'social'){{ 'active' }}@endif">Social:</span>
                        </div>
                        <div class="tab_content">
                            <div class="tab_item"
                                 @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] != 'basic-nfo') style="display: none;" @endif>
                                <form id="ProfileInfoEdit">
                                    <input type="hidden" name="name" value="{{ $user->name }}">
                                    <input type="hidden" name="location" value="{{ $user->location }}">
                                    {{ csrf_field() }}
                                    <div class="post-item">
                                        <span class="post-title edit_iccon">Subscription</span>
                                        @if (is_null($user->subscription_end) or date('Y-m-d H:i:s') >= $user->subscription_end)
                                            <a href="{{ route('subscription') }}">
                                                <div class="btn_follow">Subscribe</div>
                                            </a>
                                        @else
                                            <h3 style=" font-size: 16px; color: #fff; ">Subscription end:
                                                <span style=" color: #D0021B; ">{{ date('d M Y H:i', strtotime($user->subscription_end)) }}</span>
                                            </h3>
                                            <a href="{{ route('subscription', ['extend' => 'y']) }}">
                                                <div class="btn_follow">To extend</div>
                                            </a>
                                        @endif
                                    </div>

                                    <div class="post-item basic_info_edit">
                                        <p class="avaliable_for">
                                            <span>Add your PayPal account for payouts</span>
                                        </p>
                                        <input type="text" name="paypal" id="paypal" class="edit_input"
                                               value="{{ $user->paypal }}">
                                    </div>

                                    <p class="avaliable_for">
                                        <span>Categories</span>
                                    </p>
                                    <div class="avaliable_checkbox" style="display: block;">
                                        @php
                                            $avaliable = json_decode($user->avaliable, true);

                                            if (!is_array($avaliable))
                                                $avaliable = [];

                                            $CategoriesAvailable = \App\CategoriesAvailable::get();
                                        @endphp
                                        @foreach ($CategoriesAvailable as $CategoryAvailable)
                                            <label class="avaliable_label">
                        						<span class="switch">
                        							  <input type="checkbox" data-val="{{ $CategoryAvailable->name }}"
                                                             @if (in_array($CategoryAvailable->name, $avaliable)) checked @endif>
                        						   	  <span class="checkbox_block"></span>
                        						</span>
                                                <span class="checkbox_name">{{ $CategoryAvailable->name }}</span>
                                            </label>
                                        @endforeach
                                        @foreach (\App\UsersCategoriesAvailable::getUserCategories() as $category)
                                            <label class="avaliable_label">
                        						<span class="switch">
                        							  <input type="checkbox" data-val="{{ $category->name }}"
                                                             @if (in_array($category->name, $avaliable)) checked @endif>
                        						   	  <span class="checkbox_block"></span>
                        						</span>
                                                <span class="checkbox_name">{{ $category->name }}</span>
                                            </label>
                                        @endforeach

                                    </div>
                                    <a href="#" id="AddCategory"
                                       style=" color: #D0021B; margin-top: 10px; display: inline-block; ">Add Category
                                    </a>

                                    <div class="post-item basic_info_edit">
                                        <span class="post-title edit_iccon basic_inf_tittle">Basic Info</span>
                                        <div class="table-grup">
                                            <div class="left-table">
                                                <table class="table-1">
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Nationality:</td>
                                                        <td>
                                                            <input class="edit_input" type="text" name="nationality"
                                                                   value="{{ $user->nationality }}" placeholder="American"/>
                                                        </td>
                                                        {{--<td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">American</td>--}}
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Birthday:</td>
                                                        <td>
                                                            <input class="edit_input birthday_input" type="text" name="birthday"
                                                                   value="{{ $user->birthday }}" placeholder="30/01/1990"/>
                                                        </td>
                                                        {{--<td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">30/01/1990</td>--}}
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Profession:</td>
                                                        <td>
                                                            <input class="edit_input" type="text" name="profession"
                                                                   value="{{ $user->profession }}" placeholder="Actor"/>
                                                        </td>
                                                        {{--<td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">Actor</td>--}}
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Experience:</td>
                                                        <td>
                                                            <input class="edit_input" type="number" name="experience"
                                                                   value="{{ $user->experience }}" placeholder="3 (years)"/>
                                                        </td>
                                                        {{--<td style=" padding-left: 10px; font-size: 80%; opacity: 0.5; ">3 (years)</td>--}}
                                                    </tr>
                                                    <tr>
                                                        <td style=" padding-right: 10px; ">Sex:</td>
                                                        <td>
                                                            <select name="sex" class="edit_input">
                                                                <option @if ($user->sex == 'Female') selected
                                                                        @endif value="Female">Female
                                                                </option>
                                                                <option @if ($user->sex == 'Male') selected
                                                                        @endif value="Male">Male
                                                                </option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-table">
                                                <table class="table-2">
                                                    <tbody><tr>
                                                        <td>Height:</td>
                                                        <td><input class="edit_input" type="text" name="height" placeholder="5ft 6inchs" value="{{ $user->height }}"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Weight:</td>
                                                        <td><input class="edit_input" type="text" name="weight" placeholder="65kg" value="{{ $user->weight }}"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Chest:</td>
                                                        <td><input class="edit_input" type="text" name="chest" placeholder="34 EE" value="{{ $user->chest }}"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Waist:</td>
                                                        <td><input class="edit_input" type="text" name="waist" placeholder="26" value="{{ $user->waist }}"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hips:</td>
                                                        <td><input class="edit_input" type="text" name="hips" placeholder="36" value="{{ $user->hips }}"></td>
                                                    </tr>
                                                    </tbody></table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="post-item basic_info_edit">
                                        <span class="post-title edit_iccon basic_inf_tittle">Avatar</span>
                                        <span class="upload-button-wrapper">
                                            Upload Avatar
                                            <input type="file" id="AvatarUpload">
                                        </span>
                                        <input type="hidden" name="avatar" value="{{ $user->avatar }}">
                                    </div>

                                    <div class="post-item basic_info_edit">
                                        <span class="post-title edit_iccon basic_inf_tittle">Website</span>
                                        <input type="text" name="website" class="edit_input"
                                               value="@if($user->website == NULL){{ 'http://' }}@else{{ $user->website }}@endif">
                                    </div>

                                    <div class="post-item edit_iccon blog_edit">
                                        <span class="post-title">Blog <span>Limited Characters: 1000</span></span>
                                        <textarea class="textarea_edit" name="about_me">{{ $user->about_me }}</textarea>
                                    </div>

                                    <div class="btn_follow ProfileInfoEdit_save">Save changes</div>

                                </form>
                            </div>
                            @if (in_array(\App\User::isType($user->id), ['business']))
                                <div class="tab_item"
                                     @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'job-alerts') style="display: block;" @endif>

                                    <a href="#" class="job-bar-link open-bar-link">Add Job</a>
                                    @include('includes.new-job')
                                    <div class="jobs_list">
                                        @php
                                            $Jobs = \App\Job::where('user_id', $user->id)->orderBy('id', 'desc')->get();
                                        @endphp
                                        @foreach ($Jobs as $Job)
                                            @php
                                                $fields = json_decode($Job->fields, true);
                                            @endphp
                                            <div class="tab_job_block job_box" data-id="{{ $Job->fields }}">
                                                <div class="tab_job_block_top">
                                                    <div class="tab_job_left">
                                                        <div class="job_tittle">{{ $Job->title }}</div>
                                                        @if(isset($fields['Location']))
                                                            <div class="job_location">Location: {{ $fields['Location'] }}</div>@endif
                                                        @if(!is_null($Job->user_job))
                                                            @php
                                                                $UserJob = \App\User::find($Job->user_job);
                                                            @endphp
                                                            @if (!is_null($UserJob))
                                                                <div class="job_location">
                                                                    <a href="{{ route('id_profile', ['id' => $UserJob->id]) }}">{{ $UserJob->name }}</a>
                                                                </div>
                                                            @endif
                                                        @endif
                                                        <div class="job_data">{{ \App\DateConvert::Convert($Job->created_at) }}</div>
                                                    </div>
                                                    <div class="tab_job_right ">
                                                        <div class="job_right_text job_right_textDetails">
                                                            <a href="#" class="delete_job"
                                                               data-id="{{ $Job->id }}">@if(!is_null($Job->user_job)){{ 'End job' }}@else{{ 'Delete' }}@endif</a>
                                                            <a href="#" class="edit_job" data-id="{{ $Job->id }}">Edit
                                                            </a>
                                                            <p class="open_job_profile">View Details</p>   <!-- Close Details -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab_job_block_bottom" style="display: none;">
                                                    <div class="tab_job_block_wrap">
                                                        <p>
                                                            {{ $Job->text }}
                                                        </p>
                                                        @if ($Job->responsibilities != NULL)
                                                            @php
                                                                $responsibilities = json_decode($Job->responsibilities, true);
                                                            @endphp
                                                            @if (is_array($responsibilities))
                                                                <h6>Responsibilities</h6>
                                                                <ul>
                                                                    @foreach ($responsibilities as $responsibility)
                                                                        <li>{{ $responsibility }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif
                                                        @endif
                                                        @if (is_array($fields))
                                                            @foreach ($fields as $key => $value)
                                                                <div class="job_info">
                                                                    <span>{{ $key }}: </span>{{ $value }}</div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            @endif
                            <div class="tab_item"
                                 @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'videos') style="display: block;" @endif>
                                <a href="#" class="video-bar-link open-bar-link">Add video</a>
                                @include('includes.new-video')
                                <div class="videos-box coll-two">
                                    @foreach ($Videos as $Video)
                                        <div class="item" data-id="{{ $Video->id }}">
                                            <div class="video-wraper">
                                                <img src="{{ asset($Video->preview_image) }}" alt="{{ $Video->title }}">
                                                <div class="video-bottom-block">
                                                    <div class="left-box">
                                                        <i class="v-icon play"></i>
                                                        <div class="video-bottom-text">
                                                            <span class="title">{{ $Video->title }}</span>
                                                            <p class="lite-text">Run Time: {{ \App\Video::convertTime($Video->time) }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="video-information-wrapper" style="display: none;">
                                                <div class="video-bottom-footer">
                                                    <a>Likes: &nbsp;<span>{{ \App\ProfileVideoLike::countLike($Video->id) }}
                                                    </a>
                                                    <a href="#" class="delete_video" data-id="{{ $Video->id }}">Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab_item"
                                 @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'profile') style="display: block;" @endif>
                                <div class="post-item basic_info_edit">
                                    <span class="post-title edit_iccon basic_inf_tittle">Basic Info</span>
                                    <div class="table-grup">
                                        <form id="ProfileEdit">
                                            {{ csrf_field() }}
                                            <table class="table-1">
                                                <tr>
                                                    <td>E-mail (required):</td>
                                                    <td>
                                                        <input class="edit_input" type="text" name="email"
                                                               value="{{ $user->email }}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Your Password (required):</td>
                                                    <td>
                                                        <input class="edit_input" type="password" name="your_password"
                                                               value=""/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>New Password:</td>
                                                    <td>
                                                        <input class="edit_input" type="password" name="password"
                                                               value=""/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Password confirmation:</td>
                                                    <td>
                                                        <input class="edit_input" type="password"
                                                               name="password_confirmation" value=""/>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="btn_follow ProfileEdit_save">Save changes</div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_item"
                                 @if(isset($_REQUEST['tab']) and $_REQUEST['tab'] == 'social') style="display: block;" @endif>

                                <div class="post-item basic_info_edit">
                                    <span class="post-title edit_iccon basic_inf_tittle">Intagram</span>
                                    <div class="table-grup">
                                        @if (is_null($user->instagram))
                                            <div class="btn_follow add_insta_post" style="width: 100%; margin-right: 10px;">Link Post</div>
                                        @else
                                            <div class="btn_follow add_insta_post" style="width: 100%; margin-right: 10px;"
                                                 data-url="{{ $user->instagram }}">Link Post
                                            </div>
                                        @endif
                                        @if (is_null($user->instagram_link))
                                            <div class="btn_follow add_social_link" data-field="instagram_link"
                                                 style="width: 100%;  margin-left: 10px;">Link Page
                                            </div>
                                        @else
                                            <div class="btn_follow add_social_link" data-field="instagram_link"
                                                 style="width: 100%; margin-left: 10px;"
                                                 data-url="{{ $user->instagram_link }}">Link Page
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="post-item basic_info_edit">
                                    <span class="post-title edit_iccon basic_inf_tittle">Twitter</span>
                                    <div class="table-grup">
                                        @if (is_null($user->twitter))
                                            <div class="btn_follow add_tweet" style="width: 100%; margin-right: 10px;">Link Tweet</div>
                                        @else
                                            <div class="btn_follow add_tweet" style="width: 100%; margin-right: 10px;"
                                                 data-url="{{ $user->twitter }}">Link Tweet
                                            </div>
                                        @endif
                                        @if (is_null($user->twitter_link))
                                            <div class="btn_follow add_social_link" data-field="twitter_link"
                                                 style="width: 100%; margin-left: 10px;">Link Feed
                                            </div>
                                        @else
                                            <div class="btn_follow add_social_link" data-field="twitter_link"
                                                 style="width: 100%; margin-left: 10px;"
                                                 data-url="{{ $user->twitter_link }}">Link Feed
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="post-item basic_info_edit">
                                    <span class="post-title edit_iccon basic_inf_tittle">Facebook</span>
                                    <div class="table-grup">
                                        @if (is_null($user->facebook_link))
                                            <div class="btn_follow add_social_link" data-field="facebook_link"
                                                 style="width: 100%;">Link Page
                                            </div>
                                        @else
                                            <div class="btn_follow add_social_link" data-field="facebook_link"
                                                 style="width: 100%;"
                                                 data-url="{{ $user->facebook_link }}">Link Page
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                {{-- <div class="post-item basic_info_edit">
                                    <span class="post-title edit_iccon basic_inf_tittle">Snapchat</span>
                                    <div class="table-grup">
                                        @if (\App\SocialAuth::check('snapchat'))
                                            <a style="width: 100%;" class="btn_follow"
                                               href="{{ route('logout_instagram') }}">Logout
                                            </a>
                                        @else
                                            <div id="my-login-button-target">
                                            </div>
                                        @endif
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                    </div>

                </div>

                @include('includes.right-sidebar')

            </div>
        </div>
    </section>

    <script>
        /* window.snapKitInit = function () {
            var loginButtonIconId = 'my-login-button-target';
            // Mount Login Button
            snap.loginkit.mountButton(loginButtonIconId, {
                clientId: 'dca734a0-3a91-4b25-8665-ee31d4f63f32',
                redirectURI: 'https://ladies.cgp.systems/social/snapchat/callback',
                scopeList: [
                    'user.display_name',
                    'user.bitmoji.avatar',
                ],
                handleResponseCallback: function () {
                    snap.loginkit.fetchUserInfo()
                        .then(data => console.log('User info:', data));
                },
            });
        };

        // Load the SDK asynchronously
        (function (d, s, id) {
            var js, sjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://sdk.snapkit.com/js/v1/login.js";
            sjs.parentNode.insertBefore(js, sjs);
        }(document, 'script', 'loginkit-sdk')); */
    </script>
@endsection

@section('custom_js')
    @if(!is_null(\Request::input('photo')))
        <script>
            $(document).ready(function () {
                $('.photo_edit_block').click();
            });
        </script>
    @endif
@endsection
