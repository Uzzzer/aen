@extends('layouts.layout')

@section('content')
    <a href="#" class="groups-tab-link" id="NotificationSettingsBoxShow" style="margin-bottom: 25px; display: block; text-align: center;">Settings Notifications</a>

    <div class="avaliable_checkbox NotificationSettingsBox" style="display: none;">
        <form id="NotificationSettings">
            {{ csrf_field() }}
            @foreach ($NotificationTypes as $NotificationType)
                @if (!$NotificationType->business or $user->business)
                    <label class="avaliable_label">
                        <span class="switch">
                            <input type="checkbox" value="{{ $NotificationType->id }}" name="NotificationType[]" @if (in_array($NotificationType->id, $NotificationOn)) checked @endif>
                            <span class="checkbox_block"></span>
                        </span>
                        <span class="checkbox_name">{{ $NotificationType->name_view }}</span>
                    </label>
                @endif
            @endforeach
            <div class="btn_follow" id='NotificationSettingsSubmit' style=" margin-bottom: 15px; ">Save changes</div>
        </form>
    </div>


    <input type="hidden" name="load_page_type" value="load_notifications">
    <div class="post_load_list">
        @foreach ($Notifications as $Notification)
            @php
                $GroupNotice = null;
                $UserNotice = null;

                if (!is_null($Notification->from_group_id)) {
                    $type = 'group';
                    $GroupNotice = \App\Group::find($Notification->from_group_id);
                } else {
                    $type = 'user';
                    $UserNotice = \App\User::find($Notification->from_user_id);
                }

                if (is_null($UserNotice) and is_null($GroupNotice))
                    continue;
            @endphp
            <div class="right_container post_box">
                <div class="m_r_top">
                    <div class="m_rl_top">
                        <div class="ph_person"><a href="@if($type == 'user'){{ route('id_profile', ['id' => $UserNotice->id]) }}@else{{ route('group', ['id' => $GroupNotice->user_id]) }}@endif"><img src="@if($type == 'user'){{ asset($UserNotice->avatar) }}@else{{ asset($GroupNotice->avatar) }}@endif" alt="Alternate Text"/></a></div>
                        <div class="person_text">
                            <p>
                                <span>@if($type == 'group')Group @endif<a href="@if($type == 'user'){{ route('id_profile', ['id' => $UserNotice->id]) }}@else{{ route('group', ['id' => $GroupNotice->user_id]) }}@endif" class="user-name-link">@if($type == 'user'){{ $UserNotice->name }}@else{{ $GroupNotice->name }}@endif</a>,</span> - {{ \App\DateConvert::Convert($Notification->created_at) }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="m_bot_container">
                    <div class="m_data">
                        @php echo $Notification->text; @endphp
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection