<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AEN - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/reset.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/header.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/StyleSheet.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fonts.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ruslan.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/max.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css') }}"
          type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/additional.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/lightbox.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/cropper.min.css') }}" type="text/css" rel="stylesheet">
    @if(isset($css))
        @foreach($css as $src)
            <link rel="stylesheet" href="{{$src}}"/>
        @endforeach
    @endif
</head>
<body>

<!--header -->
<header class="heaedr-main">
    <div class="top-header-menu">
        <div class="container-fluid container-nav">

            <div class="item-nav-menu">
                <div class="item-nav-logo">
                    <a class="brand" href="{{ route('home') }}">
                        <img src="{{ asset('img/logo.svg') }}" alt="" class="logo">
                    </a>
                </div>
                <div class="search-mob">
                    <input text="text" class="SearchInput" placeholder="SEARCH"
                           @if(isset($search) && !is_null($search)) value="{{ $search }}" @endif/>
                </div>
                <nav class="navbar navbar-expand-lg navbar-full navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="main-nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="/feed?type=news">news</a>
                                <br>
                                <span>what’s going on in the<br>Network Users</span>
                            </li>
                            <li class="nav-item">
                                <a href="/feed?type=event">events</a>
                                <br>
                                <span>from the Adult Enterainment<br>Adult Entertainment World</span>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('business') }}">businesses</a>
                                <br>
                                <span>in the Adult Entertainment <br>Industry - updates - Jobs</span>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('professionals') }}">professionals</a>
                                <br>
                                <span>Actors, Bar Staff, Models, Dancers<br>& Film Crew in Adult Entertainment</span>
                            </li>
                            <li class="nav-item">
                                <div class="search-menu">
                                    <input text="text" class="SearchInput" placeholder="SEARCH"
                                           @if(isset($search) && !is_null($search)) value="{{ $search }}" @endif/>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="search">
                    <input text="text" class="SearchInput" placeholder="SEARCH"
                           @if(isset($search) && !is_null($search)) value="{{ $search }}" @endif/>
                </div>
            </div>
        </div>
    </div>
    <span class="border"></span>
    @if (Auth::check() )
        <div class="bottom-header-menu">
            <div class="container-fluid">
                <ul class="bottom-menu">
                    <li>
                        <a href="#">{{ Auth::user()->name }}</a>
                    </li>
                    <li class="dialog-messages-link" style="width: 150px;">
                        <a href="#" class="OpenChat">
                            Messages
                            @if (\Chat::messages()->for(Auth::user())->unreadCount() > 0)
                                <div class="star">{{ \Chat::messages()->for(Auth::user())->unreadCount() }}</div>
                            @endif
                        </a>
                    </li>
                    <li style=" width: 310px; ">
                        <a href="@if (\App\ConnectionRequest::countRequest() > 0){{ route('connection-requests') }}@else{{ route('connections') }}@endif"
                           id="ConnectionRequestBox">
                            Connection Request
                            @if (\App\ConnectionRequest::countRequest() > 0)
                                <div class="star">{{ \App\ConnectionRequest::countRequest() }}</div>
                            @endif
                        </a>
                    </li>
                    @if (!in_array($UserType, ['user', 'public_user', false]))
                        <li>
                            <a href="{{ route('job-alerts') }}">Job Alerts</a>
                        </li>
                        <li class="post-bar-link">
                            <a href="#">Post News, Updates & Events</a>
                        </li>
                    @endif
                </ul>
                <a href="#" class="mobile-button">
                    <span></span>
                </a>
            </div>
        </div>
    @endif
</header>
<!-- header end -->

<div class="main_wrap">
    <div class="container-fluid">


        <div class="main_container">
            @if (!isset($left_sidebar) or $left_sidebar)
                <div class="m_left_block">
                    @include('includes.left-sidebar')
                </div>
            @endif
            <div class="m_right_block" style="{{ (isset($left_sidebar) and !$left_sidebar) ? 'width: 100%;' : '' }}">

                @include('includes.new-post')

                @yield('content')

            </div>
        </div>
    </div>
</div>

<<<<<<< HEAD
<footer class="main-footer">
    <div class="container-fluid">
        <div class="ft-wrap">
            <div class="ft-info">
                <ul>
                    <li>
                        <a href="{{ route('terms_and_conditions') }}" target="_blank">Terms and conditions</a>
                    </li>
                    <li>
                        <a href="#">Privacy</a>
                    </li>
                </ul>
=======
@if (!isset($left_sidebar) or $left_sidebar)
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="ft-wrap">
                <div class="ft-info">
                    <ul>
                        <li>
                            <a href="{{ route('terms_and_conditions') }}" target="_blank">Terms and conditions</a>
                        </li>
                        <li>
                            <a href="#">Privacy</a>
                        </li>
                    </ul>
                </div>
                <div class="ft-copiright">©2018 AEN | All rights reserved</div>
>>>>>>> 01fe240bc43c9e46c5a72cbb3f77efb24be0ac79
            </div>
        </div>
    </footer>
@endif

@include('includes.dialog')

<script src="{{ asset('js/jquery.3.3.1.js') }}"></script>
<script src="https://cdn.pubnub.com/pubnub-3.7.14.min.js" type="application/javascript"></script>
<script src="{{ asset('js/webrtc.js') }}" type="application/javascript"></script>
<script src="https://cdn.pubnub.com/webrtc/rtc-controller.js"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.all.min.js"></script>
<script src="{{ asset('js/lightbox.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/wow.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/JavaScript.js') }}"></script>
<script src="{{ asset('js/max.js') }}"></script>
<script src="{{ asset('js/chat.js') }}"></script>
<script src="{{ asset('js/audio_chat.js') }}"></script>
<script src="{{ asset('js/posts.js') }}"></script>
<script src="{{ asset('js/groups.js') }}"></script>
<script src="https://use.fontawesome.com/97636aa816.js"></script>
<script src="{{ asset('js/cropper.min.js') }}"></script>
<script src="{{ asset('js/jquery-cropper.min.js') }}"></script>

@yield('custom_js')

@if(isset($js))
    @foreach($js as $src)
        <script src="{{$src}}"></script>
    @endforeach
@endif
</body>
</html>