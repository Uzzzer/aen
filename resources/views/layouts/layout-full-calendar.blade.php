<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AEN - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('assets/animate.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/reset.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/header.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/fonts.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/StyleSheet.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/ruslan.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/max.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/lightbox.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css') }}"
          type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/additional.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/cropper.min.css') }}" type="text/css" rel="stylesheet">

    @if(isset($css))
        @foreach($css as $src)
            <link rel="stylesheet" href="{{$src}}"/>
        @endforeach
    @endif
</head>
<body>

<!--header -->
<header class="heaedr-main"> <!--  heaedr-main_2  -->
    <div class="top-header-menu">
        <div class="container-fluid container-nav">
            <div class="item-nav-menu">
                <div class="item-nav-logo">
                    <a class="brand" href="{{ route('home') }}">
                        <img src="{{ asset('img/logo.svg') }}" alt="" class="logo">
                    </a>
                </div>
                <div class="search-mob">
                    <input text="text" class="SearchInput" placeholder="SEARCH"/>
                </div>
                <nav class="navbar navbar-expand-lg navbar-full navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="main-nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="/feed?type=news">news</a>
                                <br>
                                <span>what’s going on in the<br>Network Users</span>
                            </li>
                            <li class="nav-item">
                                <a href="/feed?type=event">events</a>
                                <br>
                                <span>from the Adult Enterainment<br>Adult Entertainment World</span>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('business') }}">businesses</a>
                                <br>
                                <span>in the Adult Entertainment <br>Industry - updates - Jobs</span>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('professionals') }}">professionals</a>
                                <br>
                                <span>Actors, Bar Staff, Models, Dancers<br>& Film Crew in Adult Entertainment</span>
                            </li>
                            <li class="nav-item">
                                <div class="search-menu">
                                    <input text="text" class="SearchInput" placeholder="SEARCH"
                                           @if(isset($search) && !is_null($search)) value="{{ $search }}" @endif/>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="search">
                    <input text="text" class="SearchInput" placeholder="SEARCH"/>
                </div>
            </div>
        </div>
    </div>
    <span class="border"></span>

    <div class="profile-header-menu">
        <div class="container-fluid">
            <a href="{{ url('profile') }}" class="back-link">{{ Auth::user()->name }}</a>
            <div class="right-block">
                <div class="calendar-header-wrapper">
                    <ul class="calendar-header-btns">
                        <li class="li-bnt left"><button id="calendar-prev-button" type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left" aria-label="prev"><span class="fc-icon fc-icon-left-single-arrow"></span></button></li>
                        <li id="selected-month"></li>
                        <li class="li-bnt right"><button id="calendar-next-button" type="button" class="fc-next-button fc-button fc-state-default fc-corner-right" aria-label="next"><span class="fc-icon fc-icon-right-single-arrow"></span></button></li>
                    </ul>
                    <div class="vertival-line"></div>
                </div>
                <ul class="profile-header-nav">
                    @php
                        $prefix = '';
                        if ($user->id != Auth::user()->id)
                            $prefix = 'id/'.$user->id.'/';
                    @endphp
                    @if (!in_array(\App\User::isType($user->id), ['user', 'public_user', false]))
                        <li class="active">
                            <a href="{{ url('profile/'.$prefix) }}">Profile</a>
                        </li>
                        <li>
                            <a href="{{ url('profile/'.$prefix.'news-and-events') }}">News/Events</a>
                        </li>
                        <li>
                            <a href="{{ url('profile/'.$prefix.'videos') }}">Videos</a>
                        </li>
                        <li>
                            <a href="{{ url('profile/'.$prefix.'photos') }}">Photos</a>
                        </li>
                        @if ($user->website != NULL)
                            <li>
                                <a href="{{ $user->website }}" target="_blank">Website</a>
                            </li>
                        @endif
                        <li>
                            <a href="{{ route('social', ['id' => $user->id]) }}">Social</a>
                        </li>
                    @endif
                    @if (Auth::user()->business)
                        <li>
                            <a href="{{ route('balance_log') }}">Balance:
                                {{ \App\Option::option('currency') }}<span style="color: #D0021B;font-size: 16px;padding-right: 3px;padding-left: 3px;">{{ Auth::user()->balance }}</span>
                            </a>
                        </li>
                    @endif
                </ul>
                <ul class="rofile-header-btns">
                    <li class="user" id="ConnectionRequestBox">
                        <a href="@if (\App\ConnectionRequest::countRequest() > 0){{ route('connection-requests') }}@else{{ route('connections') }}@endif">
                            <i class="v-icon user"></i>
                        </a>
                        @if (\App\ConnectionRequest::countRequest() > 0)
                            <div class="star">{{ \App\ConnectionRequest::countRequest() }}</div>
                        @endif
                    </li>
                    <li class="dialog-messages-link OpenChat" id="OpenChatUnread">
                        <a href="#">
                            <i class="v-icon message"></i>
                        </a>
                        @if (\Chat::messages()->for(Auth::user())->unreadCount() > 0)
                            <div class="star">{{ Chat::messages()->for(Auth::user())->unreadCount() }}</div>
                        @endif
                    </li>
                    @if ($user->id == \Auth::user()->id)
                        <li style=" position: relative; " id="NotificationUnread">
                            <a href="{{ route('notifications') }}">
                                <i class="v-icon jobs"></i>
                            </a>
                            @if (\App\Notification::countUnreadNotice())
                                <div class="star">{{ \App\Notification::countUnreadNotice() }}</div>
                            @endif
                        </li>
                        <li class="vertical-line"></li>
                        <li class="post">
                            <a href="{{ url('profile/profile-editing') }}">
                                <i class="v-icon post"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- header end -->

@yield('content')

@include('includes.dialog')

<script src="{{ asset('js/jquery.3.3.1.js') }}"></script>
<script src="https://cdn.pubnub.com/pubnub-3.7.14.min.js" type="application/javascript"></script>
<script src="{{ asset('js/webrtc.js') }}" type="application/javascript"></script>
<script src="https://cdn.pubnub.com/webrtc/rtc-controller.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.all.min.js"></script>
<script src="{{ asset('js/lightbox.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/wow.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/JavaScript.js') }}"></script>
<script src="{{ asset('js/max.js') }}"></script>
<script src="{{ asset('js/jquery.mask.js') }}"></script>
<script src="{{ asset('js/chat.js') }}"></script>
<script src="{{ asset('js/audio_chat.js') }}"></script>
<script src="{{ asset('js/posts.js') }}"></script>
<script src="https://use.fontawesome.com/97636aa816.js"></script>
<script src="{{ asset('js/cropper.min.js') }}"></script>
<script src="{{ asset('js/jquery-cropper.min.js') }}"></script>

@if(isset($js))
    @foreach($js as $src)
        <script src="{{$src}}"></script>
    @endforeach
@endif
</body>
</html>