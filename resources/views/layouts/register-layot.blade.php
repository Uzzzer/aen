<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AEN - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/reset.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/header.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/StyleSheet.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fonts.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ruslan.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/max.css') }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset('css/additional.css') }}" type="text/css" rel="stylesheet"/>
    @if(isset($css))
        @foreach($css as $src)
            <link rel="stylesheet" href="{{$src}}"/>
        @endforeach
    @endif
</head>
<body>

<!--header -->
<header class="heaedr-main">
    <div class="top-header-menu">
        <div class="container-fluid container-nav">
            <div class="item-nav-menu">
                <div class="item-nav-logo">
                    <a class="brand" href="{{ route('home') }}">
                        <img src="{{ asset('img/logo.svg') }}" alt="" class="logo">
                    </a>
                </div>
                <div class="search-mob">
                    <input text="text" class="SearchInput" placeholder="SEARCH"/>
                </div>
                <nav class="navbar navbar-expand-lg navbar-full navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="main-nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#">news</a>
                                <br>
                                <span>what’s going on in the<br>Network Users</span>
                            </li>
                            <li class="nav-item">
                                <a href="#">events</a>
                                <br>
                                <span>from the Adult Enterainment<br>Adult Entertainment World</span>
                            </li>
                            <li class="nav-item">
                                <a href="#">businesses</a>
                                <br>
                                <span>in the Adult Entertainment <br>Industry - updates - Jobs</span>
                            </li>
                            <li class="nav-item">
                                <a href="#">professionals</a>
                                <br>
                                <span>Actors, Bar Staff, Models, Dancers<br>& Film Crew in Adult Entertainment</span>
                            </li>
                            <li class="nav-item">
                                <div class="search-menu">
                                    <input text="text" class="SearchInput" placeholder="SEARCH"/>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="search">
                    <input text="text" class="SearchInput" placeholder="SEARCH"/>
                </div>
            </div>
        </div>
    </div>
    <span class="border"></span>
</header>
<!-- header end -->

<div class="main_wrap">
    <div class="container-fluid">
        <div class="main_container">

            @yield('content')

        </div>
    </div>
</div>

<script src="{{ asset('js/jquery.3.3.1.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
        integrity="sha256-Kg2zTcFO9LXOc7IwcBx1YeUBJmekycsnTsq2RuFHSZU=" crossorigin="anonymous"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/wow.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/JavaScript.js') }}"></script>
<script src="{{ asset('js/max.js') }}"></script>
@if(isset($js))
    @foreach($js as $src)
        <script src="{{$src}}"></script>
    @endforeach
@endif
</body>
</html>