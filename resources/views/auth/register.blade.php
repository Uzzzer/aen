@extends('layouts.register-layot')

@section('content')
    <form class="register-form" method="post" action="{{ route('register') }}">
        {{ csrf_field() }}
        <input type="email" placeholder="E-mail" required="" value="{{ old('email') }}" name="email">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <input type="text" placeholder="Name" required="" value="{{ old('name') }}" name="name">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <input type="text" placeholder="Birthday dd/mm/yyyy" class="birthday-date" required="" value="{{ old('birthday') }}" name="birthday">
        @if ($errors->has('birthday'))
            <span class="help-block">
                <strong>{{ $errors->first('birthday') }}</strong>
            </span>
        @endif
        <select name="sex" id="sex">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
        <input type="password" placeholder="Password" required="" name="password">
        <input type="password" placeholder="Password confirmation" required="" name="password_confirmation">
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        {{-- <div class="checkbox_box">
            <input type="checkbox" name="business" id="business_checbox" value="1"><label for="business_checbox">Business account</label>
        </div> --}}
        <select name="user_category_id" id="user_category_id">
            <option value="1">User</option>
            @foreach (\App\UserCategory::where('business', 1)->get() as $category)
                <option value="{{ $category->id }}" {{ $category->id == old('user_category_id') ? 'selected' : '' }}>{{ $category->name }}</option>
            @endforeach
        </select>
        <input type="text" placeholder="Full Name" value="{{ old('full_name') }}" name="full_name"
               style="display: {{ (old('user_category_id') != 1 && old('user_category_id') != null) ? 'block' : 'none' }}">
        @if ($errors->has('full_name'))
            <span class="help-block">
                <strong>{{ $errors->first('full_name') }}</strong>
            </span>
        @endif
        <input type="text" placeholder="Street address, P.O. box, company name, c/o" value="{{ old('address') }}"
               name="address"
               style="display: {{ (old('user_category_id') != 1 && old('user_category_id') != null) ? 'block' : 'none' }}">
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
        <input type="text" placeholder="Apartment, unit, building, floor, etc" value="{{ old('address2') }}"
               name="address2"
               style="display: {{ (old('user_category_id') != 1 && old('user_category_id') != null) ? 'block' : 'none' }}">
        @if ($errors->has('address2'))
            <span class="help-block">
                <strong>{{ $errors->first('address2') }}</strong>
            </span>
        @endif
        <input type="text" placeholder="City" value="{{ old('location') }}" name="location"
               style="display: {{ (old('user_category_id') != 1 && old('user_category_id') != null) ? 'block' : 'none' }}">
        @if ($errors->has('location'))
            <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
        <input type="text" placeholder="State/Province/Region" value="{{ old('state') }}" name="state"
               style="display: {{ (old('user_category_id') != 1 && old('user_category_id') != null) ? 'block' : 'none' }}">
        @if ($errors->has('state'))
            <span class="help-block">
                <strong>{{ $errors->first('state') }}</strong>
            </span>
        @endif
        <input type="text" placeholder="Zip/Postal Code" value="{{ old('postal_code') }}" name="postal_code"
               style="display: {{ (old('user_category_id') != 1 && old('user_category_id') != null) ? 'block' : 'none' }}">
        @if ($errors->has('postal_code'))
            <span class="help-block">
                <strong>{{ $errors->first('postal_code') }}</strong>
            </span>
        @endif
        <select name="country_id" id="country_id"
                style="display: {{ (old('user_category_id') != 1 && old('user_category_id') != null) ? 'block' : 'none' }}">
            @foreach (\App\Country::getAllCountries() as $country)
                <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endforeach
        </select>
        <div class="terms_and_conditions" style="margin-left: 35%;">
            <input type="checkbox" name="terms_and_conditions" {{ old('terms_and_conditions') ? 'checked' : '' }}>
            <span style="color: white"><a href="{{ route('terms_and_conditions') }}"
                                          target="_blank"> Terms and Conditions</a></span>
            @if ($errors->has('terms_and_conditions'))
                <span class="help-block" style="text-align: inherit">
                <strong>{{ $errors->first('terms_and_conditions') }}</strong>
            </span>
            @endif
        </div>
        <div class="no_dob" style="margin-left: 35%; margin-bottom: 15px">
            <input type="checkbox" name="no_dob" id="no_dob" {{ old('no_dob') ? 'checked' : '' }}>
            <label for="no_dob">
                <p style="color: white"> I confirm that I have 18</p>
            </label>
            @if ($errors->has('no_dob'))
                <span class="help-block" style="text-align: inherit">
                <strong>{{ $errors->first('no_dob') }}</strong>
            </span>
            @endif
        </div>

        <button type="submit" class="btn btn-primary">Create account</button>
    </form>

@endsection