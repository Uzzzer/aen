@extends('adminlte::page')

@section('title', 'Braintree Settings')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Braintree Settings</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Braintree Settings</h3>
                </div>
                <form id="PaypalSettings">
                    <div class="box-body" style="padding: 10px 0px;">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Merchant Id</label>
                                <input type="text" class="form-control" name="braintree_merchant_id" value="{{ \App\Option::option('braintree_merchant_id') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Environment</label>
                                <select class="form-control" name="braintree_env">
                                    <option value='production' @if(\App\Option::option("braintree_env") == 'production'){{ 'selected' }}@endif>Production</option>
                                    <option value='sandbox' @if(\App\Option::option("braintree_env") == 'sandbox'){{ 'selected' }}@endif>Sandbox</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Public Key</label>
                                <input type="text" class="form-control" name="braintree_public_key" value="{{ \App\Option::option('braintree_public_key') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Private Key</label>
                                <input type="text" class="form-control" name="braintree_private_key" value="{{ \App\Option::option('braintree_private_key') }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/paypal_settings.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
