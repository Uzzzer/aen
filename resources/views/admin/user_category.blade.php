@extends('adminlte::page')

@section('title', 'User Category #'.$UserCategory->id)

@section('content_header')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin-dashboard') }}">
                <i class="fa fa-dashboard"></i>
                Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin-user-categories') }}">User Categories</a>
        </li>
        <li class="active">User Category #{{ $UserCategory->id }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <form id="UpdateUserCategory">
                    <div class="box-body">
                        <input type="hidden" value="{{ $UserCategory->id }}" name="user_category_id">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $UserCategory->name }}">
                        </div>
                        <div class="form-group">
                            <label>Prices ({{ \App\Option::option('currency') }})</label>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: middle; width: 130px;">12 month:</td>
                                    <td style="vertical-align: middle;">
                                        <input type="number" class="form-control" name="prices[12]"
                                               value="{{ isset(json_decode($UserCategory->prices, true)[12]) ? json_decode($UserCategory->prices, true)[12] : '0' }}"
                                               required>
                                    </td>
                                    <td style="vertical-align: middle; width: 130px;">3 month:</td>
                                    <td style="vertical-align: middle;">
                                        <input type="number" class="form-control" name="prices[3]"
                                               value="{{ isset(json_decode($UserCategory->prices, true)[3]) ? json_decode($UserCategory->prices, true)[3] : '0' }}"
                                               required>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle;">1 month:</td>
                                    <td style="vertical-align: middle;">
                                        <input type="number" class="form-control" name="prices[1]"
                                               value="{{ isset(json_decode($UserCategory->prices, true)[1]) ? json_decode($UserCategory->prices, true)[1] : '0' }}"
                                               required>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        {{ \App\Option::option('trial_days') }} {{ intval(\App\Option::option('trial_days')) > 1 ? 'days' : 'day' }} trial:
                                        <input type="checkbox" name="trial" @if($UserCategory->trial_period) checked @endif>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <input type="number" class="form-control" name="prices[trial]"
                                               @if(!$UserCategory->trial_period) style="display: none;" @endif
                                               value="{{ isset(json_decode($UserCategory->prices, true)['trial']) ? json_decode($UserCategory->prices, true)['trial'] : '0' }}"
                                               @if($UserCategory->trial_period) required @endif>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <label>Plan IDs</label>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: middle; width: 130px;">12 month:</td>
                                    <td style="vertical-align: middle;">
                                        <input type="text" class="form-control" name="planIDs[12]"
                                               value="{{ isset(json_decode($UserCategory->planIDs, true)[12]) ? json_decode($UserCategory->planIDs, true)[12] : '' }}"
                                               required>
                                    </td>
                                    <td style="vertical-align: middle; width: 130px;">3 month:</td>
                                    <td style="vertical-align: middle;">
                                        <input type="text" class="form-control" name="planIDs[3]"
                                               value="{{ isset(json_decode($UserCategory->planIDs, true)[3]) ? json_decode($UserCategory->planIDs, true)[3] : '' }}"
                                               required>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle;">1 month:</td>
                                    <td style="vertical-align: middle;">
                                        <input type="text" class="form-control" name="planIDs[1]"
                                               value="{{ isset(json_decode($UserCategory->planIDs, true)[1]) ? json_decode($UserCategory->planIDs, true)[1] : '' }}"
                                               required>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--/.col -->
    </div>

@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/user-categories.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
