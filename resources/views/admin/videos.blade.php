@extends('adminlte::page')

@section('title', 'Profile Videos')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Profile Videos</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Profile Videos</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Videos" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th style="width: 70px">Preview</th>
                                <th>Title</th>
                                <th>User</th>
                                <th style="width: 95px">Price</th>
                                <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th style="width: 40px"><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th style="width: 95px">Date created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Videos as $Video)
                                <tr data-url="{{ route('admin-video', ['id' => $Video->id]) }}">
                                    <td>{{ $Video->id }}</td>
                                    <td><img src="{{ asset($Video->preview_image) }}" class="avatar_user"/></td>
                                    <td>{{ $Video->title }}</td>
                                    <td><a href="{{ route('admin-user', ['id' => $Video->user_id]) }}">{{ $Video->user_name }}</a></td>
                                    <td>{{ $Video->price }} {{ \App\Option::option('currency') }}</td>
                                    <td>{{ \App\ProfileVideoLike::countLike($Video->id) }}</td>
                                    <td>{{ \App\ProfileVideoComment::where('video_id', $Video->id)->count() }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($Video->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Preview</th>
                                <th>Title</th>
                                <th>User</th>
                                <th>Price</th>
                                <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th>Date created</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/videos.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
