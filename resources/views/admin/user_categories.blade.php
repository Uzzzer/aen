@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">User Categories</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">User Categories</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="UserCategories" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th>Name</th>
                                <th>User type</th>
                                <th style="width: 70px">12 month</th>
                                <th style="width: 70px">3 month</th>
                                <th style="width: 70px">1 month</th>
                                <th style="width: 70px">{{ \App\Option::option('trial_days') }} {{ intval(\App\Option::option('trial_days')) > 1 ? 'days' : 'day' }} trial</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($UserCategories as $UserCategory)
                                <tr data-url="{{ route('admin-user-category', ['id' => $UserCategory->id]) }}">
                                    <td>{{ $UserCategory->id }}</td>
                                    <td>{{ $UserCategory->name }}</td>
                                    <td>{{ ($UserCategory->business) ? 'Business' : 'User' }}</td>
                                    <td>{{ isset(json_decode($UserCategory->prices, true)[12]) ? json_decode($UserCategory->prices, true)[12] : '0' }} {{ \App\Option::option('currency') }}</td>
                                    <td>{{ isset(json_decode($UserCategory->prices, true)[3]) ? json_decode($UserCategory->prices, true)[3] : '0' }} {{ \App\Option::option('currency') }}</td>
                                    <td>{{ isset(json_decode($UserCategory->prices, true)[1]) ? json_decode($UserCategory->prices, true)[1] : '0' }} {{ \App\Option::option('currency') }}</td>
                                    <td>{{ isset(json_decode($UserCategory->prices, true)['trial']) ? json_decode($UserCategory->prices, true)['trial'] : '0' }} {{ \App\Option::option('currency') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>12 month</th>
                                <th>3 month</th>
                                <th>1 month</th>
                                <th>{{ \App\Option::option('trial_days') }} {{ intval(\App\Option::option('trial_days')) > 1 ? 'days' : 'day' }} trial</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/user-categories.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
