@extends('adminlte::page')

@section('title', 'Posts')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Posts</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Posts</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Posts" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th style="width: 70px">Media type</th>
                                <th>Title</th>
                                <th style="width: 70px">Post type</th>
                                <th>User</th>
                                <th>Group</th>
                                <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th style="width: 40px"><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th style="width: 95px">Date created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Posts as $Post)
                                <tr data-url="{{ route('admin-post', ['id' => $Post->id]) }}">
                                    <td>{{ $Post->id }}</td>
                                    <td>{{ (is_null($Post->file_type)) ? 'No media' : ucfirst($Post->file_type) }}</td>
                                    <td>{{ $Post->title }}</td>
                                    <td>{{ ucfirst(str_replace('_', ' ', $Post->post_type)) }}</td>
                                    <td><a href="{{ route('admin-user', ['id' => $Post->user_id]) }}">{{ $Post->user_name }}</a></td>
                                    <td>@if(!is_null($Post->group_id))<a href="{{ route('admin-group', ['id' => $Post->group_id]) }}">{{ $Post->group_name }}</a>@endif</td>
                                    <td>{{ \App\PostLike::where('post_id', $Post->id)->count() }}</td>
                                    <td>{{ \App\PostComment::where('post_id', $Post->id)->count() }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($Post->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Media type</th>
                                <th>Title</th>
                                <th>Post type</th>
                                <th>User</th>
                                <th>Group</th>
                                <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th>Date created</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/posts.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
