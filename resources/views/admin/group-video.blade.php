@extends('adminlte::page')

@section('title', 'Group Video #'.$GroupVideo->id)

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('admin-group-videos') }}">Group Videos</a></li>
        <li class="active">Group Video #{{$GroupVideo->id}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="form-group">

                                <video width="100%" height="210px" controls>
                                    <source src="{{ asset($GroupVideo->file_name) }}">
                                    Your browser does not support the video tag.
                                </video>

                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="padding: 0px 0px 0px 2px;">
                                    <button type="button" class="btn btn-danger" id="DeleteGroupVideo" style="width: 100%;">Delete</button>
                                </div>
                            </div>
                        </div>
                        @if (!is_null($User) or !is_null($Group))
                            <table class="table">
                                <tbody>
                                    @if (!is_null($User))
                                        <tr>
                                            <th style="width: 65px; padding-left: 0px;">User:</th>
                                            <th style="padding-left: 0px;"><a href="{{ route('admin-user', ['id' => $User->id]) }}">{{ $User->name }}</a></th>
                                        </tr>
                                    @endif
                                    @if (!is_null($Group))
                                        <tr>
                                            <th style="width: 65px; padding-left: 0px;">Group:</th>
                                            <th style="padding-left: 0px;"><a href="{{ route('admin-group', ['id' => $Group->id]) }}">{{ $Group->name }}</a></th>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-9">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#basic_info" data-toggle="tab" aria-expanded="true">Basic info</a></li>
                    <li><a href="#likes" data-toggle="tab" aria-expanded="true">Likes ({{ count($Likes) }})</a></li>
                    <li><a href="#comments" data-toggle="tab" aria-expanded="true">Comments ({{ \App\MediaComment::where('video_id', $GroupVideo->id)->count() }})</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="basic_info">
                        <form id="SaveBasicInfo">
                            <input type="hidden" value="{{ $GroupVideo->id }}" name="video_id">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title" value="{{ $GroupVideo->title }}">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="2" name="description">{{ $GroupVideo->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="likes">
                        <table id="Likes" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                    <th>Date like</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Likes as $Like)
                                    <tr data-url="{{ route('admin-user', ['id' => $Like->user_id]) }}">
                                        <td>{{ $Like->user_id }}</td>
                                        <td><img src="{{ asset($Like->user_avatar) }}" class="avatar_user"></td>
                                        <td>{{ $Like->user_name }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Like->created_at, true) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Date like</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="comments">
                        <table id="Comments" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th>User</th>
                                    <th>Text</th>
                                    <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                    <th style="width: 125px">Date</th>
                                    <th style="width: 70px">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Comments as $Comment)
                                    @php
                                        $ReplyComments = \App\MediaComment::where('media_comments.video_id', $GroupVideo->id)->where('media_comments.reply_id', $Comment->id)->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                    @endphp
                                    <tr style="cursor: default;" @if (count($ReplyComments)) class="next_tr" @endif>
                                        <td style="width: 30px">{{ $Comment->id }}</td>
                                        <td>
                                            <a href="{{ route('id_profile', ['id' => $Comment->user_id]) }}">{{ $Comment->user_name }}</a>
                                        </td>
                                        <td>
                                            {{ $Comment->comment }}
                                            @if (!is_null($Comment->image))
                                                <img src="{{ asset($Comment->image) }}" style="max-height: 150px; display: block; @if(!is_null($Comment->comment)){{ 'margin-top: 5px;' }}@endif" />
                                            @endif
                                        </td>
                                        <td>{{ \App\MediaCommentLike::where('comment_id', $Comment->id)->count() }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Comment->created_at, true) }}</td>
                                        <td><button type="button" class="btn btn-danger" id="DeleteComment" data-id="{{ $Comment->id }}" style="width: 100%;">Delete</button></td>
                                    </tr>
                                    @if (count($ReplyComments))
                                        <tr style="background-color: #f5f5f5; cursor: default;">
                                            <td colspan="2" style="text-align: center; color: #b9b8b8;"><i class="fa fa-angle-double-up" style=" font-size: 22px; padding-right: 10px; vertical-align: sub; "></i>Replies to the comment</td>
                                            <td colspan="4">
                                                <table id="ReplyComments" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 30px">ID</th>
                                                            <th>User</th>
                                                            <th>Text</th>
                                                            <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                                            <th style="width: 125px">Date</th>
                                                            <th style="width: 70px">Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($ReplyComments as $Comment)
                                                            <tr style="cursor: default;">
                                                                <td style="width: 30px">{{ $Comment->id }}</td>
                                                                <td>
                                                                    <a href="{{ route('id_profile', ['id' => $Comment->user_id]) }}">{{ $Comment->user_name }}</a>
                                                                </td>
                                                                <td>
                                                                    {{ $Comment->comment }}
                                                                    @if (!is_null($Comment->image))
                                                                        <img src="{{ asset($Comment->image) }}" style="max-height: 150px; display: block; @if(!is_null($Comment->comment)){{ 'margin-top: 5px;' }}@endif" />
                                                                    @endif
                                                                </td>
                                                                <td>{{ \App\MediaCommentLike::where('comment_id', $Comment->id)->count() }}</td>
                                                                <td>{{ \App\DateConvert::dashboardDate($Comment->created_at, true) }}</td>
                                                                <td><button type="button" class="btn btn-danger" id="DeleteComment" data-id="{{ $Comment->id }}" style="width: 100%;">Delete</button></td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>User</th>
                                                            <th>Text</th>
                                                            <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                                            <th>Date</th>
                                                            <th>Delete</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>User</th>
                                    <th>Text</th>
                                    <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                    <th>Date</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/group-videos.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
