@extends('adminlte::page')

@section('title', 'Photos')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Photos</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Photos</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Photos" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th style="width: 70px">Photo</th>
                                <th>Title</th>
                                <th>User</th>
                                <th>Group</th>
                                <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th style="width: 40px"><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th style="width: 95px">Date created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Photos as $Photo)
                                <tr data-url="{{ route('admin-photo', ['id' => $Photo->id]) }}">
                                    <td>{{ $Photo->id }}</td>
                                    <td><img src="{{ asset($Photo->file_name) }}" class="avatar_user"/></td>
                                    <td>{{ $Photo->title }}</td>
                                    <td><a href="{{ route('admin-user', ['id' => $Photo->user_id]) }}">{{ $Photo->user_name }}</a></td>
                                    <td>@if(!is_null($Photo->group_id))<a href="{{ route('admin-group', ['id' => $Photo->group_id]) }}">{{ $Photo->group_name }}</a>@endif</td>
                                    <td>{{ \App\PhotoLike::countLike($Photo->id) }}</td>
                                    <td>{{ \App\MediaComment::where('photo_id', $Photo->id)->count() }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($Photo->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Photo</th>
                                <th>Title</th>
                                <th>User</th>
                                <th>Group</th>
                                <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th>Date created</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/photos.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
