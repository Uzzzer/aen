@extends('adminlte::page')

@section('title', 'Payments')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Payments</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Payments</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Payments" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th style="width: 70px">Sum</th>
                                <th>Purchase</th>
                                <th>User</th>
                                <th style="width: 95px">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Payments as $Payment)
                                <tr>
                                    <td>{{ $Payment->id }}</td>
                                    <td>{{ $Payment->amount }} {{ $Payment->currency }}</td>
                                    <td>
                                        @php
                                            $text = '';
                                            $product = json_decode($Payment->product);

                                            switch ($product->type) {
                                                case "subscription":
                                                    $text = ($product->period == 'trial') ? 'Subscription for a trial period' : $product->period.' month subscription';
                                                    break;
                                                case "video":
                                                    $Video = \App\Video::find($product->id);
                                                    $UserVideo = (is_null($Video)) ? NULL : \App\User::find($Video->user_id);
                                                    $text = (is_null($Video)) ? 'Video: Video Deleted' : 'Video: '.$Video->title.'';
                                                    $text .= (is_null($UserVideo)) ? '' : ', from <a href="'.route('admin-user', ['id' => $UserVideo]).'">'.$UserVideo->name.'</a>' ;
                                                    break;
                                            }

                                            echo $text;
                                        @endphp
                                    </td>
                                    <td><a href="{{ route('admin-user', ['id' => $Payment->user_id]) }}">{{ $Payment->user_name }}</a></td>
                                    <td>{{ \App\DateConvert::dashboardDate($Payment->created_at, true) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Sum</th>
                                <th>Purchase</th>
                                <th>User</th>
                                <th>Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/payments.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
