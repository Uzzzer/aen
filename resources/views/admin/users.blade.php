@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Users</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users</h3>
                    <a href="/admin/user/export" target="_blank">
                        <button type="button" class="btn btn-xs btn-danger" style=" vertical-align: top; margin-left: 20px; padding: 2px 9px; ">Export</button>
                    </a>
                    <form action="/admin/user/import" method="post" style="display: inline;" class="form-import" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-xs btn-success select-import" style=" vertical-align: top; margin-left: 7px; padding: 2px 9px; ">Import</button>
                        <input type="file" name="file" accept=".csv" style="display:none;">
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Users" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th style="width: 70px">Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>E-mail</th>
                                <th style="width: 40px"><i class="fa fa-users" style="font-size: 18px;" title="Connections"></i></th>
                                <th style="width: 40px"><i class="fa fa-file-video-o" style="font-size: 18px;" title="Videos"></i></th>
                                <th style="width: 40px"><i class="fa fa-file-image-o" style="font-size: 18px;" title="Photos"></i></th>
                                <th style="width: 95px">Date created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Users as $User)
                                <tr data-url="{{ route('admin-user', ['id' => $User->id]) }}">
                                    <td>{{ $User->id }}</td>
                                    <td><img src="{{ asset($User->avatar) }}" class="avatar_user"></td>
                                    <td>{{ $User->name }}</td>
                                    <td>{{ ($User->business) ? 'Business' : 'User' }}</td>
                                    <td>{{ $User->email }}</td>
                                    <td>{{ \App\ConnectionRequest::where('user_id_2', $User->id)->orWhere('user_id_1', $User->id)->count() }}</td>
                                    <td>{{ \App\Video::getCountVideos($User->id) }}</td>
                                    <td>{{ \App\Gallery::getCountPhotosUser($User->id) }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($User->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>E-mail</th>
                                <th><i class="fa fa-users" style="font-size: 18px;" title="Connections"></i></th>
                                <th><i class="fa fa-file-video-o" style="font-size: 18px;" title="Videos"></i></th>
                                <th><i class="fa fa-file-image-o" style="font-size: 18px;" title="Photos"></i></th>
                                <th>Date created</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/users.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if (!is_null(\Request::input('imported')))
            swal('Success!', '{{ \Request::input('imported') }} users imported', 'success');
            @endif
            @if (!is_null(\Request::input('error')))
            swal('Error!', '{{ \Request::input('error') }}', 'error');
            @endif
        })
    </script>
    @stack('js')
    @yield('js')
@stop
