@extends('adminlte::page')

@section('title', 'Settings')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Settings</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">General Settings</h3>
                </div>
                <form id="UpdateGeneralSettings">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Currency</label>
                            <input type="hidden" name="currency_iso" value="{{ \App\Option::option('currency_iso') }}">
                            <select class="form-control" name="currency">
                                <option value="$" data-iso="USD"{{ \App\Option::option('currency') == '$' ? ' selected' : '' }}>USD ($)</option>
                                <option value="€" data-iso="EUR"{{ \App\Option::option('currency') == '€' ? ' selected' : '' }}>EUR (€)</option>
                                <option value="£" data-iso="GBP"{{ \App\Option::option('currency') == '£' ? ' selected' : '' }}>GBP (£)</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Trial Days</label>
                            <input type="number" class="form-control" name="trial_days" value="{{ \App\Option::option('trial_days') }}">
                        </div>
                        <div class="form-group">
                            <label>System commission (%)</label>
                            <input type="number" step="0.01" class="form-control" name="commission" value="{{ \App\Option::option('commission') }}">
                        </div>
                        <div class="form-group">
                            <label>Min sum withdraw</label>
                            <input type="number" step="0.01" class="form-control" name="min_withdraw" value="{{ \App\Option::option('min_withdraw') }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--/.col -->
        <div class="col-md-5">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Change Admin Password</h3>
                </div>
                <form id="ChangePassword">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" value="">
                        </div>
                        <div class="form-group">
                            <label>New password</label>
                            <input type="password" class="form-control" name="new_password" value="">
                        </div>
                        <div class="form-group">
                            <label>Confirmation password</label>
                            <input type="password" class="form-control" name="new_password_confirmation" value="">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/settings.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
