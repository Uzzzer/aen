@extends('adminlte::page')

@section('title', 'Chats')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Chats</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Chats</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Chats" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th>Users</th>
                                <th style="width: 40px"><i class="fa fa-commenting-o" style="font-size: 18px;" title="Count messages"></i></th>
                                <th style="width: 95px">Last message</th>
                                <th style="width: 95px">Date created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Chats as $id => $Chat)
                                 <tr data-url="{{ route('admin-chat', ['id' => $id]) }}">
                                    <td>{{ $id }}</td>
                                    <td>
                                        @foreach ($Chat['users'] as $key => $user)
                                            @if ($key){{ ', '}}@endif
                                            <a href="{{ route('admin-user', ['id' => $user->id]) }}">{{ $user->name }}</a>
                                        @endforeach
                                    </td>
                                    <td>{{ $Chat['count_messages'] }}</td>
                                    <td>{{ $Chat['last_message'] }}</td>
                                    <td>{{ $Chat['date_created'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Users</th>
                                <th><i class="fa fa-commenting-o" style="font-size: 18px;" title="Count messages"></i></th>
                                <th>Last message</th>
                                <th>Date created</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/chats.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
