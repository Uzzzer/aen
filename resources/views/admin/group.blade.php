@extends('adminlte::page')

@section('title', 'Group #'.$Group->id)

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('admin-groups') }}">Groups</a></li>
        <li class="active">Group #{{$Group->id}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $Group->name }}</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="form-group">
                                <img src="{{ asset($Group->avatar) }}" style="width: 100%;" class="avatar_user_page">
                            </div>
                            <div class="form-group">
                                <div class="col-md-6" style="padding: 0px 2px 0px 0px;">
                                    <button type="button" id="AvatarUploadClick" class="btn btn-primary" style="width: 100%;">Upload Avatar</button>
                                    <input type="file" id="AvatarUpload" style="display: none;">
                                </div>
                                <div class="col-md-6" style="padding: 0px 0px 0px 2px;">
                                    @if ($Group->status == 'blocked')
                                        <button type="button" class="btn btn-primary" id="UnblockGroup" style="width: 100%;">Unblock</button>
                                    @else
                                        <button type="button" class="btn btn-danger" id="BlockGroup" style="width: 100%;">Block</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if (!is_null($Owner))
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width: 65px; padding-left: 0px;">Owner:</th>
                                        <th style="padding-left: 0px;"><a href="{{ route('admin-user', ['id' => $Owner->id]) }}">{{ $Owner->name }}</a></th>
                                    </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-9">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#basic_info" data-toggle="tab" aria-expanded="true">Basic info</a></li>
                    <li><a href="#members" data-toggle="tab" aria-expanded="true">Members ({{ count($Members) }})</a></li>
                    <li><a href="#posts" data-toggle="tab" aria-expanded="false">Posts ({{ count($Posts) }})</a></li>
                    <li><a href="#photos" data-toggle="tab" aria-expanded="false">Photos ({{ count($Photos) }})</a></li>
                    <li><a href="#videos" data-toggle="tab" aria-expanded="false">Videos ({{ count($Videos) }})</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="basic_info">
                        <form id="SaveBasicInfo">
                            <input type="hidden" value="{{ $Group->id }}" name="group_id">
                            {{ csrf_field() }}
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: middle;">Name:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="name" value="{{ $Group->name }}">
                                        </td>
                                        <td style="vertical-align: middle;">Group type:</td>
                                        <td style="vertical-align: middle;">
                                            <select name="type" class="form-control">
                                                <option value="public" @if($Group->type == 'public') selected @endif>Public</option>
                                                <option value="private" @if($Group->type == 'private') selected @endif>Private</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Keywords:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="keywords" value="{{ $Group->keywords }}">
                                        </td>
                                        <td style="vertical-align: middle;">Category:</td>
                                        <td style="vertical-align: middle;">
                                            <select name="category_id" class="form-control">
                                                @foreach ($GroupCategories as $GroupCategory)
                                                    <option value="{{ $GroupCategory->id }}" @if($GroupCategory->id == $Group->category_id) selected @endif>{{ $GroupCategory->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="2" name="description">{{ $Group->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="members">
                        <table id="Members" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Members as $Member)
                                    <tr data-url="{{ route('admin-user', ['id' => $Member->id]) }}">
                                        <td>{{ $Member->id }}</td>
                                        <td><img src="{{ asset($Member->avatar) }}" class="avatar_user"></td>
                                        <td>{{ $Member->name }}</td>
                                        <td>{{ ($Member->business) ? 'Business' : 'User' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="posts">
                        <table id="Posts" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Media type</th>
                                    <th>Title</th>
                                    <th>Date create</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Posts as $Post)
                                    <tr data-url="{{ route('admin-post', ['id' => $Post->id]) }}">
                                        <td>{{ $Post->id }}</td>
                                        <td>{{ (is_null($Post->file_type)) ? 'No media' : ucfirst($Post->file_type) }}</td>
                                        <td>{{ $Post->title }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Post->created_at) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Media type</th>
                                    <th>Title</th>
                                    <th>Date create</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="photos">
                        <table id="Photos" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Photo</th>
                                    <th>Title</th>
                                    <th style="width: 95px">Date create</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Photos as $Photo)
                                    <tr data-url="{{ route('admin-photo', ['id' => $Photo->id]) }}">
                                        <td>{{ $Photo->id }}</td>
                                        <td><img src="{{ asset($Photo->file_name) }}" class="avatar_user"></td>
                                        <td>{{ $Photo->title }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Photo->created_at) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Photo</th>
                                    <th>Title</th>
                                    <th>Date create</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="videos">
                        <table id="Videos" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Preview</th>
                                    <th>Title</th>
                                    <th>Date create</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Videos as $Video)
                                    <tr data-url="">
                                        <td>{{ $Video->id }}</td>
                                        <td><img src="{{ asset($Video->preview) }}" class="avatar_user"></td>
                                        <td>{{ $Video->title }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Video->created_at) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Preview</th>
                                    <th>Title</th>
                                    <th>Date create</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!--/.col (right) -->
    </div>

    <div class="modal modal-info fade" id="UploadAvatarModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="CancelCropp" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload Avatar</h4>
                </div>
                <div class="modal-body">
                    <img class="cropper-box">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" id="CancelCropp">Cancel</button>
                    <button type="button" class="btn btn-outline" id="ApplyCropp">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/groups.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
