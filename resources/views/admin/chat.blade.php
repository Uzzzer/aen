@extends('adminlte::page')

@section('title', 'Chat #'.$Conversation->id)

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('admin-chats') }}">Chats</a></li>
        <li class="active">Chat #{{$Conversation->id}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-12" style="padding: 0px 0px 0px 0px;">
                                <button type="button" class="btn btn-danger" id="DeleteChat" data-id="{{$Conversation->id}}" style="width: 100%;">Delete</button>
                            </div>
                        </div>
                        <table class="table">
                            <tbody>
                                @foreach ($Users as $key => $user)
                                    <tr>
                                        <th style="width: 65px; padding-left: 0px;">User {{ $key+1 }}:</th>
                                        <th style="padding-left: 0px;"><a href="{{ route('admin-user', ['id' => $user->id]) }}">{{ $user->name }}</a></th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-header ui-sortable-handle">
                    <i class="fa fa-comments-o"></i>
                    <h3 class="box-title">
                        Chat #{{$Conversation->id}}
                    </h3>
                </div>
                <div class="box-body chat" id="chat-box">
                    @php
                        $Messages = \Chat::conversation($Conversation)->for($Users[0])->getMessages();
                    @endphp
                    @foreach($Messages as $message)
                        @php
                            switch ($message->type) {
                                case 'call':
                                    $class = 'incoming_call_mgs';
                                    $body = htmlspecialchars($message->body, ENT_QUOTES);
                                    break;
                                case 'job':
                                    $class = 'job_mgs';
                                    $body = json_decode($message->body);
                                    break;
                                case 'job_answer':
                                    $class = 'job_mgs';
                                    $body = json_decode($message->body);
                                    break;
                                default:
                                    $class = null;
                                    $body = htmlspecialchars($message->body, ENT_QUOTES);
                                    break;
                            }
                        @endphp
                        <!-- chat item -->
                        <div class="item" data-id="{{ $message->id }}">
                            <img src="{{ asset($UsersArray[$message->user_id]['avatar']) }}" alt="user image">

                            <p class="message">
                                <small class="text-muted pull-right">
                                    <i class="fa fa-clock-o"></i> {{ \App\DateConvert::Convert($message->created_at, true) }}<br>
                                    <a herf="#" class="RemoveMessage" data-id="{{ $message->id }}" style="cursor: pointer;">Delete</a>
                                </small>
                                <a href="{{ route('admin-user', ['id' => $message->user_id]) }}" class="name" style=" display: inline-block; ">
                                    {{ $UsersArray[$message->user_id]['name'] }}
                                </a>

                                @if (is_object($body))
                                    @if (isset($body->user))
                                        <br>To user:
                                        <span class="@if (!is_null($class)){{ 'system_msg '.$class }}@endif" style="display: table;">
                                            @php echo $body->user; @endphp
                                        </span>
                                    @endif
                                    @if (isset($body->business))
                                        To business:
                                        <span class="@if (!is_null($class)){{ 'system_msg '.$class }}@endif" style="display: table;">
                                            @php echo $body->business; @endphp
                                        </span>
                                    @endif
                                @else
                                <span class="@if (!is_null($class)){{ 'system_msg '.$class }}@endif" style="display: table;">
                                    @php echo $body; @endphp
                                </span>
                                @endif
                            </p>
                        </div>
                        <!-- /.item -->
                    @endforeach
                </div>
                <!-- /.chat -->
            </div>
        </div>
        <!--/.col (right) -->
    </div>
    <style>
        .system_msg {
            border: 1px solid #777777;
            padding: 5px;
            display: inline-block;
            border-radius: 5px;
            background: #777777;
            color: #fff;
        }

        .job_mgs:before {
            content: '';
            background: url({{ asset('/img/job.svg') }});
            width: 20px;
            height: 20px;
            display: inline-block;
            background-size: cover;
            vertical-align: top;
        }

        .incoming_call_mgs:before {
            content: '';
            background: url({{ asset('/img/telephone.svg') }});
            width: 20px;
            height: 20px;
            display: inline-block;
            background-size: cover;
        }

        .chat .item {
            border-bottom: 1px solid #efefef;
        }

        .chat .item:hover {
            border-bottom: 1px solid #cac7c7;
        }
    </style>
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/chats.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
