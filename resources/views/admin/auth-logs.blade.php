@extends('adminlte::page')

@section('title', 'Auth logs')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Auth logs</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Auth logs</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="AuthLogs" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th>User</th>
                                <th>Event</th>
                                <th style="width: 95px">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($AuthLogs as $AuthLog)
                                <tr>
                                    <td>{{ $AuthLog->id }}</td>
                                    <td><a href="{{ route('admin-user', ['id' => $AuthLog->user_id]) }}">{{ $AuthLog->user_name }}</a></td>
                                    <td>{{ $AuthLog->event }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($AuthLog->created_at, true) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>User</th>
                                <th>Event</th>
                                <th>Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/auth-logs.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
