@extends('adminlte::page')

@section('title', 'Post #'.$Post->id)

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('admin-posts') }}">Posts</a></li>
        <li class="active">Post #{{$Post->id}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="form-group post_file">

                                @if ($Post->file_type == 'image')
                                    <img src="{{ asset($Post->file_name) }}" style="width: 100%;" class="avatar_user_page">
                                @elseif ($Post->file_type == 'video')
                                    <video width="100%" height="210px" controls>
                                        <source src="{{ asset($Post->file_name) }}">
                                        Your browser does not support the video tag.
                                    </video>
                                @endif

                            </div>
                            <div class="form-group">
                                <div class="col-md-6" style="padding: 0px 2px 0px 0px;">
                                    <button type="button" id="UploadClick" class="btn btn-primary"
                                            style="width: 100%;">Upload
                                    </button>
                                    <input type="file" id="Upload" style="display: none;">
                                </div>
                                <div class="col-md-6" style="padding: 0px 0px 0px 2px;">
                                    <button type="button" class="btn btn-danger" id="DeletePost"
                                            style="width: 100%;">Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (!is_null($User) or !is_null($Group))
                            <table class="table">
                                <tbody>
                                    @if (!is_null($User))
                                        <tr>
                                            <th style="width: 65px; padding-left: 0px;">User:</th>
                                            <th style="padding-left: 0px;"><a href="{{ route('admin-user', ['id' => $User->id]) }}">{{ $User->name }}</a></th>
                                        </tr>
                                    @endif
                                    @if (!is_null($Group))
                                        <tr>
                                            <th style="width: 65px; padding-left: 0px;">Group:</th>
                                            <th style="padding-left: 0px;"><a href="{{ route('admin-group', ['id' => $Group->id]) }}">{{ $Group->name }}</a></th>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-9">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#basic_info" data-toggle="tab" aria-expanded="true">Basic info</a></li>
                    <li><a href="#likes" data-toggle="tab" aria-expanded="true">Likes ({{ count($Likes) }})</a></li>
                    <li><a href="#comments" data-toggle="tab" aria-expanded="true">Comments ({{ \App\PostComment::where('post_id', $Post->id)->count() }})</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="basic_info">
                        <form id="SaveBasicInfo">
                            <input type="hidden" value="{{ $Post->id }}" name="post_id">
                            {{ csrf_field() }}
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: middle;">Title:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="title" value="{{ $Post->title }}">
                                        </td>
                                        <td style="vertical-align: middle;">Keywords:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="keywords" value="{{ $Post->keywords }}">
                                        </td>
                                    </tr>
                                    @if ($Post->post_type == 'event' or $Post->post_type != 'group_post')
                                    <tr>
                                        @if ($Post->post_type != 'group_post')
                                            <td style="vertical-align: middle;">Category:</td>
                                            <td style="vertical-align: middle;">
                                                <select name="category_id" class="form-control">
                                                    @foreach ($PostCategories as $PostCategory)
                                                        <option value="{{ $PostCategory->id }}" @if($PostCategory->id == $Post->category_id) selected @endif>{{ $PostCategory->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        @endif
                                        @if ($Post->post_type == 'event')
                                            <td style="vertical-align: middle;">Frequency:</td>
                                            <td style="vertical-align: middle;">
                                                <select name="frequency" class="form-control">
                                                    <option value="Occurs Once" @if($Post->frequency == 'Occurs Once') selected @endif>Occurs Once</option>
                                                    <option value="Occurs Twice" @if($Post->frequency == 'Occurs Twice') selected @endif>Occurs Twice</option>
                                                    <option value="Occurs 3-5 times" @if($Post->frequency == 'Occurs 3-5 times') selected @endif>Occurs 3-5 times</option>
                                                    <option value="More..." @if($Post->frequency == 'More...') selected @endif>More...</option>
                                                </select>
                                            </td>
                                        @endif
                                    </tr>
                                    @endif
                                    @if ($Post->post_type == 'event')
                                        <tr>
                                            <td style="vertical-align: middle;">Start:</td>
                                            <td style="vertical-align: middle;">
                                                <input type="date" class="form-control" name="start" value="{{ $Post->start }}">
                                            </td>
                                            <td style="vertical-align: middle;">End:</td>
                                            <td style="vertical-align: middle;">
                                                <input type="date" class="form-control" name="end" value="{{ $Post->end }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;">Location:</td>
                                            <td style="vertical-align: middle;">
                                                <input type="text" class="form-control" name="location" value="{{ $Post->location }}">
                                            </td>
                                            <td style="vertical-align: middle;"></td>
                                            <td style="vertical-align: middle;"></td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="2" name="description">{{ $Post->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="likes">
                        <table id="Likes" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                    <th>Date like</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Likes as $Like)
                                    <tr data-url="{{ route('admin-user', ['id' => $Like->user_id]) }}">
                                        <td>{{ $Like->user_id }}</td>
                                        <td><img src="{{ asset($Like->user_avatar) }}" class="avatar_user"></td>
                                        <td>{{ $Like->user_name }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Like->created_at, true) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Date like</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="comments">
                        <table id="Comments" class="table table-bordered table-hover table-link">
                            <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th>User</th>
                                    <th>Text</th>
                                    <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                    <th style="width: 125px">Date</th>
                                    <th style="width: 70px">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Comments as $Comment)
                                    @php
                                        $ReplyComments = \App\PostComment::where('post_comments.post_id', $Post->id)->where('post_comments.reply_id', $Comment->id)->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();
                                    @endphp
                                    <tr style="cursor: default;" @if (count($ReplyComments)) class="next_tr" @endif>
                                        <td style="width: 30px">{{ $Comment->id }}</td>
                                        <td>
                                            <a href="{{ route('id_profile', ['id' => $Comment->user_id]) }}">{{ $Comment->user_name }}</a>
                                        </td>
                                        <td>
                                            {{ $Comment->comment }}
                                            @if (!is_null($Comment->image))
                                                <img src="{{ asset($Comment->image) }}" style="max-height: 150px; display: block; @if(!is_null($Comment->comment)){{ 'margin-top: 5px;' }}@endif" />
                                            @endif
                                        </td>
                                        <td>{{ \App\PostCommentLike::where('comment_id', $Comment->id)->count() }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Comment->created_at, true) }}</td>
                                        <td><button type="button" class="btn btn-danger" id="DeleteComment" data-id="{{ $Comment->id }}" style="width: 100%;">Delete</button></td>
                                    </tr>
                                    @if (count($ReplyComments))
                                        <tr style="background-color: #f5f5f5; cursor: default;">
                                            <td colspan="2" style="text-align: center; color: #b9b8b8;"><i class="fa fa-angle-double-up" style=" font-size: 22px; padding-right: 10px; vertical-align: sub; "></i>Replies to the comment</td>
                                            <td colspan="4">
                                                <table id="ReplyComments" class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 30px">ID</th>
                                                            <th>User</th>
                                                            <th>Text</th>
                                                            <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                                            <th style="width: 125px">Date</th>
                                                            <th style="width: 70px">Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($ReplyComments as $Comment)
                                                            <tr style="cursor: default;">
                                                                <td style="width: 30px">{{ $Comment->id }}</td>
                                                                <td>
                                                                    <a href="{{ route('id_profile', ['id' => $Comment->user_id]) }}">{{ $Comment->user_name }}</a>
                                                                </td>
                                                                <td>
                                                                    {{ $Comment->comment }}
                                                                    @if (!is_null($Comment->image))
                                                                        <img src="{{ asset($Comment->image) }}" style="max-height: 150px; display: block; @if(!is_null($Comment->comment)){{ 'margin-top: 5px;' }}@endif" />
                                                                    @endif
                                                                </td>
                                                                <td>{{ \App\PostCommentLike::where('comment_id', $Comment->id)->count() }}</td>
                                                                <td>{{ \App\DateConvert::dashboardDate($Comment->created_at, true) }}</td>
                                                                <td><button type="button" class="btn btn-danger" id="DeleteComment" data-id="{{ $Comment->id }}" style="width: 100%;">Delete</button></td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>User</th>
                                                            <th>Text</th>
                                                            <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                                            <th>Date</th>
                                                            <th>Delete</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>User</th>
                                    <th>Text</th>
                                    <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                    <th>Date</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!--/.col (right) -->
    </div>

    <div class="modal modal-info fade" id="UploadModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="CancelCropp" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload</h4>
                </div>
                <div class="modal-body">
                    <img class="cropper-box">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" id="CancelCropp">Cancel</button>
                    <button type="button" class="btn btn-outline" id="ApplyCropp">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/posts.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
