@extends('adminlte::page')

@section('title', 'Groups')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Groups</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Groups</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Groups" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th style="width: 70px">Avatar</th>
                                <th>Name</th>
                                <th style="width: 70px">Type</th>
                                <th style="width: 40px"><i class="fa fa-users" style="font-size: 18px;" title="Members"></i></th>
                                <th style="width: 40px"><i class="fa fa-file-image-o" style="font-size: 18px;" title="Photos"></i></th>
                                <th style="width: 40px"><i class="fa fa-file-video-o" style="font-size: 18px;" title="Videos"></i></th>
                                <th style="width: 95px">Date created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Groups as $Group)
                                <tr data-url="{{ route('admin-group', ['id' => $Group->id]) }}">
                                    <td>{{ $Group->id }}</td>
                                    <td><img src="{{ asset($Group->avatar) }}" class="avatar_user"></td>
                                    <td>{{ $Group->name }}</td>
                                    <td>{{ ucfirst($Group->type) }}</td>
                                    <td>{{ \App\GroupFollower::getCountFollowersGroup($Group->id) }}</td>
                                    <td>{{ \App\Gallery::getCountPhotosGroup($Group->id) }}</td>
                                    <td>{{ \App\VideoGallery::getCountVideoGroup($Group->id) }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($Group->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th><i class="fa fa-users" style="font-size: 18px;" title="Members"></i></th>
                                <th><i class="fa fa-file-image-o" style="font-size: 18px;" title="Photos"></i></th>
                                <th><i class="fa fa-file-video-o" style="font-size: 18px;" title="Videos"></i></th>
                                <th>Date created</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/groups.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
