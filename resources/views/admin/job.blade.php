@extends('adminlte::page')

@section('title', 'Job #'.$Job->id)

@section('content_header')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin-dashboard') }}">
                <i class="fa fa-dashboard"></i>
                Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin-jobs') }}">Job Alerts</a>
        </li>
        <li class="active">Job #{{$Job->id}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="form-group job_file">

                                @if ($Job->file_type == 'image')
                                    <img src="{{ asset($Job->file_name) }}" style="width: 100%;"
                                         class="avatar_job_page">
                                @elseif ($Job->file_type == 'video')
                                    <video width="100%" height="210px" controls>
                                        <source src="{{ asset($Job->file_name) }}">
                                        Your browser does not support the video tag.
                                    </video>
                                @endif

                            </div>
                            <div class="form-group">
                                <div class="col-md-6" style="padding: 0px 2px 0px 0px;">
                                    <button type="button" id="UploadClick" class="btn btn-primary"
                                            style="width: 100%;">Upload
                                    </button>
                                    <input type="file" id="Upload" style="display: none;">
                                </div>
                                <div class="col-md-6" style="padding: 0px 0px 0px 2px;">
                                    <button type="button" class="btn btn-danger" id="DeleteJob"
                                            style="width: 100%;">Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                        @if (!is_null($User))
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th style="width: 65px; padding-left: 0px;">User:</th>
                                    <th style="padding-left: 0px;">
                                        <a href="{{ route('admin-user', ['id' => $User->id]) }}">{{ $User->name }}</a>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-9">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#basic_info" data-toggle="tab" aria-expanded="true">Basic info</a>
                    </li>
                    <li>
                        <a href="#requests" data-toggle="tab" aria-expanded="true">Requests ({{ count($Requests) }})</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="basic_info">
                        <form id="SaveBasicInfo">
                            <input type="hidden" value="{{ $Job->id }}" name="job_id">
                            {{ csrf_field() }}
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="vertical-align: middle;">Title:</td>
                                    <td style="vertical-align: middle;">
                                        <input type="text" class="form-control" name="title" value="{{ $Job->title }}">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="2"
                                          name="description">{{ $Job->text }}</textarea>
                            </div>
                            <div class="form-group responsibilities">
                                <label>Responsibilities</label>
                                <a href="#" class="add_responsibilities">+</a>
                                @php
                                    $responsibilities = json_decode($Job->responsibilities, true);
                                @endphp
                                @if (!count($responsibilities))
                                    <input type="text" name="responsibilities[]" class="copy_row form-control">
                                @else
                                    @foreach ($responsibilities as $key => $value)
                                        <input type="text" name="responsibilities[]"
                                               class="{{ !$key ? 'copy_row' : '' }} form-control" value="{{ $value }}">
                                    @endforeach
                                @endif
                            </div>
                            @php
                                $fields = json_decode($Job->fields, true);
                            @endphp
                            <div class="form-group">
                                <label>Job Type</label>
                                <input type="text" class="form-control"
                                       value="{{ isset($fields['Job Type']) ? $fields['Job Type'] : '' }}"
                                       name="fields[Job Type]">
                            </div>
                            <div class="form-group">
                                <label>Salary</label>
                                <input type="text" class="form-control"
                                       value="{{ isset($fields['Salary']) ? $fields['Salary'] : '' }}"
                                       name="fields[Salary]">
                            </div>
                            <div class="form-group">
                                <label class="">Experience</label>
                                <input type="text" class="form-control"
                                       value="{{ isset($fields['Experience']) ? $fields['Experience'] : '' }}"
                                       name="fields[Experience]">
                            </div>
                            <div class="form-group">
                                <label class="">Location</label>
                                <input type="text" class="form-control"
                                       value="{{ isset($fields['Location']) ? $fields['Location'] : '' }}"
                                       name="fields[Location]">
                            </div>
                            <div class="form-group">
                                <label class="">Licence</label>
                                <input type="text" class="form-control"
                                       value="{{ isset($fields['Licence']) ? $fields['Licence'] : '' }}"
                                       name="fields[Licence]">
                            </div>
                            <div class="form-group">
                                <label class="">Language</label>
                                <input type="text" class="form-control"
                                       value="{{ isset($fields['Language']) ? $fields['Language'] : '' }}"
                                       name="fields[Language]">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="requests">
                        <table id="Requests" class="table table-bordered table-link">
                            <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th>User</th>
                                <th>Avatar</th>
                                <th>Message</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($Requests as $Request)
                                <tr>
                                    <td>{{ $Request->id }}</td>
                                    <td>
                                        <a href="{{ route('admin-user', ['id' => $Request->user_id]) }}">{{ $Request->user_name }}</a>
                                    </td>
                                    <td>
                                        <img src="{{ asset($Request->user_avatar) }}" class="avatar_user">
                                    </td>
                                    <td>
                                        @if (strlen($Request->message) > 25)
                                            <span data-id="{{ $Request->id }}"
                                                  class="summary_txt">{{ trim(substr($Request->message, 0, 25)) }}...</span>
                                            <a data-id="{{ $Request->id }}" class="read_more" href="#">Read more</a>
                                            <span data-id="{{ $Request->id }}" class="full_txt"
                                                  style="display: none;">{{ $Request->message }}</span>
                                        @else
                                            {{ $Request->message }}
                                        @endif
                                    </td>
                                    <td>{{ \App\DateConvert::dashboardDate($Request->created_at, true) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>User</th>
                                <th>Avatar</th>
                                <th>Message</th>
                                <th>Date</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!--/.col (right) -->
    </div>

    <div class="modal modal-info fade" id="UploadModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="CancelCropp" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload</h4>
                </div>
                <div class="modal-body">
                    <img class="cropper-box">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" id="CancelCropp">Cancel</button>
                    <button type="button" class="btn btn-outline" id="ApplyCropp">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <style>
        .responsibilities input {
            margin-top: 10px;
        }

        .responsibilities input:first-child {
            margin-top: 0px !important;
        }
    </style>
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/jobs.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
