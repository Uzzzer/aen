@extends('adminlte::page')

@section('title', 'Jobs')

@section('content_header')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin-dashboard') }}">
                <i class="fa fa-dashboard"></i>
                Dashboard
            </a>
        </li>
        <li class="active">Job Alerts</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Job Alerts</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Jobs" class="table table-bordered table-hover table-link">
                        <thead>
                        <tr>
                            <th style="width: 30px">ID</th>
                            <th>Title</th>
                            <th style="width: 70px">Status</th>
                            <th>User</th>
                            <th style="width: 40px">
                                <i class="fa fa-send-o" style="font-size: 18px;" title="Requests"></i>
                            </th>
                            <th style="width: 95px">Date created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($Jobs as $Job)
                            <tr data-url="{{ route('admin-job', ['id' => $Job->id]) }}">
                                <td>{{ $Job->id }}</td>
                                <td>{{ $Job->title }}</td>
                                <td>{{ ucfirst($Job->status) }}</td>
                                <td>
                                    <a href="{{ route('admin-user', ['id' => $Job->user_id]) }}">{{ $Job->user_name }}</a>
                                </td>
                                <td>{{ \App\JobRequest::where('job_id', $Job->id)->count() }}</td>
                                <td>{{ \App\DateConvert::dashboardDate($Job->created_at) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>User</th>
                            <th>
                                <i class="fa fa-send-o" style="font-size: 18px;" title="Requests"></i>
                            </th>
                            <th>Date created</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/jobs.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
