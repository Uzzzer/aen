@extends('adminlte::page')

@section('title', 'Group Videos')

@section('content_header')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Group Videos</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Group Videos</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="GroupVideos" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 30px">ID</th>
                                <th style="width: 70px">Preview</th>
                                <th>Title</th>
                                <th>User</th>
                                <th>Group</th>
                                <th style="width: 40px"><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th style="width: 40px"><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th style="width: 95px">Date created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($GroupVideos as $Video)
                                <tr data-url="{{ route('admin-group-video', ['id' => $Video->id]) }}">
                                    <td>{{ $Video->id }}</td>
                                    <td><img src="{{ asset($Video->preview) }}" class="avatar_user"/></td>
                                    <td>{{ $Video->title }}</td>
                                    <td><a href="{{ route('admin-user', ['id' => $Video->user_id]) }}">{{ $Video->user_name }}</a></td>
                                    <td><a href="{{ route('admin-group', ['id' => $Video->group_id]) }}">{{ $Video->group_name }}</a></td>
                                    <td>{{ \App\VideoLike::countLike($Video->id) }}</td>
                                    <td>{{ \App\MediaComment::where('video_id', $Video->id)->count() }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($Video->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Preview</th>
                                <th>Title</th>
                                <th>User</th>
                                <th>Group</th>
                                <th><i class="fa fa-thumbs-up" style="font-size: 18px;" title="Likes"></i></th>
                                <th><i class="fa fa-commenting" style="font-size: 18px;" title="Comments"></i></th>
                                <th>Date created</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/group-videos.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
