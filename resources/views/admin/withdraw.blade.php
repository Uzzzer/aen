@extends('adminlte::page')

@section('title', 'Payments')

@section('content_header')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin-dashboard') }}">
                <i class="fa fa-dashboard"></i>
                Dashboard
            </a>
        </li>
        <li class="active">Withdraw Money</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Withdraw Money</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Withdraws" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th style="width: 30px">ID</th>
                            <th style="width: 70px">Sum</th>
                            <th style="width: 95px">Status</th>
                            <th>User</th>
                            <th style="width: 95px">Date</th>
                            <th style="width: 95px">Date update</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($Withdraws as $withdraw)
                            <tr>

                                <td>{{ $withdraw->id }}</td>
                                <td>{{ $withdraw->sum }} {{ $withdraw->currency }}</td>
                                <td>
                                    @switch($withdraw->status)
                                        @case('approved')
                                        <span class="label label-success">{{ ucfirst($withdraw->status) }}</span>
                                        @break

                                        @case('pending')
                                        <span class="label label-warning">{{ ucfirst($withdraw->status) }}</span>
                                        @break

                                        @case('canceled')
                                        <span class="label label-danger">{{ ucfirst($withdraw->status) }}</span>
                                        @break
                                    @endswitch
                                </td>
                                <td>
                                    <a href="{{ route('admin-user', ['id' => $withdraw->user_id]) }}">{{ $withdraw->user_name }}</a>
                                </td>
                                <td>{{ \App\DateConvert::dashboardDate($withdraw->created_at, true) }}</td>
                                <td>{{ \App\DateConvert::dashboardDate($withdraw->updated_at, true) }}</td>
                                <td>
                                    @if ($withdraw->status == 'pending')
                                        <button type="button" data-id="{{ $withdraw->id }}" class="btn btn-success approved_withdraw">Approved
                                        </button>
                                        <button type="button" data-id="{{ $withdraw->id }}" class="btn btn-danger canceled_withdraw">Canceled
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Sum</th>
                            <th>Status</th>
                            <th>User</th>
                            <th>Date</th>
                            <th>Date update</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/withdraw.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
