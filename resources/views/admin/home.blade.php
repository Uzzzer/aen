@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard" style="margin-right: 5px;"></i> Dashboard</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-4">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ \App\User::where('role', '!=', 'admin')->count() }}</h3>
                    <p>Users</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="{{ route('admin-users') }}" class="small-box-footer">All users <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-md-4">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ \App\Group::count() }}</h3>
                    <p>Groups</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="{{ route('admin-groups') }}" class="small-box-footer">All groups <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-md-4">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ \App\Post::leftjoin('users', 'posts.user_id', '=', 'users.id')->leftjoin('groups', 'posts.group_id', '=', 'groups.id')->count() }}</h3>
                    <p>Posts</p>
                </div>
                <div class="icon">
                    <i class="fa fa-fw fa-tasks"></i>
                </div>
                <a href="{{ route('admin-posts') }}" class="small-box-footer">All posts <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Recent users</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Users" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th style="width: 50px">Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th style="width: 50px">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Users as $User)
                                <tr data-url="{{ route('admin-user', ['id' => $User->id]) }}">
                                    <td>{{ $User->id }}</td>
                                    <td><img src="{{ asset($User->avatar) }}" class="avatar_user"></td>
                                    <td>{{ $User->name }}</td>
                                    <td>{{ ($User->business) ? 'Business' : 'User' }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($User->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Recent groups</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Groups" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th style="width: 50px">Avatar</th>
                                <th>Name</th>
                                <th style="width: 50px">Type</th>
                                <th style="width: 50px">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Groups as $Group)
                                <tr data-url="{{ route('admin-group', ['id' => $Group->id]) }}">
                                    <td>{{ $Group->id }}</td>
                                    <td><img src="{{ asset($Group->avatar) }}" class="avatar_user"></td>
                                    <td>{{ $Group->name }}</td>
                                    <td>{{ ucfirst($Group->type) }}</td>
                                    <td>{{ \App\DateConvert::dashboardDate($Group->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Avatar</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Recent posts</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="Posts" class="table table-bordered table-hover table-link">
                        <thead>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>Title</th>
                                <th style="width: 70px">Post type</th>
                                <th>User</th>
                                <th style="width: 50px">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Posts as $Post)
                                <tr data-url="{{ route('admin-post', ['id' => $Post->id]) }}">
                                    <td>{{ $Post->id }}</td>
                                    <td>{{ $Post->title }}</td>
                                    <td>{{ ucfirst(str_replace('_', ' ', $Post->post_type)) }}</td>
                                    <td><a href="{{ route('admin-user', ['id' => $Post->user_id]) }}">{{ $Post->user_name }}</a></td>
                                    <td>{{ \App\DateConvert::dashboardDate($Post->created_at) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Post type</th>
                                <th>User</th>
                                <th>Date</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/dashboard.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
