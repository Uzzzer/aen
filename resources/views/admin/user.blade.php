@extends('adminlte::page')

@section('title', 'User #'.$User->id)

@section('content_header')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin-dashboard') }}">
                <i class="fa fa-dashboard"></i>
                Dashboard
            </a>
        </li>
        <li>
            <a href="{{ route('admin-users') }}">Users</a>
        </li>
        <li class="active">User #{{$User->id}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $User->name }}</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="form-group">
                                <img src="{{ asset($User->avatar) }}" style="width: 100%;" class="avatar_user_page">
                            </div>
                            <div class="form-group">
                                <div class="col-md-4" style="padding: 0px 2px 0px 0px;">
                                    <button type="button" id="AvatarUploadClick" class="btn btn-primary"
                                            style="width: 100%;">Upload Avatar
                                    </button>
                                    <input type="file" id="AvatarUpload" style="display: none;">
                                </div>
                                <div class="col-md-4" style="padding: 0px 2px 0px 2px;">
                                    <a href="{{ route('admin-login', ['id' => $User->id]) }}">
                                        <button type="button" class="btn btn-primary" style="width: 100%;">Login
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-4" style="padding: 0px 0px 0px 2px;">
                                    @if ($User->status == 'blocked')
                                        <button type="button" class="btn btn-primary" id="UnblockUser"
                                                style="width: 100%;">Unblock
                                        </button>
                                    @else
                                        <button type="button" class="btn btn-danger" id="BlockUser"
                                                style="width: 100%;">Block
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-9">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#basic_info" data-toggle="tab" aria-expanded="true">Basic info</a>
                    </li>
                    @if ($User->business)
                        <li class="">
                            <a href="#connections" data-toggle="tab"
                               aria-expanded="false">Fans ({{ \App\ConnectionRequest::countConnection($User->id, false, false) }})
                            </a>
                        </li>
                        {{--<li class="">
                            <a href="#associates" data-toggle="tab" aria-expanded="false">Associates (0)</a>
                        </li>--}}
                        <li class="">
                            <a href="#worked_with" data-toggle="tab"
                               aria-expanded="false">Worked with ({{ \App\WorksWith::countWorked($User->id) }})
                            </a>
                        </li>
                        <li class="">
                            <a href="#groups" data-toggle="tab"
                               aria-expanded="false">Groups ({{ \App\GroupFollower::getCountFollow($User->id) }})
                            </a>
                        </li>
                        <li class="">
                            <a href="#posts" data-toggle="tab"
                               aria-expanded="false">Posts ({{ \App\Post::countPosts($User->id) }})
                            </a>
                        </li>
                        <li class="">
                            <a href="#videos" data-toggle="tab" aria-expanded="false">Videos ({{ count($Videos) }})</a>
                        </li>
                        <li class="">
                            <a href="#photos" data-toggle="tab" aria-expanded="false">Photos ({{ count($Photos) }})</a>
                        </li>
                        <li class="">
                            <a href="#chats" data-toggle="tab" aria-expanded="false">Chats ({{ count($Chats) }})</a>
                        </li>
                        @if ($User->user_category_id == 10)
                            <li class="">
                                <a href="#jobs" data-toggle="tab"
                                   aria-expanded="false">Job Alerts ({{ count($Jobs) }})
                                </a>
                            </li>
                        @endif
                    @else
                        <li class="">
                            <a href="#connections" data-toggle="tab"
                               aria-expanded="false">Connections ({{ \App\ConnectionRequest::countConnection($User->id, false, false) }})
                            </a>
                        </li>
                        <li class="">
                            <a href="#companies" data-toggle="tab"
                               aria-expanded="false">Companies ({{ \App\WorksWith::countCompanies($User->id) }})
                            </a>
                        </li>
                        <li class="">
                            <a href="#groups" data-toggle="tab"
                               aria-expanded="false">Groups ({{ \App\GroupFollower::getCountFollow($User->id) }})
                            </a>
                        </li>
                        <li class="">
                            <a href="#posts" data-toggle="tab"
                               aria-expanded="false">Posts ({{ \App\Post::countPosts($User->id) }})
                            </a>
                        </li>
                        <li class="">
                            <a href="#photos" data-toggle="tab" aria-expanded="false">Photos ({{ count($Photos) }})</a>
                        </li>
                        <li class="">
                            <a href="#chats" data-toggle="tab" aria-expanded="false">Chats ({{ count($Chats) }})</a>
                        </li>
                        <li class="">
                            <a href="#purchased_video" data-toggle="tab"
                               aria-expanded="false">Purchased Video ({{ \App\PurchasedVideo::countPurchase($User->id) }})
                            </a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content">
                    @if ($User->business)
                        <div class="tab-pane active" id="basic_info">
                            <form id="SaveBasicInfo">
                                <input type="hidden" value="{{ $User->id }}" name="user_id">
                                {{ csrf_field() }}
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td style="vertical-align: middle;">Name:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="name"
                                                   value="{{ $User->name }}">
                                        </td>
                                        <td style="vertical-align: middle;">Location:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="location"
                                                   value="{{ $User->location }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Website:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="website"
                                                   value="{{ $User->website }}">
                                        </td>
                                        <td style="vertical-align: middle;"></td>
                                        <td style="vertical-align: middle;"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label style="display: block;">Categories</label>
                                    @php
                                        $avaliable = json_decode($User->avaliable, true);

                                        if (!is_array($avaliable))
                                            $avaliable = [];

                                        $CategoriesAvailable = \App\CategoriesAvailable::get();
                                    @endphp
                                    @foreach ($CategoriesAvailable as $key => $CategoryAvailable)
                                        <div class="col-md-2">
                                            <label for="avaliable{{$key}}">
                                                <input type="checkbox" id="avaliable{{$key}}" name="avaliable[]"
                                                       value="{{ $CategoryAvailable->name }}"
                                                       @if (in_array($CategoryAvailable->name, $avaliable)) checked @endif>
                                                {{ $CategoryAvailable->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-group">
                                    <label>Blog</label>
                                    <textarea class="form-control" rows="2"
                                              name="about_me">{{ $User->about_me }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="connections">
                            <table id="ConnectionRequests" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($ConnectionRequests as $Request)
                                    <tr data-url="{{ route('admin-user', ['id' => $Request->user_id]) }}">
                                        <td>{{ $Request->user_id }}</td>
                                        <td>
                                            <img src="{{ asset($Request->user_avatar) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Request->user_name }}</td>
                                        <td>{{ ($Request->business) ? 'Business' : 'User' }}</td>
                                        <td>{{ ($Request->status == 'request') ? 'Request' : 'Connection' }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="associates">

                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="worked_with">
                            @php
                                $WorkedWith = \App\WorksWith::where('business_id', $User->id)->join('users', 'works_with.user_id', '=', 'users.id')->get();
                            @endphp
                            <table id="WorksWith" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($WorkedWith as $Work)
                                    <tr data-url="{{ route('admin-user', ['id' => $Work->id]) }}">
                                        <td>{{ $Work->id }}</td>
                                        <td>
                                            <img src="{{ asset($Work->avatar) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Work->name }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="groups">
                            <table id="Groups" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Groups as $Group)
                                    <tr data-url="{{ route('admin-group', ['id' => $Group->id]) }}">
                                        <td>{{ $Group->id }}</td>
                                        <td>
                                            <img src="{{ asset($Group->avatar) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Group->name }}</td>
                                        <td>{{ ucfirst($Group->type) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="posts">
                            <table id="Posts" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Media type</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Date create</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Posts as $Post)
                                    <tr data-url="{{ route('admin-post', ['id' => $Post->id]) }}">
                                        <td>{{ $Post->id }}</td>
                                        <td>{{ (is_null($Post->file_type)) ? 'No media' : ucfirst($Post->file_type) }}</td>
                                        <td>{{ $Post->title }}</td>
                                        <td>{{ ucfirst($Post->post_type) }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Post->created_at) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Media type</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Date create</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="videos">
                            <table id="Videos" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Preview</th>
                                    <th>Title</th>
                                    <th style="width: 70px">Price</th>
                                    <th style="width: 95px">Date create</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Videos as $Video)
                                    <tr data-url="{{ route('admin-video', ['id' => $Video->id]) }}">
                                        <td>{{ $Video->id }}</td>
                                        <td>
                                            <img src="{{ asset($Video->preview_image) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Video->title }}</td>
                                        <td>{{ $Video->price }} {{ \App\Option::option('currency') }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Video->created_at) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Preview</th>
                                    <th>Title</th>
                                    <th>Price</th>
                                    <th>Date create</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="photos">
                            <table id="Photos" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Photo</th>
                                    <th>Title</th>
                                    <th style="width: 95px">Date create</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Photos as $Photo)
                                    <tr data-url="{{ route('admin-photo', ['id' => $Photo->id]) }}">
                                        <td>{{ $Photo->id }}</td>
                                        <td>
                                            <img src="{{ asset($Photo->file_name) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Photo->title }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Photo->created_at) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Photo</th>
                                    <th>Title</th>
                                    <th>Date create</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="chats">
                            <table id="Chats" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th>Users</th>
                                    <th style="width: 40px">
                                        <i class="fa fa-commenting-o" style="font-size: 18px;"
                                           title="Count messages"></i>
                                    </th>
                                    <th style="width: 95px">Last message</th>
                                    <th style="width: 95px">Date created</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Chats as $id => $Chat)
                                    <tr data-url="{{ route('admin-chat', ['id' => $id]) }}">
                                        <td>{{ $id }}</td>
                                        <td>
                                            @foreach ($Chat['users'] as $key => $user)
                                                @if ($user->id != $User->id)
                                                    <a href="{{ route('admin-user', ['id' => $user->id]) }}">{{ $user->name }}</a>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{ $Chat['count_messages'] }}</td>
                                        <td>{{ $Chat['last_message'] }}</td>
                                        <td>{{ $Chat['date_created'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Users</th>
                                    <th>
                                        <i class="fa fa-commenting-o" style="font-size: 18px;"
                                           title="Count messages"></i>
                                    </th>
                                    <th>Last message</th>
                                    <th>Date created</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        @if ($User->user_category_id == 10)
                            <div class="tab-pane" id="jobs">
                                <table id="Jobs" class="table table-bordered table-hover table-link">
                                    <thead>
                                    <tr>
                                        <th style="width: 30px">ID</th>
                                        <th>Title</th>
                                        <th style="width: 70px">Status</th>
                                        <th style="width: 40px">
                                            <i class="fa fa-send-o" style="font-size: 18px;" title="Requests"></i>
                                        </th>
                                        <th style="width: 95px">Date created</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($Jobs as $Job)
                                        <tr data-url="{{ route('admin-job', ['id' => $Job->id]) }}">
                                            <td>{{ $Job->id }}</td>
                                            <td>{{ $Job->title }}</td>
                                            <td>{{ ucfirst($Job->status) }}</td>
                                            <td>{{ \App\JobRequest::where('job_id', $Job->id)->count() }}</td>
                                            <td>{{ \App\DateConvert::dashboardDate($Job->created_at) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Status</th>
                                        <th>
                                            <i class="fa fa-send-o" style="font-size: 18px;" title="Requests"></i>
                                        </th>
                                        <th>Date created</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                        @endif
                    @else
                        <div class="tab-pane active" id="basic_info">
                            <form id="SaveBasicInfo">
                                <input type="hidden" value="{{ $User->id }}" name="user_id">
                                {{ csrf_field() }}
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td style="vertical-align: middle;">Name:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="name"
                                                   value="{{ $User->name }}">
                                        </td>
                                        <td style="vertical-align: middle;">Location:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="location"
                                                   value="{{ $User->location }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Nationality:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="nationality"
                                                   value="{{ $User->nationality }}">
                                        </td>
                                        <td style="vertical-align: middle;">Height:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="height"
                                                   value="{{ $User->height }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Birthday:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="birthday"
                                                   value="{{ $User->birthday }}">
                                        </td>
                                        <td style="vertical-align: middle;">Weight:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="weight"
                                                   value="{{ $User->weight }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Profession:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="profession"
                                                   value="{{ $User->profession }}">
                                        </td>
                                        <td style="vertical-align: middle;">Chest:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="chest"
                                                   value="{{ $User->chest }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Experience:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="experience"
                                                   value="{{ $User->experience }}">
                                        </td>
                                        <td style="vertical-align: middle;">Waist:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="waist"
                                                   value="{{ $User->waist }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Sex:</td>
                                        <td style="vertical-align: middle;">
                                            <select name="sex" class="form-control">
                                                <option value="Female"
                                                        @if ($User->sex == 'Female') selected @endif>Female
                                                </option>
                                                <option value="Male" @if ($User->sex == 'Male') selected @endif>Male
                                                </option>
                                            </select>
                                        </td>
                                        <td style="vertical-align: middle;">Hips:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="hips"
                                                   value="{{ $User->hips }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Website:</td>
                                        <td style="vertical-align: middle;">
                                            <input type="text" class="form-control" name="website"
                                                   value="{{ $User->website }}">
                                        </td>
                                        <td style="vertical-align: middle;"></td>
                                        <td style="vertical-align: middle;"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label style="display: block;">Available</label>
                                    @php
                                        $avaliable = json_decode($User->avaliable, true);

                                        if (!is_array($avaliable))
                                            $avaliable = [];

                                        $CategoriesAvailable = \App\CategoriesAvailable::get();
                                    @endphp
                                    @foreach ($CategoriesAvailable as $key => $CategoryAvailable)
                                        <div class="col-md-2">
                                            <label for="avaliable{{$key}}">
                                                <input type="checkbox" id="avaliable{{$key}}" name="avaliable[]"
                                                       value="{{ $CategoryAvailable->name }}"
                                                       @if (in_array($CategoryAvailable->name, $avaliable)) checked @endif>
                                                {{ $CategoryAvailable->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-group">
                                    <label>Blog</label>
                                    <textarea class="form-control" rows="2"
                                              name="about_me">{{ $User->about_me }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" style="width: 150px;">Save</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="connections">
                            <table id="ConnectionRequests" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($ConnectionRequests as $Request)
                                    <tr data-url="{{ route('admin-user', ['id' => $Request->user_id]) }}">
                                        <td>{{ $Request->user_id }}</td>
                                        <td>
                                            <img src="{{ asset($Request->user_avatar) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Request->user_name }}</td>
                                        <td>{{ ($Request->business) ? 'Business' : 'User' }}</td>
                                        <td>{{ ($Request->status == 'request') ? 'Request' : 'Connection' }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="companies">
                            @php
                                $WorksWith = \App\WorksWith::where('user_id', $User->id)->join('users', 'works_with.business_id', '=', 'users.id')->select('users.*')->get();
                            @endphp
                            <table id="WorksWith" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($WorksWith as $Work)
                                    <tr data-url="{{ route('admin-user', ['id' => $Work->id]) }}">
                                        <td>{{ $Work->id }}</td>
                                        <td>
                                            <img src="{{ asset($Work->avatar) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Work->name }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="groups">
                            <table id="Groups" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Groups as $Group)
                                    <tr data-url="{{ route('admin-group', ['id' => $Group->id]) }}">
                                        <td>{{ $Group->id }}</td>
                                        <td>
                                            <img src="{{ asset($Group->avatar) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Group->name }}</td>
                                        <td>{{ ucfirst($Group->type) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="posts">
                            <table id="Posts" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Media type</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Date create</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Posts as $Post)
                                    <tr data-url="{{ route('admin-post', ['id' => $Post->id]) }}">
                                        <td>{{ $Post->id }}</td>
                                        <td>{{ (is_null($Post->file_type)) ? 'No media' : ucfirst($Post->file_type) }}</td>
                                        <td>{{ $Post->title }}</td>
                                        <td>{{ ucfirst($Post->post_type) }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Post->created_at) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Media type</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Date create</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="photos">
                            <table id="Photos" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Photo</th>
                                    <th>Title</th>
                                    <th style="width: 95px">Date create</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Photos as $Photo)
                                    <tr data-url="{{ route('admin-photo', ['id' => $Photo->id]) }}">
                                        <td>{{ $Photo->id }}</td>
                                        <td>
                                            <img src="{{ asset($Photo->file_name) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Photo->title }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Photo->created_at) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Photo</th>
                                    <th>Title</th>
                                    <th>Date create</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="purchased_video">
                            <table id="Videos" class="table table-bordered table-hover table-link">
                                <thead>
                                <tr>
                                    <th style="width: 30px">ID</th>
                                    <th style="width: 70px">Preview</th>
                                    <th>Title</th>
                                    <th style="width: 70px">Cost</th>
                                    <th style="width: 120px">Purchase date</th>
                                    <th style="width: 95px">Date create</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($Videos as $Video)
                                    <tr data-url="{{ route('admin-video', ['id' => $Video->video_id]) }}">
                                        <td>{{ $Video->video_id }}</td>
                                        <td>
                                            <img src="{{ asset($Video->preview_image) }}" class="avatar_user">
                                        </td>
                                        <td>{{ $Video->title }}</td>
                                        <td>{{ $Video->cost }} {{ $Video->currency }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Video->created_at) }}</td>
                                        <td>{{ \App\DateConvert::dashboardDate($Video->video_created_at) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Preview</th>
                                    <th>Title</th>
                                    <th>Cost</th>
                                    <th>Purchase date</th>
                                    <th>Date create</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    @endif
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
        <!--/.col (right) -->
    </div>

    <div class="modal modal-info fade" id="UploadAvatarModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="CancelCropp" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Upload Avatar</h4>
                </div>
                <div class="modal-body">
                    <img class="cropper-box">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" id="CancelCropp">Cancel</button>
                    <button type="button" class="btn btn-outline" id="ApplyCropp">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@stop

@section('adminlte_js')
    <script src="{{ asset('/js/admin/users.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
