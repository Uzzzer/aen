    var last_file_changed = false;

$(document).ready(function () {

    $('.plagin_btn_block').first().addClass('plagin_btn_block_active');
    $('.plagin_block').first().addClass('plagin_block_active');


    $(".tabs-item li").on("click", function(){
        let idx = $(this).index();

        $(".tabs-item li").removeClass("active");
        $(this).addClass("active");

        $(".tab-content-item").hide();

        $(".tab-content-item").eq(idx).show();

    });

    $(document).on('click', '.message_no_subscription', function () {
        Swal.fire({
            title: 'Subscription required',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#f8bb86',
            confirmButtonText: 'Subscribe Now'
        }).then((result) => {
            if (result.value) {
                window.location.href = "/profile/subscription";
            }
        });

        return false;
    });

    $(document).on('click', '.hover-block', function () {

        if ($(this).hasClass('return-true'))
            return true;

        $(this).toggleClass('opened');

        if ($(this).hasClass('opened')) {
            $(this).closest('.post_box').find('.detail-bottom-body').show(300);
            $(this).closest('.post_box').find('.detail-bottom-footer').show(300).css('display', 'flex');
        } else {
            $(this).closest('.post_box').find('.detail-bottom-body').hide(300);
            $(this).closest('.post_box').find('.detail-bottom-footer').hide(300);
            $(this).closest('.post_box').find('.coment_write').hide(300);
        }

        return false;
    });

    /* Register */

    /*$('#business_checbox').on('change', function() {
        if ($(this).is(':checked')) {
            $('select[name="user_category_id"]').show('fast');
            $('input[name="address"]').show('fast');
        } else {
            $('select[name="user_category_id"]').hide('fast');
            $('input[name="address"]').hide('fast');
        }

    });*/

    $('#user_category_id').on('change', function() {
        if ($(this).val() === '1') {
            // $('input[name="address"]').removeAttr('required').hide('fast');
            $('input[name="address"]').hide('fast');
            $('input[name="full_name"]').hide('fast');
            $('input[name="address2"]').hide('fast');
            $('input[name="location"]').hide('fast');
            $('input[name="state"]').hide('fast');
            $('input[name="postal_code"]').hide('fast');
            $('select[name="country_id"]').hide('fast');
            $('.terms_and_conditions').hide('fast');
            $('.no_dob').hide('fast');


        } else {
            // $('input[name="address"]').attr('required', 'required').show('fast');
            $('input[name="address"]').show('fast');
            $('input[name="full_name"]').show('fast');
            $('input[name="address2"]').show('fast');
            $('input[name="location"]').show('fast');
            $('input[name="state"]').show('fast');
            $('input[name="postal_code"]').show('fast');
            $('select[name="country_id"]').show('fast');
            $('.terms_and_conditions').show('fast');
            $('.no_dob').show('fast');

        }
    });

    /* Search */

    $('.SearchInput').on('keyup', function(event) {
        if(event.keyCode == 13) {
            window.location.href = "/search?search="+$(this).val();
        }
    });

    /* Notification Settings */

    $('#NotificationSettingsSubmit').on('click', function() {
        $('#NotificationSettings').submit();
    });

    $('#NotificationSettingsBoxShow').on('click', function() {
        if ($('.NotificationSettingsBox').css('display') == 'none')
            $('.NotificationSettingsBox').show('fast');
        else
            $('.NotificationSettingsBox').hide('fast');
    });

    $('#NotificationSettings').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/profile/save_notifications_setting',
            data: $(this).serialize(),
            beforeSend: function() {
                search = true;
            },
            success: function(data) {
                if (data.status == 'success') {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', data.message, 'error');
                }

                search = false;
            }
        });

        return false;
    });

    /* profile-tab-search */

    let search = false;

    $('#SearchVideos').on('submit', function() {
        if (search == false)
            $.ajax({
                type: 'POST',
                url: '/profile/video/search',
                data: $(this).serialize(),
                beforeSend: function() {
                    search = true;
                },
                success: function(data) {
                    if (data.status == 'success') {
                        $('.videos-box-list').empty();

                        for (key in data.posts) {
                            let $new = $(data.posts[key]).hide();

                            $('.videos-box-list').append($new);

                            $new.show('fast');
                        }

                    } else {
                        swal('Error', data.message, 'error');
                    }

                    search = false;
                }
            });

        return false;
    });

    $('#SearchVideos input[name="search"]').on('keyup', function(event) {
        if(event.keyCode == 13) {
            $(this).closest('form.search-line').submit();
            return false;
        }
    });

    $('#SearchPhotos').on('submit', function() {
        if (search == false)
            $.ajax({
                type: 'POST',
                url: '/gallery/search',
                data: $(this).serialize(),
                beforeSend: function() {
                    search = true;
                },
                success: function(data) {
                    if (data.status == 'success') {
                        $('.photos-box-list').empty();

                        for (key in data.posts) {
                            let $new = $(data.posts[key]).hide();

                            $('.photos-box-list').append($new);

                            $new.show('fast');
                        }

                    } else {
                        swal('Error', data.message, 'error');
                    }

                    search = false;
                }
            });

        return false;
    });

    $('#SearchPhotos input[name="search"]').on('keyup', function(event) {
        if(event.keyCode == 13) {
            $(this).closest('form.search-line').submit();
            return false;
        }
    });

    /* Works with edit*/
    $(".works_with_tittle").click(function () {
        $(".work-with-box").hide();
        $(".works_with_container").show(300);
    })

    $(".works_with_btn ").click(function () {
        $(this).parents(".works_with_block").find(".worked_save_container").show(100);
    })
    $(".work_save_btn").click(function () {
        $(".work-with-box").show(300);
        $(".worked_save_container").hide();
        $(".works_with_container").hide();
    })

    /* upload show block*/
    $(".photo_edit_block").click(function () {
        $(".photo_edit_block").hide();
        $(".add_to_gallary").show(300);
    })
    /*  upload input  */

    function readURL(input , eq) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(".uploadedImg")[eq].style.backgroundImage = 'url(' + e.target.result + ')';
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file").on('change', function (e) {
    	last_file_changed = this;
        var eq = $(this).parents(".add_to_gallary_block").index();
        readURL(this , eq);
        $(this).parents(".imageUploadForm").addClass('loading');

        let files = e.target.files;
        let $this = $(this);

        let fData = new FormData;
        fData.append('dirname', 'photos');
        fData.append('preview', 1);
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                	window.gallery_cropp_file = data.uploads;
                    $this.closest('form.imageUploadForm').find('input[name="photo"]').val(data.uploads);
                    $this.closest('form.imageUploadForm').find('input[name="preview"]').val(data.preview);

                    $('#UploadAvatarBox').show();
                    $('body').addClass('overflow');
                    $('#UploadAvatarBox img').attr('src', URL.createObjectURL(files[0]));

                    window.ccro = $('#UploadAvatarBox img').cropper({
                        aspectRatio: 1 / 1,
                        zoomable: false,
                        viewMode: 1,
                        minCropBoxWidth: 200,
                        minCropBoxHeight: 200
                    });

                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });
    });


    /* Available for */
    $(".avaliable_for").click(function () {

        $(".avaliable_checkbox").show();
        $(".profile-avaliable-txt").hide();
         
    });
    var text_input;
    /*$(".avaliable_save").click(function () {
        $(".profile-avaliable-txt, .avaliable_checkbox").toggle();
        $('.profile-avaliable-txt').empty()
        var input =  $(".switch input");
        for (var i = 0; i < input.length; i++) {
            if (input[i].checked == true) {
        
                $('.profile-avaliable-txt').append('<span>' + $(".checkbox_name").eq(i).text() + '</span>');             
            }
        }    
        $(".avaliable_checkbox").hide();
        $(".profile-avaliable-txt").show();
    });*/


    /* */
    $(".l_close").click(function () {
        $(".m_left_block").css("display", "none");
        $("body").removeClass("popup-opened");
    });
    $(".bottom-menu li:nth-child(2)").click(function () {
        $("body").addClass("popup-opened");
        $(".m_left_block").css("display", "block");
    });
    /*lightbox*/
    var showNav = 1;
    var keyboardNav = 1;
    var showCaption = 1;
    var loop = 1;


    /* tab plagin */
    $(".plagin_btn_block").click(function () {
        var tab_eq = $(this).attr("data-tab");
        $(".plagin_btn_block").removeClass("plagin_btn_block_active");
        $(this).addClass("plagin_btn_block_active");
        $(".plagin_block").css("display", "none");
        $(".plagin_block").eq(tab_eq).css("display", "block");
    });


    //Create overlay items
    var $overlay = $('<div id="overlay"></div>');
    var $image = $('<img>');
    var $holder = $('<div class="holder"></div>');
    var $caption = $('<p></p>');
    var $nav = $('<div class="nav"><a id="close" nohref><span>&times;</span></a><a id="prev" nohref><span>&#8592;</span></a><a id="next" nohref><span>&#8594;</span></a></nav>');
    var glength = $('#imageGallery li').length;
    var imageIndex, imageLocation, captionText, allowKeyboard;

    //Add overlay items to DOM
    $('#overlay img').before('');
    $holder.append($image);
    $holder.append($nav);
    if (showCaption) { $holder.append($caption); }
    if (!showNav) { $nav.hide(); }
    $overlay.append($holder);
    $("body").append($overlay);

    //Click image link
    $('#imageGallery a.open_photo').click(function (event) {
        event.preventDefault();
        imageLocation = $(this).attr("href");

        // index of clicked item
        imageIndex = $('#imageGallery a').index(this) + 1;

        //Image focused on
        $image.attr("src", imageLocation);

        //Add and remove active class on link
        $(this).addClass('active');
        $('#imageGallery a').not(this).removeClass('active');

        //Show alt attribute as caption
        captionText = $(this).children("img").attr("alt");
        $caption.text(captionText);

        allowKeyboard = 1;
        $overlay.show();
    });

    //Close overlay
    $image.click(function () {
        allowKeyboard = 0;
        $($overlay).hide();
    });

    $overlay.click(function (e) {
        if (e.target != this) return;
        $(this).hide();
    });

    $('#close').click(function () {
        $($overlay).hide();
    });
    //Cycle images
    $('#next').click(function () {
        //check to see if its the last image
        if (glength != imageIndex) {
            $('.active').closest('li').next().find('img').trigger('click');
        } else if (loop == 1) {
            $('#imageGallery li').first().find('img').trigger('click');
        }
    });

    $("body").keydown(function (e) {
        if (keyboardNav == 1 && allowKeyboard == 1) {
            if (e.which == 37) { $('#prev').trigger("click"); }
            else if (e.which == 39) { $('#next').trigger("click"); }
            else if (e.which == 27) { $($overlay).hide(); }
        }
    });

    $('#prev').click(function () {
        //check to see if its the first image	
        if (imageIndex != 1) {
            $('.active').closest('li').prev().find('img').trigger('click');
        } else if (loop == 1) {
            $('#imageGallery li').last().find('img').trigger('click');
        }
    });

    /* summit tab */
    $(".summ_btn").click(function () {
        $(".summ_btn").removeClass("sum_li_active");
        $(this).addClass("sum_li_active");
        $(".summit_tab_block").hide();
        $(".summit_tab_block").eq($(this).index()).show();
    })


    try {
    //  UPLOAD MODAL
    const Q = el => document.querySelector(el)
    let isFileOk

    Q("#file").addEventListener("change", function () {
        isFileOk = false
        const { size, type, name } = this.files[0]

        if (size < 4000000 && type === "image/jpeg")
            isFileOk = true

        if (size < 4000000 && type === "image/png")
            isFileOk = true

        Q("p").innerHTML = `${name}, ${Math.round(size / 1000)} ko`
        showFileCheck(isFileOk)
        showImage(isFileOk, this.files[0])
    })

    const showFileCheck = fileState => {
        if (fileState) {
            removeClass(".file-name", "bad-input")
            addClass(".input-submit", "set-input-submit")
            addClass(".horizontal-line", "set-horizontal-line")
        } else {
            addClass(".file-name", "bad-input")
            removeClass(".input-submit", "set-input-submit")
            removeClass(".horizontal-line", "set-horizontal-line")
        }
    }

    const showImage = (test, file) => {
        const img = Q(".image-wrapper img")
        if (test) {
            img.src = URL.createObjectURL(file)
            Q(".upload-icon").style.display = "none"
            img.style.display = "block"
        } else {
            img.style.display = "none"
            Q(".upload-icon").style.display = "block"
            img.src = ""
        }
    }

    const addClass = (element, nameClass) => Q(element).classList.add(nameClass)
    const removeClass = (element, nameClass) => Q(element).classList.remove(nameClass)

    } catch (err) {
        console.log(err);
    }

    $(".Upl_new_ph").click(function () {
        $(".modal_upload").css("display", "flex");
    });

    $(".close_modal_up").click(function () {
        $(".modal_upload").css("display", "none");
    });


    $('body').on("click", ".dot_down-btn", function(e){
        $(this).closest(".drop_tp-post_box").find(".drop_content_inner_list").toggleClass("active");
    });

    $(document).on("mouseup", function(e){

        var target = e.target;
        var dotBtn = $(".dot_down-btn");
        var toolsDropMenu = $(".drop_content_inner_list");

        if (!dotBtn.is(target) && dotBtn.has(target).length === 0 && !toolsDropMenu.is(target) && toolsDropMenu.has(target).length === 0) {
            toolsDropMenu.removeClass("active");
        }
        
    });


});
