$(document).ready(function() {
    $('#WithdrawMoneySubmit').on('click', function() {
        $.ajax({
            type: 'POST',
            url: '/profile/withdraw-money',
            data: $('#WithdrawMoney').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Money will be sent to your bank account.', 'success');
                    setTimeout(function() {
                        window.location.reload();
                    }, 1500);
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });
    });

    $('#BalanceLogs').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : false,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 20
    })
});