$(document).ready(function () {

    /* Posts */

    $('.post-bar-link').click(function (e) {
        e.preventDefault();
        if (!$('.post-bar.new-post-bar').hasClass('active')) {
            $('html, body').animate({scrollTop: 0}, 500);
            if ($('input[name="draft_id"]').length) {
                $('input[name="draft_id"]').remove();
                $('#NewPost')[0].reset();
                $('#upload-bg.image-post').attr('src', '/img/upload-bg.jpg');
                $('input[name="file_type"].image-post').val('');
                $('input[name="file_name"].image-post').val('');
            }

            if ($('input[name="post_id"]').length) {
                $('input[name="post_id"]').remove();
                $('#NewPost')[0].reset();
                $('#upload-bg.image-post').attr('src', '/img/upload-bg.jpg');
                $('input[name="file_type"].image-post').val('');
                $('input[name="file_name"].image-post').val('');
                $('input[type="submit"].post-button').val('Post');
            }
        }

        $('.post-bar.new-post-bar').toggleClass('active');
    });

    let upload_file = false;
    let cropp_file = false;

    if ($('input[name="load_page_type"]').length > 0) {
        let page;

        if ($('input[name="load_page_type"]').val() == 'load_search')
            page = {
                'posts': 1,
                'photos': 1,
                'videos': 1,
                'business': 1,
                'professionals': 1
            };
        else
            page = 1;

        let load_page = false;

        if ($('input[name="load_page_type"]').val() == 'load_index') {
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 800 && !load_page) {
                    page++;
                    $.ajax({
                        type: 'POST',
                        url: '/post/get_page',
                        data: {
                            _token: $('input[name="_token"]').val(),
                            load_page_type: $('input[name="load_page_type"]').val(),
                            page: page
                        },
                        beforeSend: function () {
                            load_page = true;
                        },
                        success: function (data) {
                            if (data.status == 'success') {

                                jQuery.each(data.Posts, function (i, val) {
                                    $('.post_load_list').append(val);
                                });

                                load_page = false;
                            }
                        }
                    });
                }
            });
        }

        if ($('input[name="load_page_type"]').val() == 'load_draft') {
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 800 && !load_page) {
                    page++;
                    $.ajax({
                        type: 'POST',
                        url: '/post/get_page',
                        data: {
                            _token: $('input[name="_token"]').val(),
                            load_page_type: $('input[name="load_page_type"]').val(),
                            page: page
                        },
                        beforeSend: function () {
                            load_page = true;
                        },
                        success: function (data) {
                            if (data.status == 'success') {

                                jQuery.each(data.Posts, function (i, val) {
                                    $('.post_load_list').append(val);
                                });

                                load_page = false;
                            }
                        }
                    });
                }
            });
        }

        if ($('input[name="load_page_type"]').val() == 'load_tag') {
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 800 && !load_page) {
                    page++;
                    $.ajax({
                        type: 'POST',
                        url: '/post/get_page',
                        data: {
                            _token: $('input[name="_token"]').val(),
                            load_page_type: $('input[name="load_page_type"]').val(),
                            tag: $('input[name="tag"]').val(),
                            page: page
                        },
                        beforeSend: function () {
                            load_page = true;
                        },
                        success: function (data) {
                            if (data.status == 'success') {

                                jQuery.each(data.Posts, function (i, val) {
                                    $('.post_load_list').append(val);
                                });

                                load_page = false;
                            }
                        }
                    });
                }
            });
        }

        if ($('input[name="load_page_type"]').val() == 'load_search') {
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 800 && !load_page) {
                    page[$('.tab.active').data('load')]++;
                    $.ajax({
                        type: 'POST',
                        url: '/post/get_page',
                        data: {
                            _token: $('input[name="_token"]').val(),
                            load_page_type: $('input[name="load_page_type"]').val(),
                            load_search: $('input[name="load_search"]').val(),
                            page: page[$('.tab.active').data('load')],
                            tab: $('.tab.active').data('load')
                        },
                        beforeSend: function () {
                            load_page = true;
                        },
                        success: function (data) {
                            if (data.status == 'success') {

                                jQuery.each(data.Posts, function (i, val) {
                                    if ($('.tab.active').data('load') === 'posts')
                                        $('.post_load_list').append(val);
                                    else if ($('.tab.active').data('load') === 'photos')
                                        $('.photo_load_list').append(val);
                                    else if ($('.tab.active').data('load') === 'videos')
                                        $('.video_load_list').append(val);
                                    else if ($('.tab.active').data('load') === 'business')
                                        $('.business_load_list').append(val);
                                    else if ($('.tab.active').data('load') === 'professionals')
                                        $('.professionals_load_list').append(val);

                                });

                            }

                            load_page = false;
                        }
                    });
                }
            });
        }

        if ($('input[name="load_page_type"]').val() == 'load_group') {
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 800 && !load_page) {
                    page++;
                    $.ajax({
                        type: 'POST',
                        url: '/post/get_page',
                        data: {
                            _token: $('input[name="_token"]').val(),
                            load_page_type: $('input[name="load_page_type"]').val(),
                            load_page_group: $('input[name="load_page_group"]').val(),
                            page: page
                        },
                        beforeSend: function () {
                            load_page = true;
                        },
                        success: function (data) {
                            if (data.status == 'success') {

                                jQuery.each(data.Posts, function (i, val) {
                                    $('.post_load_list').append(val);
                                });

                                load_page = false;
                            }
                        }
                    });
                }
            });
        }

        if ($('input[name="load_page_type"]').val() == 'load_user') {
            $('.profile-content').scroll(function () {
                if ($('.profile-content').scrollTop() + $('.profile-content').height() >= $('.profile-content').height() - 400 && !load_page) {
                    page++;
                    $.ajax({
                        type: 'POST',
                        url: '/post/get_page',
                        data: {
                            _token: $('input[name="_token"]').val(),
                            load_page_type: $('input[name="load_page_type"]').val(),
                            load_page_user: $('input[name="load_page_user"]').val(),
                            page: page
                        },
                        beforeSend: function () {
                            load_page = true;
                        },
                        success: function (data) {
                            if (data.status == 'success') {

                                jQuery.each(data.Posts, function (i, val) {
                                    $('.post_load_list').append(val);
                                });

                                load_page = false;
                            }
                        }
                    });
                }
            });
        }

        if ($('input[name="load_page_type"]').val() == 'load_notifications') {
            $(window).scroll(function () {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 400 && !load_page) {
                    page++;
                    $.ajax({
                        type: 'POST',
                        url: '/post/get_page',
                        data: {
                            _token: $('input[name="_token"]').val(),
                            load_page_type: $('input[name="load_page_type"]').val(),
                            page: page
                        },
                        beforeSend: function () {
                            load_page = true;
                        },
                        success: function (data) {
                            if (data.status == 'success') {

                                jQuery.each(data.Posts, function (i, val) {
                                    $('.post_load_list').append(val);
                                });

                                load_page = false;
                            }
                        }
                    });
                }
            });
        }
    }


    $('#UploadImageBox #CancelCropp').on('click', function () {

        $('#UploadImageBox').hide();
        $('#UploadImageBox img').cropper('destroy');

    });

    $('#UploadImageBox #ApplyCropp').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/cropp',
            data: {
                filename: cropp_file,
                _token: $('input[name="_token"]').val(),
                data: $('#UploadImageBox img').data('cropper').getData()
            },
            beforeSend: function () {
                $('#UploadImageBox').hide();
                $('body').removeClass('overflow');
                $('#UploadImageBox img').attr('src', '');
            },
            success: function (response) {
                if (response.status == "success") {
                    $('.sidebar .bl_person_photo img').attr('src', response.filename);
                    $('input[name="avatar"]').val(response.filename);

                    $('#upload-bg.image-post').attr('src', response.filename);
                    $('input[name="file_type"]').val('image');
                    $('input[name="file_name"]').val(response.filename);

                    $('#UploadImageBox img').cropper('destroy');
                } else {
                    swal('Error', response.message, 'error');
                    $('#UploadImageBox img').cropper('destroy');
                }
            }
        });

    });

    $('#upload-file.image-post').change(function (e) {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'posts');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                upload_file = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {

                    if (data.preview !== null) {
                        $('#upload-bg.image-post').attr('src', data.preview);
                        $('input[name="preview_video"]').val(data.preview);

                        $('input[name="file_type"]').val(data.type);
                        $('input[name="file_name"]').val(data.uploads);
                    } else {

                        cropp_file = data.uploads;

                        $('#UploadImageBox').show();
                        $('body').addClass('overflow');
                        $('#UploadImageBox img').attr('src', URL.createObjectURL(files[0]));

                        $('#UploadImageBox img').cropper({
                            aspectRatio: 1 / 0.75,
                            zoomable: false,
                            viewMode: 1,
                            minCropBoxWidth: 200,
                            minCropBoxHeight: 200
                        });

                        $('#upload-file.image-post').val('');
                    }

                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function () {
                upload_file = false;
            }
        });
    });

    $('#upload-file.image-post-group').change(function (e) {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'posts');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                upload_file = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    if (data.preview != null) {
                        $('#upload-bg.image-post-group').attr('src', data.preview);
                        $('input[name="preview_video"]').val(data.preview);
                    } else
                        $('#upload-bg.image-post-group').attr('src', URL.createObjectURL(files[0]));

                    $('input[name="file_type"].image-post-group').val(data.type);
                    $('input[name="file_name"].image-post-group').val(data.uploads);
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function () {
                upload_file = false;
            }
        });
    });

    $('body').on('click', '.edit_post', function (e) {
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/post/get',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                if (data.status == 'success') {
                    if (!$('.post-bar.new-post-bar').hasClass('active'))
                        $('.post-bar.new-post-bar').addClass('active');

                    $('html,body').animate({scrollTop: 0}, 500);

                    $('#PostType option:first-child').prop('selected', true).change();
                    $('input[name="file_type"]').val('');
                    $('input[name="file_name"]').val('');
                    $('input[name="preview_video"]').val('');
                    $('#upload-bg.image-post').attr('src', '/img/upload-bg.jpg');

                    $('input[name="PostTitle"]').val(data.Post.title);
                    $('input[name="PostSubtitle"]').val(data.Post.subtitle);

                    if (data.Post.category_id !== null) {
                        $('select[name="PostCategory"] option[value="' + data.Post.category_id + '"]').prop('selected', true).change();
                    } else {
                        $('select[name="PostCategory"] option:first-child').prop('selected', true).change();
                    }

                    $('textarea[name="Description"]').val($('<p>' + data.Post.description + '</p>').text());
                    $('input[name="Keywords"]').val(data.Post.keywords);

                    if (data.Post.file_name !== null) {
                        $('input[name="file_type"]').val(data.Post.file_type);
                        $('input[name="file_name"]').val(data.Post.file_name);

                        if (data.Post.file_type === 'image' && data.Post.file_name !== null) {
                            $('#upload-bg.image-post').attr('src', data.Post.file_name);
                        } else if (data.Post.file_type === 'video' && data.Post.video_preview !== null) {
                            $('input[name="preview_video"]').val(data.Post.video_preview);
                            $('#upload-bg.image-post').attr('src', data.Post.video_preview);
                        }
                    }

                    $('select[name="Frequency"] option:first-child').prop('selected', true);

                    if (data.Post.post_type == 'event') {
                        $('select[name="PostType"] option[value="Event"]').prop('selected', true).change();

                        $('input[name="Location"]').val(data.Post.location);
                        $('select[name="Frequency"] option[value="' + data.Post.frequency + '"]').prop('selected', true).change();
                        $('input[name="StartEvent"]').val(data.Post.start);
                        $('input[name="EndEvent"]').val(data.Post.end);
                    }

                    if (data.Post.post_type == 'online') {
                        $('select[name="PostType"] option[value="Online"]').prop('selected', true).change();

                        $('input[name="Start"]').val(data.Post.start);
                        $('input[name="End"]').val(data.Post.end);
                        $('input[name="StartTime"]').val(data.Post.start_time);
                        $('input[name="EndTime"]').val(data.Post.end_time);
                    }

                    $('#NewPost').append('<input type="hidden" name="post_id" value="' + data.Post.id + '">');
                    $('input[type="submit"].post-button').val('Update');
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '.edit_draft_post', function (e) {
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/post/get',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                if (data.status == 'success') {
                    if (!$('.post-bar.new-post-bar').hasClass('active'))
                        $('.post-bar.new-post-bar').addClass('active');

                    $('html,body').animate({scrollTop: 0}, 500);

                    $('#PostType option:first-child').prop('selected', true).change();
                    $('input[name="file_type"]').val('');
                    $('input[name="file_name"]').val('');
                    $('input[name="preview_video"]').val('');
                    $('#upload-bg.image-post').attr('src', '/img/upload-bg.jpg');

                    $('input[name="PostTitle"]').val(data.Post.title);

                    if (data.Post.category_id !== null) {
                        $('select[name="PostCategory"] option[value="' + data.Post.category_id + '"]').prop('selected', true).change();
                    } else {
                        $('select[name="PostCategory"] option:first-child').prop('selected', true).change();
                    }

                    $('textarea[name="Description"]').val($('<p>' + data.Post.description + '</p>').text());
                    $('input[name="Keywords"]').val(data.Post.keywords);

                    if (data.Post.file_name !== null) {
                        $('input[name="file_type"]').val(data.Post.file_type);
                        $('input[name="file_name"]').val(data.Post.file_name);

                        if (data.Post.file_type === 'image' && data.Post.file_name !== null) {
                            $('#upload-bg.image-post').attr('src', data.Post.file_name);
                        } else if (data.Post.file_type === 'video' && data.Post.video_preview !== null) {
                            $('input[name="preview_video"]').val(data.Post.video_preview);
                            $('#upload-bg.image-post').attr('src', data.Post.video_preview);
                        }
                    }

                    $('select[name="Frequency"] option:first-child').prop('selected', true);

                    if (data.Post.post_type == 'event') {
                        $('select[name="PostType"] option[value="Event"]').prop('selected', true).change();

                        $('input[name="Location"]').val(data.Post.location);
                        $('select[name="Frequency"] option[value="' + data.Post.frequency + '"]').prop('selected', true).change();
                        $('input[name="StartEvent"]').val(data.Post.start);
                        $('input[name="EndEvent"]').val(data.Post.end);
                    }

                    if (data.Post.post_type == 'online') {
                        $('select[name="PostType"] option[value="Online"]').prop('selected', true).change();

                        $('input[name="Start"]').val(data.Post.start);
                        $('input[name="End"]').val(data.Post.end);
                        $('input[name="StartTime"]').val(data.Post.start_time);
                        $('input[name="EndTime"]').val(data.Post.end_time);
                    }

                    $('#NewPost').append('<input type="hidden" name="draft_id" value="' + data.Post.id + '">');
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '.publish_draft_post', function (e) {
        let id = $(this).data('id');

        swal({
            title: 'Publish a post?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Publish'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/post/publish',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            $('.post_box[data-id="' + id + '"]').remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        });

        return false;
    });

    $('body').on('click', '.delete_post', function (e) {
        let id = $(this).data('id');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/post/delete',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            $('.post_box[data-id="' + id + '"]').remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        });

        return false;
    });

    $('body').on('click', '.like_post', function (e) {
        let like = $(this);
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/post/like',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                if (data.status == 'success') {
                    $(like).toggleClass('liked');
                    if ($('.post_box[data-id="' + id + '"] .count_like span').length > 0) {

                        if (data.count_like == 0)
                            $('.post_box[data-id="' + id + '"] .like_left').addClass('hidden-post-block');
                        else
                            $('.post_box[data-id="' + id + '"] .like_left').removeClass('hidden-post-block');

                        $('.post_box[data-id="' + id + '"] .count_like span').text(data.count_like);
                    } else {
                        if (parseInt(data.count_like) > 0)
                            $(like).find('span').text('Like (' + data.count_like + ')');
                        else
                            $(like).find('span').text('Like');
                    }
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '.interested_post', function (e) {
        let button = $(this);
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/post/interested',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                if (data.status == 'success') {
                    $(button).toggleClass('active');
                    $('.interested_count').text(data.count_interested);
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '.going_post', function (e) {
        let button = $(this);
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/post/going',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                if (data.status == 'success') {
                    $(button).toggleClass('active');
                    $('.going_count').text(data.count_going);
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('#save_draft').on('click', function () {
        $('#NewPost').append('<input type="hidden" name="status" id="draft_input" value="draft">');
        $('#NewPost').submit();
    });

    $('#NewPost').on('submit', function () {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            $('#draft_input').remove();
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/post/new_post',
            data: $(this).serializeArray(),
            success: function (data) {
                if (data.status == 'success') {
                    if ($('input[name="draft_id"]').length || $('input[name="post_id"]').length) {
                        swal('Success', 'Post updated', 'success');
                        if ($('form.add_comment_single').length) {
                            setTimeout(function () {
                                window.location.reload();
                            }, 1500);
                        }
                    } else {
                        swal('Success', 'Post added', 'success');
                    }

                    $('.post-bar.new-post-bar').toggleClass('active');
                    $('#NewPost')[0].reset();
                    $('#upload-bg.image-post').attr('src', '/img/upload-bg.jpg');
                    $('input[name="file_type"].image-post').val('');
                    $('input[name="file_name"].image-post').val('');

                    if ($('input[name="load_page_type"]').val() == 'load_index' && !$('#draft_input').length && !$('input[name="post_id"]').length) {
                        $('.post_load_list').prepend(data.Post);
                    }
                    else if ($('input[name="load_page_type"]').val() == 'load_index' && $('input[name="post_id"]').length && !$('#draft_input').length) {
                        $('.post_load_list .post_box[data-id="' + $('input[name="post_id"]').val() + '"]').after(data.Post);
                        $('.post_load_list .post_box[data-id="' + $('input[name="post_id"]').val() + '"]:first-child').remove();
                        $('input[name="post_id"]').remove();
                    }
                    else if ($('input[name="load_page_type"]').val() == 'load_draft' && $('#draft_input').length) {
                        if (!$('input[name="draft_id"]').length)
                            $('.post_load_list').prepend(data.Post);
                        else {
                            $('.post_load_list .post_box[data-id="' + $('input[name="draft_id"]').val() + '"]').after(data.Post);
                            $('.post_load_list .post_box[data-id="' + $('input[name="draft_id"]').val() + '"]:first-child').remove();
                            $('input[name="draft_id"]').remove();
                        }
                    } else if ($('input[name="load_page_type"]').val() == 'load_draft' && !$('#draft_input').length && $('input[name="draft_id"]').length) {
                        $('.post_load_list .post_box[data-id="' + $('input[name="draft_id"]').val() + '"]').remove();
                        $('input[name="draft_id"]').remove();
                    }

                    $('input[type="submit"].post-button').val('Post');
                    $('#draft_input').remove();
                } else {
                    swal('Error', data.message, 'error');
                    $('#draft_input').remove();
                }
            }
        });

        return false;
    });

    $('#NewPostGroup').on('submit', function () {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/post/new_post',
            data: $(this).serializeArray(),
            success: function (data) {
                if (data.status == 'success') {
                    swal('Success', 'Post added', 'success');
                    $('.post-bar.group-post-add').toggleClass('active');
                    $('#NewPostGroup')[0].reset();
                    $('#upload-bg.image-post-group').attr('src', '/img/upload-bg.jpg');
                    $('input[name="file_type"].image-post-group').val('');
                    $('input[name="file_name"].image-post-group').val('');
                    $('.post_load_list').prepend(data.Post);
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    var comments_full_update = [];
    var comments_full_photo = 0;
    var comments_full_video = 0;
    var comments_full_profile_video = 0;
    var comments_update = typeof comments_full_update_default === 'undefined' ? [] : comments_full_update_default;
    var comments_update_now = false;

    if (typeof $('input[name="photo_update_id"]') != 'undefined')
        comments_full_photo = $('input[name="photo_update_id"]').val();

    if (typeof $('input[name="video_update_id"]') != 'undefined')
        comments_full_video = $('input[name="video_update_id"]').val();

    if (typeof $('input[name="profile_video_update_id"]') != 'undefined')
        comments_full_profile_video = $('input[name="profile_video_update_id"]').val();

    setInterval(function () {
        if (comments_update_now == false)
            $.ajax({
                type: 'POST',
                url: '/comment/update',
                data: {
                    comments_full_update: comments_full_update,
                    comments_update: comments_update,
                    comments_full_photo: comments_full_photo,
                    comments_full_video: comments_full_video,
                    comments_full_profile_video: comments_full_profile_video,
                    _token: $('input[name="_token"]').val()
                },
                beforeSend: function () {
                    comments_update_now = true;
                },
                success: function (data) {
                    if (data.status == 'success') {

                        for (key in data.updates.update) {
                            if ($('.post_box[data-id="' + key + '"] .count_like span').length > 0) {

                                if (data.updates.update[key]['count_likes'] == 0)
                                    $('.post_box[data-id="' + key + '"] .like_left').addClass('hidden-post-block');
                                else
                                    $('.post_box[data-id="' + key + '"] .like_left').removeClass('hidden-post-block');

                                if (data.updates.update[key]['count_comments'] == 0)
                                    $('.post_box[data-id="' + key + '"] .like_right').addClass('hidden-post-block');
                                else
                                    $('.post_box[data-id="' + key + '"] .like_right').removeClass('hidden-post-block');

                                $('.post_box[data-id="' + key + '"] .count_like span').text(data.updates.update[key]['count_likes']);
                                $('.post_box[data-id="' + key + '"] .like_right span').text(data.updates.update[key]['count_comments']);
                            } else {
                                if (parseInt(data.updates.update[key]['count_likes']) > 0)
                                    $('.post_box[data-id="' + key + '"] .like_post span').text('Like (' + data.updates.update[key]['count_likes'] + ')');
                                else
                                    $('.post_box[data-id="' + key + '"] .like_post span').text('Like');

                                if (parseInt(data.updates.update[key]['count_comments']) > 0)
                                    $('.post_box[data-id="' + key + '"] .comments_post span').text('Comment (' + data.updates.update[key]['count_comments'] + ')');
                                else
                                    $('.post_box[data-id="' + key + '"] .comments_post span').text('Comment');
                            }
                        }

                        for (post_id in data.updates.full_update) {

                            for (comment_id in data.updates.full_update[post_id]) {
                                if ($('.post_box[data-id="' + post_id + '"] .coment_block[data-id="' + comment_id + '"]').length == 0) {
                                    var $new = $(data.updates.full_update[post_id][comment_id]).hide();

                                    if (typeof $new.data('reply-id') != 'undefined')
                                        $('.hide_comment_block[data-id="' + post_id + '"] .coment_block[data-id="' + $new.data('reply-id') + '"] .coment_right').first().append($new);
                                    else {
                                        $('.add_comment[data-id="' + post_id + '"]').before($new);
                                    }

                                    $new.show('fast');
                                }
                            }

                        }

                        for (post_id in data.updates.full_update_likes) {

                            for (comment_id in data.updates.full_update_likes[post_id]) {
                                if (parseInt(data.updates.full_update_likes[post_id][comment_id]) > 0)
                                    $('.post_box[data-id="' + post_id + '"] .coment_block[data-id="' + comment_id + '"] .like_comment span').text('(' + data.updates.full_update_likes[post_id][comment_id] + ')');
                                else
                                    $('.post_box[data-id="' + post_id + '"] .coment_block[data-id="' + comment_id + '"] .like_comment span').text('');
                            }

                        }

                        /* Media */

                        for (comment_id in data.updates.full_update_photo) {

                            if ($('.hide_comment_block .coment_block[data-id="' + comment_id + '"]').length == 0) {
                                var $new = $(data.updates.full_update_photo[comment_id]).hide();

                                if (typeof $new.data('reply-id') != 'undefined')
                                    $('.hide_comment_block .coment_block[data-id="' + $new.data('reply-id') + '"] .coment_right').first().append($new);
                                else {
                                    $('.add_comment.comment_before').before($new);
                                }

                                $new.show('fast');
                            }

                        }

                        for (comment_id in data.updates.full_update_photo_likes) {
                            if (parseInt(data.updates.full_update_photo_likes[comment_id]) > 0)
                                $('.coment_block[data-id="' + comment_id + '"] .like_comment span').text('(' + data.updates.full_update_photo_likes[comment_id] + ')');
                            else
                                $('.coment_block[data-id="' + comment_id + '"] .like_comment span').text('');
                        }

                        for (comment_id in data.updates.full_update_video) {

                            if ($('.hide_comment_block .coment_block[data-id="' + comment_id + '"]').length == 0) {
                                var $new = $(data.updates.full_update_video[comment_id]).hide();

                                if (typeof $new.data('reply-id') != 'undefined')
                                    $('.hide_comment_block .coment_block[data-id="' + $new.data('reply-id') + '"] .coment_right').first().append($new);
                                else {
                                    $('.add_comment.comment_before').before($new);
                                }

                                $new.show('fast');
                            }

                        }

                        for (comment_id in data.updates.full_update_video_likes) {
                            if (parseInt(data.updates.full_update_video_likes[comment_id]) > 0)
                                $('.coment_block[data-id="' + comment_id + '"] .like_comment span').text('(' + data.updates.full_update_video_likes[comment_id] + ')');
                            else
                                $('.coment_block[data-id="' + comment_id + '"] .like_comment span').text('');
                        }

                        /* Media end */

                        for (comment_id in data.updates.full_update_profile_video) {

                            if ($('.hide_comment_block .coment_block[data-id="' + comment_id + '"]').length == 0) {
                                var $new = $(data.updates.full_update_profile_video[comment_id]).hide();

                                if (typeof $new.data('reply-id') != 'undefined')
                                    $('.hide_comment_block .coment_block[data-id="' + $new.data('reply-id') + '"] .coment_right').first().append($new);
                                else {
                                    $('.add_comment.comment_before').before($new);
                                }

                                $new.show('fast');
                            }

                        }

                        for (comment_id in data.updates.full_update_profile_video_likes) {
                            if (parseInt(data.updates.full_update_profile_video_likes[comment_id]) > 0)
                                $('.coment_block[data-id="' + comment_id + '"] .like_comment span').text('(' + data.updates.full_update_profile_video_likes[comment_id] + ')');
                            else
                                $('.coment_block[data-id="' + comment_id + '"] .like_comment span').text('');
                        }
                    }

                    comments_update_now = false;
                }
            });
    }, 2000);

    let upload_file_comment = false;

    $('body').on('click', '.comment_image', function () {
        $(this).closest('form.add_comment').find('input[type="file"]').click();
        $('form.add_comment_single').find('input[type="file"]').click();
        return false;
    });

    $('body').on('change', 'form.add_comment input[type="file"], form.add_comment_single input[type="file"]', function (e) {
        if (upload_file_comment == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'post_coments');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        let form = $(this).closest('form.add_comment');

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                upload_file_comment = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    $(form).find('img.comment_img').attr('src', URL.createObjectURL(files[0]));
                    $(form).find('img.comment_img').show('fast');
                    $(form).find('input[name="image"]').val(data.uploads);

                    $('.add_comment_single').find('img.comment_img').attr('src', URL.createObjectURL(files[0]));
                    $('.add_comment_single').find('img.comment_img').show('fast');
                    $('.add_comment_single').find('input[name="image"]').val(data.uploads);
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function () {
                upload_file_comment = false;
            }
        });
    });

    $('.post_box').each(function (i, elem) {
        comments_update.push($(elem).data('id'));
    });

    let reply_id = 0;

    $(document).on('click', '.reply_comment_single', function () {
        $('img.comment_img').hide('fast');
        $('textarea[name="comment"]').val('');
        $('input[name="image"]').val('');
        $('.add_comment_single input[type="file"]').val('');
        setTimeout(function () {
            $('img.comment_img').attr('src', '');
        }, 300);

        $('.close_comment_reply').click();
        reply_id = $(this).data('id');

        $('.top-post-box ul').append('<li class="remove-close-reply">' +
            '   <a class="comment_reply" title="Scroll to comment">Reply to comment</a>' +
            '</li>' +
            '<li class="remove-close-reply">' +
            '   <a class="close_comment_reply" title="Cancel reply"><i class="fa fa-times" aria-hidden="true"></i></a>' +
            '</li>');

        $('.add_comment_single').prepend('<input type="hidden" name="comment_id" value="' + reply_id + '">');

        $('textarea[name="comment"]').attr('placeholder', 'Write a reply to the comment');
        $("html, body").animate({scrollTop: $('.top-post-box').offset().top - 206 }, "fast");

        return false;
    });

    $(document).on('click', '.close_comment_reply', function () {
        $('img.comment_img').hide('fast');
        $('textarea[name="comment"]').val('');
        $('input[name="image"]').val('');
        $('.add_comment_single input[type="file"]').val('');
        setTimeout(function () {
            $('img.comment_img').attr('src', '');
        }, 300);

        $('li.remove-close-reply').remove();
        reply_id = 0;
        $('textarea[name="comment"]').attr('placeholder', 'Write a comment for post');
        $('.add_comment_single input[name="comment_id"]').remove();
    });

    $(document).on('click', '.comment_reply', function () {
        $('.coment_block').css('opacity', '0.5');
        $('.coment_block[data-id="' + reply_id + '"]').css('opacity', 1);

        $("html, body").animate({scrollTop: $('.coment_block[data-id="' + reply_id + '"]').offset().top - 206 }, "fast");

        setTimeout(function () {
            $('.coment_block').css('opacity', 1);
        }, 1200)
    });

    $('body').on('click', '.reply_comment', function () {
        if (typeof $(this).data('id') == 'undefined')
            return false;

        if ($(this).closest('.coment_right').find('form').length > 0) {
            let form = $(this).closest('.coment_right').find('form');
            $(form).hide('fast');

            setTimeout(function () {
                $(form).remove();
            }, 300);
        } else {
            let copy_form = $('.copy_form').clone();
            $(copy_form).removeClass('copy_form');

            if (typeof $(this).data('post-id') != 'undefined')
                $(copy_form).find('input[name="post_id"]').val($(this).data('post-id'));

            if (typeof $(this).data('photo-id') != 'undefined')
                $(copy_form).find('input[name="photo_id"]').val($(this).data('photo-id'));

            if (typeof $(this).data('video-id') != 'undefined')
                $(copy_form).find('input[name="video_id"]').val($(this).data('video-id'));

            if (typeof $(this).data('profile-video-id') != 'undefined')
                $(copy_form).find('input[name="profile_video_id"]').val($(this).data('profile-video-id'));

            $(copy_form).find('input[name="comment_id"]').val($(this).data('id'));

            $(this).closest('.coment_right').append(copy_form);
            $(copy_form).show('fast');
        }
    });

    $('body').on('click', '.comments_post', function () {
        let block = $('.hide_comment_block[data-id="' + $(this).data('id') + '"]');

        if ($(block).is(':visible')) {
            $(block).hide('fast');
            comments_full_update.splice(comments_full_update.indexOf($(this).data('id')), 1);
        } else {
            $(block).show('fast');
            comments_full_update.push($(this).data('id'));

            setTimeout(function () {
                if ($('.profile-content.overflow_block').length > 0) {
                    if ($(block).find('form.add_comment').length > 0)
                        $('.profile-content.overflow_block').animate({scrollTop: $(block).find('form.add_comment').offset().top - 250}, 500);
                    else
                        $('.profile-content.overflow_block').animate({scrollTop: $(block).find('.coment_block').offset().top - 250}, 500);
                } else {
                    if ($(block).find('form.add_comment').length > 0)
                        $('html, body').animate({scrollTop: $(block).find('form.add_comment').offset().top - 500}, 500);
                    else
                        $('html, body').animate({scrollTop: $(block).find('.coment_block').offset().top - 500}, 500);
                }
            }, 200);
        }

        return false;
    });

    $('body').on('keyup', 'form.add_comment input[name="comment"]', function (event) {
        if (event.keyCode == 13) {
            $(this).closest('form.add_comment').submit();
            return false;
        }
    });

    $('body').on('keyup', 'form.add_comment_single textarea[name="comment"]', function (event) {
        if (event.keyCode == 13) {
            $(this).closest('form.add_comment_single').submit();
            return false;
        }
    });

    $('body').on('click', 'img.comment_img', function () {
        $(this).hide('fast');
        $(this).closest('form.add_comment').find('input[name="image"]').val('');
        $('form.add_comment_single').find('input[name="image"]').val('');
        $('form.add_comment_single').find('input[type="file"]').val('');
        setTimeout(function () {
            $(this).attr('src', '');
        }, 300);
    });

    let send_comment = false;

    $(document).on('submit', 'form.add_comment_single', function () {
        if ($(this).find('textarea[name="comment"]').val().length === 0 && $(this).find('input[name="image"]').val().length === 0)
            return false;

        if (send_comment == true)
            return false;

        if (upload_file_comment == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        let form = $(this);

        $.ajax({
            type: 'POST',
            url: '/comment/add',
            data: $(form).serializeArray(),
            beforeSend: function () {
                send_comment = true;
                $('img.comment_img').hide('fast');
                $('textarea[name="comment"]').val('');
                $('input[name="image"]').val('');
                $('.add_comment_single input[type="file"]').val('');
                setTimeout(function () {
                    $('img.comment_img').attr('src', '');
                }, 300);
            },
            complete: function () {
                send_comment = false;
            },
            success: function (data) {
                if (data.status == 'success') {

                    var $new = $(data.html_single).hide();
                    if (data.reply_id == null) {
                        $('.top-post-box').after($new);
                        $("html, body").animate({scrollTop: $('.top-post-box').offset().top - 206 }, "fast");
                    } else {
                        $('.coment_block[data-id="' + data.reply_id + '"]').after($new);
                        $("html, body").animate({scrollTop: $('.coment_block[data-id="' + data.reply_id + '"]').offset().top - 206 }, "fast");
                    }

                    $new.show('fast');

                    $('.close_comment_reply').click();

                    /*if ($('.post_box[data-id="' + data.post_id + '"] .like_right span').length > 0) {

                        if (data.count_comment == 0)
                            $('.post_box[data-id="' + data.post_id + '"] .like_right').addClass('hidden-post-block');
                        else
                            $('.post_box[data-id="' + data.post_id + '"] .like_right').removeClass('hidden-post-block');

                        $('.post_box[data-id="' + data.post_id + '"] .like_right span').text(data.count_comment);

                    } else {
                        if (parseInt(data.count_comment) > 0)
                            $('.post_box[data-id="' + data.post_id + '"] .comments_post span').text('(' + data.count_comment + ')');
                        else
                            $('.post_box[data-id="' + data.post_id + '"] .comments_post span').text('');
                    }*/
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('submit', 'form.add_comment', function () {
        if ($(this).find('input[name="comment"]').val().length == 0 && $(this).find('input[name="image"]').val().length == 0)
            return false;

        if (send_comment == true)
            return false;

        if (upload_file_comment == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        let form = $(this);

        $.ajax({
            type: 'POST',
            url: '/comment/add',
            data: $(form).serializeArray(),
            beforeSend: function () {
                $(form).find('input[name="comment"]').val('');

                $(form).find('img.comment_img').hide('fast');
                $(form).find('img.comment_img').closest('form.add_comment').find('input[name="image"]').val('');
                setTimeout(function () {
                    $(form).find('img.comment_img').attr('src', '');
                }, 300);

                if ($(form).hasClass('reply_comment_form')) {
                    $(form).hide('fast');
                    setTimeout(function () {
                        $(form).remove();
                    }, 300);
                }

                send_comment = true;
            },
            success: function (data) {
                send_comment = false;
                if (data.status == 'success') {

                    var $new = $(data.html).hide();
                    if (data.reply_id == null)
                        $('.add_comment[data-id="' + data.post_id + '"]').before($new);
                    else {
                        $new.find('.reply_comment').remove();
                        $('.hide_comment_block[data-id="' + data.post_id + '"] .coment_block[data-id="' + data.reply_id + '"] .coment_right').first().append($new);
                    }

                    $new.show('fast');

                    if ($('.post_box[data-id="' + data.post_id + '"] .like_right span').length > 0) {

                        if (data.count_comment == 0)
                            $('.post_box[data-id="' + data.post_id + '"] .like_right').addClass('hidden-post-block');
                        else
                            $('.post_box[data-id="' + data.post_id + '"] .like_right').removeClass('hidden-post-block');

                        $('.post_box[data-id="' + data.post_id + '"] .like_right span').text(data.count_comment);

                    } else {
                        if (parseInt(data.count_comment) > 0)
                            $('.post_box[data-id="' + data.post_id + '"] .comments_post span').text('(' + data.count_comment + ')');
                        else
                            $('.post_box[data-id="' + data.post_id + '"] .comments_post span').text('');
                    }
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    let search = false;

    $('#SearchNewsAndEvents').on('submit', function () {
        if (search == false)
            $.ajax({
                type: 'POST',
                url: '/post/search',
                data: $(this).serialize(),
                beforeSend: function () {
                    search = true;
                },
                success: function (data) {
                    if (data.status == 'success') {
                        $('.news-box.detail-list').empty();

                        for (key in data.posts) {
                            let $new = $(data.posts[key]).hide();

                            $('.news-box.detail-list').append($new);

                            $new.show('fast');
                        }

                    } else {
                        swal('Error', data.message, 'error');
                    }

                    search = false;
                }
            });

        return false;
    });

    $('#SearchNewsAndEvents input[name="search"]').on('keyup', function (event) {
        if (event.keyCode == 13) {
            $(this).closest('form.#SearchNewsAndEvents').submit();
            return false;
        }
    });

    $('body').on('click', '.like_comment', function (e) {
        if (typeof $(this).data('id') == 'undefined')
            return false;

        let like = $(this);
        let id = $(this).data('id');

        let data_post = {
            _token: $('input[name="_token"]').val()
        };

        if (typeof $(like).data('media') !== 'undefined')
            data_post['media_id'] = id;
        else if (typeof $(like).data('profile-video') !== 'undefined')
            data_post['profile_video_id'] = id;
        else
            data_post['id'] = id;

        $.ajax({
            type: 'POST',
            url: '/comment/like',
            data: data_post,
            success: function (data) {
                if (data.status == 'success') {
                    $(like).toggleClass('liked');
                    $(like).find('img').attr('src', !$(like).hasClass('liked') ? '/img/dis-like-icon.svg' : '/img/full-like-icon-like.svg');

                    if (typeof data_post['id'] !== 'undefined') {

                        if (parseInt(data.count_like) === 0) {
                            $(like).find('span').last().text('');
                        } else {
                            $(like).find('span').last().text(data.count_like);
                        }

                    } else {

                        if (parseInt(data.count_like) == 0)
                            $(like).find('span').text('');
                        else
                            $(like).find('span').text('(' + data.count_like + ')');

                    }
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    let share_post = null;

    $('body').on('click', '.share_post', function () {
        share_post = $(this);

        swal({
            title: 'Share',
            html: '<div class="m_soc_ic"><a href="#"><img src="https://ladies.cgp.systems/img/soc_ic_1.svg" id="ShareFacebook" alt="Alternate Text"></a><a href="#"><img src="https://ladies.cgp.systems/img/soc_ic_2.svg" id="ShareTwitter" alt="Alternate Text"></a></div> ',
            showCancelButton: false,
            showConfirmButton: false,
        });

        return false;
    });

    $('body').on('click', '#ShareFacebook', function () {
        FB.ui({
            method: 'feed',
            name: $(share_post).data('text'),
            link: $(share_post).data('url'),
            picture: $(share_post).data('img'),
            description: $(share_post).data('text')
        });
        return false;
    });

    $('body').on('click', '#ShareTwitter', function () {
        let w = 400;
        let h = 600;
        let left = Number((screen.width / 2) - (w / 2));
        let tops = Number((screen.height / 2) - (h / 2));

        window.open("https://twitter.com/intent/tweet?text=" + $(share_post).data('text') + $(share_post).data('url'), 'Share Post', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + tops + ', left=' + left);

        return false;
    });

    $('select[name="PostType"]').on('change', function () {
        if ($(this).val() == 'Event') {
            $('.event-type-hidden').show('fast').css('display', 'block');
            $('.online-type-visible').hide('fast');
        } else if ($(this).val() == 'Online') {
            $('.online-type-visible').show('fast');
            $('.online-type-hidden').hide('fast');
        } else {
            $('.online-type-visible').hide('fast');
            $('.event-type-hidden').hide('fast');
        }
    });

    /* Posts end */

});