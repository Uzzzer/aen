$(document).ready(function () {
    let microphone = false;
    let webcamera = false;
    let localstream = false;

    let phone = null;
    let ctrl = null;

    let socket;
    let conversation_id = null;

    checkAccessMedia(true, true);

    $('#stopStream').on('click', function () {
        phone.hangup();

        socket.send(JSON.stringify({
            command: 'stop_live',
            api_token: apiToken
        }));

        window.location.href = '/live';
    });

    let isEnabled = true;
    $('#muteStream').on('click', function () {
        ctrl.toggleAudio();
        if (isEnabled) {
            $('#muteStream').val('Mute microphone');
            isEnabled = false;
        } else {
            $('#muteStream').val('Turn on microphone');
            isEnabled = true;
        }
    });

    function startStream() {
        phone = window.phone = PHONE({
            number: 'stream_live_' + UserID,
            publish_key: 'pub-c-f260dc1e-2a5f-4324-9530-ed1ba3196aa3',
            subscribe_key: 'sub-c-dec33996-bd68-11e8-9de9-7af9a1823cc4',
            oneway: true,
            broadcast: true,
            ssl: true,
            iceTransports: 'relay'
        });

        ctrl = window.ctrl = CONTROLLER(phone);

        ctrl.ready(function (session) {
            ctrl.addLocalStream(document.getElementById("stream_video"));
            ctrl.stream();
            $('#stream_video').show();
        });

        ctrl.streamPresence(function (m) {
            $('#here-now').text(m.occupancy);
        });

        socket = new WebSocket("wss://www.adult-en.com/socket-stream");

        socket.onopen = function (e) {
            socket.send(JSON.stringify({
                command: 'start_live',
                api_token: apiToken
            }));
        };

        socket.close = function (e) {
            /*socket.send(JSON.stringify({
                command: 'stop_live',
                api_token: apiToken
            }));*/
        };

        socket.onmessage = function (event) {
            let data = JSON.parse(event.data);
            console.log(data);

            if (typeof data !== 'undefined' && typeof data.command !== 'undefined') {
                switch (data.command) {
                    case "start_dialog":
                        if (data.status === 'success') {
                            conversation_id = data.conversation_id;
                        }
                        break;
                    case "get_dialog":
                        if (data.status === 'success') {
                            $.each(data.messages, function (i, item) {
                                let $block = $(item);
                                if (!$('#chatBox .message-block[data-id="' + $block.data('id') +'"]').length) {
                                    $('#chatBox').append($block);
                                }
                            });
                        }
                        break;
                    case "update":
                        socket.send(JSON.stringify({
                            api_token: apiToken,
                            conversation_id: conversation_id,
                            command: 'get_dialog'
                        }));
                        break;
                }
            }
        };

        $('#chatInput').on('keypress', function (e) {
            if (e.which === 13) {
                socket.send(JSON.stringify({
                    command: 'send_message',
                    message: $('#chatInput').val(),
                    api_token: apiToken,
                    conversation_id: conversation_id
                }));

                $('#chatInput').val('');
                return false;
            }
        });
    }

    function checkAccessMedia(audio_access, video_access) {
        navigator.mediaDevices.getUserMedia({
            audio: audio_access,
            video: video_access
        }).then(function (stream) {

            microphone = audio_access;
            webcamera = video_access;
            localstream = stream;

            startStream();
        }).catch(function (err) {
            console.log(err.name + ": " + err.message);
        });
    }
});