var $stream_video = $("#stream_video");

jQuery(function ($) {
    $(window).on('beforeunload', function () {
        if (connect_user == true)
            return "It is forbidden to refresh the page.";
    });

    ViewStream(user_id);
});

function ViewStream(UserID) {
    var phone = window.phone = PHONE({
        number: "Viewer" + UserID,
        publish_key: 'pub-c-f260dc1e-2a5f-4324-9530-ed1ba3196aa3',
        subscribe_key: 'sub-c-dec33996-bd68-11e8-9de9-7af9a1823cc4',
        /*publish_key: 'pub-c-f9f84ff9-1ef9-4014-8837-e2e995455470',
        subscribe_key: 'sub-c-5253d1f8-4444-11e7-86e2-02ee2ddab7fe',*/
        oneway: true,
        ssl: true,
        iceTransports: 'relay'
    });

    var ctrl = window.ctrl = CONTROLLER(phone);

    ctrl.ready(function () {
        ctrl.isStreaming(UserID, function (isOn) {
            console.log(isOn);
            if (isOn)
                ctrl.joinStream(UserID);
            else
                alert("User is not streaming!");
        });
    });

    ctrl.receive(function (session) {
        console.log('receive');

        session.connected(function (session) {
            console.log('connected');

            $stream_video.html(session.video);
            $stream_video.show(500);
        });

        session.ended(function (session) {
            handleNoStream();
        });
    });

    ctrl.streamPresence(function (m) {

    });


    return false;
}

function handleNoStream() {
    console.log('handleNoStream')
}