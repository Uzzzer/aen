$(document).ready(function() {
    let phone = null;
    let ctrl = null;

    let socket;

    viewStream();


	$(document).on("click","#fullScreen", function(){

		var elem = $("video")[0];

		if (elem.requestFullscreen) {
			elem.requestFullscreen();
		} else if (elem.mozRequestFullScreen) { /* Firefox */
			elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
			elem.webkitRequestFullscreen();
		} else if (elem.msRequestFullscreen) { /* IE/Edge */
			elem.msRequestFullscreen();
		}
	
	
	})

    function viewStream() {
        phone = window.phone = PHONE({
            number: 'viewer_' + Math.floor(Math.random()*100),
            publish_key: 'pub-c-f260dc1e-2a5f-4324-9530-ed1ba3196aa3',
            subscribe_key: 'sub-c-dec33996-bd68-11e8-9de9-7af9a1823cc4',
            oneway: true,
            ssl: true
        });

        ctrl = window.ctrl = CONTROLLER(phone);

        ctrl.ready(function(){
            ctrl.isStreaming('stream_live_' + streamUserID, function(isOn) {
                if (isOn) {
                    ctrl.joinStream('stream_live_' + streamUserID);
                } else {
                    alert("User is not streaming!");
                }
            });
        });

        ctrl.receive(function (session) {
            session.connected(function(session) {
                $('#stream_video').html(session.video).show();
                $('video').trigger('play');
                setTimeout(function(){
	             //$("video")[0].play();
                },1000)
            });
            
            session.ended(function(session){
                window.location.href = '/live';
            });
            
        });

        ctrl.streamPresence(function (m) {
            $('#here-now').text(m.occupancy);
        });

        socket = new WebSocket("wss://www.adult-en.com/socket-stream");

        socket.onopen = function(e) {
            socket.send(JSON.stringify({
                command: 'get_dialog',
                api_token: apiToken,
                conversation_id: conversation_id
            }));
        };

        socket.onmessage = function(event) {
            let data = JSON.parse(event.data);
            console.log(data);

            if (typeof data !== 'undefined' && typeof data.command !== 'undefined') {
                switch (data.command) {
                    case "get_dialog":
                        if (data.status === 'success') {
                            $.each(data.messages, function (i, item) {
                                let $block = $(item);
                                if (!$('#chatBox .message-block[data-id="' + $block.data('id') +'"]').length) {
                                    $('#chatBox').append($block);
                                }
                            });
                        }
                        break;
                    case "update":
                        socket.send(JSON.stringify({
                            api_token: apiToken,
                            conversation_id: conversation_id,
                            command: 'get_dialog'
                        }));
                        break;
                }
            }
        };

        $('#chatInput').on('keypress', function (e) {
            if (e.which === 13) {
                socket.send(JSON.stringify({
                    command: 'send_message',
                    message: $('#chatInput').val(),
                    api_token: apiToken,
                    conversation_id: conversation_id
                }));

                $('#chatInput').val('');
                return false;
            }
        });
    }


});
