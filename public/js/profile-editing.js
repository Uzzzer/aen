$(document).ready(function() {

    if ($('#MessageErrorOnLoad').length > 0)
        swal('Error', $('#MessageErrorOnLoad').val(), 'error');

    if ($('#MessageSuccessOnLoad').length > 0)
        swal('Success', $('#MessageSuccessOnLoad').val(), 'success');

    let upload_file = false;
    let cropp_file = false;

    if ($('.birthday_input').length) {
        $('.birthday_input').mask('00/00/0000');
    }

    $('#CancelCropp').on('click', function () {

        $('#UploadAvatarBox').hide();
        $('#UploadAvatarBox img').cropper('destroy');

    });

    $('#ApplyCropp').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/cropp',
            data: {
                filename: cropp_file,
                _token: $('input[name="_token"]').val(),
                data: $('#UploadAvatarBox img').data('cropper').getData()
            },
            beforeSend: function () {
                $('#UploadAvatarBox').hide();
                $('#UploadAvatarBox img').attr('src', '');
            },
            success: function (response) {
                if (response.status == "success") {
                    $('.sidebar .bl_person_photo img').attr('src', response.filename);
                    $('input[name="avatar"]').val(response.filename);

                    $('#UploadAvatarBox img').cropper('destroy');
                } else {
                    swal('Error', response.message, 'error');
                    $('#UploadAvatarBox img').cropper('destroy');
                }
            }
        });

    });

    $('#AvatarUpload').change(function(e) {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'avatars');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function() {
                upload_file = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    cropp_file = data.uploads;

                    $('#UploadAvatarBox').show();
                    $('#UploadAvatarBox img').attr('src', URL.createObjectURL(files[0]));

                    $('#UploadAvatarBox img').cropper({
                        aspectRatio: 1 / 1,
                        zoomable: false,
                        viewMode: 1,
                        minCropBoxWidth: 200,
                        minCropBoxHeight: 200
                    });

                    $('#AvatarUpload').val('');
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function() {
                upload_file = false;
            }
        });
    });

    $('.ProfileInfoEdit_save').on('click', function () {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        $('input[type="hidden"][name="name"]').val($('input[type="text"][name="name"]').val());
        $('input[type="hidden"][name="location"]').val($('input[type="text"][name="location"]').val());

        $.ajax({
            type: 'POST',
            url: '/profile/update',
            data: $('#ProfileInfoEdit').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Information updated', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

    });

    $('.ProfileEdit_save').on('click', function () {

        $.ajax({
            type: 'POST',
            url: '/profile/update_profile',
            data: $('#ProfileEdit').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Information updated', 'success');
                    $('input[name="your_password"]').val('');
                    $('input[name="password"]').val('');
                    $('input[name="password_confirmation"]').val('');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

    });

    $('.CategoryEdit_save').on('click', function () {

        $('input[type="hidden"][name="name"]').val($('input[type="text"][name="name"]').val());
        $('input[type="hidden"][name="location"]').val($('input[type="text"][name="location"]').val());

        let data_array = $('#CategoryEdit').serializeArray();
        let data = {};

        jQuery.each(data_array, function (i, val) {
            data[val.name] = val.value;
        });

        var avaliable = [];

        $('.switch input[type="checkbox"]').each(function (i, elem) {
            if ($(elem).is(':checked'))
                avaliable.push($(elem).data('val'));
        });

        data['avaliable'] = avaliable;

        $.ajax({
            type: 'POST',
            url: '/profile/update_category',
            data: data,
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Information updated', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });
    });
    $('#AddCategory').on('click', function () {
        swal.fire({
            title: 'Enter category name',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Add',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/profile/add_category',
                    data: {
                        name: result.value,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {

                            $('.avaliable_checkbox').append('<label class="avaliable_label">' +
                                '   <span class="switch">' +
                                '       <input type="checkbox" data-val="' + result.value + '">' +
                                '       <span class="checkbox_block"></span>' +
                                '   </span>' +
                                '   <span class="checkbox_name">' + result.value + '</span>' +
                                '</label>');

                            swal('Success', '', 'success');
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        });

        return false;
    });

    $('.avaliable_save').on('click', function() {
        var avaliable = [];

        $('.switch input[type="checkbox"]').each(function(i,elem) {
            if ($(elem).is(':checked'))
               avaliable.push($(elem).data('val'));
        });

        $.ajax({
            type: 'POST',
            url: '/profile/update_avaliable',
            data: {avaliable: avaliable, _token: $('input[name="_token"]').val()},
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Availables updated', 'success');
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    let add_gallery = false;

    $('.upload_to_gallery_btn').on('click', function () {
        let formData = [];

        let forms = $('.imageUploadForm');

        $.each(forms, function(key, form){
            if ($(form).find('input[name="photo"]').val().length > 0)
                formData.push({
                    img: $(form).find('input[name="photo"]').val(),
                    title: $(form).closest('.add_to_gallary_block').find('input[name="title"]').val(),
                    price: $(form).closest('.add_to_gallary_block').find('.price').val(),
                    description: $(form).closest('.add_to_gallary_block').find('textarea[name="description"]').val()
                });
        });

        if (add_gallery == false)
            $.ajax({
                url: '/gallery/add',
                data: {
                    photos: formData,
                    _token: $('input[name="_token"]').val()
                },
                type: 'POST',
                beforeSend: function() {
                    add_gallery = true;
                },
                success: function (data) {
                    if (data.status == 'success') {
                        swal('Success', 'Gallery updated', 'success');

                        $.each(forms, function(key, form) {
                            $(form).removeClass('loading');
                            $(form).find('.uploadedImg').css('background-image', 'none');
                            $(form).find('input[name="photo"]').val('');
                            $(form).closest('.add_to_gallary_block').find('input[name="title"]').val('');
                            $(form).closest('.add_to_gallary_block').find('textarea[name="description"]').val('');
                        });
                    } else {
                        swal('Error', data.message, 'error');
                    }
                },
                complete: function() {
                    add_gallery = false;
                }
            });
    });

    $('input[name="weight"]').mask('00kg');
});
