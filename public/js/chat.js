var conversation_id = false;

$(document).ready(function() {
    var load_dialog = false;
    var load_dialogs = false;
    var loading_ajax = false;
    var search_text = '';
    var page = 1;

    $('.window-chat #chat-window .dialog-body').slimScroll({
        width: '417px',
        height: '400px',
        start: 'bottom',
    });

    $('.OpenChat').on('click', function() {
        if ($('.dialog.window-chat').hasClass('active')) {
            conversation_id = false;
            $('.dialog.window-chat').removeClass('chat-opened');
            $('.dialog.window-chat').removeClass('active');
            return false;
        }

        if (typeof $(this).data('conversation_id') != 'undefined' && conversation_id != 0 && conversation_id != false && conversation_id == parseInt($(this).data('conversation_id')))
            return false;

        let id = $(this).data('id');
        let name = $(this).data('name');
        conversation_id = $(this).data('conversation_id');

        $.ajax({
            type: 'POST',
            url: '/chat/get_dialogs',
            data: {
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function() {
                load_dialogs = true;
            },
            success: function(data) {
                if (data.status = 'success') {
                    $(".window-chat #SearchBox").val('');
                    $('.window-chat #SearchBox').addClass('placeholder-shown')
                    search_text = '';

                    $('.window-chat #chat-list .dialog-body').html('');

                    if (data.unreadCount > 0)
                        $('.window-chat #chat-list .dialog-header-title span').html('<b class="red">'+data.unreadCount+'</b> New Messages');
                    else
                        $('.window-chat #chat-list .dialog-header-title span').html('');

                    jQuery.each(data.dialogs, function(i, val) {
                        $('.window-chat #chat-list .dialog-body').append(val.html);
                    });

                    load_dialogs = false;

                    if (typeof id != 'undefined') {
                        $('.window-chat #MessageBox').data('id', id);
                        $('.window-chat #Call').data('id', id);
                        $('.window-chat #VideoCall').data('id', id);

                        if (typeof conversation_id != 'undefined' && $('.window-chat .dialog-message-box[data-id="'+conversation_id+'"]').length > 0) {
                            $('.window-chat .dialog-message-box[data-id="'+conversation_id+'"]').click();
                        } else {
                            page = 1;
                            loading_ajax = false;
                            $('.window-chat #chat-window .dialog-body').html('');
                            $('.window-chat #chat-window .chat-person p').text(name);
                            $('#dialog.window-chat ').addClass('chat-opened');
                            $('.dialog.window-chat').addClass('active');
                        }
                    } else
                        $('.dialog.window-chat').addClass('active');
                }
            }
        });

        return false;
    });

    function is_visible(element) {
        var documentHeight = $('.window-chat #chat-window .dialog-body').height();
        var distanceFromBottom = documentHeight - (element.position().top + element.outerHeight(true)) + 500;

        return (distanceFromBottom < 0) ? false : true;
    }

    setInterval(function() {
        if (typeof conversation_id != 'undefined' && conversation_id != 0 && conversation_id != false) {
            if (load_dialog == false)
                $.ajax({
                    type: 'POST',
                    url: '/chat/get_dialog',
                    data: {
                        conversation_id: conversation_id,
                        last_hour: 1,
                        _token: $('input[name="_token"]').val(),
                        page: 1
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $('.window-chat #chat-window .chat-person span').text(data.last_activity);

                            jQuery.each(data.messages, function(i, val) {
                                if ($('.window-chat #chat-window .dialog-body .box-msg[data-id="'+i+'"]').length == 0 && load_dialog == false) {
                                    var $new = $(val).hide();
                                    $('.window-chat #chat-window .dialog-body').append($new);
                                    $new.show('fast');

                                    var audio = new Audio();
                                    audio.src = '/assets/notification.mp3';
                                    audio.autoplay = true;

                                    if (is_visible($new))
                                        $('.window-chat #chat-window .dialog-body').slimScroll({scrollTo: $('.window-chat #chat-window .dialog-body')[0].scrollHeight + 30});
                                }
                            });
                        }
                    }
                });
       } else if ($('.dialog.window-chat').hasClass('active') == true && $('.dialog.window-chat').hasClass('chat-opened') != true) {
            if (load_dialogs == false)
                $.ajax({
                    type: 'POST',
                    url: '/chat/get_dialogs',
                    data: {
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status = 'success') {
                            jQuery.each(data.dialogs, function(i, val) {

                                if ($('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').length == 0) {
                                    if (search_text.length > 0) {
                                        $box = $(val.html);
                                        if ($box.data('name').toLowerCase().indexOf(search_text.toLowerCase()) == -1)
                                            $box.hide();

                                        $('.window-chat #chat-list .dialog-body').prepend($box);
                                    } else
                                        $('.window-chat #chat-list .dialog-body').prepend(val.html);

                                    var audio = new Audio();
                                    audio.src = '/assets/notification.mp3';
                                    audio.autoplay = true;
                                } else {
                                    let $block = $(val.html);

                                    if ($block.hasClass('readed') == false && $('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').hasClass('readed') == true) {
                                        $('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').prependTo('.window-chat #chat-list .dialog-body');

                                        var audio = new Audio();
                                        audio.src = '/assets/notification.mp3';
                                        audio.autoplay = true;
                                    } else if ($('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"] p').text() != $block.find('p').text()) {
                                        $('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').prependTo('.window-chat #chat-list .dialog-body');

                                        var audio = new Audio();
                                        audio.src = '/assets/notification.mp3';
                                        audio.autoplay = true;
                                    }

                                    if ($block.hasClass('readed'))
                                        $('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').addClass('readed');
                                    else
                                        $('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').removeClass('readed');

                                    $('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"] p').text($block.find('p').text());
                                    $('.window-chat #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"] .message-date').text($block.find('.message-date').text());
                                }

                            });

                            if (data.unreadCount > 0)
                                $('.window-chat #chat-list .dialog-header-title span').html('<b class="red">'+data.unreadCount+'</b> New Messages');
                            else
                                $('.window-chat #chat-list .dialog-header-title span').html('');
                        }
                    }
                });
       }
    }, 2000);

    $('#dialog.window-chat').on('click', '.dialog-message-box', function() {
        conversation_id = parseInt($(this).data('id'));
        let id = $(this).data('id');
        let user_id = $(this).data('user_id');
        page = 1;
        loading_ajax = false;

        $('.window-chat #chat-window .dialog-body').html('');
        $('.window-chat #chat-window .chat-person p').text($(this).data('name'));


        $.ajax({
            type: 'POST',
            url: '/chat/get_dialog',
            data: {
                conversation_id: conversation_id,
                _token: $('input[name="_token"]').val(),
                page: page
            },
            beforeSend: function() {
                load_dialog = true;
            },
            success: function(data) {
                if (data.status == 'success') {
                    $(".window-chat #SearchBox").val('');
                    $('.window-chat #SearchBox').addClass('placeholder-shown');
                    search_text = '';

                    $('.window-chat #MessageBox').data('id', id);
                    $('.window-chat #Call').data('id', user_id);
                    $('.window-chat #VideoCall').data('id', user_id);

                    $('.window-chat #chat-window .chat-person span').text(data.last_activity);

                    jQuery.each(data.messages, function(i, val) {
                        if ($('.window-chat #chat-window .dialog-body .box-msg[data-id="'+i+'"]').length == 0) {
                            $('.window-chat #chat-window .dialog-body').append(val);
                        }
                    });

                    load_dialog = false;

                    $('.window-chat #chat-window .dialog-body').slimScroll({scrollTo: $('.window-chat #chat-window .dialog-body')[0].scrollHeight});

                    if ($('.dialog.window-chat').hasClass('active') == false)
                        $('.dialog.window-chat').addClass('active');

                    $('#dialog.window-chat').addClass('chat-opened');
                }
            }
        });
    });

    $('#dialog.window-chat').on('click', '.dialog-closed-window', function() {
        swal({
            title: 'Close dialog?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $('#dialog.window-chat').removeClass('active');
            }
        });

        return false;
    });

    $('.window-chat .chat-back').click(function() {
        conversation_id = false;

        $.ajax({
            type: 'POST',
            url: '/chat/get_dialogs',
            data: {
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function() {
                load_dialogs = true;
            },
            success: function(data) {
                if (data.status = 'success') {
                    $('.window-chat #chat-list .dialog-body').html('');

                    if (data.unreadCount > 0)
                        $('.window-chat #chat-list .dialog-header-title span').html('<b class="red">'+data.unreadCount+'</b> New Messages');
                    else
                        $('.window-chat #chat-list .dialog-header-title span').html('');

                    jQuery.each(data.dialogs, function(i, val) {
                        $('.window-chat #chat-list .dialog-body').append(val.html);
                    });

                    $('#dialog.window-chat').removeClass('chat-opened');

                    load_dialogs = false;
                }
            }
        });

        return false;
    });

    $(".window-chat #MessageBox").on('keyup', function(event) {
        if(event.keyCode == 13) {
            SendMessage();
            return false;
        }
    });

    $(".window-chat #MessageSend").on('click', function(event) {
        SendMessage();
    });

    function SendMessage() {
        let input = $('.window-chat #MessageBox');

        if (typeof conversation_id == 'undefined')
            conversation_id = 0;

        $.ajax({
            type: 'POST',
            url: '/chat/send_message',
            data: {
                message: input.val(),
                conversation_id: conversation_id,
                user_id: $('.window-chat #MessageBox').data('id'),
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function() {
                input.val('').trigger('autosize.resize');
            },
            success: function(data) {

                if (data.status == 'success') {
                    conversation_id = data.conversation_id;

                    if ($('.OpenChat[data-id="'+data.user_id+'"]').length > 0)
                        $('.OpenChat[data-id="'+data.user_id+'"]').data('conversation_id', conversation_id);

                    var $new = $(data.html).hide();
                    $('.window-chat #chat-window .dialog-body').append($new);
                    $new.show('fast');

                    $('.window-chat #chat-window .dialog-body').slimScroll({scrollTo: $('.window-chat #chat-window .dialog-body')[0].scrollHeight + 30});

                }

            }
        });

    }

    $(".window-chat #SearchBox").on('keyup', function(event) {
        search_text = $(this).val();

        if (search_text.length == 0)
            $('.window-chat #chat-list .dialog-body .dialog-message-box').show('fast');
        else
            $('.window-chat #chat-list .dialog-body .dialog-message-box').each(function(i,elem) {
                if ($(elem).data('name').toLowerCase().indexOf(search_text.toLowerCase()) == -1)
                    $(elem).hide('fast');
                else
                    $(elem).show('fast');
            });
    });

    $('.window-chat #chat-window .dialog-body').scroll(function(){

        if ($('.window-chat #chat-window .dialog-body').scrollTop() < 300) {
            if (loading_ajax == false) {
                page++;
                $.ajax({
                    type: 'POST',
                    url: '/chat/get_dialog',
                    data: {
                        conversation_id: conversation_id,
                        _token: $('input[name="_token"]').val(),
                        page: page,
                        reverse: 1
                    },
                    beforeSend: function() {
                        loading_ajax = true;
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            let last_key = false;
                            jQuery.each(data.messages, function(i, val) {
                                if ($('.window-chat #chat-window .dialog-body .box-msg[data-id="'+i+'"]').length == 0) {
                                    if (last_key == false)
                                        $('.window-chat #chat-window .dialog-body').prepend(val);
                                    else
                                        $('.window-chat #chat-window .dialog-body .box-msg[data-id="'+last_key+'"]').after(val);

                                    last_key = i;
                                }
                            });

                            $('.window-chat #chat-window .dialog-body').slimScroll({scrollTo: $('.window-chat #chat-window .dialog-body').scrollTop()});

                            loading_ajax = false;
                        }
                    }
                });
            }
        }
    });

    //PLACEHOLDER-SHOWN POLYFILL
    function placeholderPolyfill() {
        this.classList[this.value ? 'remove' : 'add']('placeholder-shown');
    }

    document.querySelectorAll('[placeholder]').forEach(el => {
        el.addEventListener('change', placeholderPolyfill);
        el.addEventListener('keyup', placeholderPolyfill);
    });

    /* Job */

    $('body').on('click', '.apply_for_job', function(e) {
        let id = $(this).data('id');

        swal({
            title: 'Send request?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {

                swal({
                    title: 'Message',
                    input: 'textarea',
                    showCancelButton: true,
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '/job-alerts/request',
                            data: {
                                id: id,
                                message: result.value,
                                _token: $('input[name="_token"]').val()
                            },
                            success: function(data) {
                                if (data.status == 'success') {
                                    swal('Success', 'Request has been sent', 'success');
                                } else {
                                    swal('Error', data.message, 'error');
                                }
                            }
                        });
                    } else if (typeof result.dismiss == 'undefined') {
                        swal('Error', 'Message required', 'error');
                    }
                })

            }
        })

        return false;
    });

    $('body').on('click', '.approve_job', function() {
        let id = $(this).data('id');

        swal({
            title: 'Approve request?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/job-alerts/request_approve',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            swal('Success', 'Request approved', 'success');
                            $('.job_btn_chat[data-id="'+id+'"]').remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('body').on('click', '.cancel_job', function() {
        let id = $(this).data('id');

        swal({
            title: 'Cancel request?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/job-alerts/request_cancel',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            swal('Success', 'Request canceled', 'success');
                            $('.job_btn_chat[data-id="'+id+'"]').remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    /* Job end */

});