var microphone = false;
var webcamera = false;
var connect_user = false;
var localstream = false;
var $stream_video = $("#stream_video");

jQuery(function ($) {
    $(window).on('beforeunload', function () {
        if (connect_user == true)
            return "It is forbidden to refresh the page.";
    });

    $('#StartStream').on('click', function () {
        if (microphone != true) {
            checkAccessMedia(true, true);
            return false;
        }
    });
});

function StartStream(UserID) {
    var phone = window.phone = PHONE({
        number: UserID,
        publish_key: 'pub-c-f260dc1e-2a5f-4324-9530-ed1ba3196aa3',
        subscribe_key: 'sub-c-dec33996-bd68-11e8-9de9-7af9a1823cc4',
        /*publish_key: 'pub-c-f9f84ff9-1ef9-4014-8837-e2e995455470',
        subscribe_key: 'sub-c-5253d1f8-4444-11e7-86e2-02ee2ddab7fe',*/
        oneway: true,
        broadcast: true,
        ssl: true,
        iceTransports: 'relay'
    });

    var ctrl = window.ctrl = CONTROLLER(phone);

    ctrl.ready(function (session) {
        ctrl.addLocalStream(document.getElementById("stream_video"));
        ctrl.stream();
        $stream_video.show(500);
    });

    ctrl.streamPresence(function (m) {

    });

    return false;
}

function checkAccessMedia(audio_access, video_access) {
    navigator.mediaDevices.getUserMedia({
        audio: audio_access,
        video: video_access
    }).then(function (stream) {

        microphone = audio_access;
        webcamera = video_access;
        localstream = stream;

        StartStream(user_id);
    }).catch(function (err) {
        console.log(err.name + ": " + err.message);
    });
}