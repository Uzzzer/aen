$(document).ready(function() {

    /* Group */

    $('.group-bar-link').click(function(e) {
        e.preventDefault();
        $('.post-bar.group-post-add').toggleClass('active');
    });

    $('.group-bar-link').click(function(e) {
        e.preventDefault();
        $('.post-bar.new-group-bar').toggleClass('active');
    });

    let upload_file_group = false;

    $('#upload-file.image-group').change(function(e) {
        if (upload_file_group == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'groups_avatars');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function() {
                upload_file_group = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    $('#upload-bg.image-group').attr('src', URL.createObjectURL(files[0]));
                    $('input[name="file_type"].image-group').val(data.type);
                    $('input[name="file_name"].image-group').val(data.uploads);
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function() {
                upload_file_group = false;
            }
        });
    });

    $('#NewGroup').on('submit', function() {
        if (upload_file_group == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/group/new_group',
            data: $(this).serializeArray(),
            success: function(data) {
                if (data.status == 'success') {
                    swal('Success', 'Group created', 'success');
                    $('.post-bar.new-group-bar').toggleClass('active');
                    $('#NewGroup')[0].reset();
                    $('#upload-bg.image-group').attr('src', '/img/upload-bg.jpg');
                    $('input[name="file_type"].image-group').val('');
                    $('input[name="file_name"].image-group').val('');
                    $('.groups_list').prepend(data.html);
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '.leave_group', function(e) {
        let a = $(this);
        let id = $(this).data('id');

        swal({
            title: 'Leave the group?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/group/leave',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            swal('Success', 'You left the group', 'success');

                            $(a).removeClass('leave_group');
                            $(a).addClass('join_group');


                            if (typeof $(a).data('private') != 'undefined')
                                $(a).text('Send request');
                            else
                                $(a).text('Join group');

                            if (typeof group_page != 'undefined' && group_page == 'subscribed-groups')
                                $('.groups-item[data-id="'+id+'"]').remove();


                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('body').on('click', '.join_group', function(e) {
        let a = $(this);
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/group/join',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function(data) {
                if (data.status == 'success') {
                    swal('Success', data.message, 'success');

                    $(a).removeClass('join_group');
                    $(a).addClass('leave_group');
                    if (typeof $(a).data('private') != 'undefined')
                        $(a).text('Request has been sent');
                    else
                        $(a).text('Leave group');
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    /* End group */

});