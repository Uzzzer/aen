$(document).ready(function () {
    //$('#card_number').mask('0000 0000 0000 0000');

    if ($('#MessageErrorOnLoad').length > 0)
        swal('Error', $('#MessageErrorOnLoad').val(), 'error');

    if ($('#MessageSuccessOnLoad').length > 0)
        swal('Success', $('#MessageSuccessOnLoad').val(), 'success');

    $('.job-bar-link').click(function (e) {
        e.preventDefault();
        if ($('#NewJob input[name="update_id"]').length) {
            $('#NewJob')[0].reset();
            $('#NewJob input[name="responsibilities[]"]').remove();
            $('.create-line.responsibilities').append('<input type="text" name="responsibilities[]" class="copy_row">');
            $('#NewJob input[name="update_id"]').remove();

            $('#upload-bg.image-job').attr('src', '/img/upload-bg.jpg');
            $('input[name="file_type"].image-job').val('');
            $('input[name="file_name"].image-job').val('');
            $('input[name="file_preview"].image-job').val('');
        } else
            $('.post-bar.job-post-add').toggleClass('active');
    });

    $('.video-bar-link').click(function (e) {
        e.preventDefault();
        $('.post-bar.video-post-add').toggleClass('active');
    });

    $('body').on('click', '.add_responsibilities', function () {
        let copy_row = $('.copy_row').clone();
        $(copy_row).removeClass('copy_row');
        $(copy_row).val('');
        console.log(copy_row);
        $(this).closest('.create-line').append(copy_row);
        return false;
    });

    $('#NewJob').on('submit', function () {
        if (upload_file_job == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        let url = '/job-alerts/add';
        let id = false;

        if ($('#NewJob input[name="update_id"]').length) {
            url = '/job-alerts/update';
            id = $('#NewJob input[name="update_id"]').val();
        }

        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serializeArray(),
            success: function (data) {
                if (data.status == 'success') {
                    swal('Success', 'Job created', 'success');
                    $('.post-bar.job-post-add').toggleClass('active');
                    $('#NewJob')[0].reset();

                    $('#NewJob input[name="responsibilities[]"]').remove();
                    $('.create-line.responsibilities').append('<input type="text" name="responsibilities[]" class="copy_row">');
                    $('#NewJob input[name="update_id"]').remove();

                    $('#upload-bg.image-job').attr('src', '/img/upload-bg.jpg');
                    $('input[name="file_type"].image-job').val('');
                    $('input[name="file_name"].image-job').val('');
                    $('input[name="file_preview"].image-job').val('');

                    if (id) {
                        $('.job_box[data-id="' + id + '"]').after(data.html);
                        $('.jobs_list').find('.job_box[data-id="' + id + '"]').first().remove();
                    } else
                        $('.jobs_list').prepend(data.html);
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('#NewVideo').on('submit', function () {
        if (upload_file_video == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/profile/video/add',
            data: $(this).serializeArray(),
            success: function (data) {
                if (data.status == 'success') {
                    swal('Success', 'Video added', 'success');
                    $('.post-bar.video-post-add').toggleClass('active');
                    $('#NewVideo')[0].reset();
                    $('img.image-video').attr('src', '/img/upload-bg.jpg');
                    $('input[name="file_type"].image-video').val('');
                    $('input[name="file_name"].image-video').val('');
                    $('.videos-box').prepend(data.html);
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '.delete_job', function (e) {
        let block = $(this).closest('.job_box');
        let id = $(this).data('id');

        swal({
            title: 'Delete job?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/job-alerts/delete',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {

                            $(block).remove();

                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('body').on('click', '.edit_job', function (e) {
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/job-alerts/get',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                if (data.status === 'success') {

                    $('.profile-content.overflow_block').animate({scrollTop: 280}, 500);

                    $('#NewJob')[0].reset();
                    $('#upload-bg.image-job').attr('src', '/img/upload-bg.jpg');
                    $('input[name="file_type"].image-job').val('');
                    $('input[name="file_name"].image-job').val('');

                    if (!$('.job-post-add').hasClass('active'))
                        $('.job-post-add').addClass('active');

                    $('#NewJob input[type="submit"]').val('Update job');
                    $('#NewJob').append('<input type="hidden" name="update_id" value="' + id + '">');

                    $('#NewJob input[name="JobTitle"]').val(data.job.title);
                    $('#NewJob textarea[name="Description"]').val(data.job.text);

                    $('#NewJob input[name="responsibilities[]"]').remove();
                    $('.create-line.responsibilities').append('<input type="text" name="responsibilities[]" class="copy_row">');

                    let responsibilities = JSON.parse(data.job.responsibilities);
                    let fields = JSON.parse(data.job.fields);

                    $.each(responsibilities, function (i, val) {
                        if (i === 0) {
                            $('input[name="responsibilities[]"]').val(val);
                        } else {
                            $('.create-line.responsibilities').append('<input type="text" name="responsibilities[]" value="' + val + '">');
                        }
                    });

                    $.each(fields, function (i, val) {
                        $('#NewJob input[name="fields[' + i + ']"]').val(val);
                    });

                    if (data.job.file_type == 'video') {
                        if (data.job.file_preview != null) {
                            $('#NewJob #upload-bg.image-job').attr('src', data.job.file_preview);
                            $('#NewJob input[name="file_type"].image-job').val('video');
                            $('#NewJob input[name="file_name"].image-job').val(data.job.file_name);
                            $('#NewJob input[name="file_preview"].image-job').val(data.job.file_preview);
                        } else {
                            $('#NewJob #upload-bg.image-job').attr('src', '/img/upload-bg.jpg');
                        }
                    } else if (data.job.file_type == 'image') {
                        $('#NewJob #upload-bg.image-job').attr('src', data.job.file_name);
                        $('#NewJob input[name="file_type"].image-job').val('image');
                        $('#NewJob input[name="file_name"].image-job').val(data.job.file_name);
                        $('#NewJob input[name="file_preview"].image-job').val('');
                    }

                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('.add_insta_post').on('click', function () {
        $this = $(this);

        swal.fire({
            title: 'Post url',
            input: 'text',
            inputValue: $(this).data('url') ? $(this).data('url') : '',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: $(this).data('url') ? 'Edit' : 'Add',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/profile/add_insta_post',
                    data: {
                        url: result.value,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {

                            $this.data('url', result.value);
                            $this.text('Edit Post');

                            swal('Success', '', 'success');
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        });

        return false;
    });

    $('.add_social_link').on('click', function () {
        $this = $(this);

        swal.fire({
            title: 'Page link',
            input: 'text',
            inputValue: $(this).data('url') ? $(this).data('url') : '',
            inputPlaceholder: 'https://',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: $(this).data('url') ? 'Edit' : 'Add',
        }).then((result) => {
            if (typeof result.dismiss === 'undefined') {
                $.ajax({
                    type: 'POST',
                    url: '/profile/set_social_link',
                    data: {
                        field: $this.data('field'),
                        url: result.value.length ? result.value : null,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {

                            $this.data('url', result.value);
                            if (result.value.length) {
                                $this.text('Edit link to page');
                            } else {
                                $this.text('Add link to page');
                            }

                            swal('Success', '', 'success');
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        });

        return false;
    });

    $('.add_tweet').on('click', function () {
        $this = $(this);

        swal.fire({
            title: 'Tweet url',
            input: 'text',
            inputValue: $(this).data('url') ? $(this).data('url') : '',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: $(this).data('url') ? 'Edit' : 'Add',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/profile/add_tweet',
                    data: {
                        url: result.value,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {

                            $this.data('url', result.value);
                            $this.text('Edit Tweet');

                            swal('Success', '', 'success');
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        });

        return false;
    });

    $('#AddCategory').on('click', function () {
        swal.fire({
            title: 'Enter category name',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Add',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/profile/add_category',
                    data: {
                        name: result.value,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {

                            $('.avaliable_checkbox').append('<label class="avaliable_label">' +
                                '   <span class="switch">' +
                                '       <input type="checkbox" data-val="' + result.value + '">' +
                                '       <span class="checkbox_block"></span>' +
                                '   </span>' +
                                '   <span class="checkbox_name">' + result.value + '</span>' +
                                '</label>');

                            swal('Success', '', 'success');
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        });

        return false;
    });

    let upload_file_job = false;


    $('input[type="file"].image-job').change(function (e) {
        if (upload_file_job == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'jobs');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                upload_file_job = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    if (data.preview != null) {
                        $('img.image-job').attr('src', data.preview);
                        $('input[name="file_preview"].image-job').val(data.preview);
                    } else
                        $('img.image-job').attr('src', URL.createObjectURL(files[0]));
                    $('input[name="file_type"].image-job').val(data.type);
                    $('input[name="file_name"].image-job').val(data.uploads);
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function () {
                upload_file_job = false;
            }
        });
    });

    $('body').on('click', '.delete_video', function (e) {
        let block = $(this).closest('.item');
        let id = $(this).data('id');

        swal({
            title: 'Delete video?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/profile/video/delete',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function (data) {
                        if (data.status == 'success') {
                            $(block).remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    let upload_file_video = false;

    $('input[type="file"].image-video').change(function (e) {
        if (upload_file_video == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'videos');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
            	$("#NewVideo .upload-section").append("<div class='upload_progress'><span class='percent_text'></span><div class='percent'></div></div>");
                upload_file_video = true;
            },
			xhr: function() {
				var myXhr = $.ajaxSettings.xhr();
				if(myXhr.upload){
					myXhr.upload.addEventListener('progress',function(e){
						if(e.lengthComputable){
							var max = e.total;
							var current = e.loaded;

							var percentage = (current * 100)/max;
							$(".upload_progress .percent").css("width", percentage+"%");
							$(".upload_progress .percent_text").html(parseInt(percentage)+"%");

							if(percentage >= 100){
								$(".upload_progress").remove();
							}
						}  


					}, false);
				}
				return myXhr;
			},
            
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    $('img.image-video').attr('src', URL.createObjectURL(files[0]));
                    $('input[name="file_type"].image-video').val(data.type);
                    $('input[name="file_name"].image-video').val(data.uploads);
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function () {
                upload_file_video = false;
            }
        });
    });

    var upload_file = false;
    var cropp_file = false;

    $('#UploadAvatarBox #CancelCropp').on('click', function () {

        $('#UploadAvatarBox').hide();
        $('body').removeClass('overflow');
        $('#UploadAvatarBox img').cropper('destroy');

    });
    
    $('#UploadAvatarBox #ApplyCropp').on('click', function () {
    	if(!cropp_file && typeof window.gallery_cropp_file!='undefined'){
    		cropp_file = window.gallery_cropp_file;
    	}
    	if(typeof window.ccro!='undefined'){
    		var dataurl = window.ccro[0].cropper.getCroppedCanvas().toDataURL();
    		if(typeof last_file_changed!='undefined' && last_file_changed!=false){
    			$(last_file_changed).closest("form").find(".uploadedImg").css("background-image", dataurl)
    		}
    	}
    	
    	
        $.ajax({
            type: 'POST',
            url: '/cropp',
            data: {
                filename: cropp_file,
                _token: $('input[name="_token"]').val(),
                data: $('#UploadAvatarBox img').data('cropper').getData()
            },
            beforeSend: function () {
                $('#UploadAvatarBox').hide();
                $('body').removeClass('overflow');
                $('#UploadAvatarBox img').attr('src', '');
            },
            success: function (response) {
                if (response.status == "success") {
                	if(typeof window.ccro=='undefined' || window.ccro==false){
		                $('.sidebar .bl_person_photo img').attr('src', response.filename);
		                $('input[name="avatar"]').val(response.filename);
                    }
                    $('#UploadAvatarBox img').cropper('destroy');
                } else {
                    swal('Error', response.message, 'error');
                    $('#UploadAvatarBox img').cropper('destroy');
                }
            },
            error: function(){
            	console.log("can't crop file");
            	$('#UploadAvatarBox img').cropper('destroy');
            }
        });

    });

    $('#AvatarUpload').change(function (e) {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'avatars');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                upload_file = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    cropp_file = data.uploads;

                    $('#UploadAvatarBox').show();
                    $('body').addClass('overflow');
                    $('#UploadAvatarBox img').attr('src', URL.createObjectURL(files[0]));

                    $('#UploadAvatarBox img').cropper({
                        aspectRatio: 1 / 1,
                        zoomable: false,
                        viewMode: 1,
                        minCropBoxWidth: 200,
                        minCropBoxHeight: 200
                    });

                    $('#AvatarUpload').val('');
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function () {
                upload_file = false;
            }
        });
    });

    $('.ProfileInfoEdit_save').on('click', function () {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        $('input[type="hidden"][name="name"]').val($('input[type="text"][name="name"]').val());
        $('input[type="hidden"][name="location"]').val($('input[type="text"][name="location"]').val());

        let data_array = $('#ProfileInfoEdit').serializeArray();
        let data = {};

        jQuery.each(data_array, function (i, val) {
            data[val.name] = val.value;
        });

        var avaliable = [];

        $('.switch input[type="checkbox"]').each(function (i, elem) {
            if ($(elem).is(':checked'))
                avaliable.push($(elem).data('val'));
        });

        data['avaliable'] = avaliable;

        $.ajax({
            type: 'POST',
            url: '/profile/update/business',
            data: data,
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Information updated', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

    });

    $('.ProfileEdit_save').on('click', function () {

        $.ajax({
            type: 'POST',
            url: '/profile/update_profile',
            data: $('#ProfileEdit').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Information updated', 'success');
                    $('input[name="your_password"]').val('');
                    $('input[name="password"]').val('');
                    $('input[name="password_confirmation"]').val('');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

    });

    let add_gallery = false;

    $('.upload_to_gallery_btn').on('click', function () {
        let formData = [];

        let forms = $('.imageUploadForm'); 

        $.each(forms, function (key, form) {
            if ($(form).find('input[name="photo"]').val().length > 0)
                formData.push({
                    img: $(form).find('input[name="photo"]').val(),
                    preview: $(form).find('input[name="preview"]').val(),
                    title: $(form).closest('.add_to_gallary_block').find('input[name="title"]').val(),
                    price: $(form).closest('.add_to_gallary_block').find('.price').val(),
                    description: $(form).closest('.add_to_gallary_block').find('textarea[name="description"]').val()
                });
        });

        if (add_gallery == false)
            $.ajax({
                url: '/gallery/add',
                data: {
                    photos: formData,
                    _token: $('input[name="_token"]').val()
                },
                type: 'POST',
                beforeSend: function () {
                    add_gallery = true;
                },
                success: function (data) {
                    if (data.status == 'success') {
                        swal('Success', 'Gallery updated', 'success');

                        $.each(forms, function (key, form) {
                            $(form).removeClass('loading');
                            $(form).find('.uploadedImg').css('background-image', 'none');
                            $(form).find('input[name="photo"]').val('');
                            $(form).closest('.add_to_gallary_block').find('input[name="title"]').val('');
                            $(form).closest('.add_to_gallary_block').find('textarea[name="description"]').val('');
                        });
                    } else {
                        swal('Error', data.message, 'error');
                    }
                },
                complete: function () {
                    add_gallery = false;
                }
            });
    });

    $('input[name="weight"]').mask('00kg');
});
