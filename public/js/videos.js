$(document).ready(function() {

    $('body').on('click', '.like_video', function(e) {
        let $like = $(this);

        $.ajax({
            type: 'POST',
            url: '/profile/video/like',
            data: {
                id: $like.data('id'),
                _token: $('input[name="_token"]').val()
            },
            success: function(data) {
                if (data.status == 'success') {
                    $like.toggleClass('liked');
                    if (parseInt(data.count_like) > 0)
                        $like.find('span').text(' ('+data.count_like+')');
                    else
                        $like.find('span').text('');
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', 'a.must_buy_video', function() {
        swal('Error', 'You must buy this video.', 'error');
        return false;
    });
});