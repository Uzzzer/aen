$(document).ready(function() {
    $('#card_number').mask('0000 0000 0000 0000');
    $('#card_exp').mask('00/00');
    $('#card_cvv').mask('000');

    $('#PaymentSubmit').on('click', function() {
        $.ajax({
            type: 'POST',
            url: '/profile/payment',
            data: $('#PaymentForm').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', response.message, 'success');
                    if (typeof response.redirect_url != 'undefined' && response.redirect_url.length)
                        setTimeout(function() {
                            window.location.href = response.redirect_url;
                        }, 1500);
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });
    });
});