var initMap;

$(document).ready(function() {

    if ($('.birthday-date').length) {
        $('.birthday-date').mask('00/00/0000');
    }

    $('.login_mobile_popup').on('click', function () {
        $('.m_left_block').show();
    });

    $('.login_mobile_popup_close').on('click', function () {
        $('.m_left_block').hide();
    });

    function initSummitMap(mapEl) {
        console.log('Init Summit map');
        //API KEY AIzaSyAHUj5qNXTpr3Q4VRuTkPCxeEG_h5V_G4M

        var map;
        var apiState = document.getElementById('gmaps-api-script') || false;
        if (!apiState) {
            initMap = function () {
                var myLatLng = { lat: 38.703691, lng: -9.467296 };

                map = new google.maps.Map(mapEl, {
                    center: myLatLng,
                    zoom: 18,
                    styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: 'The Oitavos R. de Oitavos, Cascais, Portugal'
                });
            }

            var gmapsApiScript = $('<script id="gmaps-api-script" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHUj5qNXTpr3Q4VRuTkPCxeEG_h5V_G4M&callback=initMap" async defer ></script >');

            gmapsApiScript.insertAfter($(mapEl));
        }
    }

    $('.groups-item-wheel').click(function(e) {
        e.preventDefault();
    });

    $('.summit_btns a').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });

    $('#show_map_btn').click(function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.toggleClass('active');
        if ($this.hasClass('active')) {
            $this.text('Hide Map');
        } else {
            $this.text('Show Map');
        }

        var summitMapContainer = document.getElementById('summit_map') || false;
        if (summitMapContainer) {
            summitMapContainer.classList.toggle('active');
            initSummitMap(summitMapContainer);
        }
    });

    /* connections */

    $('.connection_request').on('click', function() {
        let id = $(this).data('id');
        let btn = $(this);

        swal({
            title: 'Send the request?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/connections/request',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            swal('Success', 'Request has been sent', 'success');
                            if ($(btn).hasClass('btn_follow'))
                                $(btn).addClass('btn_disabled')
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('.approved_request').on('click', function() {
        let id = $(this).data('id');

        swal({
            title: 'Approved request?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/connections/approved',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            swal('Success', 'Request approved', 'success');
                            $('.follow-item[data-id="'+id+'"]').remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('.remove_connection').on('click', function() {
        let id = $(this).data('id');

        swal({
            title: 'Remove the connection?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/connections/remove',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            swal('Success', 'Connection removed', 'success');
                            $('.follow-item[data-id="'+id+'"]').remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    setInterval(function() {
        $.ajax({
            type: 'POST',
            url: '/chat/checkCountUnread',
            data: {
                _token: $('input[name="_token"]').val()
            },
            success: function(data) {
                if (data.status == 'success') {
                    if (parseInt(data.unreadCount) > 0) {
                        if ($('#OpenChatUnread .star').length == 0)
                            $('#OpenChatUnread').append('<div class="star"></div>');

                        $('#OpenChatUnread .star').text(data.unreadCount);
                    } else {
                        $('#OpenChatUnread .star').remove();
                    }

                    if (parseInt(data.unreadNoticeCount) > 0) {
                        if ($('#NotificationUnread .star').length == 0)
                            $('#NotificationUnread').append('<div class="star"></div>');

                        $('#NotificationUnread .star').text(data.unreadNoticeCount);
                    } else {
                        $('#NotificationUnread .star').remove();
                    }

                    if (parseInt(data.requestCount) > 0) {
                        if ($('#ConnectionRequestBox .star').length == 0)
                            $('#ConnectionRequestBox').append('<div class="star"></div>');

                        $('#ConnectionRequestBox .star').text(data.requestCount);
                        $('#ConnectionRequestBox a').attr('href', '/connection-requests');
                        $('a#ConnectionRequestBox').attr('href', '/connection-requests');
                    } else {
                        $('#ConnectionRequestBox .star').remove();
                        $('#ConnectionRequestBox a').attr('href', '/connections');
                        $('a#ConnectionRequestBox').attr('href', '/connections');
                    }
                }
            }
        });
    }, 5000);

    /* connections end */

    /* business */

    $('body').on('click', '.open_job_profile', function() {
        $(this).closest('.job_box').find('.tab_job_block_bottom').toggle('fast');

        if ($(this).text() == 'View Details')
            $(this).text('Close Details');
        else
            $(this).text('View Details');
    });

    /* business end */

});