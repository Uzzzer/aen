$(document).ready(function () {
    $("#firstCar").owlCarousel({
        autoplay: false,
        lazyLoad: true,
        loop: false,
        margin: 20,
        responsiveClass: true,
        autoHeight: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    $(".tab-wrapper .tab").click(function () {
        $(this).parent().find('.tab').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.tab-wrapper').find('.tab_item').hide().eq($(this).index()).fadeIn();
    });

    $(".work-with-box .item").click(function () {
        $(this).parent().find(".item").not(this).removeClass("active");
        $(this).toggleClass("active");
    });

    // $(".videos-box .item").click(function() {
    //   if(!$(this).find(".overlay-block").hasClass('active') && !$(this).find(".price-box").hasClass('active')){
    //     $(this).find(".overlay-block").addClass('active');
    //     $(this).find(".price-box").addClass('active');
    //   }else if($(this).find(".overlay-block").hasClass('active') && $(this).find(".price-box").hasClass('active') && !$(this).find(".circle").hasClass('active')){
    //     $(this).find(".price-box").removeClass('active');
    //     $(this).find(".circle").addClass('active');
    //   }else{
    //     $(this).find(".circle").removeClass('active');
    //     $(this).find(".overlay-block").removeClass('active');
    //   }
    // });

    $(".videos-box").on('click', '.item', function (e) {
        let $target = $(e.target);
        if (typeof $target.attr('href') == 'undefined') {
            $(this).toggleClass("detail-item");
            $(this).find(".video-information-wrapper").slideToggle();
        }
    });

    $(".photos-box").on('click', '.item', function (e) {
        let $target = $(e.target);
        if (typeof $target.attr('href') == 'undefined') {
            //$(this).toggleClass("detail-item");
            //$(this).find(".photo-information-wrapper").slideToggle();
        }
    });

    new WOW().init();

});  
 