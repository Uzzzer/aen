$(document).ready(function() {
    $('#BalanceLogs').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : false,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 20
    })
});