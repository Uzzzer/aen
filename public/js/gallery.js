$(document).ready(function () {

    $('body').on('click', '.like_photo', function (e) {
        let like = $(this);
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/gallery/like',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function (data) {
                if (data.status == 'success') {
                    $(like).toggleClass('liked');

                    if (like.hasClass('liked'))
                        $(like).html('<i class="fa fa-thumbs-up" aria-hidden="true"></i>' + data.count_like);
                    else
                        $(like).html('<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' + data.count_like);

                    if (parseInt(data.count_like) > 0) {
                        $('.photos-box .item[data-id="' + id + '"] .like_photo span').text(' (' + data.count_like + ')');
                    } else {
                        $('.photos-box .item[data-id="' + id + '"] .like_photo span').text('');
                    }
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });
});