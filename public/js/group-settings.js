$(document).ready(function() {

    let upload_file = false;
    let cropp_file = false;

    $('#UploadAvatarBox #CancelCropp').on('click', function () {

        $('#UploadAvatarBox').hide();
        $('#UploadAvatarBox img').cropper('destroy');

    });

    $('#UploadAvatarBox #ApplyCropp').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/cropp',
            data: {
                filename: cropp_file,
                _token: $('input[name="_token"]').val(),
                data: $('#UploadAvatarBox img').data('cropper').getData()
            },
            beforeSend: function () {
                $('#UploadAvatarBox').hide();
                $('body').removeClass('overflow');
                $('#UploadAvatarBox img').attr('src', '');
            },
            success: function (response) {
                if (response.status == "success") {
                    $('.sidebar .bl_person_photo img').attr('src', response.filename);
                    $('input[name="avatar"]').val(response.filename);
                    $('#UploadAvatarBox img').cropper('destroy');
                } else {
                    swal('Error', response.message, 'error');
                    $('#UploadAvatarBox img').cropper('destroy');
                }
            }
        });

    });

    $('#AvatarUpload').change(function(e) {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'avatars');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function() {
                upload_file = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    cropp_file = data.uploads;

                    $('#UploadAvatarBox').show();
                    $('body').addClass('overflow');
                    $('#UploadAvatarBox img').attr('src', URL.createObjectURL(files[0]));

                    $('#UploadAvatarBox img').cropper({
                        aspectRatio: 1 / 1,
                        zoomable: false,
                        viewMode: 1,
                        minCropBoxWidth: 200,
                        minCropBoxHeight: 200
                    });

                    $('#AvatarUpload').val('');
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function() {
                upload_file = false;
            }
        });
    });

    $('.GroupInfoEdit_save').on('click', function () {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        $('input[type="hidden"][name="GroupName"]').val($('input[type="text"][name="GroupName"]').val());

        $.ajax({
            type: 'POST',
            url: '/group/update',
            data: $('#GroupInfoEdit').serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', 'Information updated', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

    });

    let add_gallery = false;

    $('.upload_to_gallery_btn').on('click', function () {
        let formData = [];

        let forms = $('.imageUploadForm');


        $.each(forms, function(key, form){
            if ($(form).find('input[name="photo"]').val().length > 0)
                formData.push({
                    img: $(form).find('input[name="photo"]').val(),
                    preview: $(form).find('input[name="preview"]').val(),
                    title: $(form).closest('.add_to_gallary_block').find('input[name="title"]').val(),
                    description: $(form).closest('.add_to_gallary_block').find('textarea[name="description"]').val()
                });
        });

        let url = '/gallery/add';
        if ($(this).hasClass('video_gallery'))
            url = '/video-gallery/add';

        if (add_gallery == false)
            $.ajax({
                url: url,
                data: {
                    photos: formData,
                    _token: $('input[name="_token"]').val(),
                    group_id: $('input[name="group_id"]').val()
                },
                type: 'POST',
                beforeSend: function() {
                    add_gallery = true;
                },
                success: function (data) {
                    if (data.status == 'success') {
                        swal('Success', 'Gallery updated', 'success');

                        $.each(forms, function(key, form) {
                            $(form).removeClass('loading');
                            $(form).find('.uploadedImg').css('background-image', 'none');
                            $(form).find('input[name="photo"]').val('');
                            $(form).closest('.add_to_gallary_block').find('input[name="title"]').val('');
                            $(form).closest('.add_to_gallary_block').find('textarea[name="description"]').val('');
                        });
                    } else {
                        swal('Error', data.message, 'error');
                    }
                },
                complete: function() {
                    add_gallery = false;
                }
            });
    });

    $('body').on('click', '.delete_photo', function(e) {
        let block = $(this).closest('.item');
        let id = $(this).data('id');

        swal({
            title: 'Delete photo?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/gallery/delete',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $(block).remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('body').on('click', '.group_request_approve', function(e) {
        let block = $(this).closest('.groups-item');
        let id = $(this).data('id');

        swal({
            title: 'Approve request?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/group/approve',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $(block).remove();

                            if ($('.groups-item').length == 0) {
                                $('.tabs .tab:first-child').click();
                                $('.tabs .tab:last-child').remove();
                            }
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('body').on('click', '.group_request_cancel', function(e) {
        let block = $(this).closest('.groups-item');
        let id = $(this).data('id');

        swal({
            title: 'Cancel request?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/group/cancel',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $(block).remove();

                            if ($('.groups-item').length == 0) {
                                $('.tabs .tab:first-child').click();
                                $('.tabs .tab:last-child').remove();
                            }
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

    $('body').on('click', '.delete_video', function(e) {
        let block = $(this).closest('.item');
        let id = $(this).data('id');

        swal({
            title: 'Delete video?',
            text: "",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/video-gallery/delete',
                    data: {
                        id: id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $(block).remove();
                        } else {
                            swal('Error', data.message, 'error');
                        }
                    }
                });
            }
        })

        return false;
    });

});