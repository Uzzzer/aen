$(document).ready(function() {

    $('body').on('click', '.like_video', function(e) {
        let like = $(this);
        let id = $(this).data('id');

        $.ajax({
            type: 'POST',
            url: '/video-gallery/like',
            data: {
                id: id,
                _token: $('input[name="_token"]').val()
            },
            success: function(data) {
                if (data.status == 'success') {
                    $(like).toggleClass('liked');
                    if (parseInt(data.count_like) > 0)
                        $('.photo-bottom-footer[data-id="'+id+'"] .like_video span').text(' ('+data.count_like+')');
                    else
                        $('.photo-bottom-footer[data-id="'+id+'"] .like_video span').text('');
                } else {
                    swal('Error', data.message, 'error');
                }
            }
        });

        return false;
    });
});