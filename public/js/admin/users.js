$(document).ready(function() {
    $('#Users').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 15,
        'order'         : [[ 0, "desc" ]]
    })

    $('body').on('click', '.table-link tbody tr', function() {
        if (typeof $(this).data('url') != 'undefined')
            location.href = $(this).data('url');
    });

    $('.select-import').on('click', function () {
        $('input[name="file"]').click();
    });

    $('input[name="file"]').on('change', function () {
        $('.form-import').submit();
    });

    /* User detail */

    $('#ConnectionRequests').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#Groups').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#Jobs').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#Posts').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#WorksWith').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#Videos').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#Chats').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#SaveBasicInfo').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/admin/user/basic_info',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

    let upload_file = false;
    let cropp_file = false;

    $('#CancelCropp').on('click', function () {

        $('#UploadAvatarModal').hide();
        $('#UploadAvatarModal').removeClass('in');
        $('#UploadAvatarModal img.cropper-box').cropper('destroy');

    });

    $('#ApplyCropp').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/cropp',
            data: {
                filename: cropp_file,
                _token: $('input[name="_token"]').val(),
                data: $('#UploadAvatarModal img.cropper-box').data('cropper').getData()
            },
            beforeSend: function () {
                $('#UploadAvatarModal').hide();
                $('#UploadAvatarModal img.cropper-box').cropper('destroy');
            },
            success: function (response) {
                if (response.status == "success") {
                    $.ajax({
                        type: 'POST',
                        url: '/admin/user/avatar',
                        data: {
                            avatar: response.filename,
                            _token: $('input[name="_token"]').val(),
                            user_id: $('input[name="user_id"]').val()
                        },
                        success: function (response_avatar) {
                            if (response_avatar.status == "success") {
                                $('img.avatar_user_page').attr('src', response.filename);
                            } else {
                                swal('Error', response_avatar.message, 'error');
                            }
                        }
                    });
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

    });

    $('#AvatarUploadClick').on('click', function() {
        $('#AvatarUpload').click();
    });

    $('#AvatarUpload').change(function(e) {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'avatars');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function() {
                upload_file = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {
                    cropp_file = data.uploads;

                    $('#UploadAvatarModal').show();
                    $('#UploadAvatarModal').addClass('in');

                    $('#UploadAvatarModal img.cropper-box').attr('src', URL.createObjectURL(files[0]));

                    $('#UploadAvatarModal img.cropper-box').cropper({
                        aspectRatio: 1 / 1,
                        zoomable: false,
                        viewMode: 1,
                        minCropBoxWidth: 200,
                        minCropBoxHeight: 200
                    });

                    $('#AvatarUpload').val('');
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function() {
                upload_file = false;
            }
        });
    });

    $('body').on('click', '#BlockUser', function() {
        let $button = $(this);

        swal({
            title: "Block user?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Block",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/user/block',
                data: {
                    _token: $('input[name="_token"]').val(),
                    user_id: $('input[name="user_id"]').val()
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "The user is blocked!", "success");
                        $button.text('Unblock');
                        $button.attr('id', 'UnblockUser');
                        $button.removeClass('btn-danger');
                        $button.addClass('btn-primary');
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });

    $('body').on('click', '#UnblockUser', function() {
        let $button = $(this);

        swal({
            title: "Unblock user?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Unblock",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/user/unblock',
                data: {
                    _token: $('input[name="_token"]').val(),
                    user_id: $('input[name="user_id"]').val()
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "The user is unblocked!", "success");
                        $button.text('Block');
                        $button.attr('id', 'BlockUser');
                        $button.removeClass('btn-primary');
                        $button.addClass('btn-danger');
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });
});