$(document).ready(function() {
    $('#AuthLogs').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 50,
        'order'         : [[ 0, "desc" ]]
    })
});