$(document).ready(function () {
    $('#Withdraws').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,
        'pageLength': 15,
        'order': [[0, "desc"]]
    })

    $('body').on('click', '.approved_withdraw', function () {
        let $button = $(this);
        let $tr = $(this).closest('tr');

        swal({
                title: "Approve withdraw?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Approve",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    type: 'POST',
                    url: '/admin/withdraw-money/set_status',
                    data: {
                        _token: $('input[name="_token"]').val(),
                        withdraw_id: $button.data('id'),
                        status: 'approved'
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            swal("Success", "", "success");
                            $tr.find('td:eq( 2 )').html('<span class="label label-success">Approved</span>');
                            $tr.find('td:last-child').html('');
                        } else {
                            swal('Error', response.message, 'error');
                        }
                    }
                });
            });
    });

    $('body').on('click', '.canceled_withdraw', function () {
        let $button = $(this);
        let $tr = $(this).closest('tr');

        swal({
                title: "Canceled withdraw?",
                text: "Money will be returned to the user's balance.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Canceled",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    type: 'POST',
                    url: '/admin/withdraw-money/set_status',
                    data: {
                        _token: $('input[name="_token"]').val(),
                        withdraw_id: $button.data('id'),
                        status: 'canceled'
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            swal("Success", "", "success");
                            $tr.find('td:eq( 2 )').html('<span class="label label-danger">Canceled</span>');
                            $tr.find('td:last-child').html('');
                        } else {
                            swal('Error', response.message, 'error');
                        }
                    }
                });
            });
    });
});