$(document).ready(function() {
    $('#GroupVideos').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 15,
        'order'         : [[ 0, "desc" ]]
    })

    $('body').on('click', '.table-link tbody tr', function() {
        if (typeof $(this).data('url') != 'undefined')
            location.href = $(this).data('url');
    });

    /* Group Video detail */

    $('#Likes').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#SaveBasicInfo').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/admin/group-video/basic_info',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '#DeleteGroupVideo', function() {
        let $button = $(this);

        swal({
            title: "Delete video?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/group-video/delete',
                data: {
                    _token: $('input[name="_token"]').val(),
                    video_id: $('input[name="video_id"]').val()
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "Video deleted!", "success");
                        setTimeout(function() {
                            window.location.href = '/admin/group-videos';
                        }, 1500);
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });

    $('body').on('click', '#DeleteComment', function() {
        let $button = $(this);
        let $row = $(this).closest('tr');

        swal({
            title: "Delete comment?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/group-video/delete_comment',
                data: {
                    _token: $('input[name="_token"]').val(),
                    comment_id: $button.data('id')
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "Comment deleted!", "success");

                        if ($row.hasClass('next_tr'))
                            $row.next('tr').remove();

                        $row.remove();
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });

});