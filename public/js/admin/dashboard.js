$(document).ready(function() {
    $('#Users').DataTable({
        'paging'        : false,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : false,
        'autoWidth'     : false,
        'order'         : [[ 0, "desc" ]]
    })

    $('#Groups').DataTable({
        'paging'        : false,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : false,
        'autoWidth'     : false,
        'order'         : [[ 0, "desc" ]]
    })

    $('#Posts').DataTable({
        'paging'        : false,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : false,
        'autoWidth'     : false,
        'order'         : [[ 0, "desc" ]]
    })

    $('body').on('click', '.table-link tbody tr', function() {
        if (typeof $(this).data('url') != 'undefined')
            location.href = $(this).data('url');
    });
});