$(document).ready(function () {
    $('#Jobs').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,
        'pageLength': 15,
        'order': [[0, "desc"]]
    })

    $('body').on('click', '.table-link tbody tr', function () {
        if (typeof $(this).data('url') != 'undefined')
            location.href = $(this).data('url');
    });

    /* Job detail */

    $(document).on('click', '.read_more', function () {
        let id = $(this).data('id');

        $(this).remove();
        $('.summary_txt[data-id="' + id + '"]').remove();
        $('.full_txt[data-id="' + id + '"]').show();

        return false;
    });

    $('.add_responsibilities').on('click', function () {
        let copy_row = $('.copy_row').clone();
        $(copy_row).removeClass('copy_row');
        $(copy_row).val('');
        $(this).closest('.responsibilities').append(copy_row);
        return false;
    });

    $('#Requests').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false,
        'pageLength': 10,
        'order': [[0, "desc"]]
    })

    $('#SaveBasicInfo').on('submit', function () {
        $.ajax({
            type: 'POST',
            url: '/admin/job/basic_info',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '#DeleteJob', function () {
        let $button = $(this);

        swal({
                title: "Delete job?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Block",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    type: 'POST',
                    url: '/admin/job/delete',
                    data: {
                        _token: $('input[name="_token"]').val(),
                        job_id: $('input[name="job_id"]').val()
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            swal("Success", "Job deleted!", "success");
                            setTimeout(function () {
                                window.location.href = '/admin/jobs';
                            }, 1500);
                        } else {
                            swal('Error', response.message, 'error');
                        }
                    }
                });
            });
    });

    let upload_file = false;
    let cropp_file = false;

    $('#CancelCropp').on('click', function () {

        $('#UploadModal').hide();
        $('#UploadModal').removeClass('in');
        $('#UploadModal img.cropper-box').attr('src', '');

    });

    $('#ApplyCropp').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/cropp',
            data: {
                filename: cropp_file,
                _token: $('input[name="_token"]').val(),
                data: $('#UploadModal img.cropper-box').data('cropper').getData()
            },
            beforeSend: function () {
                $('#UploadModal').hide();
                $('#UploadModal img.cropper-box').attr('src', '');
            },
            success: function (response) {
                if (response.status == "success") {
                    $.ajax({
                        type: 'POST',
                        url: '/admin/job/image',
                        data: {
                            image: response.filename,
                            _token: $('input[name="_token"]').val(),
                            job_id: $('input[name="job_id"]').val()
                        },
                        success: function (response_avatar) {
                            if (response_avatar.status == "success") {
                                $('.job_file video').remove();
                                $('.job_file img').remove();
                                $('.job_file').append('<img src="" style="width: 100%;" class="avatar_job_page">');

                                $('img.avatar_job_page').attr('src', response.filename);
                            } else {
                                swal('Error', response_avatar.message, 'error');
                            }
                        }
                    });
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

    });

    $('#UploadClick').on('click', function () {
        $('#Upload').click();
    });

    $('#Upload').change(function (e) {
        if (upload_file == true) {
            swal('Error', 'Wait for the file to load', 'error');
            return false;
        }

        var files = e.target.files;

        var fData = new FormData;
        fData.append('dirname', 'avatars');
        fData.append('file', $(this).prop('files')[0]);
        fData.append('_token', $('input[name="_token"]').val());

        $.ajax({
            url: '/upload',
            data: fData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                upload_file = true;
            },
            success: function (data) {
                if (data.status == 'success' && data.uploads.length != 0) {

                    if (data.type == 'image') {
                        cropp_file = data.uploads;

                        $('#UploadModal').show();
                        $('#UploadModal').addClass('in');

                        $('#UploadModal img.cropper-box').attr('src', URL.createObjectURL(files[0]));

                        $('#UploadModal img.cropper-box').cropper({
                            aspectRatio: 1 / 1,
                            zoomable: false,
                            viewMode: 1,
                            minCropBoxWidth: 200,
                            minCropBoxHeight: 200
                        });
                    } else if (data.type == 'video') {
                        $.ajax({
                            type: 'POST',
                            url: '/admin/job/video',
                            data: {
                                video: data.uploads,
                                _token: $('input[name="_token"]').val(),
                                job_id: $('input[name="job_id"]').val()
                            },
                            success: function (response_avatar) {
                                if (response_avatar.status == "success") {
                                    $('.job_file video').remove();
                                    $('.job_file img').remove();

                                    $('.job_file').append('<video width="100%" height="210px" controls> <source src="' + data.uploads + '"> Your browser does not support the video tag. </video>');
                                } else {
                                    swal('Error', response_avatar.message, 'error');
                                }
                            }
                        });
                    }

                    $('#Upload').val('');
                } else {
                    swal('Error', data.message, 'error');
                }
            },
            complete: function () {
                upload_file = false;
            }
        });
    });

});