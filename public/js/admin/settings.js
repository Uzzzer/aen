$(document).ready(function() {
    $('#ChangePassword').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/admin/change_password',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

    $('#UpdateGeneralSettings').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/admin/update_general_settings',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

    $('select[name="currency"]').on('change', function() {
        $('input[name="currency_iso"]').val($(this).find('option:checked').data('iso'));
    });

});