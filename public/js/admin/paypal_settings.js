$(document).ready(function() {
    $('#PaypalSettings').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/admin/option/update',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

});