$(document).ready(function() {
    $('#Photos').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 15,
        'order'         : [[ 0, "desc" ]]
    })

    $('body').on('click', '.table-link tbody tr', function() {
        if (typeof $(this).data('url') != 'undefined')
            location.href = $(this).data('url');
    });

    /* Photo detail */

    $('#Likes').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 10,
        'order'         : [[ 0, "desc" ]]
    })

    $('#SaveBasicInfo').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/admin/photo/basic_info',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

    $('body').on('click', '#DeletePhoto', function() {
        let $button = $(this);

        swal({
            title: "Delete photo?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/photo/delete',
                data: {
                    _token: $('input[name="_token"]').val(),
                    photo_id: $('input[name="photo_id"]').val()
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "Photo deleted!", "success");
                        setTimeout(function() {
                            window.location.href = '/admin/photos';
                        }, 1500);
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });

    $('body').on('click', '#DeleteComment', function() {
        let $button = $(this);
        let $row = $(this).closest('tr');

        swal({
            title: "Delete comment?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/photo/delete_comment',
                data: {
                    _token: $('input[name="_token"]').val(),
                    comment_id: $button.data('id')
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "Comment deleted!", "success");

                        if ($row.hasClass('next_tr'))
                            $row.next('tr').remove();

                        $row.remove();
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });

});