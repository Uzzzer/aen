$(document).ready(function() {
    $('#UserCategories').DataTable({
        'paging'        : false,
        'lengthChange'  : false,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'order'         : [[ 0, "desc" ]]
    })

    $('body').on('click', '.table-link tbody tr', function() {
        if (typeof $(this).data('url') != 'undefined')
            location.href = $(this).data('url');
    });

    /* User Category detail */

    $('#UpdateUserCategory').on('submit', function() {
        $.ajax({
            type: 'POST',
            url: '/admin/user-category/update',
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == "success") {
                    swal('Success', '', 'success');
                } else {
                    swal('Error', response.message, 'error');
                }
            }
        });

        return false;
    });

    $('input[name="trial"]').on('change', function() {
        if ($(this).is(':checked')) {
            $('input[name="prices[trial]"]').css('display', 'block');
            $('input[name="prices[trial]"]').attr('required', 'required');
        } else {
            $('input[name="prices[trial]"]').css('display', 'none');
            $('input[name="prices[trial]"]').removeAttr('required');
        }
    });
});