$(document).ready(function() {
    let options = {
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : true,
        'ordering'      : true,
        'info'          : true,
        'autoWidth'     : false,
        'pageLength'    : 15,
        'order'         : [[ 0, "desc" ]]
    };

    $('#Chats').DataTable(options);

    $('body').on('click', '.table-link tbody tr', function() {
        if (typeof $(this).data('url') != 'undefined')
            location.href = $(this).data('url');
    });

    /* Chat detail */

    $('#chat-box').slimScroll({
        height: '500px',
        start: 'bottom'
    });

    $('body').on('click', '#DeleteChat', function() {
        let $button = $(this);

        swal({
            title: "Delete chat?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/chat/delete',
                data: {
                    _token: $('input[name="_token"]').val(),
                    chat_id: $button.data('id')
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "Chat deleted!", "success");
                        setTimeout(function() {
                            window.location.href = '/admin/chats';
                        }, 1500);
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });

    $('body').on('click', '.RemoveMessage', function() {
        let $button = $(this);

        swal({
            title: "Delete message?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '/admin/chat/delete_message',
                data: {
                    _token: $('input[name="_token"]').val(),
                    message_id: $button.data('id')
                },
                success: function (response) {
                    if (response.status == "success") {
                        swal("Success", "Message deleted!", "success");
                        $button.closest('.item').remove();
                    } else {
                        swal('Error', response.message, 'error');
                    }
                }
            });
        });
    });
});