var conversation_id_page = false;

$(document).ready(function() {
    let load_dialog = false;
    let load_dialogs = false;
    let loading_ajax = false;
    let search_text = '';
    let page = 1;
    let no_msg = 0;

    $('.chat_page_block #chat-window .dialog-body').slimScroll({
        width: '417px',
        height: '400px',
        start: 'bottom',
    });

    $('.open_chat_page').on('click', function() {
        if ($(this).hasClass('no_msg'))
            no_msg = 1;
        else
            no_msg = 0;

        $('.chat_page_block .chat-back').click();

        if (typeof $(this).data('conversation_id') != 'undefined' && conversation_id_page != 0 && conversation_id_page != false && conversation_id_page == parseInt($(this).data('conversation_id')))
            return false;

        let id = $(this).data('id');
        let name = $(this).data('name');
        conversation_id_page = $(this).data('conversation_id');

        $.ajax({
            type: 'POST',
            url: '/chat/get_dialogs',
            data: {
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function() {
                load_dialogs = true;
            },
            success: function(data) {
                if (data.status = 'success') {
                    $(".chat_page_block #SearchBox").val('');
                    $('.chat_page_block #SearchBox').addClass('placeholder-shown')
                    search_text = '';

                    $('.chat_page_block #chat-list .dialog-body').html('');

                    if (data.unreadCount > 0)
                        $('.chat_page_block #chat-list .dialog-header-title span').html('<b class="red">'+data.unreadCount+'</b> New Messages');
                    else
                        $('.chat_page_block #chat-list .dialog-header-title span').html('');

                    jQuery.each(data.dialogs, function(i, val) {
                        $('.chat_page_block #chat-list .dialog-body').append(val.html);
                    });

                    load_dialogs = false;

                    if (typeof id != 'undefined') {
                        $('.chat_page_block #MessageBox').data('id', id);
                        $('.chat_page_block #Call').data('id', id);
                        $('.chat_page_block #VideoCall').data('id', id);

                        if (typeof conversation_id_page != 'undefined' && $('.chat_page_block .dialog-message-box[data-id="'+conversation_id_page+'"]').length > 0) {
                            $('.chat_page_block .dialog-message-box[data-id="'+conversation_id_page+'"]').click();
                        } else {
                            page = 1;
                            loading_ajax = false;
                            $('.chat_page_block #chat-window .dialog-body').html('');
                            $('.chat_page_block #chat-window .chat-person p').text(name);
                            $('.chat_page_block #dialog').addClass('chat-opened');
                            $('.chat_page_block .dialog').addClass('active');
                        }
                    } else
                        $('.chat_page_block .dialog').addClass('active');
                }
            }
        });

        return false;
    });

    function is_visible(element) {
        var documentHeight = $('.chat_page_block #chat-window .dialog-body').height();
        var distanceFromBottom = documentHeight - (element.position().top + element.outerHeight(true)) + 500;

        return (distanceFromBottom < 0) ? false : true;
    }

    setInterval(function() {
        if (typeof conversation_id != 'undefined' && conversation_id != 0 && conversation_id != false) {
            if (load_dialog == false)
                $.ajax({
                    type: 'POST',
                    url: '/chat/get_dialog',
                    data: {
                        conversation_id: conversation_id,
                        last_hour: 1,
                        _token: $('input[name="_token"]').val(),
                        page: 1,
                        no_msg: no_msg
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $('.chat_page_block #chat-window .chat-person span').text(data.last_activity);

                            jQuery.each(data.messages, function(i, val) {
                                if ($('.chat_page_block #chat-window .dialog-body .box-msg[data-id="'+i+'"]').length == 0 && load_dialog == false) {
                                    var $new = $(val).hide();
                                    $('.chat_page_block #chat-window .dialog-body').append($new);
                                    $new.show('fast');

                                    var audio = new Audio();
                                    audio.src = '/assets/notification.mp3';
                                    audio.autoplay = true;

                                    if (is_visible($new))
                                        $('.chat_page_block #chat-window .dialog-body').slimScroll({scrollTo: $('.chat_page_block #chat-window .dialog-body')[0].scrollHeight + 30});
                                }
                            });
                        }
                    }
                });
       } else if ($('.chat_page_block .dialog').hasClass('active') == true && $('.chat_page_block .dialog').hasClass('chat-opened') != true) {
            if (load_dialogs == false)
                $.ajax({
                    type: 'POST',
                    url: '/chat/get_dialogs',
                    data: {
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status = 'success') {
                            jQuery.each(data.dialogs, function(i, val) {

                                if ($('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').length == 0) {
                                    if (search_text.length > 0) {
                                        $box = $(val.html);
                                        if ($box.data('name').toLowerCase().indexOf(search_text.toLowerCase()) == -1)
                                            $box.hide();

                                        $('.chat_page_block #chat-list .dialog-body').prepend($box);
                                    } else
                                        $('.chat_page_block #chat-list .dialog-body').prepend(val.html);

                                    var audio = new Audio();
                                    audio.src = '/assets/notification.mp3';
                                    audio.autoplay = true;
                                } else {
                                    let $block = $(val.html);

                                    if ($block.hasClass('readed') == false && $('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').hasClass('readed') == true) {
                                        $('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').prependTo('.chat_page_block #chat-list .dialog-body');

                                        var audio = new Audio();
                                        audio.src = '/assets/notification.mp3';
                                        audio.autoplay = true;
                                    } else if ($('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"] p').text() != $block.find('p').text()) {
                                        $('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').prependTo('.chat_page_block #chat-list .dialog-body');

                                        var audio = new Audio();
                                        audio.src = '/assets/notification.mp3';
                                        audio.autoplay = true;
                                    }

                                    if ($block.hasClass('readed'))
                                        $('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').addClass('readed');
                                    else
                                        $('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"]').removeClass('readed');

                                    $('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"] p').text($block.find('p').text());
                                    $('.chat_page_block #chat-list .dialog-body .dialog-message-box[data-id="'+val.id+'"] .message-date').text($block.find('.message-date').text());
                                }

                            });

                            if (data.unreadCount > 0)
                                $('.chat_page_block #chat-list .dialog-header-title span').html('<b class="red">'+data.unreadCount+'</b> New Messages');
                            else
                                $('.chat_page_block #chat-list .dialog-header-title span').html('');
                        }
                    }
                });
       }
    }, 2000);

    $('.plagin_block').on('click', '.dialog-message-box', function() {
        conversation_id_page = parseInt($(this).data('id'));
        let id = $(this).data('id');
        let user_id = $(this).data('user_id');
        page = 1;
        loading_ajax = false;

        $('.chat_page_block #chat-window .dialog-body').html('');
        $('.chat_page_block #chat-window .chat-person p').text($(this).data('name'));

        $.ajax({
            type: 'POST',
            url: '/chat/get_dialog',
            data: {
                conversation_id: conversation_id_page,
                _token: $('input[name="_token"]').val(),
                page: page,
                no_msg: no_msg
            },
            beforeSend: function() {
                load_dialog = true;
            },
            success: function(data) {
                if (data.status == 'success') {
                    $(".chat_page_block #SearchBox").val('');
                    $('.chat_page_block #SearchBox').addClass('placeholder-shown');
                    search_text = '';

                    $('.chat_page_block #MessageBox').data('id', id);
                    $('.chat_page_block #Call').data('id', user_id);
                    $('.chat_page_block #VideoCall').data('id', user_id);

                    $('.chat_page_block #chat-window .chat-person span').text(data.last_activity);

                    jQuery.each(data.messages, function(i, val) {
                        if ($('.chat_page_block #chat-window .dialog-body .box-msg[data-id="'+i+'"]').length == 0) {
                            $('.chat_page_block #chat-window .dialog-body').append(val);
                        }
                    });

                    load_dialog = false;

                    $('.chat_page_block #chat-window .dialog-body').slimScroll({scrollTo: $('.chat_page_block #chat-window .dialog-body')[0].scrollHeight});

                    if ($('.chat_page_block .dialog').hasClass('active') == false)
                        $('.chat_page_block .dialog').addClass('active');

                    if (no_msg == 1)
                        $('.chat_page_block .dialog-footer').hide();
                    else
                        $('.chat_page_block .dialog-footer').show();

                    $('.chat_page_block #dialog').addClass('chat-opened');
                }
            }
        });
    });

    $('.chat_page_block .chat-back').click(function() {
        conversation_id_page = false;

        $.ajax({
            type: 'POST',
            url: '/chat/get_dialogs',
            data: {
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function() {
                load_dialogs = true;
            },
            success: function(data) {
                if (data.status = 'success') {
                    $('.chat_page_block #chat-list .dialog-body').html('');

                    if (data.unreadCount > 0)
                        $('.chat_page_block #chat-list .dialog-header-title span').html('<b class="red">'+data.unreadCount+'</b> New Messages');
                    else
                        $('.chat_page_block #chat-list .dialog-header-title span').html('');

                    jQuery.each(data.dialogs, function(i, val) {
                        $('.chat_page_block #chat-list .dialog-body').append(val.html);
                    });

                    $('.chat_page_block #dialog').removeClass('chat-opened');

                    load_dialogs = false;
                }
            }
        });

        return false;
    });

    $(".chat_page_block #MessageBox").on('keyup', function(event) {
        if(event.keyCode == 13) {
            SendMessagePage();
            return false;
        }
    });

    $(".chat_page_block #MessageSend").on('click', function(event) {
        SendMessagePage();
    });

    function SendMessagePage() {
        let input = $('.chat_page_block #MessageBox');

        if (typeof conversation_id_page == 'undefined')
            conversation_id_page = 0;

        $.ajax({
            type: 'POST',
            url: '/chat/send_message',
            data: {
                message: input.val(),
                conversation_id: conversation_id_page,
                user_id: $('.chat_page_block #MessageBox').data('id'),
                _token: $('input[name="_token"]').val()
            },
            beforeSend: function() {
                input.val('').trigger('autosize.resize');
            },
            success: function(data) {

                if (data.status == 'success') {
                    conversation_id_page = data.conversation_id;

                    if ($('.OpenChat[data-id="'+data.user_id+'"]').length > 0)
                        $('.OpenChat[data-id="'+data.user_id+'"]').data('conversation_id', conversation_id_page);

                    var $new = $(data.html).hide();
                    $('.chat_page_block #chat-window .dialog-body').append($new);
                    $new.show('fast');

                    $('.chat_page_block #chat-window .dialog-body').slimScroll({scrollTo: $('.chat_page_block #chat-window .dialog-body')[0].scrollHeight + 30});

                }

            }
        });

    }

    $(".chat_page_block #SearchBox").on('keyup', function(event) {
        search_text = $(this).val();

        if (search_text.length == 0)
            $('.chat_page_block #chat-list .dialog-body .dialog-message-box').show('fast');
        else
            $('.chat_page_block #chat-list .dialog-body .dialog-message-box').each(function(i,elem) {
                if ($(elem).data('name').toLowerCase().indexOf(search_text.toLowerCase()) == -1)
                    $(elem).hide('fast');
                else
                    $(elem).show('fast');
            });
    });

    $('.chat_page_block #chat-window .dialog-body').scroll(function(){

        if ($('.chat_page_block #chat-window .dialog-body').scrollTop() < 300) {
            if (loading_ajax == false) {
                page++;
                $.ajax({
                    type: 'POST',
                    url: '/chat/get_dialog',
                    data: {
                        conversation_id: conversation_id_page,
                        _token: $('input[name="_token"]').val(),
                        page: page,
                        reverse: 1,
                        no_msg: no_msg
                    },
                    beforeSend: function() {
                        loading_ajax = true;
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            let last_key = false;
                            jQuery.each(data.messages, function(i, val) {
                                if ($('.chat_page_block #chat-window .dialog-body .box-msg[data-id="'+i+'"]').length == 0) {
                                    if (last_key == false)
                                        $('.chat_page_block #chat-window .dialog-body').prepend(val);
                                    else
                                        $('.chat_page_block #chat-window .dialog-body .box-msg[data-id="'+last_key+'"]').after(val);

                                    last_key = i;
                                }
                            });

                            $('.chat_page_block #chat-window .dialog-body').slimScroll({scrollTo: $('#chat-window .dialog-body').scrollTop()});

                            loading_ajax = false;
                        }
                    }
                });
            }
        }
    });

    //PLACEHOLDER-SHOWN POLYFILL
    function placeholderPolyfill() {
        this.classList[this.value ? 'remove' : 'add']('placeholder-shown');
    }

    document.querySelectorAll('[placeholder]').forEach(el => {
        el.addEventListener('change', placeholderPolyfill);
        el.addEventListener('keyup', placeholderPolyfill);
    });
});