$(document).ready(function() {
  var calendar = $('#calendar').fullCalendar({
        header: {
            left: '',
            center: '',
            right: ''
        },
		editable: false,
		navLinks: true,
		eventLimit: true,
		events: events
    })
  var moment = $('#calendar').fullCalendar('getDate');
  $('#selected-month').text(moment.format("MMMM YYYY"));
  $('#calendar-prev-button').click(function() {
    $('#calendar').fullCalendar('prev');
    var moment = $('#calendar').fullCalendar('getDate');
    $('#selected-month').text(moment.format("MMMM YYYY"));
  });
  $('#calendar-next-button').click(function() {
    $('#calendar').fullCalendar('next');
    var moment = $('#calendar').fullCalendar('getDate');
    $('#selected-month').text(moment.format("MMMM YYYY"));
  });
  /*$('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month'
    },
    editable: false,
    navLinks: true,
    eventLimit: true,
    events: events
  })*/
  /*$(document).on('click','#my-next-button',function() {
    console.log('asdasdasdads');
    calendar.next();
  });*/
  /*$('#my-next-button').on('click',function() {
    console.log('asdasdasdasd');
    calendar.next();
  });*/
});