var connect_user = false;
var ready = false;
var call = false;
var incoming_call = false;
var incoming_call_id = false;
var incoming_call_user_id = false;
var incoming_call_user_name = false;
var interval;
var v_interval;
var interlocutor_name = '';
var audio;
var video = false;
var timeout;
var microphone = false;
var webcamera = false;
var localstream;

jQuery(function($) {
    $(window).on('beforeunload', function() {
        if (call == true || connect_user == true)
            return "It is forbidden to refresh the page.";
    });

    var v_intervalCheck = setInterval(function() {
        if (incoming_call == false && connect_user == false) {
            $.ajax({
                type: 'POST',
                url: '/video/check_call',
                data: {
                    _token: $('input[name="_token"]').val()
                },
                success: function(data) {
                    if (data.status == 'success') {

                        if (microphone != true || webcamera != true) {
                            checkAccessMedia(true, true);
                        }

                        video = true;

                        incoming_call_user_id = data.user_id;
                        incoming_call_user_name = data.user_name;
                        incoming_call_id = data.call_id;
                        incoming_call = true;

                        audio = new Audio();
                        audio.src = '/assets/call.mp3';
                        audio.autoplay = true;
                        audio.loop = true;
                        audio.play();

                        swal({
                            title: 'Incoming call from '+incoming_call_user_name,
                            animation: false,
                            customClass: 'animated tada',
                            showCloseButton: true,
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonText: 'Take the call',
                            cancelButtonText: 'Cancel',
                            showCloseButton: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                                if (microphone != true || webcamera != true)
                                    swal.disableConfirmButton();
                            }
                        }).then((result) => {
                            audio.pause();

                            if (typeof result.dismiss != 'undefined') {
                                if (result.dismiss == "cancel") {
                                    incoming_call = false;

                                    $.ajax({
                                        type: 'POST',
                                        url: '/video/cancel_call',
                                        data: {
                                            call_id: incoming_call_id,
                                            _token: $('input[name="_token"]').val()
                                        },
                                        complete: function() {
                                            incoming_call = false;
                                            incoming_call_user_id = false;
                                            incoming_call_user_name = false;
                                            incoming_call_id = false;
                                        }
                                    });
                                }
                            } else if (typeof result.value != 'undefined') {
                                if (result.value == true) {
                                    $.ajax({
                                        type: 'POST',
                                        url: '/video/take_call',
                                        data: {
                                            call_id: incoming_call_id,
                                            _token: $('input[name="_token"]').val()
                                        },
                                        success: function(data) {
                                            if (data.status == 'success') {
                                                interlocutor_name = incoming_call_user_name;
                                                Make(incoming_call_user_id);
                                            } else {
                                                incoming_call = false;
                                                incoming_call_user_id = false;
                                                incoming_call_user_name = false;
                                                incoming_call_id = false;

                                                swal('Error', 'Call was interrupted', 'error');
                                            }
                                        }
                                    });
                                }
                            }
                        })
                    }
                }
            });
        } else if (incoming_call != false && connect_user == false) {
            if (video == true)
                $.ajax({
                    type: 'POST',
                    url: '/video/check_incoming',
                    data: {
                        call_id: incoming_call_id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'error') {
                            if (typeof audio != 'undefined')
                                audio.pause();

                            swal.close();

                            incoming_call = false;
                            incoming_call_user_id = false;
                            incoming_call_user_name = false;
                            incoming_call_id = false;
                        }
                    }
                });
        }
    }, 2000);

    var intervalCheck = setInterval(function() {
        if (incoming_call == false && connect_user == false) {
            $.ajax({
                type: 'POST',
                url: '/audio/check_call',
                data: {
                    _token: $('input[name="_token"]').val()
                },
                success: function(data) {
                    if (data.status == 'success') {

                        if (microphone != true) {
                            checkAccessMedia(true, false);
                        }

                        video = false;

                        incoming_call_user_id = data.user_id;
                        incoming_call_user_name = data.user_name;
                        incoming_call_id = data.call_id;
                        incoming_call = true;

                        audio = new Audio();
                        audio.src = '/assets/call.mp3';
                        audio.autoplay = true;
                        audio.loop = true;
                        audio.play();

                        swal({
                            title: 'Incoming call from '+incoming_call_user_name,
                            animation: false,
                            customClass: 'animated tada',
                            showCloseButton: true,
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonText: 'Take the call',
                            cancelButtonText: 'Cancel',
                            showCloseButton: false,
                            allowOutsideClick: false,
                            onOpen: () => {
                                if (microphone != true)
                                    swal.disableConfirmButton();
                            }
                        }).then((result) => {
                            audio.pause();

                            if (typeof result.dismiss != 'undefined') {
                                if (result.dismiss == "cancel") {
                                    incoming_call = false;

                                    $.ajax({
                                        type: 'POST',
                                        url: '/audio/cancel_call',
                                        data: {
                                            call_id: incoming_call_id,
                                            _token: $('input[name="_token"]').val()
                                        },
                                        complete: function() {
                                            incoming_call = false;
                                            incoming_call_user_id = false;
                                            incoming_call_user_name = false;
                                            incoming_call_id = false;
                                        }
                                    });
                                }
                            } else if (typeof result.value != 'undefined') {
                                if (result.value == true) {
                                    $.ajax({
                                        type: 'POST',
                                        url: '/audio/take_call',
                                        data: {
                                            call_id: incoming_call_id,
                                            _token: $('input[name="_token"]').val()
                                        },
                                        success: function(data) {
                                            if (data.status == 'success') {
                                                interlocutor_name = incoming_call_user_name;
                                                Make(incoming_call_user_id);
                                            } else {
                                                incoming_call = false;
                                                incoming_call_user_id = false;
                                                incoming_call_user_name = false;
                                                incoming_call_id = false;

                                                swal('Error', 'Call was interrupted', 'error');
                                            }
                                        }
                                    });
                                }
                            }
                        })
                    }
                }
            });
        } else if (incoming_call != false && connect_user == false) {
            if (video == false)
                $.ajax({
                    type: 'POST',
                    url: '/audio/check_incoming',
                    data: {
                        call_id: incoming_call_id,
                        _token: $('input[name="_token"]').val()
                    },
                    success: function(data) {
                        if (data.status == 'error') {
                            if (typeof audio != 'undefined')
                                audio.pause();

                            swal.close();

                            incoming_call = false;
                            incoming_call_user_id = false;
                            incoming_call_user_name = false;
                            incoming_call_id = false;
                        }
                    }
                });
        }
    }, 2000);

    $('#Call').on('click', function() {

        if (microphone != true) {
            checkAccessMedia(true, false, $(this));
            return false;
        }

        let call_btn = $(this);

        video = false;

        $.ajax({
            type: 'POST',
            url: '/audio/call',
            data: {
                user_id: $(this).data('id'),
                _token: $('input[name="_token"]').val()
            },
            success: function(data) {

                if (data.status == 'success') {
                    let call_id = data.call_id;
                    interlocutor_name = data.user_name;

                    if ($(call_btn).hasClass('page_chat_call'))
                        conversation_id_page = data.conversation_id;
                    else
                        conversation_id = data.conversation_id;

                    if ($('.OpenChat[data-id="'+data.user_id+'"]').length > 0) {
                        if ($(call_btn).hasClass('page_chat_call'))
                            $('.OpenChat[data-id="'+data.user_id+'"]').data('conversation_id', conversation_id_page);
                        else
                            $('.OpenChat[data-id="'+data.user_id+'"]').data('conversation_id', conversation_id);
                    }

                    var $new = $(data.html).hide();

                    if ($(call_btn).hasClass('page_chat_call'))
                        $('.plagin_block #chat-window .dialog-body').append($new);
                    else
                        $('.window-chat #chat-window .dialog-body').append($new);

                    $new.show('fast');

                    if ($(call_btn).hasClass('page_chat_call'))
                        $('.plagin_block #chat-window .dialog-body').slimScroll({scrollTo: $('.plagin_block #chat-window .dialog-body')[0].scrollHeight + 30});
                    else
                        $('.window-chat #chat-window .dialog-body').slimScroll({scrollTo: $('.window-chat #chat-window .dialog-body')[0].scrollHeight + 30});

                    if (data.call_status == 'call') {
                        call = true;

                        interval = setInterval(function() {
                            $.ajax({
                                type: 'POST',
                                url: '/audio/update_active_call',
                                data: {
                                    call_id: call_id,
                                    _token: $('input[name="_token"]').val()
                                },
                                success: function(data) {
                                    if (data.status == 'error') {
                                        swal('Error', 'Please call again', 'error');
                                        clearInterval(interval);
                                    } else if (data.status == 'cancel') {
                                        swal({
                                          title: 'User canceled the call'
                                        })
                                        clearInterval(interval);
                                    } if (data.status == 'speaking') {
                                        incoming_call_id = call_id;
                                        clearInterval(interval);
                                    }
                                }
                            });
                        }, 2000);

                        swal({
                            title: 'Call',
                            html: '',
                            onOpen: () => {
                                swal.showLoading();
                                swal.enableButtons();
                            },
                            cancelButtonText: 'Cancel',
                            showCancelButton: true,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        }).then((result) => {
                            if (typeof result.dismiss != 'undefined') {
                                if (result.dismiss == "cancel") {
                                    clearInterval(interval);

                                    $.ajax({
                                        type: 'POST',
                                        url: '/audio/cancel_call',
                                        data: {
                                            call_id: call_id,
                                            _token: $('input[name="_token"]').val()
                                        },
                                        complete: function() {
                                            call = false;
                                        }
                                    });
                                }
                            }
                        })
                    }

                } else
                    swal('Error', data.message, 'error');

            }
        });
    });

    $('#VideoCall').on('click', function() {

        if (microphone != true || webcamera != true) {
            checkAccessMedia(true, true, $(this));
            return false;
        }

        let call_btn = $(this);

        video = true;

        $.ajax({
            type: 'POST',
            url: '/video/call',
            data: {
                user_id: $(this).data('id'),
                _token: $('input[name="_token"]').val()
            },
            success: function(data) {

                if (data.status == 'success') {
                    let call_id = data.call_id;
                    interlocutor_name = data.user_name;

                    if ($(call_btn).hasClass('page_chat_call'))
                        conversation_id_page = data.conversation_id;
                    else
                        conversation_id = data.conversation_id;


                    if ($('.OpenChat[data-id="'+data.user_id+'"]').length > 0) {
                        if ($(call_btn).hasClass('page_chat_call'))
                            $('.OpenChat[data-id="'+data.user_id+'"]').data('conversation_id', conversation_id_page);
                        else
                            $('.OpenChat[data-id="'+data.user_id+'"]').data('conversation_id', conversation_id);
                    }


                    var $new = $(data.html).hide();

                    if ($(call_btn).hasClass('page_chat_call'))
                        $('.plagin_block #chat-window .dialog-body').append($new);
                    else
                        $('.window-chat #chat-window .dialog-body').append($new);

                    $new.show('fast');

                    if ($(call_btn).hasClass('page_chat_call'))
                        $('.plagin_block #chat-window .dialog-body').slimScroll({scrollTo: $('.plagin_block #chat-window .dialog-body')[0].scrollHeight + 30});
                    else
                        $('.window-chat #chat-window .dialog-body').slimScroll({scrollTo: $('.window-chat #chat-window .dialog-body')[0].scrollHeight + 30});

                    if (data.call_status == 'call') {
                        call = true;

                        v_interval = setInterval(function() {
                            $.ajax({
                                type: 'POST',
                                url: '/video/update_active_call',
                                data: {
                                    call_id: call_id,
                                    _token: $('input[name="_token"]').val()
                                },
                                success: function(data) {
                                    if (data.status == 'error') {
                                        swal('Error', 'Please call again', 'error');
                                        clearInterval(v_interval);
                                    } else if (data.status == 'cancel') {
                                        swal({
                                          title: 'User canceled the call'
                                        })
                                        clearInterval(v_interval);
                                    } if (data.status == 'speaking') {
                                        incoming_call_id = call_id;
                                        clearInterval(v_interval);
                                    }
                                }
                            });
                        }, 2000);

                        swal({
                            title: 'Video Call',
                            html: '',
                            onOpen: () => {
                                swal.showLoading();
                                swal.enableButtons();
                            },
                            cancelButtonText: 'Cancel',
                            showCancelButton: true,
                            showConfirmButton: false,
                            allowOutsideClick: false
                        }).then((result) => {
                            if (typeof result.dismiss != 'undefined') {
                                if (result.dismiss == "cancel") {
                                    clearInterval(v_interval);

                                    $.ajax({
                                        type: 'POST',
                                        url: '/video/cancel_call',
                                        data: {
                                            call_id: call_id,
                                            _token: $('input[name="_token"]').val()
                                        },
                                        complete: function() {
                                            call = false;
                                        }
                                    });
                                }
                            }
                        })
                    }

                } else
                    swal('Error', data.message, 'error');

            }
        });
    });

});

function loginChat(UserID, selector) {
    var phone = window.phone = PHONE({
        number        : UserID,
        publish_key   : 'pub-c-f9f84ff9-1ef9-4014-8837-e2e995455470',
        subscribe_key : 'sub-c-5253d1f8-4444-11e7-86e2-02ee2ddab7fe',
        ssl : true,
        media         : { audio : microphone, video : webcamera },
    });


    var ctrl = window.ctrl = CONTROLLER(phone);

    console.log(phone);

    ctrl.ready(function() {
        console.log('Ready');
        ready = true;

        if (selector != false)
            $(selector).click();
    });

    ctrl.receive(function(session) {
        session.connected(function(session) {
            console.log('connected');
            connect_user = true;
            clearTimeout(timeout);

            let html = '';

            if (video != true)
                html = '<div id="swal2-timer">00:00</div>';
            else
                html = '<div id="vid-box"></div>';

            swal({
                title: interlocutor_name,
                html: html,
                cancelButtonText: 'End call',
                showCancelButton: true,
                showConfirmButton: false,
                allowOutsideClick: false,
                onOpen: () => {
                    console.log(video);
                    if (video == true) {
                        $("#vid-box").html(session.video);
                    } else {
                        timerCall('#swal2-timer');
                    }
                },
            }).then((result) => {
                if (typeof result.dismiss != 'undefined') {
                    if (result.dismiss == "cancel") {
                        $.ajax({
                            type: 'POST',
                            url: '/audio/cancel_call',
                            data: {
                                call_id: incoming_call_id,
                                _token: $('input[name="_token"]').val()
                            },
                            complete: function() {
                                end();
                            }
                        });
                    }
                }
            })
        });
        session.ended(function(session) {
            console.log('disconnected');
            connect_user = false;
            call = false;
            incoming_call = false;
            incoming_call_id = false;
            incoming_call_user_id = false;
            incoming_call_user_name = false;
            interval;
            interlocutor_name = '';
            clearInterval(timerCallInterval);
            timer = 0;

            swal({
                title: 'Call completed'
            })
        });
    });

    ctrl.audioToggled(function(session, isEnabled){
        ctrl.getVideoElement(session.number).css("opacity",isEnabled ? 1 : 0.75); // 0.75 opacity is audio muted
    });
}

function mute() {
    ctrl.toggleAudio();
}

function end() {
    console.log("End chat");
    phone.hangup();
}

function Make(UserID) {
    if (!window.phone)
        alert("Your browser is not supported");
    else {
        if (connect_user === false) {
            console.log("Call to "+UserID);
            phone.dial(UserID);
            timeout = setTimeout(function() {
                swal('Error', 'Call was interrupted', 'error');
                end();
            }, 5000);
        }
    }
}

var timer = 0;
var timerCallInterval = false;
function timerCall(selector) {
    timerCallInterval = setInterval(function() {
        timer++;

        let hour = '';
        let min = '00';
        let sec = '00';

        if (timer >= 60*60) {
            hour = Math.floor(timer/(60*60));
            if (hour < 10)
                hour = '0'+hour;
            hour = hour + ':';

            timer_tmp = timer - Math.floor(timer/(60*60)) * 60 * 60;

            if (timer_tmp >= 60) {
                min = Math.floor(timer_tmp/60);
                if (min < 10)
                    min = '0'+min;

                sec = timer_tmp - min*60;
            } else
                sec = timer_tmp;

        } else if (timer >= 60) {
            min = Math.floor(timer/60);
            if (min < 10)
                min = '0'+min;

            sec = timer - min*60;
        } else
            sec = timer;

        if (sec < 10)
            sec = '0'+sec;

        $(selector).text(hour+min+':'+sec);

    }, 1000);
}

function checkAccessMedia(audio_access = false, video_access = false, selector = false) {
    navigator.mediaDevices.getUserMedia({
        audio: audio_access,
        video: video_access
    }).then(function(stream) {

        microphone = audio_access;
        webcamera = video_access;
        localstream = stream;
        swal.enableConfirmButton();

        loginChat(user_id, selector);
    }).catch(function(err) {
        console.log(err.name + ": " + err.message);
    });
}