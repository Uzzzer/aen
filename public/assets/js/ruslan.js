$('.person-checkbox').click(function() {
    $(this).toggleClass('active')
});

$(document).ready(function() { 
	$('a#modal_settings').click( function(event){ 
		event.preventDefault(); 
		$('#overlay').fadeIn(400, 
		 	function(){ 
				$('#modal_form') 
					.css('display', 'block') 
					.animate({opacity: 1, top: '3%'}, 200); 
		});
	});
	
	$('#modal_close, #overlay').click( function(){
		$('#modal_form')
			.animate({opacity: 0, top: '45%'}, 200,  
				function(){ 
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400); 
				}
			);
	});
});


const knowledgeLevels = [
  { name: "None", color: "rgb(0,0,0)" },
  { name: "Beginner", color: "rgb(23, 86, 162)" },
  { name: "Intermediate", color: "rgb(54, 200, 162)" },
  { name: "Advanced", color: "rgb(255, 205, 86)" },
  { name: "Profficient", color: "rgb(255, 99, 132)" },
  { name: "Expert", color: "rgb(160, 120, 132)" },
  { name: "God like", color: "rgb(255, 255, 255)" },
];

const languagesData = [
  { name: "C++", value: 4},
  { name: "C#", value: 3},
  { name: "Java", value: 2},
  { name: "Python", value: 2},
  { name: "JavaScript", value: 2},
  { name: "Rust", value: 1},
]

const technologiesData = [
  { name: "Unity", value: 4},
  { name: "Mono C/C++ API", value: 3},
  { name: "Node.js API", value: 2},
]

function makeData(text, datasetData)
{
  return {
    labels: datasetData.map((entry) => entry.name),
    datasets: [
      {
        label: text,
        data: datasetData.map((entry) => entry.value),
        backgroundColor: datasetData.map((entry) => knowledgeLevels[entry.value].color),
      },
    ]
  };
}

window.onload = function() {
  let ctx = document.getElementById("canvas").getContext("2d");
  
  let chart = new Chart(ctx, {
    type: "bar",
    data: makeData("Languages", languagesData),
    options : {
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) => knowledgeLevels[tooltipItem.yLabel].name,
        }
      },
      title: {
        display: true,
        text: "Programming languages knowledge.",
        fontSize: 16,
      },
      legend: {
        display: false,
      },
      scales: {
        xAxes: [
          {
            barPercentage: 0.9,
            categoryPercentage: 0.7,
          }
        ],
        yAxes: [{
          ticks: {
            min: 0,
            max: knowledgeLevels.length - 1,
            callback: (value, index, values) => knowledgeLevels[value] ? knowledgeLevels[value].name : "Undefined"
          }
        }]
      },
      animation: {
        onComplete: () => {
          let url_base64jp = document.getElementById("canvas").toDataURL("image/jpg");
          let downloadLink = document.getElementById("downloadLink");
          downloadLink.href = url_base64jp;
        }
      }
    }
  });
  window.myBar = chart;
}


