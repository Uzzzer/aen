jQuery(document).ready(function($) {
	if($('#demo-date').length > 0){
		$('#demo-date').datepicker({autoclose: true, todayHighlight: true});
	}

	// Bar chart
	if(document.getElementById("bar-chart")){
		new Chart(document.getElementById("bar-chart"), {
		    type: 'bar',
		    data: {
		      labels: ["Alex Nelson", "Mary Peterson", "Jim Peters", "Jessica Cruise", "Abam", "Andry", "Benny", "Bill", "Bob", "Bobo"],
		      datasets: [
		        {
		          label: "Average rating",
		          backgroundColor: ["#0aa89e", "#8e5ea2", "#e8c3b9","#c45850", "#2196F3", "#EB0038", "#8e5ea2", "#e8c3b9","#FFC107", "#0aa89e"],
		          data: [7.16, 5.2, 6, 7.5 , 5.5, 7.3, 4.2, 3, 6.5 , 5.2]
		        }
		      ]
		    },
		    options: {
		      legend: { display: false },
		      title: {
		        display: true,
		        text: 'Read'
		      },
			  scales: {
			    yAxes: [{
			      stacked: true,
			      gridLines: {
			        display: true,
			      }
			    }]
			   
			  }
		    }
		});
	}

	// Bar chart
	if(document.getElementById("bar-chart-punchlock")){
		new Chart(document.getElementById("bar-chart-punchlock"), {
		    type: 'bar',
		    data: {
		      labels: ["2018-03-25", "2018-03-26", "2018-03-27", "2018-03-28", "2018-03-29",  "2018-03-30"],
		      datasets: [
		        {
		          label: "Average rating",
		          backgroundColor: ["#0aa89e", "#8e5ea2", "#e8c3b9", "#c45850", "#2196F3", "#EB0038"],
		          data: [7.16, 2.2, 4, 3.5 , 6.1, 7.3]
		        }
		      ]
		    },
		    options: {
		      legend: { display: false },
		      title: {
		        display: true,
		        text: 'Read'
		      }
			  
		    }
		});
	}

	if($('#demo-date').legend > 0){
		$("#demo-date").spinner({step: 0.1, numberFormat: "n", max: 10});	
	}

   $('.delete_user').on('click', function() {
      var id = $(this).data('id');

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
         	$.ajax({
         		type: 'POST',
         		url: '/admin/delete_user',
         		data: {id: id, _token: $('input[name="_token"]').val()},
         		success: function(response){
                  if (response.status == "success") {
                      swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                      )
                      $('#datatable1 tr[data-id="'+id+'"]').remove();
                  }
         		}
         	});
        }
      })
   });

   $('.delete_horoscope').on('click', function() {
      var id = $(this).data('id');

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
         	$.ajax({
         		type: 'POST',
         		url: '/admin/delete_horoscope',
         		data: {id: id, _token: $('input[name="_token"]').val()},
         		success: function(response){
                  if (response.status == "success") {
                      swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                      )
                      $('#datatable1 tr[data-id="'+id+'"]').remove();
                  }
         		}
         	});
        }
      })
   });


});