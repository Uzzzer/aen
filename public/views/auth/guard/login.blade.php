<html lang="en" class="mdl-js">
<head>
    <title>AEN Development</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/materialadmin.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/material-design-iconic-font.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/rickshaw.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/morris.core.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/fullcalendar.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/vasya.css') }}">
{{--    <link type="text/css" rel="stylesheet" href="{{ asset('css/main.css') }}">--}}
<!-- END STYLESHEETS -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="https://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
    <script type="text/javascript" src="https://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->

    <style>

        body.login-page, body.register-page {
            background: #e5e6e6;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        body .card.contain-xs {
            width: 480px;
            max-width: 100%;
        }

    </style>
</head>

<body class="hold-transition login-page">
<div class="card contain-xs">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-light text-primary text-center">AEN</h1>
                <p class="login-box-msg">Sign in to start your session</p>
                <form method="POST" action="{{ route('development.login') }}" aria-label="{{ __('Login') }}" class="form floating-label" action="#" accept-charset="utf-8" method="post">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" id="login" name="login" value="{{ old('login') }}">
                        <label for="login">Login</label>
                        @if ($errors->has('login'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" id="password" name="password">
                        <label for="password">Password</label>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-6 text-right">
                            <button class="btn btn-primary btn-raised" type="submit">{{ __('Sign In') }}</button>
                        </div><!--end .col -->
                    </div><!--end .row -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .card-body -->
</div>
<!-- BEGIN JAVASCRIPT -->
<script src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/spin.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/cache.js') }}"></script>
<script src="{{ asset('assets/js/jquery.knob.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('assets/js/raphael-min.js') }}"></script>
<script src="{{ asset('assets/js/cache2.js') }}"></script>
<script src="{{ asset('assets/js/morris.min.js') }}"></script>
<script src="{{ asset('assets/js/rickshaw.min.js') }}"></script>
<script src="{{ asset('assets/js/cache3.js') }}"></script>
<script src="{{ asset('assets/js/Demo.js') }}"></script>
<!-- <script src="{{ asset('assets/js/DemoDashboard.js') }}"></script> -->
<script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/js/DemoCalendar.js') }}"></script>
<script src="{{ asset('assets/js/DemoCharts.js') }}"></script>
<script src="{{ asset('assets/js/Chart.min.js') }}"></script>
<script src="{{ asset('assets/js/vasya.js') }}"></script>
<!-- END JAVASCRIPT -->

<div id="device-breakpoints"><div class="device-xs visible-xs" data-breakpoint="xs"></div><div class="device-sm visible-sm" data-breakpoint="sm"></div><div class="device-md visible-md" data-breakpoint="md"></div><div class="device-lg visible-lg" data-breakpoint="lg"></div></div><div style="left: 286px; top: 394px; position: absolute;" data-original-title="" title=""></div><div style="left: 388px; top: 446px; position: absolute;" data-original-title="" title=""></div><div style="left: 380px; top: 443px; position: absolute;" data-original-title="" title=""></div><div style="left: 371px; top: 439px; position: absolute;" data-original-title="" title=""></div><div style="left: 397px; top: 449px; position: absolute;" data-original-title="" title=""></div><div style="left: 666px; top: 927px; position: absolute;" data-original-title="" title=""></div>

</body>
</html>