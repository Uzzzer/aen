<html lang="en" class="mdl-js">
<head>
    <title>AEN</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/materialadmin.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/material-design-iconic-font.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/rickshaw.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/morris.core.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/fullcalendar.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/vasya.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/register.css') }}">
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css" />--}}
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />--}}
<!-- END STYLESHEETS -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript"
            src="https://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
    <script type="text/javascript"
            src="https://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->


    <style>

        body.register-page {
            background: #e5e6e6;
            margin-top: 20px;
        }

        body .card.contain-xs {
            width: 480px;
            max-width: 100%;
        }

    </style>

<body class="hold-transition register-page">
<div class="card contain-xs">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-light text-primary text-center">AEN</h1>
                <p class="login-box-msg">Register a new membership</p>
                <form class="form floating-label" method="POST" action="{{ route('register') }}" accept-charset="utf-8">
                    {{ csrf_field() }}
                    <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                        <label for="username">Full name</label>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('nationality') ? ' has-error' : '' }}">
                        <select name="nationality" class="nationality" id="nationality">
                            @foreach($countries as $country)
                                <option value="{{ $country->name }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <label for="nationality">Nationality</label>
                        @if ($errors->has('nationality'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('nationality') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('birthday') ? ' has-error' : '' }}">
                        {{--<input type="text" name="birthday" id="birthday" value="{{ old('birthday') }}">--}}
                        {{--<label for="birthday">Birthday</label>--}}


                        <div class="form-group">
                            <div class="input-group date birthday">
                                <input type='text' class="form-control form-date" name="birthday" id="birthday"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            @if ($errors->has('birthday'))
                                <span class="help-block">
                            <strong>{{ $errors->first('birthday') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="well">--}}
                    {{--<div id="birthday" class="input-append date">--}}
                    {{--<input data-format="dd/MM/yyyy hh:mm:ss" type="text">--}}
                    {{--<span class="add-on">--}}
                    {{--<i data-time-icon="icon-time" data-date-icon="icon-calendar">--}}
                    {{--</i>--}}
                    {{--</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group has-feedback{{ $errors->has('profession') ? ' has-error' : '' }}">
                        <input type="text" name="profession" id="profession" class="form-control"
                               value="{{ old('profession') }}">
                        <label for="profession">Profession</label>
                        @if ($errors->has('profession'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('profession') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('sex') ? ' has-error' : '' }}">
                        <select name="sex" id="sex" class="custom-select sex">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        <label for="sex">Sex</label>
                        @if ($errors->has('sex'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('sex') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('height') ? ' has-error' : '' }}">
                        <input type="text" name="height" id="height" class="form-control" value="{{ old('height') }}">
                        <label for="height">Height</label>
                        @if ($errors->has('height'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('height') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('weight') ? ' has-error' : '' }}">
                        <input type="text" name="weight" id="weight" class="form-control" value="{{ old('weight') }}">
                        <label for="weight">Weight</label>
                        @if ($errors->has('weight'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('weight') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('chest') ? ' has-error' : '' }}">
                        <input type="text" name="chest" id="chest" class="form-control" value="{{ old('chest') }}">
                        <label for="chest">Chest</label>
                        @if ($errors->has('chest'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('chest') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('waist') ? ' has-error' : '' }}">
                        <input type="text" name="waist" id="waist" class="form-control" value="{{ old('waist') }}">
                        <label for="waist">Waist</label>
                        @if ($errors->has('waist'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('waist') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('hips') ? ' has-error' : '' }}">
                        <input type="text" name="hips" id="hips" class="form-control" value="{{ old('hips') }}">
                        <label for="hips">Hips</label>
                        @if ($errors->has('hips'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('hips') }}</strong>
                                </span>
                        @endif
                    </div>


                    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="text" name="email" id="useremail" class="form-control" value="{{ old('email') }}">
                        <label for="useremail">Email</label>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" id="password" name="password">
                        <label for="password">Password</label>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback ">
                        <input type="password" name="password_confirmation" id="password_confirmation"
                               class="form-control">
                        <label for="password_confirmation">Retype password</label>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <a href="{{ route('login') }}" class="text-light text-primary" style="margin-right:25px;">I
                                already have a membership</a>
                            <button class="btn btn-primary btn-raised" type="submit">Register</button>
                        </div><!--end .col -->
                    </div><!--end .row -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .card-body -->
</div>

<!-- BEGIN JAVASCRIPT -->
<script src="{{ asset('assets/js/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/spin.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.autosize.min.js') }}"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/cache.js') }}"></script>
<script src="{{ asset('assets/js/jquery.knob.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('assets/js/raphael-min.js') }}"></script>
<script src="{{ asset('assets/js/cache2.js') }}"></script>
<script src="{{ asset('assets/js/morris.min.js') }}"></script>
<script src="{{ asset('assets/js/rickshaw.min.js') }}"></script>
<script src="{{ asset('assets/js/cache3.js') }}"></script>
<script src="{{ asset('assets/js/Demo.js') }}"></script>
<!-- <script src="{{ asset('assets/js/DemoDashboard.js') }}"></script> -->
<script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/js/DemoCalendar.js') }}"></script>
<script src="{{ asset('assets/js/DemoCharts.js') }}"></script>
<script src="{{ asset('assets/js/Chart.min.js') }}"></script>
<script src="{{ asset('assets/js/vasya.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('js/register.js') }}"></script>
<!-- END JAVASCRIPT -->

<div id="device-breakpoints">
    <div class="device-xs visible-xs" data-breakpoint="xs"></div>
    <div class="device-sm visible-sm" data-breakpoint="sm"></div>
    <div class="device-md visible-md" data-breakpoint="md"></div>
    <div class="device-lg visible-lg" data-breakpoint="lg"></div>
</div>
<div style="left: 286px; top: 394px; position: absolute;" data-original-title="" title=""></div>
<div style="left: 388px; top: 446px; position: absolute;" data-original-title="" title=""></div>
<div style="left: 380px; top: 443px; position: absolute;" data-original-title="" title=""></div>
<div style="left: 371px; top: 439px; position: absolute;" data-original-title="" title=""></div>
<div style="left: 397px; top: 449px; position: absolute;" data-original-title="" title=""></div>
<div style="left: 666px; top: 927px; position: absolute;" data-original-title="" title=""></div>

</body>
</html>