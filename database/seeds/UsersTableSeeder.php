<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert(
            [
                'name'      => 'Admin',
                'role'      => 'admin',
                'password'  => bcrypt('admin@admin.com'),
                'email'     => 'admin@admin.com',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
        User::insert(
            [
            'name'      => 'Test',
            'role'      => 'user',
            'password'  => bcrypt('123456'),
            'email'     => 'test@test.com',
            'avatar'    => 'img/avatar-profile.png',
//            'nationality' => '',
//            'birthday' => '',
//            'profession' => '',
//            'experience' => '',
//            'sex' => '',
//            'height' => '',
//            'weight' => '',
//            'chest' => '',
//            'Hips' => '',
            'about_me'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem commodi consectetur cum distinctio dolorem esse, fugit illo illum ipsa laborum molestiae officiis omnis porro reiciendis, repellat tempora unde veniam vero!',
            'special_features'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem commodi consectetur cum distinctio dolorem esse, fugit illo illum ipsa laborum molestiae officiis omnis porro reiciendis, repellat tempora unde veniam vero!',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
    }
}
