<?php

return [
    'default_disk' => 'local',

    'ffmpeg.binaries' => '/usr/bin/ffmpeg',//env('FFMPEG_BINARIES','ffmpeg'),
    'ffmpeg.threads'  => 12,

    'ffprobe.binaries' => '/usr/bin/ffprobe',//env('FFPROBE_BINARIES','ffprobe'),

    'timeout' => 3600,
];