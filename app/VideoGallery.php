<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class VideoGallery extends Model {

	protected $table = 'video_gallery';
	protected $primaryKey = 'id';

    protected $fillable = ['title', 'description'];

    public static function getRandomVideoGroup($group_id, $limit) {
        return VideoGallery::where('group_id', $group_id)->inRandomOrder()->limit($limit)->get();
    }

    public static function getCountVideoGroup($group_id) {
        return VideoGallery::where('group_id', $group_id)->count();
    }

}
