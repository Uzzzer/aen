<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Going extends Model {

    protected $table = 'going';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'post_id'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

}