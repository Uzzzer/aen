<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function interested_count()
    {
        return $this->hasMany('App\Interested', 'user_id', 'id')->count();
    }

    public function going_count()
    {
        return $this->hasMany('App\Going', 'user_id', 'id')->count();
    }

    public static function changePassword($new_password)
    {
        $user = Auth::user();

        $user->password = Hash::make($new_password);

        $user->save();

        return $user;
    }

    public static function sendEmail($subject, $to, $from, $view, $data)
    {
        Mail::send($view, $data, function ($message) use ($subject, $to, $from) {
            $message->from($from)
                ->to($to)
                ->subject($subject);
        });
    }

    public static function isType($user_id = false)
    {
        $user = !$user_id ? AUth::user() : User::find($user_id);

        if (is_null($user))
            return false;

        if ($user->business == 0) {
            $type = 'user';

            if (is_null($user->subscription_end) or date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime($user->subscription_end)))
                $type = 'public_user';
        } else {
            if ($user->user_category_id == 10)
                $type = 'business';
            else
                $type = 'content_provider';
        }

        return $type;
    }

    public function generateToken()
    {
        $this->token = str_random(100);
        $this->save();
    }
}
