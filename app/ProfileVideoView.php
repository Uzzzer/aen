<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProfileVideoView extends Model {

	protected $table = 'video_views';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function viewVideo($video_id) {
        if (Auth::check()) {
            $View = ProfileVideoView::where('user_id', Auth::user()->id)->where('video_id', $video_id)->first();

            if (is_null($View)) {
                $View = new ProfileVideoView;
                $View->user_id = Auth::user()->id;
                $View->video_id = $video_id;
                $View->save();
            }
        }
    }

    public static function videoViewsVideo($video_id) {
        $count = ProfileVideoView::where('video_id', $video_id)->count();

        return ($count > 1) ? number_format($count).' views' : number_format($count).' view' ;
    }

}
