<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Job extends Model {

	protected $table = 'jobs';
	protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'title', 'text', 'responsibilities', 'fields', 'msg_txt_id', 'file_preview', 'file_name', 'file_type'];

}