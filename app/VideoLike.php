<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class VideoLike extends Model {

	protected $table = 'group_video_likes';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function checkLike($video_id) {
        if (Auth::check() and VideoLike::where('video_id', $video_id)->where('user_id', Auth::user()->id)->first() != NULL)
            return true;
        else
            return false;
    }

    public static function countLike($video_id) {
        return VideoLike::where('video_id', $video_id)->count();
    }

}
