<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = [];

    public static function getAllCountries()
    {
        return self::all();
    }
}
