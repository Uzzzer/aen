<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SocialAuth extends Model {

	protected $table = 'social_auth';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function check($type, $user_id = false) {
        if (!$user_id and Auth::check())
            $user_id = Auth::user()->id;

        $SocialAuth = SocialAuth::where('type', $type)->where('user_id', $user_id)->first();

        return (!is_null($SocialAuth)) ? true : false;
    }

}