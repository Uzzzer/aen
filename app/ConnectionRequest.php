<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ConnectionRequest extends Model {

	protected $table = 'connections';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function getFollowers($user_id = false)
    {
        $user_id = !$user_id ? Auth::user()->id : $user_id;

        return ConnectionRequest::where('user_id_1', $user_id)->orWhere('user_id_2', $user_id)->where('status', 'approved')->get();
    }

    public static function CheckConnection($user_1, $user_2 = false) {
        if ($user_2 == false and !Auth::check())
            return false;
        elseif ($user_2 == false and Auth::check())
            $user_2 = Auth::user()->id;

        $Connection = ConnectionRequest::where('user_id_1', $user_1)->where('user_id_2', $user_2)->orWhere('user_id_1', $user_2)->where('user_id_2', $user_1)->first();

        if ($Connection == NULL)
            return false;
        else
            return true;
    }

    public static function CheckConnectionApprove($user_1, $user_2 = false) {
        if ($user_2 == false and !Auth::check())
            return false;
        elseif ($user_2 == false and Auth::check())
            $user_2 = Auth::user()->id;

        $Connection = ConnectionRequest::where('user_id_1', $user_1)->where('user_id_2', $user_2)->orWhere('user_id_1', $user_2)->where('user_id_2', $user_1)->first();

        if ($Connection == NULL or $Connection->status != 'approved')
            return false;
        else
            return true;
    }

    public static function countRequest() {
        return (Auth::check()) ? ConnectionRequest::where('user_id_2', Auth::user()->id)->where('status', 'request')->count() : 0;
    }

    public static function countConnection($user_id = false, $text = false, $status = 'approved') {
        if ($user_id == false and !Auth::check())
            return false;
        elseif ($user_id == false and Auth::check())
            $user_id = Auth::user()->id;

        if ($text) {
            $count = ($status)
                        ? ConnectionRequest::where('user_id_2', $user_id)->where('status', $status)->orWhere('user_id_1', $user_id)->where('status', $status)->count()
                        : ConnectionRequest::where('user_id_2', $user_id)->orWhere('user_id_1', $user_id)->count();

            return ($count == 1) ? $count.' connection' : $count.' connections';
        } else
            return ($status)
                    ? ConnectionRequest::where('user_id_2', $user_id)->where('status', $status)->orWhere('user_id_1', $user_id)->where('status', $status)->count()
                    : ConnectionRequest::where('user_id_2', $user_id)->orWhere('user_id_1', $user_id)->count();
    }

}
