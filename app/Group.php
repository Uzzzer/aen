<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\GroupFollower;

class Group extends Model {

	protected $table = 'groups';
	protected $primaryKey = 'id';

    protected $fillable = ['name', 'avatar', 'description', 'keywords', 'category_id', 'type', 'status'];

    public static function checkPrivateForUser($group_id) {
        if (!Auth::check())
            return false;

        $Group = Group::find($group_id);

        if (is_null($Group) or $Group->type != 'private' or $Group->user_id == Auth::user()->id)
            return false;

        return (GroupFollower::CheckFollowGroup($Group->id)) ? false : true;
    }

    public static function UpdateFeed($group_id)
    {
        if (!Auth::check())
            return false;

        $Group = Group::find($group_id);

        if (is_null($Group))
            return false;

        GroupFollower::where('group_id', $group_id)->where('user_id', Auth::user()->id)->update(['last_update_feed' => date('Y-m-d H:i:s')]);

        return true;
    }

}