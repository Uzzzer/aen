<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProfileVideoLike extends Model {

	protected $table = 'video_likes';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function checkLike($video_id) {
        if (Auth::check() and ProfileVideoLike::where('video_id', $video_id)->where('user_id', Auth::user()->id)->first() != NULL)
            return true;
        else
            return false;
    }

    public static function countLike($video_id) {
        return ProfileVideoLike::where('video_id', $video_id)->count();
    }

}
