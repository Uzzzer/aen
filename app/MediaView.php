<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MediaView extends Model {

	protected $table = 'media_views';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function viewPhoto($photo_id) {
        if (Auth::check()) {
            $View = MediaView::where('user_id', Auth::user()->id)->where('photo_id', $photo_id)->first();

            if (is_null($View)) {
                $View = new MediaView;
                $View->user_id = Auth::user()->id;
                $View->photo_id = $photo_id;
                $View->save();
            }
        }
    }

    public static function viewVideo($video_id) {
        if (Auth::check()) {
            $View = MediaView::where('user_id', Auth::user()->id)->where('video_id', $video_id)->first();

            if (is_null($View)) {
                $View = new MediaView;
                $View->user_id = Auth::user()->id;
                $View->video_id = $video_id;
                $View->save();
            }
        }
    }

    public static function countViewsPhoto($photo_id) {
        $count = MediaView::where('photo_id', $photo_id)->count();

        return ($count > 1) ? number_format($count).' views' : number_format($count).' view' ;
    }

    public static function videoViewsVideo($video_id) {
        $count = MediaView::where('video_id', $video_id)->count();

        return ($count > 1) ? number_format($count).' views' : number_format($count).' view' ;
    }

}
