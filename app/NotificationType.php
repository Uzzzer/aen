<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model {

	protected $table = 'notification_types';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}
