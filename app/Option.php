<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Option extends Model {

	protected $table = 'options';
	protected $primaryKey = 'id';
    protected $fillable = ['value', 'name'];

    public static function option($name, $value = false) {
        $Option = Option::where('name', $name)->first();

        if ($value !== false) {
            if (is_null($Option)) {
                $Option = new Option;
                $Option->name = $name;
            }

            $Option->value = $value;
            $Option->save();
        }

        return (!is_null($Option)) ? $Option->value : '';
    }
}