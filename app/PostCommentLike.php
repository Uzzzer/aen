<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PostCommentLike extends Model {

	protected $table = 'post_comment_likes';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}