<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Option;
use App\User;

class WithdrawMoney extends Model {

	protected $table = 'withdraw_money';
	protected $primaryKey = 'id';

    protected $fillable = ['payout_batch_id', 'user_id', 'sum', 'currency', 'status'];

}
