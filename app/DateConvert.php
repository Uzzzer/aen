<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateConvert extends Model
{
    public static function Convert($date, $dialog = false, $activity = false) {
        $time = strtotime($date);

        if ($time >= time() - 60*3 and $activity == true) {
            $date = 'now';
        } elseif ($time >= time() - 60*59) {
            if (round((time() - $time)/60) > 0 or $activity == true)
                $date = round((time() - $time)/60).' min ago';
            else
                $date = 'now';
        } elseif (date('Y-m-d', $time) == date('Y-m-d')) {
            $date = date('H:i', $time);
        } elseif (date('Y', $time) == date('Y')) {
            $date = date('d M', $time);
            if ($dialog)
                $date .= date(' H:i', $time);
        } else {
            $date = date('d M Y', $time);
            if ($dialog)
                $date .= date(' H:i', $time);
        }

        if ($activity == true)
            $date = 'Active '.$date;

        return $date;
    }

    public static function dashboardDate($date, $time = false) {
        $timestamp = strtotime($date);

        $date = (date('Y', $timestamp) != date('Y')) ? date('j M Y', $timestamp) : date('j M', $timestamp);
        $date = ($time) ? $date.' '.date('H:i', $timestamp) : $date;

        return $date;
    }
}