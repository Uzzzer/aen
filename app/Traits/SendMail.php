<?php
/**
 * Created by PhpStorm.
 * User: westham
 * Date: 24.07.18
 * Time: 11:29
 */

namespace App\Traits;

use Illuminate\Support\Facades\Mail;

trait SendMail
{
    public static function sendEmail($subject, $to, $from, $view, $data)
    {
        Mail::send($view, $data, function ($message) use ($subject, $to, $from) {
            $message->from($from)
                ->to($to)
                ->subject($subject);
        });
    }
}