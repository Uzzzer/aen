<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model {

	protected $table = 'user_categories';
	protected $primaryKey = 'id';

    protected $fillable = ['name', 'prices', 'planIDs', 'trial_period'];

}
