<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\NotificationType;
use App\NotificationOn;
use App\NotificationOffGroup;

class Notification extends Model {

	protected $table = 'notifications';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function checkAndCreateNotification($user_id, $notification_type, $param) {
        $NotificationType = NotificationType::where('name', $notification_type)->select('id')->first();

        if (is_null($NotificationType))
            return false;

        $NotificationOn = NotificationOn::where('type_id', $NotificationType->id)->where('user_id', $user_id)->first();

        if (is_null($NotificationOn))
            return false;
        else {
            if (isset($param['group_id']) and isset($param['group_check'])) {
                if (!is_null(NotificationOffGroup::where('user_id', $user_id)->where('group_id', $param['group_id'])->where('type_id', $NotificationType->id)->first()))
                    return false;
            }

            $Notification = new Notification;
            $Notification->user_id = $user_id;
            $Notification->type_id = $NotificationType->id;
            $Notification->text = $param['text'];

            if (isset($param['group_id']))
                $Notification->from_group_id = $param['group_id'];

            if (isset($param['user_id']))
                $Notification->from_user_id = $param['user_id'];

            if (isset($param['user_id']) and $param['user_id'] == $Notification->user_id)
                return false;

            $Notification->save();

            return true;
        }
    }

    public static function countUnreadNotice() {
        if (!Auth::check())
            return false;

        return Notification::where('user_id', Auth::user()->id)->where('read', 0)->count();
    }

    public static function readNotifications($Notifications) {
        foreach ($Notifications as $Notification) {
            if ($Notification->read == 0) {
                $Notification->read = 1;
                $Notification->save();
            }
        }

        return true;
    }

}
