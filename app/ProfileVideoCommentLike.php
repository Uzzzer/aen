<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ProfileVideoCommentLike extends Model {

	protected $table = 'video_comment_likes';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}