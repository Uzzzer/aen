<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class WorksWith extends Model {

	protected $table = 'works_with';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function countCompanies($user_id = false) {
        if ($user_id == false and !Auth::check())
            return false;
        elseif ($user_id == false and Auth::check())
            $user_id = Auth::user()->id;

        return WorksWith::where('user_id', $user_id)->count();
    }

    public static function countWorked($user_id = false) {
        if ($user_id == false and !Auth::check())
            return false;
        elseif ($user_id == false and Auth::check())
            $user_id = Auth::user()->id;

        return WorksWith::where('business_id', $user_id)->count();
    }

}
