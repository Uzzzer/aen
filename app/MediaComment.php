<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MediaComment extends Model {

	protected $table = 'media_comments';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}
