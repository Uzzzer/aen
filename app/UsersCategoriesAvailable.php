<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UsersCategoriesAvailable extends Model {

    protected $table = 'users_categories_available';
    protected $primaryKey = 'id';

    protected $fillable = ['name', 'user_id'];

    public static function getUserCategories()
    {
        return UsersCategoriesAvailable::where('user_id', Auth::user()->id)->orderBy('id', 'asc')->get();
    }
}
