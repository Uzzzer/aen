<?php

namespace App\Console;

use App\InstagramPost;
use App\SocialAuth;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use MetzWeb\Instagram\Instagram;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            $SocialAuth = SocialAuth::where('type', 'instagram')->get();

            foreach ($SocialAuth as $auth) {
                $instagram = new Instagram(array(
                    'apiKey' => 'ac73408d304a4aa4b9b0f85875d40757',
                    'apiSecret' => 'cb9114f1e58b42aa8d82e8cfb1e8729d',
                    'apiCallback' => route('callback_instagram')
                ));

                $result = $instagram->setAccessToken(json_decode($auth->data));
                InstagramPost::getRecentPost($instagram, $auth->user_id);
            }

        })->daily(); //daily();

        $schedule->call(function () {

            $Paypal = new \App\Paypal();

            foreach (\App\WithdrawMoney::where('status', 'pending')->get() as $withdraw) {

                if ($Paypal->setWithdraw($withdraw->id) and $Paypal->checkUser()) {
                    $output = $Paypal->payout();

                    if (isset($output->getBatchHeader()->payout_batch_id)) {
                        $withdraw->payout_batch_id = $output->getBatchHeader()->payout_batch_id;
                        $withdraw->status = 'approved';
                        $withdraw->save();
                    }
                }

            }

        })->everyMinute(); //hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
