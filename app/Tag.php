<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = 'tags';
    protected $primaryKey = 'id';

    protected $fillable = ['tag', 'post_id'];

    public static function checkAndCreateTag($Post)
    {

        if (strpos($Post->description, '#') === FALSE)
            return $Post;

        $tags = [];

        $pos2 = 0;
        $i = 1;

        while ($i <= strlen($Post->description)) {
            $offset = !$pos2 ? $pos2 : $pos2 - 1;

            $pos = strpos($Post->description, '#', $offset);

            if ($pos === false)
                break;

            $pos++;

            $pos2 = Tag::strposa($Post->description, ['#', ' '], $pos);

            if ($pos2 === false)
                $pos2 = strlen($Post->description);

            $tag = str_replace(PHP_EOL, '', substr($Post->description, $pos, $pos2 - $pos));

            if (!in_array($tag, $tags))
                $tags[] = $tag;

            $i++;
        }

        foreach ($tags as $tag) {
            Tag::create([
                'tag' => $tag,
                'post_id' => $Post->id
            ]);

            $Post->description = preg_replace('/#' . $tag . '/', '<a href="/tag/' . $tag . '">#' . $tag . '</a>', $Post->description, 1);

        }

        $Post->save();

        return $Post;

    }

    public static function strposa($haystack, $needles = array(), $offset = 0)
    {
        $chr = array();
        foreach ($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false)
                $chr[$needle] = $res;
        }

        return empty($chr) ? false : min($chr);
    }
}
