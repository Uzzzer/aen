<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PhotoLike extends Model {

	protected $table = 'photo_likes';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function checkLike($photo_id) {
        if (Auth::check() and PhotoLike::where('photo_id', $photo_id)->where('user_id', Auth::user()->id)->first() != NULL)
            return true;
        else
            return false;
    }

    public static function countLike($photo_id) {
        return PhotoLike::where('photo_id', $photo_id)->count();
    }

}
