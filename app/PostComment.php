<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PostComment extends Model {

	protected $table = 'post_comments';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }
}
