<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{

    protected $table = 'posts';
    protected $primaryKey = 'id';

    protected $fillable = ['title', 'subtitle', 'location', 'keywords', 'frequency', 'keywords', 'description', 'category_id', 'start', 'start_time', 'end', 'end_time', 'video_preview', 'status'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    public function group()
    {
        return $this->hasOne('App\Group', 'id', 'group_id')->first();
    }

    public function going()
    {
        if (!Auth::check())
            return NULL;

        return $this->hasOne('App\Going', 'post_id', 'id')->where('user_id', \Auth::user()->id)->first();
    }

    public function going_count()
    {
        return $this->hasMany('App\Going', 'post_id', 'id')->count();
    }

    public function going_rand($limit = 3)
    {
        return $this->hasMany('App\Going', 'post_id', 'id')->limit($limit)->inRandomOrder()->get();
    }

    public function interested_count()
    {
        return $this->hasMany('App\Interested', 'post_id', 'id')->count();
    }

    public function interested_rand($limit = 3)
    {
        return $this->hasMany('App\Interested', 'post_id', 'id')->limit($limit)->inRandomOrder()->get();
    }

    public function interested()
    {
        if (!Auth::check())
            return NULL;

        return $this->hasOne('App\Interested', 'post_id', 'id')->where('user_id', \Auth::user()->id)->first();
    }

    public function comment_count()
    {
        return $this->hasMany('App\PostComment', 'post_id', 'id')->count();
    }

    public function like_count()
    {
        return $this->hasMany('App\PostLike', 'post_id', 'id')->count();
    }

    public static function getPostsByUser($user_id, $limit = 10)
    {
        return Post::whereNull('deleted_at')
            ->whereNull('group_id')
            ->where('status', 'post')
            ->where('user_id', $user_id)
            ->limit($limit)
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function getDraftsByUser($user_id, $limit = 10)
    {
        return Post::whereNull('deleted_at')
            ->whereNull('group_id')
            ->where('status', 'draft')
            ->where('user_id', $user_id)
            ->limit($limit)
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function countPosts($user_id = false, $draft = false)
    {
        if ($user_id == false and Auth::check())
            $user_id = Auth::user()->id;
        elseif ($user_id == false and !Auth::check())
            return false;

        if ($draft)
            return Post::whereNull('deleted_at')
                ->whereNull('group_id')
                ->where('user_id', $user_id)
                ->where('status', 'draft')
                ->count();
        else
            return Post::whereNull('deleted_at')
                ->whereNull('group_id')
                ->where('user_id', $user_id)
                ->where('status', 'post')
                ->count();
    }

    public static function countUnreadGroupPosts($group_id)
    {
        if (!Auth::check())
            return false;

        $GroupFollower = GroupFollower::where('group_id', $group_id)->where('user_id', Auth::user()->id)->first();

        if (is_null($GroupFollower))
            return false;

        if (!is_null($GroupFollower->last_update_feed))
            $count = Post::whereNull('deleted_at')
                ->where('group_id', $group_id)
                ->where('created_at', '>=', $GroupFollower->last_update_feed)
                ->count();
        else
            $count = Post::whereNull('deleted_at')
                ->where('group_id', $group_id)
                ->count();

        if ($count > 0)
            return $count . ' unread post';
        else
            return false;
    }

    public static function getSearchPostsByUser($user_id, $search, $limit = 10)
    {
        return Post::whereNull('deleted_at')
            ->whereNull('group_id')
            ->where('status', 'post')
            ->where('user_id', $user_id)
            ->where('title', 'like', '%' . $search . '%')
            ->orWhere('user_id', $user_id)
            ->where('description', 'like', '%' . $search . '%')
            ->where('status', 'post')
            ->whereNull('deleted_at')
            ->whereNull('group_id')
            ->limit($limit)
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function getCountPostsGroup($group_id)
    {
        return Post::whereNull('deleted_at')
            ->where('group_id', $group_id)
            ->count();
    }

}