<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Interested extends Model {

    protected $table = 'interested';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'post_id'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }
}