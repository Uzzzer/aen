<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Video extends Model {

	protected $table = 'videos';
	protected $primaryKey = 'id';

    protected $fillable = ['title', 'price'];

    public static function getSearchVideosByUser($user_id, $search, $limit = 10) {
        return Video::whereNull('deleted_at')
                ->where('user_id', $user_id)
                ->where('title', 'like', '%' . $search . '%')
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
    }

    public static function convertTime($time) {
        $h = 0;
        $m = 0;
        $s = 0;
        $string = '';

        if ($time > 3600) {
            $h = floor($time/3600);
            $string .= $h.'h ';
        }

        if ($time > 60) {
            $m = floor(($time - ($h*3600))/60);
            $string .= $m.'m ';
        }

        $s = $time - ($h*3600) - ($m*60);
        $string .= $s.'sec ';

        return $string;
    }

    public static function getCountVideos($user_id) {
        if ($user_id == false and Auth::check())
            $user_id = Auth::user()->id;
        elseif ($user_id == false and !Auth::check())
            return false;

        return Video::where('user_id', $user_id)->count();
    }

}