<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveCharMessage extends Model {

    protected $table = 'mc_messages';
    protected $primaryKey = 'id';

    protected $fillable = [];


    /**
     * Get user from stream.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}