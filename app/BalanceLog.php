<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Video;
use App\Option;
use App\User;

class BalanceLog extends Model {

	protected $table = 'balance_logs';
	protected $primaryKey = 'id';

    protected $fillable = [];

    public static function updateBalance($type_content, $content_id, $customer_id, $sum = false)
    {
        if ($type_content !== false) {
            switch ($type_content) {
                case "video":
                    $Video = Video::find($content_id);

                    if (is_null($Video))
                        return false;

                    $cost = $Video->price;
                    $currency = Option::option('currency');
                    $commission = round(($Video->price / 100 * Option::option('commission')), 2);
                    $sum = round($Video->price - $commission, 2);
                    $user_id = $Video->user_id;
                    $description = 'Bought a video&nbsp;<a href="'.route('profile_video', ['id' => $Video->id]).'">'.$Video->title.'</a>.';
                    $notice_type = 'bought_video';


                    break;
                case "photo":
                    $Photo = Gallery::find($content_id);

                    if (is_null($Photo))
                        return false;

                    $cost = $Photo->price;
                    $currency = Option::option('currency');
                    $commission = round(($Photo->price / 100 * Option::option('commission')), 2);
                    $sum = round($Photo->price - $commission, 2);
                    $user_id = $Photo->user_id;
                    $description = 'Bought a video&nbsp;<a href="'.route('profile_video', ['id' => $user_id, 'pid' => $Photo->id]).'">'.$Photo->title.'</a>.';
                    $notice_type = 'bought_photo';

                    break;
            }

            if (!isset($sum))
                return false;

            Notification::checkAndCreateNotification($user_id, $notice_type, [
                'user_id' => $customer_id,
                'text' => $description
            ]);

            $User = User::find($user_id);
            $User->balance += $sum;
            $User->save();

            $BalanceLog = new BalanceLog;
            $BalanceLog->user_id = $user_id;
            $BalanceLog->customer_id = $customer_id;
            $BalanceLog->cost = $cost;
            $BalanceLog->sum = $sum;
            $BalanceLog->commission = $commission;
            $BalanceLog->currency = $currency;
            $BalanceLog->description = $description;
            $BalanceLog->save();
        } else {
            $User = User::find($customer_id);
            $User->balance += round($sum, 2);
            $User->save();
        }

        return true;
    }
}
