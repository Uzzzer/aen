<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ProfileVideoComment extends Model {

	protected $table = 'video_comments';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}
