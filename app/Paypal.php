<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{

    private $apiContext;
    private $withdraw;
    private $user;

    public function __construct()
    {
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AXbMSGCaIz7S5xF5fN0v_-P3kK_8QSQse2hCLiufvPOVaP7zcBq82CQGZeXoGsTsXdhVzYKS3uobpUVY',     // ClientID
                'EMhQW-RS7zHwO-tSe712ClZD-K_wKi72OfjmQyIJo93uV1-aMUZj_VPh1k0GLiTbAlMrQOgFLsSrvvoZ'      // ClientSecret
            )
        );
    }

    public function setWithdraw($withdraw_id)
    {
        $this->withdraw = \App\WithdrawMoney::find($withdraw_id);

        return (is_null($this->withdraw) or $this->withdraw->status != 'pending') ? false : true;
    }

    public function checkUser()
    {
        $this->user = \App\User::find($this->withdraw->user_id);

        return (is_null($this->user) or is_null($this->user->paypal)) ? false : true;
    }

    public function payout()
    {
        $payouts = new \PayPal\Api\Payout();
        $senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid() . microtime(true))
            ->setEmailSubject("You have a Payout!");


        $senderItem = new \PayPal\Api\PayoutItem();
        $senderItem->setRecipientType('Email')
            ->setNote('You received a payout of ' . $this->withdraw->sum . ' GBP.')
            ->setReceiver($this->user->paypal)
            ->setAmount(new \PayPal\Api\Currency('{
                        "value":"' . $this->withdraw->sum . '",
                        "currency":"GBP"
                    }'));

        $payouts->setSenderBatchHeader($senderBatchHeader)->addItem($senderItem);

        try {
            $output = $payouts->create(null, $this->apiContext);
        } catch (Exception $ex) {
            return false;
        }

        return $output;
    }

}
