<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PurchasedVideo extends Model {

	protected $table = 'purchased_videos';
	protected $primaryKey = 'id';

    protected $fillable = [];

    public static function checkPurchase($video_id, $user_id = false)
    {
        if (!$user_id)
            $user_id = (Auth::check()) ? Auth::user()->id : false;

        $Video = Video::find($video_id);

        if (is_null($Video))
            return false;

        if ($user_id == $Video->user_id)
            return true;

        return (is_null(PurchasedVideo::where('video_id', $video_id)->where('user_id', $user_id)->first())) ? false : true;
    }

    public static function countPurchase($user_id = false)
    {
        if (!$user_id)
            $user_id = (Auth::check()) ? Auth::user()->id : false;

        return PurchasedVideo::where('user_id', $user_id)->count();
    }

}
