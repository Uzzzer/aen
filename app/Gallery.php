<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model {

	protected $table = 'gallery';
	protected $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'price', 'blured'];

    public static function getRandomPhotoGroup($group_id, $limit) {
        return Gallery::where('group_id', $group_id)->inRandomOrder()->limit($limit)->get();
    }

    public static function getCountPhotosGroup($group_id) {
        return Gallery::where('group_id', $group_id)->count();
    }

    public static function getCountPhotosUser($user_id) {
        return Gallery::where('user_id', $user_id)->whereNull('group_id')->count();
    }

    public static function getSearchPhotosByUser($user_id, $search, $limit = 10) {
        return (is_null($search))
            ? Gallery::where('user_id', $user_id)
                ->whereNull('group_id')
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get()
            :  Gallery::where('user_id', $user_id)
                ->whereNull('group_id')
                ->where('title', 'like', '%' . $search . '%')
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
    }

}
