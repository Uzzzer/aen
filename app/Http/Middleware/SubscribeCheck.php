<?php

namespace App\Http\Middleware;

use App\Transaction;
use Closure;
use Illuminate\Support\Facades\Auth;

class SubscribeCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $type_subscription
     * @return mixed
     * @internal param $type_subscribe
     */
    public function handle($request, Closure $next, $type_subscription)
    {
        $user = Auth::user();

        $transaction = $user->transactions()->type($type_subscription)->first();

        if ($transaction !== null) {
            return $next($request);
        } else {
            return redirect()->back();
        }
    }
}
