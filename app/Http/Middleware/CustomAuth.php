<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->status == 'blocked') {
                Auth::logout();
                return redirect('/?error=blocked');
            }

            $User = Auth::user();
            $User->last_activity = date('Y-m-d H:i:s');
            $User->save();

            return $next($request);
        } else {
            return redirect('/');
        }

    }
}
