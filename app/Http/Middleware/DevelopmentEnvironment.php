<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class DevelopmentEnvironment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $value = Cookie::get('development-environment-new');

        if (isset($value)) {

            if ($value == 'true') {
                return $next($request);
            }
        }

        return redirect(route('development.show.login'));
    }
}
