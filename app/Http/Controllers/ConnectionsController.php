<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\ConnectionRequest;
use App\Notification;
use Validator;

class ConnectionsController extends Controller
{

    public function request( Request $request ) {
        if (in_array(User::isType(), ['public_user', false]))
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (User::find($request->input('id')) == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'User not found.'
            ]);

        $Connection = ConnectionRequest::where('user_id_1', $request->input('id'))->where('user_id_2', Auth::user()->id)->orWhere('user_id_1', Auth::user()->id)->where('user_id_2', $request->input('id'))->first();

        if ($Connection != NULL)
            return response()->json([
                'status' => 'error',
                'message' => ($Connection->user_id_1 == Auth::user()->id) ? 'You already sent a request' : 'User already sent you a request'
            ]);
        else {
            $Connection = new ConnectionRequest;
            $Connection->user_id_1 = Auth::user()->id;
            $Connection->user_id_2 = $request->input('id');
            $Connection->save();

            $param = [
                'user_id' => $Connection->user_id_1,
                'text' => 'Sent you a connection request.'
            ];

            Notification::checkAndCreateNotification($Connection->user_id_2, 'connection_request', $param);
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function approved( Request $request ) {

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $ConnectionRequest = ConnectionRequest::find($request->input('id'));

        if ($ConnectionRequest == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Request not found'
            ]);

        if ($ConnectionRequest->user_id_2 != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access'
            ]);

        $ConnectionRequest->status = 'approved';
        $ConnectionRequest->save();

        $param = [
            'user_id' => $ConnectionRequest->user_id_2,
            'text' => 'Approved your connection request.'
        ];

        Notification::checkAndCreateNotification($ConnectionRequest->user_id_1, 'connection_request', $param);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function remove( Request $request ) {

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $ConnectionRequest = ConnectionRequest::find($request->input('id'));

        if ($ConnectionRequest == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Request not found'
            ]);

        if ($ConnectionRequest->user_id_2 != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access'
            ]);

        $param = [
            'user_id' => $ConnectionRequest->user_id_2,
            'text' => 'Canceled your connection request.'
        ];

        Notification::checkAndCreateNotification($ConnectionRequest->user_id_1, 'connection_request', $param);

        $ConnectionRequest->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

}