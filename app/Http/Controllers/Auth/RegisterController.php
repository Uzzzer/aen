<?php

namespace App\Http\Controllers\Auth;

use App\Country;
use App\Http\Controllers\Controller;
use App\NotificationOn;
use App\NotificationType;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\Registered;
use App\Mail\UsersConfirmEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:32',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'address' => intval($data['user_category_id']) != 1 ? 'required|string|max:255' : 'max:255',
            'location' => intval($data['user_category_id']) != 1 ? 'required|string|max:255' : 'max:255',

            "sex" => 'string|max:255',
            "full_name" => intval($data['user_category_id']) != 1 ? 'required|string|max:255' : 'nullable|string|max:255',
            "address2" => intval($data['user_category_id']) != 1 ? 'required|string|max:255' : 'nullable|string|max:255',
            "state" => intval($data['user_category_id']) != 1 ? 'required|string|max:255' : 'nullable|string|max:255',
            "postal_code" => intval($data['user_category_id']) != 1 ? 'required|string|max:255' : 'nullable|string|max:255',
            "terms_and_conditions" => 'accepted',
            "no_dob" => 'accepted',

            "birthday" => 'date_format:d/m/Y|required|before:18 years ago'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => 'user',
            'business' => intval($data['user_category_id']) != 1 ? 1 : 0,
            'location' => $data['location'],
            'password' => bcrypt($data['password']),
            'birthday' => $data['birthday'],
            'api_token' => Str::random(40),
            'avatar' => 'img/no-avatar.png',
            'user_category_id' => intval($data['user_category_id']),
            //(isset($data['business'])) ? $data['user_category_id'] : 1,
            'balance' => intval($data['user_category_id']) != 1 ? 0 : null,
            'address' => intval($data['user_category_id']) != 1 ? $data['address'] : '',

            "sex" => $data['sex'],
            "full_name" => intval($data['user_category_id']) != 1 ? $data['full_name'] : '',
            "address2" => intval($data['user_category_id']) != 1 ? $data['address2'] : '',
            "state" => intval($data['user_category_id']) != 1 ? $data['state'] : '',
            "postal_code" => intval($data['user_category_id']) != 1 ? $data['postal_code'] : '',

            "country_id" => intval($data['user_category_id']) != 1 ? intval($data['country_id']) : null,
            "terms_and_conditions" => $data['terms_and_conditions'],
            "no_dob" => $data['no_dob']
        ]);

        $NotificationType = NotificationType::get();

        foreach ($NotificationType as $Type) {
            if ($Type->business and !isset($data['business'])) {
                continue;
            }

            NotificationOn::create([
                'type_id' => $Type->id,
                'user_id' => $user->id
            ]);
        }

        $user->generateToken();

        \Mail::to($user)->send(new UsersConfirmEmail($user));

        return $user;
    }

    public function verifyEmail($token)
    {
        $subs = User::where('token', $token)->firstOrFail();
        $subs->token = null;
        $subs->save();
        return redirect()->route('home')->with('success', 'Your e-mail is verified. You can now login.');
    }

    public function showRegistrationForm()
    {
        $countries = Country::getAllCountries();

        return view('auth.register', [
            'countries' => $countries
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));

        return redirect()->route('home')
            ->with('success', 'Check your email and click on the link to verify.');
    }
}
