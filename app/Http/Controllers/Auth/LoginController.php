<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\AuthLog;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    public function authenticated($request, $user)
    {
        /*if ($user->token !== null) {
            $this->guard()->logout();
            return back();
        }*/
        $AuthLog = new AuthLog;
        $AuthLog->user_id = $user->id;
        $AuthLog->event = 'login';
        $AuthLog->save();

        if($user->role == 'admin'){
            return redirect('/admin');
        }else{
            return redirect('/profile');
        }
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function logout()
    {
        try {
            $AuthLog = new AuthLog;
            $AuthLog->user_id = Auth::user()->id;
            $AuthLog->event = 'logout';
            $AuthLog->save();

            Auth::logout();
        } catch (\Exception $exception) {

        }

        return redirect('/');
    }
}
