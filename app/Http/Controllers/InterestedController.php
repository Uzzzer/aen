<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Interested;
use App\Post;
use App\User;
use App\Group;
use App\Going;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class InterestedController extends Controller
{

    public function interested(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('id'));

        if ($Post == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        $interested = Interested::where('post_id', $request->input('id'))->where('user_id', Auth::user()->id)->first();

        if ($interested != NULL) {
            $interested->delete();
        } else {
            $interested = new Interested;
            $interested->post_id = $request->input('id');
            $interested->user_id = Auth::user()->id;
            $interested->save();

            $param = [
                'user_id' => $interested->user_id,
                'text' => 'Interested your post "' . $Post->title . '"'
            ];

            if ($Post->post_type == 'group_post') {
                $Group = Group::find($Post->group_id);

                if (!is_null($Group))
                    $param['text'] .= ', in the group&nbsp;<a href="' . route('group', ['id' => $Group->id]) . '">' . $Group->name . '</a>.';
                else
                    $param['text'] .= '.';
            } else
                $param['text'] .= '.';

            Notification::checkAndCreateNotification($Post->user_id, 'interested_post', $param);
        }

        return response()->json([
            'status' => 'success',
            'count_interested' => Interested::where('post_id', $request->input('id'))->count()
        ]);
    }

    public function going(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('id'));

        if ($Post == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        $going = Going::where('post_id', $request->input('id'))->where('user_id', Auth::user()->id)->first();

        if ($going != NULL) {
            $going->delete();
        } else {
            $going = new Going;
            $going->post_id = $request->input('id');
            $going->user_id = Auth::user()->id;
            $going->save();

            $param = [
                'user_id' => $going->user_id,
                'text' => 'Going your post "' . $Post->title . '"'
            ];

            if ($Post->post_type == 'group_post') {
                $Group = Group::find($Post->group_id);

                if (!is_null($Group))
                    $param['text'] .= ', in the group&nbsp;<a href="' . route('group', ['id' => $Group->id]) . '">' . $Group->name . '</a>.';
                else
                    $param['text'] .= '.';
            } else
                $param['text'] .= '.';

            Notification::checkAndCreateNotification($Post->user_id, 'going_post', $param);
        }

        return response()->json([
            'status' => 'success',
            'count_going' => Going::where('post_id', $request->input('id'))->count()
        ]);
    }
}