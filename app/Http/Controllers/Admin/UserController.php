<?php

namespace App\Http\Controllers\Admin;

use App\ConnectionRequest;
use App\DateConvert;
use App\Gallery;
use App\GroupFollower;
use App\Http\Controllers\Controller;
use App\Job;
use App\Post;
use App\PurchasedVideo;
use App\User;
use App\UserCategory;
use App\Video;
use Chat;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Validator;

class UserController extends Controller
{
    public function export()
    {
        $headers = [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=users.csv',
            'Expires' => '0',
            'Pragma' => 'public'
        ];

        $list = \DB::table('users')->get()->toArray();

        array_unshift($list, array_keys((array) $list[0]));

        $callback = function () use ($list) {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                $row = (array) $row;

                if (isset($row['avatar'])) {
                    $row['avatar'] = asset($row['avatar']);
                }

                fputcsv($FH, (array) $row, ';');
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }

    public function import(Request $request)
    {
        $header = NULL;
        $data = array();
        if (($handle = fopen($request->file('file')->getRealPath(), 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 10000, ';')) !== FALSE) {

                if (!$header)
                    $header = $row;
                else {
                    try {
                        $data[] = array_combine($header, $row);
                    } catch (\Exception $e) {
                        return redirect('/admin/users?error=Invalid file');
                    }
                }
            }
            fclose($handle);
        }

        if (is_array($data)) {
            $imported = 0;

            foreach ($data as $datum) {
                if (isset($datum['email'])) {
                    if (!User::where('email', $datum['email'])->count()) {
                        unset($datum['id']);
                        unset($datum['created_at']);
                        unset($datum['updated_at']);

                        foreach ($datum as $key => $val) {
                            if (!strlen($val)) {
                                unset($datum[$key]);
                            }
                        }
                        User::create($datum);
                        $imported++;
                    }
                }
            }
            return redirect('/admin/users?imported=' . $imported);
        } else {
            return redirect('/admin/users?error=Invalid file');
        }
    }

    /* User Categories */

    public function user_categories()
    {
        return view('admin.user_categories', [
            'UserCategories' => UserCategory::get()
        ]);
    }

    public function user_category($id)
    {
        $UserCategory = UserCategory::find($id);

        if (is_null($UserCategory)) {
            return redirect(route('admin-user-categories'));
        }

        return view('admin.user_category', [
            'UserCategory' => $UserCategory
        ]);
    }

    public function update_user_category(Request $request)
    {
        $validation = [
            'user_category_id' => 'required',
            'name' => 'required|string|max:255',
            'prices' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);
        }

        $UserCategory = UserCategory::find($request->input('user_category_id'));

        if (is_null($UserCategory)) {
            return response()->json([
                'status' => 'error',
                'message' => 'User Category not found.'
            ]);
        }

        $UserCategory->update([
            'name' => $request->input('name'),
            'prices' => (is_null($request->input('prices'))) ? json_encode([]) : json_encode($request->input('prices')),
            'trial_period' => (is_null($request->input('trial'))) ? 0 : 1,
            'planIDs' => (is_null($request->input('planIDs'))) ? json_encode([]) : json_encode($request->input('planIDs'))
        ]);

        return response()->json(['status' => 'success']);
    }

    /* End User Categories */

    public function index()
    {
        return view('admin.users', [
            'Users' => User::whereNotIn('role', ['admin', 'developer'])->get()
        ]);
    }

    public function user($id)
    {
        $User = User::find($id);

        if (is_null($User) or $User->role == 'admin') {
            return redirect(route('admin-users'));
        }


        $Conversations = DB::table('mc_conversation_user')->where('user_id', $id)->select('conversation_id',
            'created_at')->get();
        $Chats = [];

        foreach ($Conversations as $Conversation) {
            $conversation = Chat::conversations()->getById($Conversation->conversation_id);

            if (!isset($Chats[$Conversation->conversation_id])) {
                $last_message = DB::table('mc_messages')->where('conversation_id',
                    $Conversation->conversation_id)->select('created_at')->orderBy('id', 'desc')->first();

                $Chats[$Conversation->conversation_id] = [
                    'users' => $conversation->users,
                    'count_messages' => count(Chat::conversation($conversation)->for($User)->getMessages()),
                    'last_message' => (is_null($last_message)) ? null : DateConvert::dashboardDate($last_message->created_at,
                        true),
                    'date_created' => DateConvert::dashboardDate($Conversation->created_at, true)
                ];

            }
        }

        return view('admin.user', [
            'User' => $User,
            'Chats' => $Chats,
            'Groups' => GroupFollower::where('group_followers.user_id', $User->id)
                ->join('groups', 'group_followers.group_id', '=', 'groups.id')
                ->select('group_followers.*', 'groups.*', 'groups.user_id as owner_id')
                ->orderBy('group_followers.id', 'desc')
                ->get(),
            'Posts' => Post::whereNull('deleted_at')
                ->whereNull('group_id')
                ->where('user_id', $User->id)
                ->orderBy('id', 'desc')
                ->get(),
            'Photos' => Gallery::where('user_id', $User->id)->whereNull('group_id')->orderBy('id', 'desc')->get(),
            'Jobs' => ($User->business and $User->user_category_id == 10) ? Job::where('user_id',
                $User->id)->orderBy('id', 'desc')->get() : null,
            'Videos' => ($User->business) ? Video::where('user_id', $User->id)->orderBy('id', 'desc')->get()
                : PurchasedVideo::where('purchased_videos.user_id', $User->id)->join('videos',
                    'purchased_videos.video_id', '=', 'videos.id')->select('purchased_videos.*',
                    'videos.created_at as video_created_at', 'videos.title',
                    'videos.preview_image')->orderBy('purchased_videos.id', 'desc')->get(),
            'ConnectionRequests' => ConnectionRequest::where('user_id_1', $User->id)->orWhere('user_id_2',
                $User->id)->leftjoin('users', function ($join) use ($User) {
                $join->on('connections.user_id_1', '=', 'users.id')
                    ->where('users.id', '!=', $User->id)
                    ->orOn('connections.user_id_2', '=', 'users.id')
                    ->where('users.id', '!=', $User->id);
            })->select('connections.*', 'users.name as user_name', 'users.avatar as user_avatar', 'users.id as user_id',
                'users.business', 'users.business')->get()
        ]);
    }

    public function basic_info(Request $request)
    {
        $validation = [
            'user_id' => 'required',
            'name' => 'required|string|max:32',
            'location' => 'max:255',
            'nationality' => 'max:255',
            'profession' => 'max:255',
            'sex' => 'max:255',
            'height' => 'max:255',
            'weight' => 'max:255',
            'chest' => 'max:255',
            'waist' => 'max:255',
            'hips' => 'max:255',
            'about_me' => 'max:1000',
            'avaliable' => 'max:1000',
        ];

        if ($request->input('website') != null and $request->input('website') != 'http://') {
            $validation['website'] = 'url|max:255';
        }

        if ($request->input('experience') != null) {
            $validation['experience'] = 'integer';
        }

        if ($request->input('birthday') != null) {
            $validation['birthday'] = 'date';
        }

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);
        }

        $user = User::find($request->input('user_id'));

        if (is_null($user)) {
            return response()->json([
                'status' => 'error',
                'message' => 'User not found.'
            ]);
        }

        $user->update($request->except(['_token', 'user_id', 'avaliable']));
        $user->update(['avaliable' => (is_null($request->input('avaliable'))) ? json_encode([]) : json_encode($request->input('avaliable'))]);

        return response()->json(['status' => 'success']);
    }

    public function avatar(Request $request)
    {
        $validation = [
            'user_id' => 'required',
            'avatar' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);
        }

        $user = User::find($request->input('user_id'));

        if (is_null($user)) {
            return response()->json([
                'status' => 'error',
                'message' => 'User not found.'
            ]);
        }

        $user->update($request->except(['_token', 'user_id']));

        return response()->json(['status' => 'success']);
    }

    public function block(Request $request)
    {
        $validation = [
            'user_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);
        }

        $user = User::find($request->input('user_id'));

        if (is_null($user)) {
            return response()->json([
                'status' => 'error',
                'message' => 'User not found.'
            ]);
        }

        $user->status = "blocked";
        $user->save();

        return response()->json(['status' => 'success']);
    }

    public function unblock(Request $request)
    {
        $validation = [
            'user_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);
        }

        $user = User::find($request->input('user_id'));

        if (is_null($user)) {
            return response()->json([
                'status' => 'error',
                'message' => 'User not found.'
            ]);
        }

        $user->status = "approved";
        $user->save();

        return response()->json(['status' => 'success']);
    }

    public function login($id)
    {
        $User = User::find($id);

        if (is_null($User)) {
            return back();
        }

        Auth::login($User);

        if (Auth::user()->role != 'admin') {
            return redirect(route('profile'));
        } else {
            return back();
        }
    }
}
