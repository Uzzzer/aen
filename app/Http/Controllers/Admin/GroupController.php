<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Group;
use App\ConnectionRequest;
use App\GroupFollower;
use App\GroupCategory;
use App\Post;
use App\Gallery;
use App\VideoGallery;
use Hash;
use Validator;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function index()
    {
        return view('admin.groups', [
            'Groups' => Group::orderBy('id', 'desc')->get()
        ]);
    }

    public function group($id)
    {
        $Group = Group::find($id);

        if (is_null($Group))
            return redirect(route('admin-groups'));

        return view('admin.group', [
            'Group' => $Group,
            'Owner' => User::find($Group->user_id),
            'Members' => GroupFollower::where('group_id', $Group->id)->join('users', 'group_followers.user_id', '=', 'users.id')->select('users.name', 'users.avatar', 'users.business', 'users.id')->get(),
            'GroupCategories' => GroupCategory::get(),
            'Posts' => Post::whereNull('deleted_at')
                        ->where('group_id', $Group->id)
                        ->orderBy('id', 'desc')
                        ->get(),
            'Photos' => Gallery::where('group_id', $Group->id)->orderBy('id', 'desc')->get(),
            'Videos' => VideoGallery::where('group_id', $Group->id)->orderBy('id', 'desc')->get(),
        ]);
    }

    public function basic_info(Request $request)
    {
        $validation = [
            'group_id' => 'required',
            'name' => 'required|string|max:255',
            'type' => 'required',
            'keywords' => 'max:255',
            'category_id' => 'required',
            'description' => 'max:1000'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $group = Group::find($request->input('group_id'));

        if (is_null($group))
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        $group->update($request->except(['_token', 'group_id']));

        return response()->json(['status' => 'success']);
    }

    public function avatar(Request $request)
    {
        $validation = [
            'group_id' => 'required',
            'avatar' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $group = Group::find($request->input('group_id'));

        if (is_null($group))
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        $group->update($request->except(['_token', 'group_id']));

        return response()->json(['status' => 'success']);
    }

    public function block(Request $request)
    {
        $validation = [
            'group_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $group = Group::find($request->input('group_id'));

        if (is_null($group))
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        $group->status = "blocked";
        $group->save();

        return response()->json(['status' => 'success']);
    }

    public function unblock(Request $request)
    {
        $validation = [
            'group_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $group = Group::find($request->input('group_id'));

        if (is_null($group))
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        $group->status = "active";
        $group->save();

        return response()->json(['status' => 'success']);
    }
}
