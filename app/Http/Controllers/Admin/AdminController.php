<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Group;
use App\Post;
use Hash;
use Illuminate\Support\Facades\Auth;
use Validator;

class AdminController extends Controller
{
    public function login()
    {
        return view('adminlte::login');
    }

    public function home()
    {
        return view('admin.home', [
            'Users' => User::whereNotIn('role', ['admin', 'developer'])->orderBy('id', 'desc')->paginate(5),
            'Groups' => Group::orderBy('id', 'desc')->paginate(5),
            'Posts' => Post::leftjoin('users', 'posts.user_id', '=', 'users.id')->leftjoin('groups', 'posts.group_id', '=', 'groups.id')->select('posts.*', 'users.name as user_name', 'groups.name as group_name')->orderBy('posts.id', 'desc')->paginate(5)
        ]);
    }

    public function admin_settings()
    {
        return view('admin.settings');
    }

    public function change_password(Request $request)
    {
        $user = Auth::user();

        $validation = [
            'password' => 'required',
            'new_password' => 'required|min:6|confirmed',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (!Hash::check($request->input('password'), $user->password))
            return response()->json([
                'status' => 'error',
                'message' => 'Incorrect password'
            ]);

        $user->password = bcrypt($request->input('new_password'));
        $user->save();

        return response()->json(['status' => 'success']);
    }

}
