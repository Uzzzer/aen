<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Gallery;
use App\PhotoLike;
use App\MediaComment;
use App\Group;
use Storage;
use Hash;
use Validator;
use Illuminate\Support\Facades\Auth;

class PhotoController extends Controller
{
    public function index()
    {
        return view('admin.photos', [
            'Photos' => Gallery::leftjoin('users', 'gallery.user_id', '=', 'users.id')->leftjoin('groups', 'gallery.group_id', '=', 'groups.id')->select('gallery.*', 'users.name as user_name', 'groups.name as group_name')->orderBy('id', 'desc')->get()
        ]);
    }

    public function photo($id)
    {
        $Photo = Gallery::find($id);

        if (is_null($Photo))
            return redirect(route('admin-photos'));

        return view('admin.photo', [
            'Photo'      => $Photo,
            'Likes'     => PhotoLike::where('photo_id', $Photo->id)->join('users', 'photo_likes.user_id', '=', 'users.id')->select('photo_likes.*', 'users.name as user_name', 'users.avatar as user_avatar')->get(),
            'Comments'  => MediaComment::where('media_comments.photo_id', $Photo->id)->whereNull('media_comments.reply_id')->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->orderBy('media_comments.id', 'desc')->get(),
            'User'      => User::find($Photo->user_id),
            'Group'     => Group::find($Photo->group_id)
        ]);
    }

    public function basic_info(Request $request)
    {
        $validation = [
            'photo_id' => 'required',
            'title' => 'max:255',
            'description' => 'max:500'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $photo = Gallery::find($request->input('photo_id'));

        if (is_null($photo))
            return response()->json([
                'status' => 'error',
                'message' => 'Photo not found.'
            ]);

        $photo->update($request->except(['_token', 'photo_id']));

        return response()->json(['status' => 'success']);
    }

    public function delete(Request $request)
    {
        $validation = [
            'photo_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $photo = Gallery::find($request->input('photo_id'));

        if (is_null($photo))
            return response()->json([
                'status' => 'error',
                'message' => 'Photo not found.'
            ]);

        Storage::delete(str_replace('/storage', '', $photo->file_name));

        $MediaComments = MediaComment::where('photo_id', $photo->id)->whereNotNull('image')->get();
        foreach ($MediaComments as $MediaComment)
            Storage::delete(str_replace('/storage', '', $MediaComment->image));

        MediaComment::where('photo_id', $photo->id)->delete();
        PhotoLike::where('photo_id', $photo->id)->delete();
        $photo->delete();

        return response()->json(['status' => 'success']);
    }

    public function delete_comment(Request $request)
    {
        $validation = [
            'comment_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Comment = MediaComment::find($request->input('comment_id'));

        if (is_null($Comment))
            return response()->json([
                'status' => 'error',
                'message' => 'Comment not found.'
            ]);

        if (!is_null($Comment->image))
            Storage::delete(str_replace('/storage', '', $Comment->image));

        $MediaComments = MediaComment::where('reply_id', $Comment->id)->whereNotNull('image')->get();
        foreach ($MediaComments as $MediaComment)
            Storage::delete(str_replace('/storage', '', $MediaComment->image));

        MediaComment::where('reply_id', $Comment->id)->delete();
        $Comment->delete();

        return response()->json(['status' => 'success']);
    }
}
