<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Group;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\PostCategory;
use Storage;
use Hash;
use Validator;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        return view('admin.posts', [
            'Posts' => Post::leftjoin('users', 'posts.user_id', '=', 'users.id')->leftjoin('groups', 'posts.group_id', '=', 'groups.id')->select('posts.*', 'users.name as user_name', 'groups.name as group_name')->orderBy('posts.id', 'desc')->get()
        ]);
    }

    public function post($id)
    {
        $Post = Post::find($id);

        if (is_null($Post))
            return redirect(route('admin-posts'));

        return view('admin.post', [
            'Post'      => $Post,
            'Likes'     => PostLike::where('post_id', $Post->id)->join('users', 'post_likes.user_id', '=', 'users.id')->select('post_likes.*', 'users.name as user_name', 'users.avatar as user_avatar')->get(),
            'Comments'  => PostComment::where('post_comments.post_id', $Post->id)->whereNull('post_comments.reply_id')->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->orderBy('post_comments.id', 'desc')->get(),
            'User'      => User::find($Post->user_id),
            'Group'     => Group::find($Post->group_id),
            'PostCategories' => PostCategory::get()
        ]);
    }

    public function image(Request $request)
    {
        $validation = [
            'post_id' => 'required',
            'image' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('post_id'));

        if (is_null($Post))
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        $Post->file_name = $request->input('image');
        $Post->file_type = 'image';
        $Post->save();

        return response()->json(['status' => 'success']);
    }

    public function video(Request $request)
    {
        $validation = [
            'post_id' => 'required',
            'video' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('post_id'));

        if (is_null($Post))
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        $Post->file_name = $request->input('video');
        $Post->file_type = 'video';
        $Post->save();

        return response()->json(['status' => 'success']);
    }

    public function basic_info(Request $request)
    {
        $validation = [
            'post_id' => 'required',
            'title' => 'required|string|max:255',
            'location' => 'max:255',
            'keywords' => 'max:255',
            'frequency' => 'max:255',
            'keywords' => 'max:255',
            'description' => 'max:1000'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $post = Post::find($request->input('post_id'));

        if (is_null($post))
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        if ($post->post_type == 'event' or $post->post_type == 'news') {
            $validation['category_id'] = 'required';
            if ($post->post_type == 'event') {
                $validation['start'] = 'required';
                $validation['end'] = 'required';
            }

            $validator = Validator::make($request->all(), $validation);

            if ($validator->fails())
                return response()->json([
                    'status' => 'error',
                    'message' => $validator->errors()->all()[0]
                ]);
        }

        $post->update($request->except(['_token', 'post_id']));

        return response()->json(['status' => 'success']);
    }

    public function delete(Request $request)
    {
        $validation = [
            'post_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $post = Post::find($request->input('post_id'));

        if (is_null($post))
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        if (!is_null($post->file_name))
            Storage::delete(str_replace('/storage', '', $post->file_name));

        $PostComments = PostComment::where('post_id', $post->id)->whereNotNull('image')->get();
        foreach ($PostComments as $PostComment)
            Storage::delete(str_replace('/storage', '', $PostComment->image));

        PostComment::where('post_id', $post->id)->delete();
        PostLike::where('post_id', $post->id)->delete();
        $post->delete();

        return response()->json(['status' => 'success']);
    }

    public function delete_comment(Request $request)
    {
        $validation = [
            'comment_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Comment = PostComment::find($request->input('comment_id'));

        if (is_null($Comment))
            return response()->json([
                'status' => 'error',
                'message' => 'Comment not found.'
            ]);

        if (!is_null($Comment->image))
            Storage::delete(str_replace('/storage', '', $Comment->image));

        $PostComments = PostComment::where('reply_id', $Comment->id)->whereNotNull('image')->get();
        foreach ($PostComments as $PostComment)
            Storage::delete(str_replace('/storage', '', $PostComment->image));

        PostComment::where('reply_id', $Comment->id)->delete();

        $Comment->delete();

        return response()->json(['status' => 'success']);
    }
}
