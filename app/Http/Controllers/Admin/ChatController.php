<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\DateConvert;
use Hash;
use Chat;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{
    public function index()
    {
        $Conversations = DB::table('mc_conversations')->join('mc_conversation_user', 'mc_conversations.id', '=', 'mc_conversation_user.conversation_id')->select('mc_conversations.*', 'mc_conversation_user.user_id')->get();
        $Chats = [];

        foreach ($Conversations as $Conversation) {
            $User = User::find($Conversation->user_id);

            if (!isset($Chats[$Conversation->id])) {
                $last_message = DB::table('mc_messages')->where('conversation_id', $Conversation->id)->select('created_at')->orderBy('id', 'desc')->first();

                $conversation = Chat::conversations()->getById($Conversation->id);

                $Chats[$Conversation->id] = [
                    'users' => [],
                    'count_messages' => count(Chat::conversation($conversation)->for($User)->getMessages()),
                    'last_message' => (is_null($last_message)) ? NULL : DateConvert::dashboardDate($last_message->created_at, true),
                    'date_created' => DateConvert::dashboardDate($Conversation->created_at, true)
                ];

            }

            if (!is_null($User))
                $Chats[$Conversation->id]['users'][] = $User;
        }

        return view('admin.chats', [
            'Chats' => $Chats
        ]);
    }

    public function chat($id)
    {
        $Conversation = Chat::conversations()->getById($id);

        if (is_null($Conversation))
            return redirect(route('admin-chats'));

        $UsersConversation = $Conversation->users;
        $Users = [];

        foreach ($UsersConversation as $UserConversation) {
            $Users[$UserConversation->id] = [
                'avatar' => $UserConversation->avatar,
                'name' => $UserConversation->name
            ];
        }

        return view('admin.chat', [
            'Conversation' => $Conversation,
            'Users' => $UsersConversation,
            'UsersArray' => $Users
        ]);
    }

    public function chat_delete(Request $request)
    {
        $validation = [
            'chat_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $chat = DB::table('mc_conversations')->where('id', $request->input('chat_id'))->first();

        if (is_null($chat))
            return response()->json([
                'status' => 'error',
                'message' => 'Chat not found.'
            ]);

        DB::table('mc_conversation_user')->where('conversation_id', $chat->id)->delete();
        DB::table('mc_messages')->where('conversation_id', $chat->id)->delete();
        DB::table('mc_message_notification')->where('conversation_id', $chat->id)->delete();
        DB::table('mc_conversations')->where('id', $chat->id)->delete();

        return response()->json(['status' => 'success']);
    }

    public function delete_message(Request $request)
    {
        $validation = [
            'message_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $message = DB::table('mc_messages')->where('id', $request->input('message_id'))->first();

        if (is_null($message))
            return response()->json([
                'status' => 'error',
                'message' => 'Message not found.'
            ]);

        DB::table('mc_message_notification')->where('message_id', $message->id)->delete();
        DB::table('mc_messages')->where('id', $message->id)->delete();

        return response()->json(['status' => 'success']);
    }
}