<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\DateConvert;
use App\AuthLog;
use Hash;
use Chat;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LogController extends Controller
{
    public function auth_logs()
    {
        return view('admin.auth-logs', [
            'AuthLogs' => AuthLog::join('users', 'auth_logs.user_id', '=', 'users.id')->select('auth_logs.*', 'users.name as user_name')->orderBy('id', 'desc')->get()
        ]);
    }
}