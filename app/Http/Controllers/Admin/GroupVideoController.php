<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\VideoGallery;
use App\VideoLike;
use App\MediaComment;
use App\Group;
use Storage;
use Hash;
use Validator;
use Illuminate\Support\Facades\Auth;

class GroupVideoController extends Controller
{
    public function index()
    {
        return view('admin.group-videos', [
            'GroupVideos' => VideoGallery::leftjoin('users', 'video_gallery.user_id', '=', 'users.id')->leftjoin('groups', 'video_gallery.group_id', '=', 'groups.id')->select('video_gallery.*', 'users.name as user_name', 'groups.name as group_name')->orderBy('id', 'desc')->get()
        ]);
    }

    public function group_video($id)
    {
        $VideoGallery = VideoGallery::find($id);

        if (is_null($VideoGallery))
            return redirect(route('admin-group-videos'));

        return view('admin.group-video', [
            'GroupVideo' => $VideoGallery,
            'Likes'      => VideoLike::where('video_id', $VideoGallery->id)->join('users', 'group_video_likes.user_id', '=', 'users.id')->select('group_video_likes.*', 'users.name as user_name', 'users.avatar as user_avatar')->get(),
            'Comments'   => MediaComment::where('media_comments.video_id', $VideoGallery->id)->whereNull('media_comments.reply_id')->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->orderBy('media_comments.id', 'desc')->get(),
            'User'       => User::find($VideoGallery->user_id),
            'Group'      => Group::find($VideoGallery->group_id)
        ]);
    }

    public function basic_info(Request $request)
    {
        $validation = [
            'video_id' => 'required',
            'title' => 'max:255',
            'description' => 'max:500'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $video = VideoGallery::find($request->input('video_id'));

        if (is_null($video))
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found.'
            ]);

        $video->update($request->except(['_token', 'video_id']));

        return response()->json(['status' => 'success']);
    }

    public function delete(Request $request)
    {
        $validation = [
            'video_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $video = VideoGallery::find($request->input('video_id'));

        if (is_null($video))
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found.'
            ]);

        Storage::delete(str_replace('/storage', '', $video->file_name));
        Storage::delete(str_replace('/storage', '', $video->preview));

        $MediaComments = MediaComment::where('video_id', $video->id)->whereNotNull('image')->get();
        foreach ($MediaComments as $MediaComment)
            Storage::delete(str_replace('/storage', '', $MediaComment->image));

        MediaComment::where('video_id', $video->id)->delete();
        VideoLike::where('video_id', $video->id)->delete();
        $video->delete();

        return response()->json(['status' => 'success']);
    }

    public function delete_comment(Request $request)
    {
        $validation = [
            'comment_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Comment = MediaComment::find($request->input('comment_id'));

        if (is_null($Comment))
            return response()->json([
                'status' => 'error',
                'message' => 'Comment not found.'
            ]);

        if (!is_null($Comment->image))
            Storage::delete(str_replace('/storage', '', $Comment->image));

        $MediaComments = MediaComment::where('reply_id', $Comment->id)->whereNotNull('image')->get();
        foreach ($MediaComments as $MediaComment)
            Storage::delete(str_replace('/storage', '', $MediaComment->image));

        MediaComment::where('reply_id', $Comment->id)->delete();
        $Comment->delete();

        return response()->json(['status' => 'success']);
    }
}
