<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Video;
use App\ProfileVideoLike;
use App\ProfileVideoComment;
use Storage;
use Hash;
use Validator;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    public function index()
    {
        return view('admin.videos', [
            'Videos' => Video::leftjoin('users', 'videos.user_id', '=', 'users.id')->select('videos.*', 'users.name as user_name')->orderBy('id', 'desc')->get()
        ]);
    }

    public function video($id)
    {
        $Video = Video::find($id);

        if (is_null($Video))
            return redirect(route('admin-videos'));

        return view('admin.video', [
            'Video'     => $Video,
            'Likes'     => ProfileVideoLike::where('video_likes.video_id', $Video->id)->join('users', 'video_likes.user_id', '=', 'users.id')->select('video_likes.*', 'users.name as user_name', 'users.avatar as user_avatar')->get(),
            'Comments'  => ProfileVideoComment::where('video_comments.video_id', $Video->id)->whereNull('video_comments.reply_id')->join('users', 'video_comments.user_id', '=', 'users.id')->select('video_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->orderBy('video_comments.id', 'desc')->get(),
            'User'      => User::find($Video->user_id)
        ]);
    }

    public function basic_info(Request $request)
    {
        $validation = [
            'video_id' => 'required',
            'title' => 'max:255',
            'price' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $video = Video::find($request->input('video_id'));

        if (is_null($video))
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found.'
            ]);

        $video->update($request->except(['_token', 'video_id']));

        return response()->json(['status' => 'success']);
    }

    public function delete(Request $request)
    {
        $validation = [
            'video_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $video = Video::find($request->input('video_id'));

        if (is_null($video))
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found.'
            ]);

        Storage::delete(str_replace('/storage', '', $video->file_name));

        $ProfileVideoComments = ProfileVideoComment::where('video_id', $video->id)->whereNotNull('image')->get();
        foreach ($ProfileVideoComments as $ProfileVideoComment)
            Storage::delete(str_replace('/storage', '', $ProfileVideoComment->image));

        ProfileVideoComment::where('video_id', $video->id)->delete();
        ProfileVideoLike::where('video_id', $video->id)->delete();
        $video->delete();

        return response()->json(['status' => 'success']);
    }

    public function delete_comment(Request $request)
    {
        $validation = [
            'comment_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Comment = ProfileVideoComment::find($request->input('comment_id'));

        if (is_null($Comment))
            return response()->json([
                'status' => 'error',
                'message' => 'Comment not found.'
            ]);

        if (!is_null($Comment->image))
            Storage::delete(str_replace('/storage', '', $Comment->image));

        $ProfileVideoComments = ProfileVideoComment::where('reply_id', $Comment->id)->whereNotNull('image')->get();
        foreach ($ProfileVideoComments as $ProfileVideoComment)
            Storage::delete(str_replace('/storage', '', $ProfileVideoComment->image));

        ProfileVideoComment::where('reply_id', $Comment->id)->delete();
        $Comment->delete();

        return response()->json(['status' => 'success']);
    }
}
