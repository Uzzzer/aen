<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Job;
use App\JobRequest;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class JobController extends Controller
{
    public function index()
    {
        return view('admin.jobs', [
            'Jobs' => Job::leftjoin('users', 'jobs.user_id', '=', 'users.id')->select('jobs.*', 'users.name as user_name')->orderBy('jobs.id', 'desc')->get()
        ]);
    }

    public function job($id)
    {
        $Job = Job::find($id);

        if (is_null($Job))
            return redirect(route('admin-jobs'));

        return view('admin.job', [
            'Job' => $Job,
            'Requests' => JobRequest::where('job_requests.job_id', $Job->id)->join('users', 'job_requests.user_id', '=', 'users.id')->join('mc_messages', 'job_requests.msg_txt_id', '=', 'mc_messages.id')->select('job_requests.*', 'users.name as user_name', 'users.avatar as user_avatar', 'mc_messages.body as message')->orderBy('job_requests.id', 'desc')->get(),
            'User' => User::find($Job->user_id)
        ]);
    }

    public function basic_info(Request $request)
    {
        $validation = [
            'job_id' => 'required',
            'title' => 'required|string|max:255',
            'description' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('job_id'));

        if (is_null($Job))
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        if ($request->input('file_type') != NULL)
            $Job->file_type = $request->input('file_type');
        if ($request->input('file_name') != NULL)
            $Job->file_name = $request->input('file_name');
        $Job->title = $request->input('title');
        $Job->text = $request->input('description');
        if ($request->input('responsibilities') != NULL and is_array($request->input('responsibilities')))
            $Job->responsibilities = json_encode($request->input('responsibilities'));
        if ($request->input('fields') != NULL and is_array($request->input('fields')))
            $Job->fields = json_encode($request->input('fields'));
        $Job->save();

        return response()->json(['status' => 'success']);
    }

    public function image(Request $request)
    {
        $validation = [
            'job_id' => 'required',
            'image' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('job_id'));

        if (is_null($Job))
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        $Job->file_name = $request->input('image');
        $Job->file_type = 'image';
        $Job->save();

        return response()->json(['status' => 'success']);
    }

    public function video(Request $request)
    {
        $validation = [
            'job_id' => 'required',
            'video' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('job_id'));

        if (is_null($Job))
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        $Job->file_name = $request->input('video');
        $Job->file_type = 'video';
        $Job->save();

        return response()->json(['status' => 'success']);
    }

    public function delete(Request $request)
    {
        $validation = [
            'job_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('job_id'));

        if (is_null($Job))
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        if (!is_null($Job->file_name))
            Storage::delete(str_replace('/storage', '', $Job->file_name));

        $Job->delete();

        return response()->json(['status' => 'success']);
    }
}
