<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WithdrawMoney;
use App\BalanceLog;
use Validator;

class WithdrawController extends Controller
{
    public function withdraw()
    {
        return view('admin.withdraw', [
            'Withdraws' => WithdrawMoney::join('users', 'withdraw_money.user_id', '=', 'users.id')
                ->select('withdraw_money.*', 'users.name as user_name')
                ->orderBy('id', 'desc')->get()
        ]);
    }

    public function set_status(Request $request)
    {
        $validation = [
            'withdraw_id' => 'required',
            'status' => 'required|string|max:255',
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Withdraw = WithdrawMoney::find($request->input('withdraw_id'));

        if (is_null($Withdraw))
            return response()->json([
                'status' => 'error',
                'message' => 'Withdraw not found.'
            ]);

        $Withdraw->status = $request->input('status');
        $Withdraw->save();

        if ($Withdraw->status == 'canceled') {
            BalanceLog::updateBalance(false, false, $Withdraw->user_id, $Withdraw->sum);
        }

        return response()->json([
            'status' => $request->input('status'),
            'status' => 'success'
        ]);
    }
}
