<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;

class PaymentController extends Controller
{
    public function braintree_settings()
    {
        return view('admin.braintree_settings');
    }

    public function payments()
    {
        return view('admin.payments', [
            'Payments' => Payment::join('users', 'payments.user_id', '=', 'users.id')
                            ->select('payments.*', 'users.name as user_name')
                            ->orderBy('id', 'desc')->get()
        ]);
    }
}
