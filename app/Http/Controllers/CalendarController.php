<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Post;
use Validator;

class CalendarController extends Controller
{

    public function index($user_id) {
        if (in_array(User::isType($user_id), ['user', 'public_user', false]))
            return redirect(route('profile'));

        $user = User::find($user_id);

        if (is_null($user))
            return redirect(route('profile'));

        return view('user.calendar', [
            'Events' => Post::where('user_id', $user->id)->where('post_type', '!=', 'news')->where('status', 'post')->orderBy('id', 'asc')->get(),
            'user' => Auth::user(),
            'js' => [
                asset('/js/moment.min.js'),
                asset('/js/fullcalendar.min.js'),
                asset('/js/calendar.js')
            ],
            'css' => [
                asset('/css/fullcalendar.min.css')
            ]
        ]);
    }

}