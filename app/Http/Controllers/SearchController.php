<?php

namespace App\Http\Controllers;

use App\Post;
use App\Gallery;
use App\Video;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class SearchController extends Controller
{

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'required',
        ]);

        if ($validator->fails())
            return redirect(route('home'));

        $Posts = Post::where('title', 'like', '%' . $request->input('search') . '%')
            ->where('post_type', '!=', 'group_post')
            ->where('posts.status', 'post')
            ->orWhere('description', 'like', '%' . $request->input('search') . '%')
            ->where('post_type', '!=', 'group_post')
            ->where('posts.status', 'post')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
            ->orderBy('id', 'desc')
            ->paginate(10);

        $Photos = Gallery::where('title', 'like', '%' . $request->input('search') . '%')
            ->whereNull('group_id')
            ->orWhere('description', 'like', '%' . $request->input('search') . '%')
            ->whereNull('group_id')
            ->join('users', 'gallery.user_id', '=', 'users.id')
            ->select('gallery.*', 'users.name as user_name', 'users.avatar as user_avatar')
            ->orderBy('id', 'desc')
            ->paginate(10);

        $Videos = Video::where('title', 'like', '%' . $request->input('search') . '%')
            ->join('users', 'videos.user_id', '=', 'users.id')
            ->select('videos.*', 'users.name as user_name', 'users.avatar as user_avatar')
            ->orderBy('id', 'desc')
            ->paginate(10);

        $Category = \App\UserCategory::where('name', 'Business')->first();
        $Business = User::where('business', 1)
            ->where('name', 'like', '%' . $request->input('search') . '%')
            ->where('user_category_id', $Category->id)
            ->orderBy('id', 'desc')
            ->paginate(40);

        $Professionals = User::where('business', 1)
            ->where('name', 'like', '%' . $request->input('search') . '%')
            ->where('user_category_id', '!=', $Category->id)
            ->orderBy('id', 'desc')
            ->paginate(40);

        return view('guest.search', [
            'Posts' => $Posts,
            'Photos' => $Photos,
            'Videos' => $Videos,
            'Business' => $Business,
            'Professionals' => $Professionals,
            'search' => $request->input('search'),
            'user' => Auth::user(),
            'js' => [
                asset("/js/videos.js"),
                asset("/js/gallery.js")
            ]
        ]);
    }
}