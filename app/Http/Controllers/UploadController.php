<?php

namespace App\Http\Controllers;

use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Image;
use Storage;
use Validator;

class UploadController extends BaseController
{

    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'mimes:jpeg,png,avi,mp4,webm',
//            'file'=>'mimetypes:image/jpeg,image/png,image/gif,video/webm,video/mp4,video/avi'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $dir = ($request->input('dirname') != NULL) ? 'uploads/' . $request->input('dirname') . '/' : 'uploads/other/';
        $type = '';

        $uploads = array();
        if ($request->hasFile('file')) {
            if ($request->file('files') != NULL) {
                $files = $request->file('files');

                foreach ($files as $key => $file)
                    $uploads[] = '/storage/' . Storage::putFile($dir, $file);
            } else {
                $file = $request->file('file');
                $type = explode('/', $file->getClientMimeType());

                if (is_array($type) and count($type) == 2)
                    $type = $type[0];

                $uploads = '/storage/' . Storage::putFile($dir, $file);

                if ($type == 'image') {
                    $min_width = 600;
                    $min_height = 100;

                    $validator = Validator::make($request->all(), [
                        'file' => 'dimensions:min_width=' . $min_width . ',min_height=' . $min_height,
                    ]);

                    if ($validator->fails())
                        return response()->json([
                            'status' => 'error',
                            'message' => 'The file has invalid image dimensions. Minimum dimension: ' . $min_width . 'x' . $min_height . '.'
                        ]);
                }

                if ($type == 'video') {
                    $ffprobe = FFProbe::create([
                        'ffmpeg.binaries' => '/usr/bin/ffmpeg',
                        'ffprobe.binaries' => '/usr/bin/ffprobe'
                    ]);
                    $duration = round($ffprobe->format(public_path() . $uploads)->get('duration'));

                    $preview = '/storage/uploads/video_previews/' . uniqid('post_video_') . '.jpg';

                    $ffmpeg = FFMpeg::create([
                        'ffmpeg.binaries' => '/usr/bin/ffmpeg',
                        'ffprobe.binaries' => '/usr/bin/ffprobe'
                    ]);
                    $video = $ffmpeg->open(public_path() . $uploads);
                    $video->frame(TimeCode::fromSeconds(round($duration / 2)))->save(public_path() . $preview);
                }

                if (!is_null($request->input('preview'))) {

                    $img = Image::make(public_path($uploads));
                    $img->resize(250, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

                    $filename = md5(uniqid());

                    $img->save($storagePath . $dir . $filename . '.jpg');
                    $preview = '/storage/' . $dir . $filename . '.jpg';
                }
            }
        }

        return response()->json([
            'status' => 'success',
            'uploads' => $uploads,
            'preview' => isset($preview) ? $preview : NULL,
            'type' => $type
        ]);

    }

    public function cropp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'filename' => 'required|max:255',
            'data' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $data = $request->input('data');

        $img = Image::make(public_path() . $request->input('filename'));
        $img->crop(round($data["width"]), round($data["height"]), round($data["x"]), round($data["y"]));
        $img->save(public_path() . $request->input('filename'));

        return response()->json([
            'status' => 'success',
            'filename' => $request->input('filename')
        ]);
    }

}
