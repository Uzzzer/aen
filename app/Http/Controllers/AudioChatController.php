<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\AudioCall;
use App\DateConvert;
use Chat;
use Validator;

class AudioChatController extends Controller
{

    public function check_call(Request $request) {
        $AudioCall = AudioCall::where('user_2', Auth::user()->id)
                        ->where('status', 'call')
                        ->where('active_at', '>=', time() - 4)
                        ->first();

        if ($AudioCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        return response()->json([
            'status' => 'success',
            'user_id' => $AudioCall->user_1,
            'call_id' => $AudioCall->id,
            'user_name' => User::find($AudioCall->user_1)->name
        ]);
    }

    public function take_call(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $AudioCall = AudioCall::find($request->input('call_id'));

        if ($AudioCall == NULL or $AudioCall->status != 'call' or $AudioCall->active_at < time() - 4)
            return response()->json([
                'status' => 'error'
            ]);

        $AudioCall->status = 'speaking';
        $AudioCall->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function call(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => 'User ID is required.'
            ]);

        $User = User::find($request->input('user_id'));

        if ($User == NULL)
            return response()->json(['status' => 'error']);

        $AudioCall = AudioCall::where('user_1', Auth::user()->id)
                        ->where('status', 'speaking')
                        ->where('active_at', '>=', time() - 4)
                        ->orWhere('user_2', Auth::user()->id)
                        ->where('status', 'speaking')
                        ->where('active_at', '>=', time() - 4)
                        ->first();

        if ($AudioCall != NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'You already have an active call.'
            ]);

        $AudioCall = AudioCall::where('user_1', Auth::user()->id)
                        ->where('status', 'call')
                        ->where('user_1', Auth::user()->id)
                        ->where('user_2', $request->input('user_id'))
                        ->orWhere('user_1', $request->input('user_id'))
                        ->where('user_2', Auth::user()->id)
                        ->where('status', 'call')
                        ->first();

        if ($AudioCall != NULL) {
            if ($AudioCall->active_at > time() - 4) {
                $AudioCall->status = 'speaking';
                $AudioCall->save();

                return response()->json([
                    'status' => 'success',
                    'call_id' => $AudioCall->id,
                    'call_status' => 'speaking'
                ]);
            } else {
                $AudioCall->status = 'cancel';
                $AudioCall->save();
            }
        }

        $AudioCall = new AudioCall;
        $AudioCall->user_1 = Auth::user()->id;
        $AudioCall->user_2 = $request->input('user_id');
        $AudioCall->status = 'call';
        $AudioCall->active_at = time();
        $AudioCall->save();

        $conversation = Chat::conversations()->between(Auth::user()->id, intval($request->input('user_id')));
        if ($conversation == NULL)
            $conversation = Chat::createConversation([Auth::user()->id, intval($request->input('user_id'))]);

        $message = Chat::message('Incoming call')
            ->type('call')
            ->from(Auth::user())
            ->to($conversation)
            ->send();

        $html = '
            <div class="date-line">now</div>
            <div class="your-message-box box-msg incoming_call_mgs" data-id="'.$message->id.'">
                <p>
                    '.htmlspecialchars($message->body, ENT_QUOTES).'
                </p>
            </div>
        ';

        return response()->json([
            'status' => 'success',
            'call_id' => $AudioCall->id,
            'user_name' => $User->name,
            'html' => $html,
            'conversation_id' => $conversation->id,
            'user_id' => $User->id,
            'call_status' => 'call'
        ]);
    }

    public function check_incoming(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $AudioCall = AudioCall::find($request->input('call_id'));

        if ($AudioCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        if ($AudioCall->active_at < time() - 4 or $AudioCall->status != 'call')
            return response()->json([
                'status' => 'error'
            ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function cancel_call(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $AudioCall = AudioCall::find($request->input('call_id'));

        if ($AudioCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        $AudioCall->status = 'cancel';
        $AudioCall->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function update_active_call(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $AudioCall = AudioCall::find($request->input('call_id'));

        if ($AudioCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        if ($AudioCall->status == 'cancel')
            return response()->json([
                'status' => 'cancel'
            ]);

        if ($AudioCall->status == 'speaking')
            return response()->json([
                'status' => 'speaking'
            ]);

        $AudioCall->active_at = time();
        $AudioCall->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

}