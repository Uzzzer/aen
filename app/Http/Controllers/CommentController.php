<?php

namespace App\Http\Controllers;

use App\DateConvert;
use App\Gallery;
use App\Group;
use App\MediaComment;
use App\MediaCommentLike;
use App\Notification;
use App\Post;
use App\PostComment;
use App\PostCommentLike;
use App\PostLike;
use App\ProfileVideoComment;
use App\ProfileVideoCommentLike;
use App\PurchasedVideo;
use App\User;
use App\Video;
use App\VideoGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class CommentController extends Controller
{
    public function update(Request $request)
    {

        $updates = [
            'update' => [],
            'full_update' => [],
            'full_update_likes' => [],
            'full_update_photo' => [],
            'full_update_photo_likes' => [],
            'full_update_video' => [],
            'full_update_video_likes' => [],
            'full_update_profile_video' => [],
            'full_update_profile_video_likes' => []
        ];

        if ($request->input('comments_update') != NULL) {
            foreach ($request->input('comments_update') as $post_id) {
                $updates['update'][$post_id] = [
                    'count_comments' => PostComment::where('post_id', $post_id)->count(),
                    'count_likes' => PostLike::where('post_id', $post_id)->count()
                ];
            }
        }

        if ($request->input('comments_full_update') != NULL) {
            foreach ($request->input('comments_full_update') as $post_id) {
                $Comments = PostComment::where('post_comments.created_at', '>=', date('Y-m-d H:i:s', (time() - 10)))->where('post_comments.post_id', $post_id)->join('users', 'post_comments.user_id', '=', 'users.id')->select('post_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();

                foreach ($Comments as $Comment) {
                    $PostCommentLike = NULL;
                    if (Auth::check())
                        $PostCommentLike = PostCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                    $count_likes = PostCommentLike::where('comment_id', $Comment->id)->count();

                    $like_comment_class = ($PostCommentLike != NULL) ? 'liked' : '';
                    $count_likes = ($count_likes > 0) ? '(' . $count_likes . ')' : '';

                    $data_reply_id = ($Comment->reply_id != NULL) ? ' data-reply-id="' . $Comment->reply_id . '"' : '';
                    $image = ($Comment->image != NULL) ? '<a href="' . asset($Comment->image) . '" data-lightbox="image-' . $Comment->id . '"><img src="' . asset($Comment->image) . '"/></a>' : '';

                    $reply_auth = (Auth::check()) ? ' data-id="' . $Comment->id . '" data-post-id="' . $Comment->post_id . '"' : '';
                    $reply = ($Comment->reply_id == NULL) ? '<li class="reply_comment"' . $reply_auth . '>Reply</li>' : '';

                    $updates['full_update'][$post_id][$Comment->id] = '
                            <div class="coment_block" data-id="' . $Comment->id . '"' . $data_reply_id . '>
                                <div class="coment_photo_person">
                                    <a href="' . url('/profile/id/' . $Comment->user_id) . '"><img src="' . asset($Comment->user_avatar) . '" alt="' . $Comment->user_name . '" title="' . $Comment->user_name . '"/></a>
                                </div>
                                <div class="coment_right">
                                    <div class="coment_text">
                                        ' . $Comment->comment . '
                                        ' . $image . '
                                    </div>
                                    <ul class="coment_data">
                                        <li class="like_comment ' . $like_comment_class . '" data-id="' . $Comment->id . '">Like <span>' . $count_likes . '</span></li>
                                        ' . $reply . '
                                        <li>' . DateConvert::Convert($Comment->created_at) . '</li>
                                    </ul>
                                </div>
                            </div>
                    ';

                }

                $Comments = PostComment::where('post_comments.post_id', $post_id)->get();

                foreach ($Comments as $Comment) {
                    $updates['full_update_likes'][$post_id][$Comment->id] = PostCommentLike::where('comment_id', $Comment->id)->count();
                }
            }
        }

        if (intval($request->input('comments_full_photo')) != 0) {
            $Comments = MediaComment::where('media_comments.created_at', '>=', date('Y-m-d H:i:s', (time() - 10)))->where('media_comments.photo_id', intval($request->input('comments_full_photo')))->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();

            foreach ($Comments as $Comment) {
                $PostCommentLike = NULL;
                if (Auth::check())
                    $PostCommentLike = MediaCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                $count_likes = MediaCommentLike::where('comment_id', $Comment->id)->count();

                $like_comment_class = ($PostCommentLike != NULL) ? 'liked' : '';
                $count_likes = ($count_likes > 0) ? '(' . $count_likes . ')' : '';

                $data_reply_id = ($Comment->reply_id != NULL) ? ' data-reply-id="' . $Comment->reply_id . '"' : '';
                $image = ($Comment->image != NULL) ? '<a href="' . asset($Comment->image) . '" data-lightbox="image-' . $Comment->id . '"><img src="' . asset($Comment->image) . '"/></a>' : '';

                $reply_auth = (Auth::check()) ? ' data-id="' . $Comment->id . '" data-photo-id="' . $Comment->photo_id . '"' : '';
                $reply = ($Comment->reply_id == NULL) ? '<li class="reply_comment"' . $reply_auth . '>Reply</li>' : '';

                $updates['full_update_photo'][$Comment->id] = '
                        <div class="coment_block" data-id="' . $Comment->id . '"' . $data_reply_id . '>
                            <div class="coment_photo_person">
                                <a href="' . url('/profile/id/' . $Comment->user_id) . '"><img src="' . asset($Comment->user_avatar) . '" alt="' . $Comment->user_name . '" title="' . $Comment->user_name . '"/></a>
                            </div>
                            <div class="coment_right">
                                <div class="coment_text">
                                    ' . $Comment->comment . '
                                    ' . $image . '
                                </div>
                                <ul class="coment_data">
                                    <li data-media="1" class="like_comment ' . $like_comment_class . '" data-id="' . $Comment->id . '">Like <span>' . $count_likes . '</span></li>
                                    ' . $reply . '
                                    <li>' . DateConvert::Convert($Comment->created_at) . '</li>
                                </ul>
                            </div>
                        </div>
                ';

            }

            $Comments = MediaComment::where('media_comments.photo_id', intval($request->input('comments_full_photo')))->get();

            foreach ($Comments as $Comment) {
                $updates['full_update_photo_likes'][$Comment->id] = MediaCommentLike::where('comment_id', $Comment->id)->count();
            }

        }

        if (intval($request->input('comments_full_video')) != 0) {
            $Comments = MediaComment::where('media_comments.created_at', '>=', date('Y-m-d H:i:s', (time() - 10)))->where('media_comments.video_id', intval($request->input('comments_full_video')))->join('users', 'media_comments.user_id', '=', 'users.id')->select('media_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();

            foreach ($Comments as $Comment) {
                $PostCommentLike = NULL;
                if (Auth::check())
                    $PostCommentLike = MediaCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                $count_likes = MediaCommentLike::where('comment_id', $Comment->id)->count();

                $like_comment_class = ($PostCommentLike != NULL) ? 'liked' : '';
                $count_likes = ($count_likes > 0) ? '(' . $count_likes . ')' : '';

                $data_reply_id = ($Comment->reply_id != NULL) ? ' data-reply-id="' . $Comment->reply_id . '"' : '';
                $image = ($Comment->image != NULL) ? '<a href="' . asset($Comment->image) . '" data-lightbox="image-' . $Comment->id . '"><img src="' . asset($Comment->image) . '"/></a>' : '';

                $reply_auth = (Auth::check()) ? ' data-id="' . $Comment->id . '" data-video-id="' . $Comment->video_id . '"' : '';
                $reply = ($Comment->reply_id == NULL) ? '<li class="reply_comment"' . $reply_auth . '>Reply</li>' : '';

                $updates['full_update_video'][$Comment->id] = '
                        <div class="coment_block" data-id="' . $Comment->id . '"' . $data_reply_id . '>
                            <div class="coment_photo_person">
                                <a href="' . url('/profile/id/' . $Comment->user_id) . '"><img src="' . asset($Comment->user_avatar) . '" alt="' . $Comment->user_name . '" title="' . $Comment->user_name . '"/></a>
                            </div>
                            <div class="coment_right">
                                <div class="coment_text">
                                    ' . $Comment->comment . '
                                    ' . $image . '
                                </div>
                                <ul class="coment_data">
                                    <li data-media="1" class="like_comment ' . $like_comment_class . '" data-id="' . $Comment->id . '">Like <span>' . $count_likes . '</span></li>
                                    ' . $reply . '
                                    <li>' . DateConvert::Convert($Comment->created_at) . '</li>
                                </ul>
                            </div>
                        </div>
                ';

            }

            $Comments = MediaComment::where('media_comments.video_id', intval($request->input('comments_full_video')))->get();

            foreach ($Comments as $Comment) {
                $updates['full_update_video_likes'][$Comment->id] = MediaCommentLike::where('comment_id', $Comment->id)->count();
            }

        }

        if (intval($request->input('comments_full_profile_video')) != 0) {
            $Comments = ProfileVideoComment::where('video_comments.created_at', '>=', date('Y-m-d H:i:s', (time() - 10)))->where('video_comments.video_id', intval($request->input('comments_full_profile_video')))->join('users', 'video_comments.user_id', '=', 'users.id')->select('video_comments.*', 'users.name as user_name', 'users.avatar as user_avatar')->get();

            foreach ($Comments as $Comment) {
                $PostCommentLike = NULL;
                if (Auth::check())
                    $PostCommentLike = ProfileVideoCommentLike::where('comment_id', $Comment->id)->where('user_id', Auth::user()->id)->first();
                $count_likes = ProfileVideoCommentLike::where('comment_id', $Comment->id)->count();

                $like_comment_class = ($PostCommentLike != NULL) ? 'liked' : '';
                $count_likes = ($count_likes > 0) ? '(' . $count_likes . ')' : '';

                $data_reply_id = ($Comment->reply_id != NULL) ? ' data-reply-id="' . $Comment->reply_id . '"' : '';
                $image = ($Comment->image != NULL) ? '<a href="' . asset($Comment->image) . '" data-lightbox="image-' . $Comment->id . '"><img src="' . asset($Comment->image) . '"/></a>' : '';

                $reply_auth = (Auth::check()) ? ' data-id="' . $Comment->id . '" data-video-id="' . $Comment->video_id . '"' : '';
                $reply = ($Comment->reply_id == NULL) ? '<li class="reply_comment"' . $reply_auth . '>Reply</li>' : '';

                $updates['full_update_profile_video'][$Comment->id] = '
                        <div class="coment_block" data-id="' . $Comment->id . '"' . $data_reply_id . '>
                            <div class="coment_photo_person">
                                <a href="' . url('/profile/id/' . $Comment->user_id) . '"><img src="' . asset($Comment->user_avatar) . '" alt="' . $Comment->user_name . '" title="' . $Comment->user_name . '"/></a>
                            </div>
                            <div class="coment_right">
                                <div class="coment_text">
                                    ' . $Comment->comment . '
                                    ' . $image . '
                                </div>
                                <ul class="coment_data">
                                    <li data-profile-video="1" class="like_comment ' . $like_comment_class . '" data-id="' . $Comment->id . '">Like <span>' . $count_likes . '</span></li>
                                    ' . $reply . '
                                    <li>' . DateConvert::Convert($Comment->created_at) . '</li>
                                </ul>
                            </div>
                        </div>
                ';

            }

            $Comments = ProfileVideoComment::where('video_comments.video_id', intval($request->input('comments_full_profile_video')))->get();

            foreach ($Comments as $Comment) {
                $updates['full_update_profile_video_likes'][$Comment->id] = ProfileVideoCommentLike::where('comment_id', $Comment->id)->count();
            }

        }

        return response()->json([
            'status' => 'success',
            'updates' => $updates
        ]);
    }

    public function add(Request $request)
    {
        $UserType = User::isType();

        if (!$UserType or $UserType == 'public_user')
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        if (!is_null($request->input('profile_video_id')))
            $validator = Validator::make($request->all(), [
                'profile_video_id' => 'required',
                'comment' => 'max:500'
            ]);
        elseif (is_null($request->input('post_id')) and is_null($request->input('photo_id')))
            $validator = Validator::make($request->all(), [
                'video_id' => 'required',
                'comment' => 'max:500'
            ]);
        elseif (is_null($request->input('post_id')) and is_null($request->input('video_id')))
            $validator = Validator::make($request->all(), [
                'photo_id' => 'required',
                'comment' => 'max:500'
            ]);
        else
            $validator = Validator::make($request->all(), [
                'post_id' => 'required',
                'comment' => 'max:500'
            ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (!is_null($request->input('post_id'))) {
            $Post = Post::find($request->input('post_id'));

            if ($Post == NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Post not found'
                ]);

            $PostComment = new PostComment;
            $PostComment->post_id = $request->input('post_id');

            $text = 'Commented on your post "' . $Post->title . '"';

            if (!is_null($Post->group_id)) {
                $Group = Group::find($Post->group_id);
                $text .= (!is_null($Group)) ? ', in the group&nbsp;<a href="' . route('group', ['id' => $Group->id]) . '">' . $Group->name . '</a>.' : '.';
                $type_notice = 'comment_group_post';
            } else {
                $text .= '.';
                $type_notice = 'comment_post';
            }

            $user_id_notice = $Post->user_id;
        }

        if (!is_null($request->input('photo_id'))) {
            $Gallery = Gallery::find($request->input('photo_id'));

            if ($Gallery == NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Photo not found'
                ]);

            $PostComment = new MediaComment;
            $PostComment->photo_id = $request->input('photo_id');

            $text = 'Commented on your photo&nbsp;<a href="';
            $text .= (!is_null($Gallery->group_id))
                ? route('photo_group', ['id' => $Gallery->group_id, 'pid' => $Gallery->id]) . '">'
                : route('profile_photo', ['id' => $Gallery->user_id, 'pid' => $Gallery->id]) . '">';

            if (!is_null($Gallery->group_id)) {
                $Group = Group::find($Gallery->group_id);
                $text .= (!is_null($Gallery->title)) ? $Gallery->title . '</a>' : 'No title</a>';
                $text .= (!is_null($Group)) ? ', in the group&nbsp;<a href="' . route('group', ['id' => $Group->id]) . '">' . $Group->name . '</a>.' : '.';
                $type_notice = 'comment_group_photo';
            } else {
                $text .= (!is_null($Gallery->title)) ? $Gallery->title . '</a>.' : 'No title</a>.';
                $type_notice = 'comment_photo';
            }

            $user_id_notice = $Gallery->user_id;
        }

        if (!is_null($request->input('video_id'))) {
            $VideoGallery = VideoGallery::find($request->input('video_id'));

            if ($VideoGallery == NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Video not found'
                ]);

            $PostComment = new MediaComment;
            $PostComment->video_id = $request->input('video_id');

            $text = 'Commented on your video&nbsp;<a href="' . route('video_group', ['id' => $VideoGallery->group_id, 'vid' => $PostComment->video_id]) . '">';
            $text .= (is_null($VideoGallery->title)) ? $VideoGallery->title . '</a>' : 'No title</a>';


            $Group = Group::find($VideoGallery->group_id);
            $text .= (!is_null($Group)) ? ', in the group&nbsp;<a href="' . route('group', ['id' => $Group->id]) . '">' . $Group->name . '</a>.' : '.';
            $type_notice = 'comment_group_video';

            $user_id_notice = $VideoGallery->user_id;
        }

        if (!is_null($request->input('profile_video_id'))) {
            $Video = Video::find($request->input('profile_video_id'));

            if ($Video == NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Video not found'
                ]);

            if (!PurchasedVideo::checkPurchase($Video->id))
                return response()->json([
                    'status' => 'error',
                    'message' => 'You must buy this video.'
                ]);

            $PostComment = new ProfileVideoComment;
            $PostComment->video_id = $request->input('profile_video_id');

            $text = 'Commented on your video&nbsp;<a href="' . route('profile_video', ['id' => $Video->id]) . '">';
            $text .= (is_null($Video->title)) ? $Video->title . '</a>' : 'No title</a>';


            $type_notice = 'comment_video';
            $user_id_notice = $Video->user_id;
        }

        $PostComment->user_id = Auth::user()->id;
        $PostComment->comment = $request->input('comment');
        $PostComment->reply_id = $request->input('comment_id');
        $PostComment->image = $request->input('image');
        $PostComment->save();

        $data_reply_id = ($PostComment->reply_id != NULL) ? ' data-reply-id="' . $PostComment->reply_id . '"' : '';
        $image = ($PostComment->image != NULL) ? '<a href="' . asset($PostComment->image) . '" data-lightbox="image-' . $PostComment->id . '"><img src="' . asset($PostComment->image) . '"/></a>' : '';

        $data_id = (!is_null($request->input('post_id'))) ? 'data-post-id' : ((!is_null($request->input('photo_id'))) ? 'data-photo-id' : 'data-video-id');
        $data_media = (!is_null($request->input('post_id'))) ? ''
            : ((!is_null($request->input('profile_video_id')))
                ? 'data-profile-video="1"'
                : 'data-media="1"');

        $html = '
            <div class="coment_block" data-id="' . $PostComment->id . '"' . $data_reply_id . '>
                <div class="coment_photo_person">
                    <a href="' . url('/profile/id/' . $PostComment->user_id) . '"><img src="' . asset(Auth::user()->avatar) . '" alt="' . Auth::user()->name . '" title="' . Auth::user()->name . '"/></a>
                </div>
                <div class="coment_right">
                    <div class="coment_text">
                        ' . $PostComment->comment . '
                        ' . $image . '
                    </div>
                    <ul class="coment_data">
                        <li ' . $data_media . ' class="like_comment" data-id="' . $PostComment->id . '">Like <span></span></li>
                        <li class="reply_comment" data-id="' . $PostComment->id . '" ' . $data_id . '="' . $PostComment->post_id . '">Reply</li>
                        <li>now</li>
                    </ul>
                </div>
            </div>
        ';

        $html_single = '';

        if (isset($Post))
            $html_single = view('parts.comment_single', [
                'Comment' => $PostComment,
                'Post' => $Post
            ])->render();

        $param = [
            'user_id' => $PostComment->user_id,
            'text' => $text
        ];

        Notification::checkAndCreateNotification($user_id_notice, $type_notice, $param);

        $count_comment = 0;

        if (!is_null($request->input('post_id')))
            $count_comment = PostComment::where('post_id', $PostComment->post_id)->count();

        if (!is_null($request->input('photo_id')))
            $count_comment = MediaComment::where('photo_id', $PostComment->photo_id)->count();

        if (!is_null($request->input('profile_video_id')))
            $count_comment = ProfileVideoComment::where('video_id', $PostComment->video_id)->count();

        $post_id = null;

        if (!is_null($request->input('post_id')))
            $post_id = $PostComment->post_id;

        if (!is_null($request->input('photo_id')))
            $post_id = $PostComment->photo_id;

        if (!is_null($request->input('profile_video_id')))
            $post_id = $PostComment->video_id;

        return response()->json([
            'status' => 'success',
            'count_comment' => $count_comment,
            'post_id' => $post_id,
            'reply_id' => $PostComment->reply_id,
            'html' => $html,
            'html_single' => $html_single
        ]);
    }

    public function like(Request $request)
    {
        if (!is_null($request->input('media_id')))
            $validator = Validator::make($request->all(), [
                'media_id' => 'required',
            ]);
        elseif (!is_null($request->input('profile_video_id')))
            $validator = Validator::make($request->all(), [
                'profile_video_id' => 'required',
            ]);
        else
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (!is_null($request->input('id'))) {
            $PostComment = PostComment::find($request->input('id'));

            if ($PostComment == NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Comment not found'
                ]);

            $PostCommentLike = PostCommentLike::where('comment_id', $request->input('id'))->where('user_id', Auth::user()->id)->first();
        }

        if (!is_null($request->input('media_id'))) {
            $PostComment = MediaComment::find($request->input('media_id'));

            if ($PostComment == NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Comment not found'
                ]);

            $PostCommentLike = MediaCommentLike::where('comment_id', $request->input('media_id'))->where('user_id', Auth::user()->id)->first();
        }

        if (!is_null($request->input('profile_video_id'))) {
            $PostComment = ProfileVideoComment::find($request->input('profile_video_id'));

            if ($PostComment == NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'Comment not found'
                ]);

            $PostCommentLike = ProfileVideoCommentLike::where('comment_id', $request->input('profile_video_id'))->where('user_id', Auth::user()->id)->first();
        }

        if ($PostCommentLike != NULL) {
            $PostCommentLike->delete();
        } else {
            if (!is_null($request->input('id'))) {
                $PostCommentLike = new PostCommentLike;
                $PostCommentLike->comment_id = $request->input('id');
            }

            if (!is_null($request->input('media_id'))) {
                $PostCommentLike = new MediaCommentLike;
                $PostCommentLike->comment_id = $request->input('media_id');
            }

            if (!is_null($request->input('profile_video_id'))) {
                $PostCommentLike = new ProfileVideoCommentLike;
                $PostCommentLike->comment_id = $request->input('profile_video_id');
            }

            $PostCommentLike->user_id = Auth::user()->id;
            $PostCommentLike->save();
        }

        $count_like = 0;

        if (!is_null($request->input('id')))
            $count_like = PostCommentLike::where('comment_id', $request->input('id'))->count();

        if (!is_null($request->input('profile_video_id')))
            $count_like = ProfileVideoCommentLike::where('comment_id', $request->input('profile_video_id'))->count();

        if (!is_null($request->input('media_id')))
            $count_like = MediaCommentLike::where('comment_id', $request->input('media_id'))->count();

        return response()->json([
            'status' => 'success',
            'count_like' => $count_like
        ]);
    }

}