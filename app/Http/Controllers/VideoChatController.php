<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\VideoCall;
use App\DateConvert;
use Chat;
use Validator;

class VideoChatController extends Controller
{

    public function check_call(Request $request) {
        $VideoCall = VideoCall::where('user_2', Auth::user()->id)
                        ->where('status', 'call')
                        ->where('active_at', '>=', time() - 4)
                        ->first();

        if ($VideoCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        return response()->json([
            'status' => 'success',
            'user_id' => $VideoCall->user_1,
            'call_id' => $VideoCall->id,
            'user_name' => User::find($VideoCall->user_1)->name
        ]);
    }

    public function take_call(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $VideoCall = VideoCall::find($request->input('call_id'));

        if ($VideoCall == NULL or $VideoCall->status != 'call' or $VideoCall->active_at < time() - 4)
            return response()->json([
                'status' => 'error'
            ]);

        $VideoCall->status = 'speaking';
        $VideoCall->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function call(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => 'User ID is required.'
            ]);

        $User = User::find($request->input('user_id'));

        if ($User == NULL)
            return response()->json(['status' => 'error']);

        $VideoCall = VideoCall::where('user_1', Auth::user()->id)
                        ->where('status', 'speaking')
                        ->where('active_at', '>=', time() - 4)
                        ->orWhere('user_2', Auth::user()->id)
                        ->where('status', 'speaking')
                        ->where('active_at', '>=', time() - 4)
                        ->first();

        if ($VideoCall != NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'You already have an active call.'
            ]);

        $VideoCall = VideoCall::where('user_1', Auth::user()->id)
                        ->where('status', 'call')
                        ->where('user_1', Auth::user()->id)
                        ->where('user_2', $request->input('user_id'))
                        ->orWhere('user_1', $request->input('user_id'))
                        ->where('user_2', Auth::user()->id)
                        ->where('status', 'call')
                        ->first();

        if ($VideoCall != NULL) {
            if ($VideoCall->active_at > time() - 4) {
                $VideoCall->status = 'speaking';
                $VideoCall->save();

                return response()->json([
                    'status' => 'success',
                    'call_id' => $VideoCall->id,
                    'call_status' => 'speaking'
                ]);
            } else {
                $VideoCall->status = 'cancel';
                $VideoCall->save();
            }
        }

        $VideoCall = new VideoCall;
        $VideoCall->user_1 = Auth::user()->id;
        $VideoCall->user_2 = $request->input('user_id');
        $VideoCall->status = 'call';
        $VideoCall->active_at = time();
        $VideoCall->save();

        $conversation = Chat::conversations()->between(Auth::user()->id, intval($request->input('user_id')));
        if ($conversation == NULL)
            $conversation = Chat::createConversation([Auth::user()->id, intval($request->input('user_id'))]);

        $message = Chat::message('Incoming call')
            ->type('call')
            ->from(Auth::user())
            ->to($conversation)
            ->send();

        $html = '
            <div class="date-line">now</div>
            <div class="your-message-box box-msg incoming_call_mgs" data-id="'.$message->id.'">
                <p>
                    '.htmlspecialchars($message->body, ENT_QUOTES).'
                </p>
            </div>
        ';

        return response()->json([
            'status' => 'success',
            'call_id' => $VideoCall->id,
            'user_name' => $User->name,
            'html' => $html,
            'conversation_id' => $conversation->id,
            'user_id' => $User->id,
            'call_status' => 'call'
        ]);
    }

    public function check_incoming(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $VideoCall = VideoCall::find($request->input('call_id'));

        if ($VideoCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        if ($VideoCall->active_at < time() - 4 or $VideoCall->status != 'call')
            return response()->json([
                'status' => 'error'
            ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function cancel_call(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $VideoCall = VideoCall::find($request->input('call_id'));

        if ($VideoCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        $VideoCall->status = 'cancel';
        $VideoCall->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function update_active_call(Request $request) {
        $validator = Validator::make($request->all(), [
            'call_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error'
            ]);

        $VideoCall = VideoCall::find($request->input('call_id'));

        if ($VideoCall == NULL)
            return response()->json([
                'status' => 'error'
            ]);

        if ($VideoCall->status == 'cancel')
            return response()->json([
                'status' => 'cancel'
            ]);

        if ($VideoCall->status == 'speaking')
            return response()->json([
                'status' => 'speaking'
            ]);

        $VideoCall->active_at = time();
        $VideoCall->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

}