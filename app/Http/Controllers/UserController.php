<?php

namespace App\Http\Controllers;

use App\BalanceLog;
use App\ConnectionRequest;
use App\Gallery;
use App\Notification;
use App\NotificationOffGroup;
use App\NotificationOn;
use App\NotificationType;
use App\Option;
use App\Post;
use App\ProfileVideoLike;
use App\ProfileVideoView;
use App\PurchasedVideo;
use App\Traits\HoroscopeTrait;
use App\Traits\SendMail;
use App\User;
use App\UserCategory;
use App\UsersCategoriesAvailable;
use App\Video;
use App\VideoTag;
use App\WithdrawMoney;
use App\WorksWith;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Validator;


use App\PaymentContent as PaymentContent;

class UserController extends Controller
{
    use SendMail;

    public function dashboard()
    {
        return view('user.dashboard');
    }

    public function add_tweet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $User = Auth::user();

        if (!$User->business)
            return response()->json([
                'status' => 'error'
            ]);

        $User->twitter = $request->input('url');
        $User->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function add_insta_post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $User = Auth::user();

        if (!$User->business)
            return response()->json([
                'status' => 'error'
            ]);

        $User->instagram = $request->input('url');
        $User->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function add_category(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $User = Auth::user();

        /*if (!$User->business)
            return response()->json([
                'status' => 'error'
            ]);*/

        if (!is_null(UsersCategoriesAvailable::where('user_id', $User->id)->where('name', $request->input('name'))->first()))
            return response()->json([
                'status' => 'error',
                'message' => 'You already have a category with that name.'
            ]);

        $UsersCategoriesAvailable = new UsersCategoriesAvailable;
        $UsersCategoriesAvailable->user_id = $User->id;
        $UsersCategoriesAvailable->name = $request->input('name');
        $UsersCategoriesAvailable->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function balance_log()
    {
        $User = Auth::user();

        if (!$User->business)
            return redirect(route('profile'));

        return view('user.balance_log', [
            'user' => $User,
            'BalanceLogs' => BalanceLog::where('balance_logs.user_id', $User->id)
                ->join('users', 'balance_logs.customer_id', '=', 'users.id')
                ->select('balance_logs.*', 'users.name as user_name')
                ->orderby('balance_logs.id', 'desc')
                ->get(),
            'total' => BalanceLog::where('balance_logs.user_id', $User->id)
                ->sum('sum'),
            'css' => [
                '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css'
            ],
            'js' => [
                '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js',
                asset('js/balance_log.js')
            ]
        ]);
    }

    public function withdraw_money_page()
    {
        $User = Auth::user();

        if (!$User->business)
            return redirect(route('profile'));

        return view('user.withdraw_money', [
            'user' => $User,
            'WithdrawLogs' => WithdrawMoney::where('user_id', $User->id)->orderBy('id', 'desc')->get(),
            'css' => [
                '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css'
            ],
            'js' => [
                '//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js',
                asset('js/withdraw_money.js')
            ]
        ]);
    }

    public function withdraw_money(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sum' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $User = Auth::user();

        if (!$User->business)
            return response()->json([
                'status' => 'error'
            ]);

        if (is_null($User->cc_number) or !strlen($User->cc_number))
            return response()->json([
                'status' => 'error',
                'message' => 'You need to fill out a credit card number. To do this, go to the settings.'
            ]);

        if ($request->input('sum') < Option::option('min_withdraw'))
            return response()->json([
                'status' => 'error',
                'message' => 'Minimum sum ' . Option::option('min_withdraw') . ' ' . Option::option('currency') . '.'
            ]);

        if ($User->balance < $request->input('sum'))
            return response()->json([
                'status' => 'error',
                'message' => 'Not enough funds on balance..'
            ]);

        $User->balance -= $request->input('sum');
        $User->save();

        $WithdrawMoney = new WithdrawMoney;
        $WithdrawMoney->user_id = $User->id;
        $WithdrawMoney->sum = $request->input('sum');
        $WithdrawMoney->currency = Option::option('currency');
        $WithdrawMoney->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function subscription()
    {
        $User = Auth::user();

        return view('user.subscription', [
            'user' => $User,
            'UserCategory' => UserCategory::where('id', $User->user_category_id)->first()
        ]);
    }

    public function profile_video_search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Videos = Video::getSearchVideosByUser($request->input('user_id'), $request->input('search'), 10);
        $posts = [];

        $user = User::find($request->input('user_id'));

        foreach ($Videos as $Post)
            $posts[] = view('includes/profile-video', ['Video' => $Post, 'user' => $user])->render();

        return response()->json([
            'status' => 'success',
            'posts' => $posts
        ]);
    }

    public function profile_video_add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file_name' => 'required',
            'Title' => 'required|max:255',
            'Content' => 'required',
            'Tags' => 'max:1000',
            'Price' => 'required|integer',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Video = new Video;
        $Video->user_id = Auth::user()->id;
        $Video->file_name = $request->input('file_name');

        $ffprobe = FFProbe::create([
            'ffmpeg.binaries' => '/usr/bin/ffmpeg',
            'ffprobe.binaries' => '/usr/bin/ffprobe'
        ]);
        $Video->time = round($ffprobe->format(public_path() . $Video->file_name)->get('duration'));
        $Video->preview_image = '/storage/uploads/video_previews/' . uniqid('user_video_') . '.jpg';

        $ffmpeg = FFMpeg::create([
            'ffmpeg.binaries' => '/usr/bin/ffmpeg',
            'ffprobe.binaries' => '/usr/bin/ffprobe'
        ]);
        $video = $ffmpeg->open(public_path() . $Video->file_name);
        $video->frame(TimeCode::fromSeconds(round($Video->time / 2)))->save(public_path() . $Video->preview_image);

        $Video->title = $request->input('Title');
        $Video->price = $request->input('Price');
        $Video->content = json_encode($request->input('Content'));
        $Video->save();

        if (!is_null($request->input('Tags'))) {
            $tags = explode(',', $request->input('Tags'));
            if (count($tags))
                foreach ($tags as $tag) {
                    $tag = trim($tag);
                    if (strlen($tag)) {
                        $Tag = new VideoTag;
                        $Tag->tag = $tag;
                        $Tag->video_id = $Video->id;
                        $Tag->save();
                    }
                }
        }

        return response()->json([
            'status' => 'success',
            'html' => view('includes.profile-video-setting', ['Video' => $Video])->render()
        ]);
    }

    public function profile_video_delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Video = Video::find($request->input('id'));

        if (is_null($Video))
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found.'
            ]);

        if ($Video->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        VideoTag::where('video_id', $Video->id)->delete();
        $Video->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function notifications()
    {
        $Notifications = Notification::where('user_id', Auth::user()->id)->limit(10)->orderBy('id', 'desc')->get();
        Notification::readNotifications($Notifications);

        $NotificationOn = NotificationOn::where('user_id', Auth::user()->id)->get();

        $NotificationOnArray = [];
        foreach ($NotificationOn as $on)
            $NotificationOnArray[] = $on->type_id;

        return view('user.notifications', [
            'user' => Auth::user(),
            'NotificationOn' => $NotificationOnArray,
            'NotificationTypes' => NotificationType::get(),
            'Notifications' => $Notifications
        ]);
    }

    public function setting_notifications()
    {
        $NotificationOn = NotificationOn::where('user_id', Auth::user()->id)->get();

        $NotificationOnArray = [];
        foreach ($NotificationOn as $on)
            $NotificationOnArray[] = $on->type_id;

        return view('user.setting_notifications', [
            'user' => Auth::user(),
            'NotificationOn' => $NotificationOnArray,
            'NotificationTypes' => NotificationType::get()
        ]);
    }

    public function save_notifications_setting(Request $request)
    {
        if ($request->input('group_id') != NULL) {
            NotificationOffGroup::where('user_id', Auth::user()->id)->where('group_id', $request->input('group_id'))->delete();

            $ids = [1, 2, 3];
            if (is_null($request->input('NotificationOff')))
                $NotificationOff = [];
            else
                $NotificationOff = $request->input('NotificationOff');

            foreach ($ids as $id) {
                if (!in_array($id, $NotificationOff)) {
                    $NotificationOffGroup = new NotificationOffGroup;
                    $NotificationOffGroup->user_id = Auth::user()->id;
                    $NotificationOffGroup->type_id = $id;
                    $NotificationOffGroup->group_id = $request->input('group_id');
                    $NotificationOffGroup->save();
                }
            }
        } else {
            $ids = [];
            if (!is_null($request->input('NotificationType')) and is_array($request->input('NotificationType')))
                foreach ($request->input('NotificationType') as $type) {
                    $NotificationOn = new NotificationOn;
                    $NotificationOn->user_id = Auth::user()->id;
                    $NotificationOn->type_id = $type;
                    $NotificationOn->save();
                    $ids[] = $NotificationOn->id;
                }

            NotificationOn::where('user_id', Auth::user()->id)->whereNotIn('id', $ids)->delete();
        }

        return response()->json(['status' => 'success']);
    }

    public function my_profile()
    {
        $User = Auth::user();
        $view = ($User->business) ? 'profile-business' : 'profile';

        return view('user.' . $view, [
            'user' => $User
        ]);
    }

    public function profile($id)
    {
    
    	$auth_id = auth()->user()->id;
        $User = User::find($id);
        if (is_null($User))
            return redirect(route('home'));

        $view = ($User->business) ? 'profile-business' : 'profile';

		$GalleryPhotos = \App\Gallery::where('user_id', $User->id)->whereNull('group_id')->get();
		foreach($GalleryPhotos as $ga){
			$np = false;
			if((float)$ga->price>0){
				$bought = \App\PaymentContent::where(["user_id"=>$auth_id, "obj_id"=>$ga->id])->get()->first();
				if(!$bought){
					$np = true;
				}

				if ($ga->user_id == $auth_id) {
                    $np = false;
                }
			}
			$ga->need_payment = $np;
		}            


        return view('user.' . $view, [
            'user' => $User,
            "GalleryPhotos"=>$GalleryPhotos,
            "newGallery"=>$GalleryPhotos
        ]);
    }

    public function connection_requests()
    {
        return view('user.connection_requests', [
            'ConnectionRequests' => ConnectionRequest::where('user_id_2', Auth::user()->id)->where('connections.status', 'request')->join('users', 'connections.user_id_1', '=', 'users.id')->select('connections.*', 'users.name as user_name', 'users.avatar as user_avatar')->get(),
            'user' => Auth::user()
        ]);
    }

    public function connections($user_id = false)
    {
        if ($user_id) {
            $User = User::find($user_id);

            if (is_null($User))
                return redirect(route('profile'));
        } else
            $User = Auth::user();

        if ($User->id != Auth::user()->id)
            if ($User->status == 'blocked' or (in_array(User::isType($User->id), ['business']) and !ConnectionRequest::CheckConnectionApprove($User->id) and $User->id != Auth::user()->id))
                return redirect(route('id_profile', ['id' => $User->id]));

        return view('user.connections', [
            'Connections' => ConnectionRequest::where('user_id_1', $User->id)->where('connections.status', 'approved')->orWhere('user_id_2', $User->id)->where('connections.status', 'approved')->leftjoin('users', function ($join) use ($User) {
                $join->on('connections.user_id_1', '=', 'users.id')
                    ->where('users.id', '!=', $User->id)
                    ->orOn('connections.user_id_2', '=', 'users.id')
                    ->where('users.id', '!=', $User->id);
            })->select('connections.*', 'users.name as user_name', 'users.avatar as user_avatar', 'users.id as user_id')->get(),
            'user' => $User
        ]);
    }

    public function worked_with($user_id = false)
    {
        if (in_array(User::isType($user_id), ['user', 'public_user', false]))
            return redirect(route('profile'));

        $User = User::find($user_id);

        if (is_null($User))
            return redirect(route('profile'));

        if ($User->status == 'blocked' or (in_array(User::isType($User->id), ['business']) and !ConnectionRequest::CheckConnectionApprove($User->id) and $User->id != Auth::user()->id))
            return redirect(route('id_profile', ['id' => $User->id]));

        return view('user.worked_with', [
            'WorkedWith' => WorksWith::where('business_id', $User->id)->join('users', 'works_with.user_id', '=', 'users.id')->select('users.*')->get(),
            'user' => $User
        ]);
    }

    public function works_with($user_id = false)
    {
        $user = User::find($user_id);

        if (is_null($user) or $user->business == 1)
            return redirect(route('profile'));

        return view('user.works_with', [
            'WorksWith' => WorksWith::where('user_id', $user->id)->join('users', 'works_with.business_id', '=', 'users.id')->select('users.*')->get(),
            'user' => $user
        ]);
    }

    public function profile_editing()
    {
        $User = Auth::user();

        $view = ($User->business) ? 'profile-editing-business' : 'profile-editing';
        $js = ($User->business) ? 'profile-editing-business' : 'profile-editing';

        return view('user.' . $view, [
            'user' => $User,
            'Videos' => Video::where('user_id', $User->id)->orderBy('id', 'desc')->get(),
            'js' => [
                asset('js/' . $js . '.js')
            ]
        ]);
    }

    public function draft()
    {
        $User = Auth::user();

        if (in_array(User::isType($User->id), ['user', 'public_user', false]))
            return redirect(route('profile'));

        if (is_null($User))
            return redirect(route('profile'));

        return view('user.drafts', [
            'user' => $User,
            'Drafts' => Post::getDraftsByUser($User->id)
        ]);
    }

    public function news_and_events($user_id = false)
    {
        if ($user_id == false)
            $user_id = Auth::user()->id;

        if (in_array(User::isType($user_id), ['user', 'public_user', false]))
            return redirect(route('profile'));

        $User = User::find($user_id);

        if (is_null($User))
            return redirect(route('profile'));

        if ($User->status == 'blocked' or (in_array(User::isType($User->id), ['business']) and !ConnectionRequest::CheckConnectionApprove($User->id) and $User->id != Auth::user()->id))
            return redirect(route('id_profile', ['id' => $User->id]));

        return view('user.news_and_events', [
            'user' => $User,
            'Posts' => Post::getPostsByUser($user_id)
        ]);
    }

    public function profile_videos($user_id = false)
    {
        if ($user_id == false)
            $user_id = Auth::user()->id;

        if (in_array(User::isType($user_id), ['user', 'public_user', false]))
            return redirect(route('profile'));

        $User = User::find($user_id);

        if (is_null($User))
            return redirect(route('profile'));

        if ($User->status == 'blocked' or (in_array(User::isType($User->id), ['business']) and !ConnectionRequest::CheckConnectionApprove($User->id) and $User->id != Auth::user()->id))
            return redirect(route('id_profile', ['id' => $User->id]));

        return view('user.profile-videos', [
            'user' => User::find($user_id),
            'Videos' => Video::where('videos.user_id', $User->id)->orderBy('videos.id', 'desc')->get(),
            'js' => [
                asset('/js/videos.js')
            ]
        ]);
    }

    public function profile_video($video_id)
    {
        $Video = Video::find($video_id);

        if (is_null($Video) or !PurchasedVideo::checkPurchase($Video->id))
            return redirect(route('profile'));

        ProfileVideoView::viewVideo($Video->id);

        return view('user.profile-video', [
            'user' => User::find($Video->user_id),
            'Video' => $Video,
            'js' => [
                asset('/js/videos.js')
            ]
        ]);
    }

    public function purchased_videos()
    {
        return view('user.purchased-videos', [
            'user' => Auth::user(),
            'Videos' => PurchasedVideo::where('purchased_videos.user_id', Auth::user()->id)
                ->join('videos', 'purchased_videos.video_id', '=', 'videos.id')
                ->join('users', 'videos.user_id', '=', 'users.id')
                ->orderBy('purchased_videos.id', 'desc')
                ->select('purchased_videos.*', 'videos.user_id as video_user_id', 'videos.title', 'videos.time', 'videos.file_name', 'videos.preview_image', 'videos.content', 'videos.created_at as video_created_at', 'users.name as video_user_name')
                ->get(),
            'js' => [
                asset('/js/videos.js')
            ]
        ]);
    }

    public function like_video(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Video = Video::find($request->input('id'));

        if ($Video == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found.'
            ]);

        if (!PurchasedVideo::checkPurchase($Video->id))
            return response()->json([
                'status' => 'error',
                'message' => 'You must buy this video.'
            ]);

        $VideoLike = ProfileVideoLike::where('video_id', $request->input('id'))->where('user_id', Auth::user()->id)->first();

        if ($VideoLike != NULL) {
            $VideoLike->delete();
        } else {
            $VideoLike = new ProfileVideoLike;
            $VideoLike->video_id = $request->input('id');
            $VideoLike->user_id = Auth::user()->id;
            $VideoLike->save();
        }

        return response()->json([
            'status' => 'success',
            'count_like' => ProfileVideoLike::where('video_id', $request->input('id'))->count()
        ]);
    }

    public function profile_photos($user_id = false)
    {
    
        if ($user_id == false)
            $user_id = Auth::user()->id;

        if (in_array(User::isType($user_id), ['user', 'public_user', false]))
            return redirect(route('profile'));

        $User = User::find($user_id);

        if (is_null($User))
            return redirect(route('profile'));

        if ($User->status == 'blocked' or (in_array(User::isType($User->id), ['business']) and !ConnectionRequest::CheckConnectionApprove($User->id) and $User->id != Auth::user()->id))
            return redirect(route('id_profile', ['id' => $User->id]));


		$auth_id = Auth::user()->id;

		$gallery = Gallery::where('user_id', $user_id)->whereNull('group_id')->orderBy('id', 'desc')->get();
		foreach($gallery as $ga){
			$np = false;
			if((float)$ga->price>0){
				$bought = PaymentContent::where(["user_id"=>$auth_id, "obj_id"=>$ga->id])->get()->first();
				if(!$bought){
					$np = true;
				}

                if ($ga->user_id == $auth_id) {
                    $np = false;
                }
			}
		
			$ga->need_payment = $np;
		}
		

        return view('user.profile-photos', [
            'Gallery' => $gallery,
            'newGallery' => $gallery,
            'user' => $User,
            'js' => [
                asset('/js/gallery.js')
            ]
        ]);
    }

    public function profile_photo($id, $pid)
    {
        $User = User::find($id);
        $Gallery = Gallery::find($pid);

        $auth_id = Auth::user()->id;

        $np = false;
        if((float)$Gallery->price>0){
            $bought = \App\PaymentContent::where(["user_id"=>$auth_id, "obj_id"=>$Gallery->id])->get()->first();
            if(!$bought){
                $np = true;
            }

            if ($Gallery->user_id == $auth_id) {
                $np = false;
            }
        }

        $Gallery->need_payment = $np;

        if (in_array(User::isType(), ['public_user', false]) or $Gallery->need_payment)
            return redirect(route('profile'));

        if (is_null($User) or is_null($Gallery) or $Gallery->user_id != $User->id or $Gallery->group_id != NULL)
            return redirect(route('profile'));

        \App\MediaView::viewPhoto($Gallery->id);

        return view('user.profile-photo', [
            'Gallery' => $Gallery,
            'user' => $User,
            'token' => $bought->token ?? false,
            'js' => [asset('/js/gallery.js')]
        ]);
    }

    public function social($id)
    {
        if (in_array(User::isType($id), ['user', 'public_user', false]))
            return redirect(route('profile'));

        $User = User::find($id);

        if (is_null($User))
            return redirect(route('profile'));

        if ($User->status == 'blocked' or (in_array(User::isType($User->id), ['business']) and !ConnectionRequest::CheckConnectionApprove($User->id) and $User->id != Auth::user()->id))
            return redirect(route('id_profile', ['id' => $User->id]));

        $twitter = false;
        $instagram = false;

        if ($User->twitter) {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://publish.twitter.com/oembed?url=" . $User->twitter);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = json_decode(curl_exec($ch), true);
            curl_close($ch);

            if (!isset($output["errors"]) and isset($output["html"])) {
                $twitter = $output["html"];
            }
        }

        if ($User->instagram) {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api.instagram.com/oembed/?url=" . $User->instagram);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = json_decode(curl_exec($ch), true);
            curl_close($ch);

            if (!is_null($output) and isset($output["html"])) {
                $instagram = $output["html"];
            }
        }

        return view('user.social', [
            'user' => $User,
            'js' => [
                asset('/js/chat_page.js')
            ],
            'twitter' => $twitter,
            'instagram' => $instagram
        ]);
    }

    public function update_business(Request $request)
    {
        $validation = [
            'name' => 'required|string|max:32',
            'location' => 'required|string|max:255',
            'about_me' => 'max:1000',
            'avatar' => 'required',
            'paypal' => is_null($request->input('paypal')) ? '' : 'email',
            'nationality' => 'max:255',
            'birthday' => is_null($request->input('birthday')) ? '' : '',
            'profession' => 'max:255',
            'experience' => is_null($request->input('experience')) ? '' : 'integer',
            'sex' => 'max:255',
            'height' => 'max:255',
            'weight' => 'max:255',
            'chest' => 'max:255',
            'waist' => 'max:255',
            'hips' => 'max:255',
        ];

        if ($request->input('website') != NULL and $request->input('website') != 'http://')
            $validation['website'] = 'url|max:255';

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $user = Auth::user();
        $user->name = $request->input('name');
        $user->location = $request->input('location');
        $user->about_me = $request->input('about_me');
        $user->website = ($request->input('website') == 'http://') ? NULL : $request->input('website');
        $user->avatar = $request->input('avatar');
        $user->paypal = $request->input('paypal');
        $user->avaliable = json_encode($request->input('avaliable'));
        $user->paypal = $request->input('paypal');
        $user->nationality = $request->input('nationality');
        $user->birthday = $request->input('birthday');
        $user->profession = $request->input('profession');
        $user->experience = $request->input('experience');
        $user->sex = $request->input('sex');
        $user->height = $request->input('height');
        $user->weight = $request->input('weight');
        $user->chest = $request->input('chest');
        $user->waist = $request->input('waist');
        $user->hips = $request->input('hips');
        $user->save();

        return response()->json(['status' => 'success']);
    }

    public function update(Request $request)
    {
        $validation = [
            'name' => 'required|string|max:32',
            'location' => 'required|string|max:255',
            'nationality' => 'max:255',
            'birthday' => is_null($request->input('birthday')) ? '' : 'date',
            'profession' => 'max:255',
            'experience' => is_null($request->input('experience')) ? '' : 'integer',
            'sex' => 'max:255',
            'height' => 'max:255',
            'weight' => 'max:255',
            'chest' => 'max:255',
            'waist' => 'max:255',
            'hips' => 'max:255',
            'about_me' => 'max:1000',
            'avatar' => 'required',
        ];

        if ($request->input('website') != NULL and $request->input('website') != 'http://')
            $validation['website'] = 'url|max:255';

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $user = Auth::user();
        $user->update($request->except('_token'));

        return response()->json(['status' => 'success']);
    }

    public function update_category(Request $request)
    {
        $user = Auth::user();
        $user->avaliable = json_encode($request->input('avaliable'));
        $user->save();

        return response()->json(['status' => 'success']);
    }


    public function update_profile(Request $request)
    {
        $user = Auth::user();

        $validation = [
            'email' => 'required|email|max:255',
            'your_password' => 'required',
        ];

        if ($user->email != $request->input('email'))
            $validation['email'] = 'required|email|max:255|unique:users';

        if ($request->input('password') != NULL)
            $validation['password'] = 'required|string|min:6|confirmed';

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (!Hash::check($request->input('your_password'), $user->password))
            return response()->json([
                'status' => 'error',
                'message' => 'Incorrect password.'
            ]);

        $update = [
            'email' => $request->input('email')
        ];

        if ($request->input('password') != NULL)
            $update['password'] = bcrypt($request->input('password'));

        $user->update($update);

        return response()->json(['status' => 'success']);
    }

    public function update_avaliable(Request $request)
    {
        $user = Auth::user();

        $user->update([
            'avaliable' => json_encode($request->input('avaliable'))
        ]);

        return response()->json(['status' => 'success']);
    }

    public function uploadFiles(Request $request)
    {
        dd($request->all());

        if ($request->hasFile('file')) {
            $files = $request->file('file');

            foreach ($files as $key => $file) {
                $name = uniqid(Auth::user()->id . '_') . "." . $file->getClientOriginalExtension();
                File::makeDirectory(public_path() . 'files' . Auth::user()->id . '/', $mode = 0777, true, true);

                $file->move(public_path() . 'files' . Auth::user()->id . '/', $name);
            }
        }

        return redirect()->back();
    }
}
