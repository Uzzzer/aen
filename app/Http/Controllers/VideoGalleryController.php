<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\VideoGallery;
use App\VideoLike;
use App\Group;
use Validator;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Coordinate\TimeCode;
use App\GroupFollower;
use App\Notification;

class VideoGalleryController extends Controller
{

    public function add( Request $request ) {
        $UserType = User::isType();

        if (!$UserType or $UserType == 'user' or $UserType == 'public_user')
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'photos' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (!is_null($request->input('group_id'))) {
            $Group = Group::find($request->input('group_id'));

            if (is_null($Group))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Group not found.'
                ]);

            if (!Auth::check() or Auth::user()->id != $Group->user_id)
                return response()->json([
                    'status' => 'error',
                    'message' => 'No access.'
                ]);
        }

        foreach ($request->input('photos') as $photo) {
            $VideoGallery = new VideoGallery;
            $VideoGallery->user_id = Auth::user()->id;
            $VideoGallery->file_name = $photo['img'];

            $ffprobe = FFProbe::create();
            $VideoGallery->duration = round($ffprobe->format(public_path().$VideoGallery->file_name)->get('duration'));

            $VideoGallery->preview = '/storage/uploads/video_previews/'.uniqid('group_video_').'.jpg';

            $ffmpeg = FFMpeg::create();
            $video = $ffmpeg->open( public_path().$VideoGallery->file_name );
            $video->frame(TimeCode::fromSeconds(round($VideoGallery->duration/2)))->save(public_path().$VideoGallery->preview);

            $VideoGallery->title = $photo['title'];
            $VideoGallery->description = $photo['description'];
            $VideoGallery->group_id = $request->input('group_id');
            $VideoGallery->save();
        }

        $GroupFollowers = GroupFollower::where('group_id', $request->input('group_id'))->get();

        $param = [
            'group_id' => $request->input('group_id'),
            'text' => (count($request->input('photos')) == 1) ? 'Added new video.' : 'Added new videos.'
        ];

        foreach ($GroupFollowers as $GroupFollower)
            Notification::checkAndCreateNotification($GroupFollower->user_id, 'group_photo', $param);

        return response()->json([
            'status' => 'success'
        ]);

    }

    public function like(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $VideoGallery = VideoGallery::find($request->input('id'));

        if ($VideoGallery == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found'
            ]);

        $VideoLike = VideoLike::where('video_id', $request->input('id'))->where('user_id', Auth::user()->id)->first();

        if ($VideoLike != NULL) {
            $VideoLike->delete();
        } else {
            $VideoLike = new VideoLike;
            $VideoLike->video_id = $request->input('id');
            $VideoLike->user_id = Auth::user()->id;
            $VideoLike->save();
        }

        return response()->json([
            'status' => 'success',
            'count_like' => VideoLike::where('video_id', $request->input('id'))->count()
        ]);
    }

    public function delete(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $VideoGallery = VideoGallery::find($request->input('id'));

        if ($VideoGallery == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Video not found'
            ]);

        if (!Auth::check() or $VideoGallery->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $VideoGallery->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }
}