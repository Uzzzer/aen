<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Mail\UsersConfirmEmail;

class GuestController extends Controller
{
    public function first_page()
    {
        if (!is_null(Cookie::get('check-age')))
            return redirect(route('home'));

        return view('guest.first-page');
    }

    public function check_age()
    {
        Cookie::queue('check-age', 'yes');
        return redirect(route('home'));
    }

    public function showLoginFormDevelopment()
    {
        return view('auth.guard.login');
    }

    public function developmentLogin(Request $request)
    {
        $this->validate($request, [
            'login' => 'required|string',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $request->login)->first();

        if ($user !== null) {
            if (Hash::check($request->password, $user->password)) {
                Cookie::queue('development-environment-new', 'true');
                return redirect('/');
            }
        }

        return redirect()->back()->with('error', 'Username or password was entered incorrectly!');
    }

    public function test()
    {
        $user = User::find(44);
       // var_dump(\Mail::to('dipehaw203@eroyal.net')->send(new UsersConfirmEmail($user)));


        \Mail::raw('Hi, welcome user!', function ($message) {
            $message->to('dipehaw203@eroyal.net')->subject('sdsds');
        });

        \Mail::raw('Hi, welcome user!', function ($message) {
            $message->to('arthur.salenko@gmail.com')->subject('sdsds');
        });
    }

    public function index(Request $request)
    {
        $post_type = [];

        if (is_null($request->input('type')) or ($request->input('type') != 'event' and $request->input('type') != 'news'))
            $post_type = ['event', 'news', 'online'];
        else
            $post_type[] = $request->input('type');

        return view('guest.index', [
            'title' => 'Ladies',
            'Posts' => Post::whereNull('deleted_at')
                ->whereIn('post_type', $post_type)
                ->where('posts.status', 'post')
                ->join('users', 'posts.user_id', '=', 'users.id')
                ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                ->orderBy('id', 'desc')
                ->paginate(10),
            'user' => Auth::user()
        ]);
    }

    public function business()
    {
        $Category = \App\UserCategory::where('name', 'Business')->first();

        return view('guest.business', [
            'title' => 'Business',
            'business_users' => User::where('business', 1)->where('user_category_id', $Category->id)->limit(100)->orderBy('id', 'desc')->get(),
            'user' => Auth::user()
        ]);
    }

    public function professionals()
    {
        $Category = \App\UserCategory::where('name', 'Business')->first();

        return view('guest.business', [
            'title' => 'Professionals',
            'business_users' => User::where('business', 1)->where('user_category_id', '!=', $Category->id)->limit(100)->orderBy('id', 'desc')->get(),
            'user' => Auth::user()
        ]);
    }

    public function videos()
    {
        return view('guest.videos');
    }

    public function gallery()
    {
        return view('guest.gallery');
    }

    public function calendar_business()
    {
        return view('guest.calendar_business');
    }

    public function profile_business()
    {
        return view('guest.profile_business');
    }

    public function summit_page()
    {
        return view('guest.summit-page');
    }
}