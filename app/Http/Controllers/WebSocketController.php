<?php

namespace App\Http\Controllers;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Stream\LiveController;
use App\Http\Controllers\Stream\ChatLiveController;

/**
 * @author Rohit Dhiman | @aimflaiims
 */
class WebSocketController implements MessageComponentInterface
{
    protected $clients;
    private $subscriptions;
    private $users;
    private $userIds;
    private $userresources;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->subscriptions = [];
        $this->users = [];
        $this->userIds = [];
        $this->userresources = [];
    }

    /**
     * [onOpen description]
     * @method onOpen
     * @param  ConnectionInterface $conn [description]
     * @return [JSON]                    [description]
     * @example connection               var conn = new WebSocket('ws://localhost:8090');
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        $this->users[$conn->resourceId] = $conn;
    }

    /**
     * [onMessage description]
     * @method onMessage
     * @param  ConnectionInterface $conn [description]
     * @param  [JSON.stringify]              $msg  [description]
     * @return [JSON]                    [description]
     * @example subscribe                conn.send(JSON.stringify({command: "subscribe", channel: "global"}));
     * @example groupchat                conn.send(JSON.stringify({command: "groupchat", message: "hello glob", channel: "global"}));
     * @example message                  conn.send(JSON.stringify({command: "message", to: "1", from: "9", message: "it needs xss protection"}));
     * @example register                 conn.send(JSON.stringify({command: "register", userId: 9}));
     */
    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $request = new Request();
        $request->replace(json_decode($msg, true));

        if (isset($request->command)) {

            $user = User::where('api_token', $request->api_token)->first();

            if ($user->conn_id != $conn->resourceId) {
                $user->conn_id = $conn->resourceId;
                $user->save();
            }

            if (is_null($user)) {
                $this->users[$conn->resourceId]->send(json_encode(['status' => 'error']));
            } else {
                $this->userIds[$conn->resourceId] = $user->id;
                $liveController = new LiveController($user);
                $chatController = new ChatLiveController($user);

                switch ($request->command) {
                    case "start_live":
                        $this->users[$conn->resourceId]->send(json_encode($liveController->startStream($chatController->start_dialog())));
                        break;
                    case "stop_live":
                        $this->users[$conn->resourceId]->send(json_encode($liveController->stopStream()));
                        break;
                    case "get_dialog":
                        $this->users[$conn->resourceId]->send(json_encode($chatController->get_dialog($request)));
                        break;
                    case "send_message":
                        $response = $chatController->send_message($request);

                        if (isset($response['users'])) {
                            foreach ($response['users'] as $user) {
                                if (!empty($this->users[$user->conn_id])) {
                                    $this->users[$user->conn_id]->send(json_encode(['command' => 'update']));
                                }
                            }
                        }

                        unset($response['users']);
                        $this->users[$conn->resourceId]->send(json_encode($response));
                        break;
                    default:
                        echo $this->users[$conn->resourceId]->send(json_encode(['status' => 'success', 'command' => 'default']));
                        break;
                }
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";


        /*$user = User::find($this->userIds[$conn->resourceId]);

        if (!is_null($user)) {
            $liveController = new LiveController($user);
            $liveController->stopStream();
        }*/

        unset($this->users[$conn->resourceId]);
        unset($this->subscriptions[$conn->resourceId]);

        foreach ($this->userresources as &$userId) {
            foreach ($userId as $key => $resourceId) {
                if ($resourceId == $conn->resourceId) {
                    unset($userId[$key]);
                }
            }
        }
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}