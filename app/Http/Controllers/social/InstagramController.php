<?php

namespace App\Http\Controllers\social;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\SocialAuth;
use App\InstagramPost;
use Validator;
use MetzWeb\Instagram\Instagram;

class InstagramController extends Controller
{

    public function auth_redirect()
    {
        $instagram = new Instagram(array(
        	'apiKey'      => 'ac73408d304a4aa4b9b0f85875d40757',
        	'apiSecret'   => 'cb9114f1e58b42aa8d82e8cfb1e8729d',
        	'apiCallback' => route('callback_instagram')
        ));

        return redirect($instagram->getLoginUrl(array('basic')));
    }

    public function test() {
        $SocialAuth = SocialAuth::where('type', 'instagram')->get();

        foreach ($SocialAuth as $auth) {
            $instagram = new Instagram(array(
            	'apiKey'      => 'ac73408d304a4aa4b9b0f85875d40757',
            	'apiSecret'   => 'cb9114f1e58b42aa8d82e8cfb1e8729d',
            	'apiCallback' => route('callback_instagram')
            ));

            $instagram->setAccessToken(json_decode($auth->data));
            var_dump($instagram);
            InstagramPost::getRecentPost($instagram, $auth->user_id);
        }
    }

    public function callback_instagram()
    {
        $instagram = new Instagram(array(
        	'apiKey'      => 'ac73408d304a4aa4b9b0f85875d40757',
        	'apiSecret'   => 'cb9114f1e58b42aa8d82e8cfb1e8729d',
        	'apiCallback' => route('callback_instagram')
        ));

        $code = $_GET['code'];
        $data = $instagram->getOAuthToken($code);

        if (isset($data->access_token) and isset($data->access_token)) {
            $SocialAuth = SocialAuth::where('user_id', Auth::user()->id)->where('type', 'instagram')->first();

            if (is_null($SocialAuth)) {
                $SocialAuth = new SocialAuth;
                $SocialAuth->type = 'instagram';
                $SocialAuth->data = json_encode($data);
                $SocialAuth->user_id = Auth::user()->id;
                $SocialAuth->save();
            } else {
                $SocialAuth->data = json_encode($data);
                $SocialAuth->save();
            }

            $instagram->setAccessToken($data);

            \App\InstagramPost::getRecentPost($instagram);

            return redirect(route('profile-editing', ['tab' => 'social']))->with('success', 'Instagram is authorized.');
        } else {
            return redirect(route('profile-editing', ['tab' => 'social']))->with('error', 'Login failed!');
        }
    }

    public function logout() {
        SocialAuth::where('user_id', Auth::user()->id)->where('type', 'instagram')->delete();
        InstagramPost::where('user_id', Auth::user()->id)->delete();
        return redirect(route('profile-editing', ['tab' => 'social']))->with('success', 'Instagram unauthorized.');
    }

}
