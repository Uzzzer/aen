<?php

namespace App\Http\Controllers\Stream;

use App\User;
use App\Stream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StreamController extends \App\Http\Controllers\Controller
{
    public function start_stream($id)
    {
        $Stream = Stream::find($id);
        if (is_null($Stream))
            return redirect(route('profile'));

        $User = User::find($Stream->user_id);
        if (is_null($User))
            return redirect(route('profile'));

        return view('stream.start_stream', [
            'user' => $User,
            'stream' => $Stream,
            'js' => [asset('js/start_stream.js')]
        ]);
    }

    public function connect_stream($id)
    {
        $Stream = Stream::find($id);
        if (is_null($Stream))
            return redirect(route('profile'));

        $User = User::find($Stream->user_id);
        if (is_null($User))
            return redirect(route('profile'));

        return view('stream.connect_stream', [
            'user' => $User,
            'stream' => $Stream,
            'js' => [asset('js/connect_stream.js')]
        ]);
    }
}