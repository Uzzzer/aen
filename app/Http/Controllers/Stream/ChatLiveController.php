<?php

namespace App\Http\Controllers\Stream;

use App\LiveCharMessage;
use App\User;
use Chat;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChatLiveController extends Controller
{
    public $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function start_dialog()
    {
        $conversation = Chat::createConversation([$this->user->id]);
        $conversation->update(['data' => ['type' => 'stream']]);

        return $conversation->id;
    }

    public function send_message(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'conversation_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 'error'));
        }

        $conversation = Chat::conversations()->getById(intval($request->input('conversation_id')));

        Chat::message($request->input('message'))
            ->from($this->user)
            ->to($conversation)
            ->send();

        return [
            'users' => $conversation->users,
            'status' => 'success',
            'conversation_id' => $conversation->id
        ];
    }

    public function get_dialog(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'conversation_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 'error'));
        }

        $conversation = Chat::conversations()->getById(intval($request->input('conversation_id')));

        if ($conversation == null) {
            return response()->json(array('status' => 'error'));
        }

        if (!DB::table('mc_conversation_user')->where('user_id', $this->user->id)->where('conversation_id', $conversation->id)->count()) {
            DB::table('mc_conversation_user')->insert([
                'user_id' => $this->user->id,
                'conversation_id' => $conversation->id
            ]);
        }

        /*if ($request->input('page') == null or intval($request->input('page')) == 0) {
            $page = 1;
        } else {
            $page = intval($request->input('page'));
        }

        $mess = Chat::conversation($conversation)->for($this->user)->setPaginationParams([
            'page' => $page,
            'perPage' => 20,
            'sorting' => "desc",
        ])->getMessages()->items();*/

        $mess = LiveCharMessage::where('conversation_id', $conversation->id)->limit(50)->get();

        $messages = [];

        foreach ($mess as $message) {
            $messages[$message->id] = '
                <div class="message-block" data-id="' . $message->id . '"><b>' . User::where('id', $message->user_id)->first()->name . '</b>:' . htmlspecialchars($message->body, ENT_QUOTES) . '</div>
            ';
        }

        return [
            'status' => 'success',
            'command' => 'get_dialog',
            'messages' => $messages
        ];
    }

}