<?php

namespace App\Http\Controllers\Stream;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\LiveStream;

class LiveController extends Controller
{
    public $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        return view('live.index', [
            'user' => Auth::user(),
            'streams' => LiveStream::where('status', 'stream')->get(),
        ]);
    }

    public function view(User $user)
    {
        if (is_null($user)) {
            abort(404);
        }

        $stream = LiveStream::where('user_id', $user->id)->where('status', 'stream')->first();

        if (is_null($stream)) {
            return redirect(route('index-live'));
        }

        return view('live.view', [
            'user' => Auth::user(),
            'stream' => $stream,
        ]);
    }

    public function start_live()
    {
        $user = Auth::user();

        if (!$user->business) {
            abort(404);
        }

        return view('live.start-live', [
            'user' => $user
        ]);
    }

    public function stream_list()
    {
        $user = Auth::user();

        return view('live.start-live', [
            'user' => $user
        ]);
    }

    public function startStream($conversation_id)
    {
        LiveStream::startStream($this->user->id, $conversation_id);

        return [
            'status' => 'success',
            'command' => 'start_dialog',
            'conversation_id' => $conversation_id
        ];
    }

    public function stopStream()
    {
        return LiveStream::stopAllStreamForUser($this->user->id);
    }
}
