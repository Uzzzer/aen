<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\DateConvert;
use Chat;
use Validator;
use App\ConnectionRequest;
use App\Notification;

class ChatController extends Controller
{

    public function send_message(Request $request) {
        $validator = Validator::make($request->all(), [
            'conversation_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => 'error'));
        }

        $conversation = NULL;
        if (intval($request->input('conversation_id')) != 0)
            $conversation = Chat::conversations()->getById(intval($request->input('conversation_id')));

        if ($conversation == NULL) {
            $conversation = Chat::conversations()->between(Auth::user()->id, intval($request->input('user_id')));
            if ($conversation == NULL)
                $conversation = Chat::createConversation([Auth::user()->id, intval($request->input('user_id'))]);
        }

        $html = '';

        $message = Chat::message($request->input('message'))
            ->from(Auth::user())
            ->to($conversation)
            ->send();

        $html = '
            <div class="date-line">now</div>
            <div class="your-message-box box-msg" data-id="'.$message->id.'">
                <p>
                    '.htmlspecialchars($message->body, ENT_QUOTES).'
                </p>
            </div>
        ';

        return response()->json([
            'status' => 'success',
            'user_id' => $request->input('user_id'),
            'html' => $html,
            'conversation_id' => $conversation->id
        ]);
    }

    public function get_dialog(Request $request) {
        $validator = Validator::make($request->all(), [
            'conversation_id' => 'required|integer',
        ]);

        if ($validator->fails())
            return response()->json(array('status' => 'error'));

        $conversation = Chat::conversations()->getById(intval($request->input('conversation_id')));

        if ($conversation == NULL)
            return response()->json(array('status' => 'error'));

        if ($request->input('page') == NULL or intval($request->input('page')) == 0)
            $page = 1;
        else
            $page = intval($request->input('page'));

        $Messages = Chat::conversation($conversation)->for(Auth::user())->setPaginationParams([
            'page' => $page,
            'perPage' => 20,
            'sorting' => "desc",
        ])->getMessages()->items();

        $messages = [];

        $users = $conversation->users;
        foreach ($users as $user)
            if ($user->id != Auth::user()->id)
                break;

        foreach($Messages as $message) {
            if ($request->input('last_hour') != NULL and strtotime($message->created_at) < time() - 60*60)
                continue;

            if ($request->input('no_msg') != NULL and intval($request->input('no_msg')) == 1 and $message->type != 'call')
                continue;

            $date = NULL;

            Chat::message($message)->for(Auth::user())->markRead();

            $date = DateConvert::Convert($message->created_at, true);

            switch ($message->type) {
                case 'call':
                    $class = 'incoming_call_mgs';
                    $body = htmlspecialchars($message->body, ENT_QUOTES);
                    break;
                case 'job':
                    $class = 'job_mgs';
                    $body = json_decode($message->body);
                    if (Auth::user()->business == 1)
                        $body = $body->business;
                    else
                        $body = $body->user;
                    break;
                case 'job_answer':
                    $class = 'job_mgs';
                    $body = json_decode($message->body);
                    if (Auth::user()->business == 1)
                        $body = $body->business;
                    else
                        $body = $body->user;
                    break;
                default:
                    $class = '';
                    $body = htmlspecialchars($message->body, ENT_QUOTES);
                    break;
            }

            if ($message->user_id != Auth::user()->id) {
                $avatar = User::where('id', $message->user_id)->first()->avatar;
                $html = '
                    <div class="date-line">'.$date.'</div>
                    <div class="friend-message-box box-msg '.$class.'" data-id="'.$message->id.'">
                        <div class="message-author">
                            <img src="'.asset($avatar).'" alt="" style="width: 50px; height: 50px;">
                        </div>
                        <div class="friend-message">
                            <p>
                                '.$body.'
                            </p>
                        </div>
                    </div>
                ';
            } else {
                $html = '
                    <div class="date-line">'.$date.'</div>
                    <div class="your-message-box box-msg '.$class.'" data-id="'.$message->id.'">
                        <p>
                            '.$body.'
                        </p>
                    </div>
                ';
            }

            $messages[$message->id] = $html;
        }

        if (!count($messages) and $request->input('reverse') != NULL)
            return response()->json(array('status' => 'error'));

        if ($request->input('reverse') != NULL) {
            $messages = array_reverse($messages, true);
        }

        return response()->json([
            'status' => 'success',
            'last_activity' => DateConvert::Convert($user->last_activity, false, true),
            'messages' => $messages
        ]);
    }

    public function get_dialogs(Request $request) {
        $conversations = DB::table('mc_conversation_user')->where('user_id', Auth::user()->id)->select('conversation_id')->get();

        $dialogs = [];
        $sortArray = [];

        foreach ($conversations as $conversation) {
            $conversation = Chat::conversations()->getById($conversation->conversation_id);

            if (isset($conversation->data['type']) and $conversation->data['type'] == 'stream')
                continue;

            $users = $conversation->users;

            foreach ($users as $user)
                if ($user->id != Auth::user()->id)
                    break;

            $is_seen = false;
            $seen = DB::table('mc_message_notification')->where('user_id', Auth::user()->id)->where('conversation_id', $conversation->id)->where('is_seen', 0)->orderBy('id', 'desc')->select('created_at')->first();
            if ($seen != NULL)
                $is_seen = true;

            $last_msg = DB::table('mc_message_notification')->where('user_id', Auth::user()->id)->where('conversation_id', $conversation->id)->orderBy('id', 'desc')->select('created_at')->first();
            $last_msg = ($last_msg != NULL) ? strtotime($last_msg->created_at) : time();

            $recent_message = Chat::conversation($conversation)->for(Auth::user())->limit(99999)->getMessages()->items();

            $prefix = '';
            $mess = '';
            $date = '';

            if (count($recent_message)) {
                $recent_message = end($recent_message);

                if ($recent_message->user_id == Auth::user()->id)
                    $prefix = '<b>You:</b> ';

                switch ($recent_message->type) {
                    case 'job':
                        $body = json_decode($recent_message->body);
                        if (Auth::user()->business == 1)
                            $mess = htmlspecialchars(strip_tags($body->business), ENT_QUOTES);
                        else
                            $mess = htmlspecialchars(strip_tags($body->user), ENT_QUOTES);

                        $mess = str_replace(['Approve', 'Cancel'], '', $mess);

                        break;
                    case 'job_answer':
                        $body = json_decode($recent_message->body);
                        if (Auth::user()->business == 1)
                            $mess = htmlspecialchars(strip_tags($body->business), ENT_QUOTES);
                        else
                            $mess = htmlspecialchars(strip_tags($body->user), ENT_QUOTES);
                        break;
                    default:
                        $mess = htmlspecialchars($recent_message->body, ENT_QUOTES);
                        break;
                }

                $date = DateConvert::Convert($recent_message->created_at);
            }

            if (strlen($mess) > 50)
                $mess = trim(substr($mess, 0, 50))."...";

            $seen_class = (!$is_seen) ? 'readed' : '';

            $html = '
                <div class="dialog-message-box '.$seen_class.'" data-id="'.$conversation->id.'" data-name="'.$user->name.'" data-user_id="'.$user->id.'">
                    <div class="message-author">
                        <img src="'.asset($user->avatar).'" alt="" style="width: 50px; height: 50px;">
                    </div>
                    <div class="message-content">
                        <div class="message-top-line">
                            <div class="author-name">'.$user->name.'</div>
                            <div class="message-date">'.$date.'</div>
                        </div>
                        <p>
                            '.$prefix.$mess.'
                        </p>
                    </div>
                </div>
            ';

            $dialogs[$conversation->id]['id'] = $conversation->id;
            $dialogs[$conversation->id]['html'] = $html;
            $dialogs[$conversation->id]['is_seen'] = $is_seen;
            $dialogs[$conversation->id]['last_msg'] = $last_msg;

            $sortArray[$conversation->id] = $last_msg;
        }

        array_multisort($sortArray, SORT_DESC, SORT_NUMERIC, $dialogs);

        return response()->json([
            'status' => 'success',
            'dialogs' => $dialogs,
            'unreadCount' => Chat::messages()->for(Auth::user())->unreadCount()
        ]);
    }

    public function checkCountUnread() {

        return response()->json([
            'status' => 'success',
            'unreadCount' => Chat::messages()->for(Auth::user())->unreadCount(),
            'requestCount' => ConnectionRequest::countRequest(),
            'unreadNoticeCount' => Notification::countUnreadNotice()
        ]);

    }

}