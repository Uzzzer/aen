<?php

namespace App\Http\Controllers;

use App\BalanceLog;
use App\Gallery;
use App\Http\Controllers\Controller;
use App\Option;
use App\Payment;
use App\PurchasedVideo;
use App\User;
use App\UserCategory;
use App\Video;
use Braintree_Gateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\PaymentContent;
use Illuminate\Support\Str;

class PaymentController extends Controller
{

    public function payment_success(Request $request)
    {
        $User = Auth::user();

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'ATDUv4DOHEcoy4AxouWhAY8gXkwHO0hs_TbCvezGuHBrl1iTCOxWC3YY4nGXJzd395dnaTOtwapB_xdI',
                'EBxxeX3WEqriI517WFflD_HPHrT9EbN9kfdCRUaimQ9ypsTzfNsB-wAke8iqumY_fktkVEY0ryH2XXI1'
            )
        );

        $result = \PayPal\Api\Payment::get($request->paymentId, $apiContext);

        try {
            if ($result->getState()) {
                $product = json_decode($request->input('product'));
                $redirect_url = route('profile');

                if ($product->type == 'subscription') {
                    $UserCategory = UserCategory::where('id', $User->user_category_id)->first();

                    if (!is_null($UserCategory)) {
                        $period = ($product->period == 'trial') ? Option::option('trial_days') . ' days' : intval($product->period) . ' months';

                        $User->subscription_end = date('Y-m-d H:i:s', strtotime('+' . $period));
                        $User->subscription_id = NULL;
                        $User->save();
                    }
                }

                if ($product->type == 'video') {
                    $Video = Video::find($product->id);

                    if (!is_null($Video)) {
                        $price = $Video->price;
                        $PurchasedVideo = new PurchasedVideo;
                        $PurchasedVideo->user_id = $User->id;
                        $PurchasedVideo->video_id = $Video->id;
                        $PurchasedVideo->cost = $price;
                        $PurchasedVideo->currency = Option::option('currency');
                        $PurchasedVideo->save();
                        $redirect_url = route('profile_video', ['id' => $Video->id]);

                        BalanceLog::updateBalance('video', $Video->id, $User->id);
                    }
                }

                if ($product->type == 'photo') {
                    $Photo = Gallery::find($product->id);
                    if (!is_null($Photo)) {
                        $price = $Photo->price;
                        $PaymentContent = new PaymentContent;
                        $PaymentContent->user_id = $User->id;
                        $PaymentContent->obj_id = $Photo->id;
                        $PaymentContent->type = 'gallery';
                        $PaymentContent->token = Str::random(40);
                        $PaymentContent->save();
                        $redirect_url = route('profile_photo', ['id' => $Photo->user_id, 'pid' => $Photo->id]);

                        BalanceLog::updateBalance('photo', $Photo->id, $User->id);
                    }
                }

                $Payment = new Payment;
                $Payment->amount = $price;
                $Payment->currency = Option::option('currency');
                $Payment->user_id = $User->id;
                $Payment->product = $request->input('product');
                $Payment->save();
            }
        } catch (\Exception $exception) {

        }

        return view('user.payment_success', [
            'user' => $User,
            'redirect_url' => $redirect_url
        ]);
    }

    public function payment_error(Request $request)
    {
        $User = Auth::user();

        return view('user.payment_error', [
            'user' => $User
        ]);
    }

    public function payment_page(Request $request)
    {
        $User = Auth::user();

        $all = $request->all();

        if (!isset($all['subscription']) and !isset($all['video']) and !isset($all['photo']))
            return redirect(route('profile'));

        $description = '';
        $price = 0;
        $product = [];

        if (isset($all['subscription'])) {
            $UserCategory = UserCategory::where('id', $User->user_category_id)->first();

            if (is_null($UserCategory))
                return redirect(route('profile'));

            $prices = json_decode($UserCategory->prices, true);

            $subscription_type = ['12', '3', '1', 'trial'];
            $product = [
                'type' => 'subscription',
                'period' => $all['subscription']
            ];

            if (!in_array($all['subscription'], $subscription_type))
                return redirect(route('subscription'));

            if ($all['subscription'] == 'trial' and !$UserCategory->trial_period)
                return redirect(route('subscription'));

            switch ($all['subscription']) {
                case '12':
                    $description = 'Subscription for 12 months.';
                    $price = (isset($prices[12])) ? $prices[12] : 0;
                    break;
                case '3':
                    $description = 'Subscription for 3 months.';
                    $price = (isset($prices[3])) ? $prices[3] : 0;
                    break;
                case '1':
                    $description = 'Subscription for 1 month.';
                    $price = (isset($prices[1])) ? $prices[1] : 0;
                    break;
                case 'trial':
                    $description = Option::option('trial_days') . ' day trial subscription.';
                    $price = (isset($prices['trial'])) ? $prices['trial'] : 0;
                    break;
            }
        }

        if (isset($all['video'])) {
            $Video = Video::find($all['video']);

            if (is_null($Video))
                return redirect(route('profile'));

            $product = [
                'type' => 'video',
                'id' => $Video->id
            ];

            $description = 'Buying a video "' . $Video->title . '".';
            $price = $Video->price;
        }

        if (isset($all['photo'])) {
            $Photo = Gallery::find($all['photo']);

            if (is_null($Photo))
                return redirect(route('profile'));

            $product = [
                'type' => 'photo',
                'id' => $Photo->id
            ];

            $description = 'Buying a photo "' . $Photo->title . '".';
            $price = $Photo->price;
        }

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'ATDUv4DOHEcoy4AxouWhAY8gXkwHO0hs_TbCvezGuHBrl1iTCOxWC3YY4nGXJzd395dnaTOtwapB_xdI',
                'EBxxeX3WEqriI517WFflD_HPHrT9EbN9kfdCRUaimQ9ypsTzfNsB-wAke8iqumY_fktkVEY0ryH2XXI1'
            )
        );

        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal($price);
        $amount->setCurrency('GBP');

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount);

        $query = '?product=' . urlencode(json_encode($product));

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl("https://www.adult-en.com/profile/payment/success" . $query)
            ->setCancelUrl("https://www.adult-en.com/profile/payment/error");

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        try {
            $payment->create($apiContext);
            $paypal_link = $payment->getApprovalLink();
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            var_dump($ex->getData());
            $paypal_link = '#';
        }

        return view('user.payment', [
            'user' => $User,
            'price' => $price,
            'description' => $description,
            'product' => json_encode($product),
            'js' => [asset('js/payment.js')],
            'paypal_link' => $paypal_link
        ]);
    }

    public function payment(Request $request)
    {
        $validation = [
            'product' => 'required',
            'card_number' => 'required',
            'card_exp' => 'required|max:5',
            'card_cvv' => 'required'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $User = Auth::user();
        $product = json_decode($request->input('product'));

        if ($product->type == 'subscription') {
            $UserCategory = UserCategory::where('id', $User->user_category_id)->first();

            if (is_null($UserCategory))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Try again.'
                ]);

            $period = ($product->period == 'trial') ? Option::option('trial_days') . ' days' : intval($product->period) . ' months';

            $Category = json_decode($UserCategory->planIDs, true);
            $planId = (isset($Category[$product->period])) ? $Category[$product->period] : 0;

            $prices = json_decode($UserCategory->prices, true);
            $price = (isset($prices[$product->period])) ? $prices[$product->period] : 0;

            if (!$planId or empty($planId))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Plan does not exist.'
                ]);

            $gateway = new Braintree_Gateway([
                'environment' => Option::option('braintree_env'),
                'merchantId' => Option::option('braintree_merchant_id'),
                'publicKey' => Option::option('braintree_public_key'),
                'privateKey' => Option::option('braintree_private_key')
            ]);

            // $clientToken = $gateway->clientToken()->generate();

            $result = $gateway->customer()->create([
                'firstName' => '',
                'lastName' => '',
                'email' => $User->email,
                'creditCard' => [
                    'expirationDate' => $request->input('card_exp'),
                    'cvv' => $request->input('card_cvv'),
                    'cardholderName' => $request->input('holderName'),
                    'number' => $request->input('card_number')
                ]
            ]);

            if ($result->success) {
                if (!is_null($User->subscription_end) and time() < strtotime($User->subscription_end) and !is_null($User->subscription_id)) {
                    $result_cancel = $gateway->subscription()->cancel($User->subscription_id);

                    if (!$result_cancel->success)
                        return response()->json([
                            'status' => 'error',
                            'message' => 'canceling current subscription.'
                        ]);

                    $User->subscription_end = NULL;
                }

                $result = $gateway->subscription()->create([
                    'paymentMethodToken' => $result->customer->paymentMethods[0]->token,
                    'planId' => $planId
                ]);

                if ($result->success) {
                    $User->subscription_end = date('Y-m-d H:i:s', strtotime('+' . $period));
                    $User->subscription_id = $result->subscription->id;
                    $User->save();

                    $redirect_url = route('profile');
                } else
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Check if the credit card is entered correctly.'
                    ]);

            } else
                return response()->json([
                    'status' => 'error',
                    'message' => 'Check if the credit card is entered correctly.'
                ]);
        }

        if ($product->type == 'video') {
            $Video = Video::find($product->id);

            if (is_null($Video))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Try again.'
                ]);

            if (PurchasedVideo::checkPurchase($Video->id))
                return response()->json([
                    'status' => 'error',
                    'message' => 'You already bought this video.'
                ]);

            $price = $Video->price;

            $gateway = new Braintree_Gateway([
                'environment' => Option::option('braintree_env'),
                'merchantId' => Option::option('braintree_merchant_id'),
                'publicKey' => Option::option('braintree_public_key'),
                'privateKey' => Option::option('braintree_private_key')
            ]);

            $result = $gateway->customer()->create([
                'firstName' => '',
                'lastName' => '',
                'email' => $User->email,
                'creditCard' => [
                    'expirationDate' => $request->input('card_exp'),
                    'cvv' => $request->input('card_cvv'),
                    'cardholderName' => $request->input('holderName'),
                    'number' => $request->input('card_number')
                ]
            ]);

            if ($result->success) {
                $result = $gateway->transaction()->sale([
                    'amount' => $price,
                    'paymentMethodToken' => $result->customer->paymentMethods[0]->token,
                    'options' => [
                        'submitForSettlement' => True
                    ]
                ]);

                if ($result->success) {
                    $PurchasedVideo = new PurchasedVideo;
                    $PurchasedVideo->user_id = $User->id;
                    $PurchasedVideo->video_id = $Video->id;
                    $PurchasedVideo->cost = $price;
                    $PurchasedVideo->currency = Option::option('currency');
                    $PurchasedVideo->save();
                    $redirect_url = route('profile_video', ['id' => $Video->id]);

                    BalanceLog::updateBalance('video', $Video->id, $User->id);
                } else
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Check if the credit card is entered correctly.'
                    ]);
            } else
                return response()->json([
                    'status' => 'error',
                    'message' => 'Check if the credit card is entered correctly.'
                ]);
        }

        if ($product->type == 'photo') {
            $Photo = Gallery::find($product->id);

            if (is_null($Photo))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Try again.'
                ]);

            $price = $Photo->price;

            $gateway = new Braintree_Gateway([
                'environment' => Option::option('braintree_env'),
                'merchantId' => Option::option('braintree_merchant_id'),
                'publicKey' => Option::option('braintree_public_key'),
                'privateKey' => Option::option('braintree_private_key')
            ]);

            $result = $gateway->customer()->create([
                'firstName' => '',
                'lastName' => '',
                'email' => $User->email,
                'creditCard' => [
                    'expirationDate' => $request->input('card_exp'),
                    'cvv' => $request->input('card_cvv'),
                    'cardholderName' => $request->input('holderName'),
                    'number' => $request->input('card_number')
                ]
            ]);

            if ($result->success) {
                $result = $gateway->transaction()->sale([
                    'amount' => $price,
                    'paymentMethodToken' => $result->customer->paymentMethods[0]->token,
                    'options' => [
                        'submitForSettlement' => True
                    ]
                ]);

                if ($result->success) {

                    $price = $Photo->price;
                    $PaymentContent = new PaymentContent;
                    $PaymentContent->user_id = $User->id;
                    $PaymentContent->obj_id = $Photo->id;
                    $PaymentContent->type = 'gallery';
                    $PaymentContent->token = Str::random(40);
                    $PaymentContent->save();
                    $redirect_url = route('profile_photo', ['id' => $Photo->user_id, 'pid' => $Photo->id]);

                    BalanceLog::updateBalance('photo', $Photo->id, $User->id);
                } else
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Check if the credit card is entered correctly.'
                    ]);
            } else
                return response()->json([
                    'status' => 'error',
                    'message' => 'Check if the credit card is entered correctly.'
                ]);
        }

        if (!isset($redirect_url))
            return response()->json([
                'status' => 'error',
                'message' => 'Try again.'
            ]);

        $Payment = new Payment;
        $Payment->amount = $price;
        $Payment->currency = Option::option('currency');
        $Payment->user_id = $User->id;
        $Payment->product = $request->input('product');
        $Payment->save();

        return response()->json([
            'status' => 'success',
            'redirect_url' => $redirect_url
        ]);
    }
}
