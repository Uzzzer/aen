<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Gallery;
use App\PhotoLike;
use App\Group;
use App\GroupFollower;
use App\Notification;
use Validator;
use App\PaymentContent as PaymentContent;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;


class GalleryController extends Controller
{
	
	public function paymentPhoto(Request $req, $token){
		$user = Auth::user();
		if(!$token || !$user){
			abort(404);
		}
		$exist = PaymentContent::where(["token"=>$token, "user_id"=>$user->id])->get()->first();
		
		if(!$exist){
			abort(404);
		}
		$prefix = public_path();
		switch($exist->type){
			case 'gallery':
				$content_obj = Gallery::where(["id"=>$exist->obj_id])->get()->first();
				$src = $prefix.$content_obj->file_name;
			break;
			default:
				$src = false;
				$content_obj = false;
			break;
		}
		
		if($content_obj && $src && is_file($src)){
			header('Content-Type: image/jpeg');
			readfile($src);
			exit();

		}else{
			abort(404);
		}
		
	}


    public function profile_photo_search(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Gallery = Gallery::getSearchPhotosByUser($request->input('user_id'), $request->input('search'), 10);
        $posts = [];

        $user = User::find($request->input('user_id'));

        foreach ($Gallery as $photo)
            $posts[] = view('includes/profile-photo', ['photo' => $photo, 'user' => $user])->render();

        return response()->json([
            'status' => 'success',
            'posts' => $posts
        ]);
    }

    public function add( Request $request ) {
    
        $UserType = User::isType();

        if (!$UserType or $UserType == 'user' or $UserType == 'public_user')
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'photos' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (!is_null($request->input('group_id'))) {
            $Group = Group::find($request->input('group_id'));

            if (is_null($Group))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Group not found.'
                ]);

            if (!Auth::check() or Auth::user()->id != $Group->user_id)
                return response()->json([
                    'status' => 'error',
                    'message' => 'No access.'
                ]);
        }

        foreach ($request->input('photos') as $photo) {
            $Gallery = new Gallery;
            $Gallery->user_id = Auth::user()->id;
            $Gallery->file_name = $photo['img'];
            $Gallery->preview = isset($photo['preview']) ? $photo['preview'] : '';
            $Gallery->title = strip_tags($photo['title']);
            $Gallery->price = (float)$photo['price'];
            $Gallery->description = strip_tags($photo['description']);
            $Gallery->group_id = $request->input('group_id');
//        	exit(Storage::path('/'));
            if((float)$photo['price']>0){
            	$prefix = public_path();
            	$thumb = $photo['img'].time()."-s.jpg";
				$thumb2 = $photo['img'].time()."-b.jpg";
            	$img = Image::make($prefix.$photo['img'])->resize(272, null, function($con){
            		$con->aspectRatio();
            	})->blur(50)->save($prefix.$thumb); //->insert('public/watermark.png');

            	$img = Image::make($prefix.$photo['img'])->resize(451, null, function($con){
            		$con->aspectRatio();
            	})->blur(50)->save($prefix.$thumb2); //->insert('public/watermark.png');


            	$Gallery->blured = $thumb;
            	$Gallery->blured2 = $thumb2;
            }
            $Gallery->save();
        }

        $GroupFollowers = GroupFollower::where('group_id', $request->input('group_id'))->get();

        $param = [
            'group_id' => $request->input('group_id'),
            'text' => (count($request->input('photos')) == 1) ? 'Added new photo.' : 'Added new photos.'
        ];

        foreach ($GroupFollowers as $GroupFollower)
            Notification::checkAndCreateNotification($GroupFollower->user_id, 'group_photo', $param);

        return response()->json([
            'status' => 'success'
        ]);

    }

    public function like(Request $request) {
        $UserType = User::isType();

        if (!$UserType or $UserType == 'public_user')
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Gallery = Gallery::find($request->input('id'));

        if ($Gallery == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Photo not found'
            ]);

        $PhotoLike = PhotoLike::where('photo_id', $request->input('id'))->where('user_id', Auth::user()->id)->first();

        if ($PhotoLike != NULL) {
            $PhotoLike->delete();
        } else {
            $PhotoLike = new PhotoLike;
            $PhotoLike->photo_id = $request->input('id');
            $PhotoLike->user_id = Auth::user()->id;
            $PhotoLike->save();

            $text = (is_null($Gallery->group_id))
                ? 'Liked your photo&nbsp;<a href="'.route('profile_photo', ['id' => $Gallery->user_id, 'pid' => $Gallery->id]).'">'
                : 'Liked your photo&nbsp;<a href="'.route('photo_group', ['id' => $Gallery->group_id, 'pid' => $Gallery->id]).'">';
            $text .= (!is_null($Gallery->title)) ? $Gallery->title."</a>" : 'No title</a>';

            if (!is_null($Gallery->group_id)) {
                $Group = Group::find($Gallery->group_id);
                $text .= (!is_null($Group)) ? ', in the group&nbsp;<a href="'.route('group', ['id' => $Group->id]).'">'.$Group->name.'</a>.' : '.';
            } else
                $text .= '.';

            $param = [
                'user_id' => $PhotoLike->user_id,
                'text' => $text
            ];

            Notification::checkAndCreateNotification($Gallery->user_id, 'like_photo', $param);
        }

        return response()->json([
            'status' => 'success',
            'count_like' => PhotoLike::where('photo_id', $request->input('id'))->count()
        ]);
    }

    public function delete(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Gallery = Gallery::find($request->input('id'));

        if ($Gallery == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Photo not found'
            ]);

        if (!Auth::check() or $Gallery->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $Gallery->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

}
