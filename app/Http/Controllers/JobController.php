<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Job;
use App\JobRequest;
use App\Notification;
use App\User;
use App\WorksWith;
use Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class JobController extends Controller
{

    public function index()
    {
        if (in_array(User::isType(), ['public_user', 'user', false]))
            return redirect(route('profile'));

        return view('guest.job_alerts', [
            'Jobs' => Job::where('jobs.status', 'active')->join('users', 'jobs.user_id', '=', 'users.id')->select('jobs.*', 'users.avatar as user_avatar', 'users.name as user_name')->orderBy('jobs.id', 'desc')->limit(10)->get(),
            'user' => Auth::user()
        ]);
    }

    public function request(Request $request)
    {
        if (in_array(User::isType(), ['public_user', 'user', false]))
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        if (Auth::user()->business == 1 and Auth::user()->business == 10)
            return response()->json([
                'status' => 'error',
                'message' => 'Business accounts cannot send requests.'
            ]);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('id'));

        if ($Job == NULL or $Job->status != 'active')
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        $JobRequest = JobRequest::where('user_id', Auth::user()->id)->where('job_id', $Job->id)->first();

        if (!is_null($JobRequest))
            return response()->json([
                'status' => 'error',
                'message' => 'You have already sent a request.'
            ]);

        $JobRequest = new JobRequest;
        $JobRequest->user_id = Auth::user()->id;
        $JobRequest->job_id = $Job->id;
        $JobRequest->save();

        $conversation = NULL;
        if ($conversation == NULL) {
            $conversation = Chat::conversations()->between($Job->user_id, Auth::user()->id);
            if ($conversation == NULL)
                $conversation = Chat::createConversation([$Job->user_id, Auth::user()->id]);
        }

        $msg['business'] = 'You received a request for <span>' . $Job->title . '</span><a href="#" class="approve_job job_btn_chat" data-id="' . $JobRequest->id . '">Approve</a><a href="#" class="cancel_job job_btn_chat" data-id="' . $JobRequest->id . '">Cancel</a>';
        $msg['user'] = 'You send a request to <span>' . $Job->title . '</span>';

        $message = Chat::message(json_encode($msg))
            ->type('job')
            ->from(Auth::user())
            ->to($conversation)
            ->send();

        $JobRequest->msg_id = $message->id;
        $JobRequest->save();

        $message = Chat::message($request->input('message'))
            ->type('text')
            ->from(Auth::user())
            ->to($conversation)
            ->send();

        $JobRequest->msg_txt_id = $message->id;
        $JobRequest->save();

        $param = [
            'user_id' => $JobRequest->user_id,
            'text' => 'Sent a request for a job "' . $Job->title . '".'
        ];

        Notification::checkAndCreateNotification($Job->user_id, 'job_request', $param);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function request_approve(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $JobRequest = JobRequest::find($request->input('id'));

        if ($JobRequest == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Request not found.'
            ]);

        $Job = Job::find($JobRequest->job_id);

        if ($Job == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        if ($Job->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $conversation = NULL;
        if ($conversation == NULL) {
            $conversation = Chat::conversations()->between($JobRequest->user_id, Auth::user()->id);
            if ($conversation == NULL)
                $conversation = Chat::createConversation([$JobRequest->user_id, Auth::user()->id]);
        }

        $msg['user'] = 'You have been approved for <span>' . $Job->title . '</span>';
        $msg['business'] = 'You have approved a user for <span>' . $Job->title . '</span>';

        $message = Chat::message(json_encode($msg))
            ->type('job_answer')
            ->from(Auth::user())
            ->to($conversation)
            ->send();

        $msg = DB::table('mc_messages')->where('id', $JobRequest->msg_id)->first();

        if (!is_null($msg)) {
            $message = json_decode($msg->body);
            $message->business = 'You received a request for <span>' . $Job->title . '</span>';
            DB::table('mc_messages')->where('id', $JobRequest->msg_id)->update([
                'body' => json_encode($message)
            ]);
        }

        $param = [
            'user_id' => $Job->user_id,
            'text' => 'Approved request for the job "' . $Job->title . '".'
        ];

        Notification::checkAndCreateNotification($JobRequest->user_id, 'job_request', $param);

        $Job->status = 'job';
        $Job->user_job = $JobRequest->user_id;
        $Job->save();

        $JobRequest->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }


    public function request_cancel(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $JobRequest = JobRequest::find($request->input('id'));

        if ($JobRequest == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Request not found.'
            ]);

        $Job = Job::find($JobRequest->job_id);

        if ($Job == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        if ($Job->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $conversation = NULL;
        if ($conversation == NULL) {
            $conversation = Chat::conversations()->between($JobRequest->user_id, Auth::user()->id);
            if ($conversation == NULL)
                $conversation = Chat::createConversation([$JobRequest->user_id, Auth::user()->id]);
        }

        $msg['user'] = 'You have been canceled for <span>' . $Job->title . '</span>';
        $msg['business'] = 'You canceled the user for <span>' . $Job->title . '</span>';

        $message = Chat::message(json_encode($msg))
            ->type('job_answer')
            ->from(Auth::user())
            ->to($conversation)
            ->send();

        $msg = DB::table('mc_messages')->where('id', $JobRequest->msg_id)->first();

        if (!is_null($msg)) {
            $message = json_decode($msg->body);
            $message->business = 'You received a request for <span>' . $Job->title . '</span>';
            DB::table('mc_messages')->where('id', $JobRequest->msg_id)->update([
                'body' => json_encode($message)
            ]);
        }

        $param = [
            'user_id' => $Job->user_id,
            'text' => 'Canceled request for the job "' . $Job->title . '".'
        ];

        Notification::checkAndCreateNotification($JobRequest->user_id, 'job_request', $param);

        $JobRequest->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('id'));

        if ($Job == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        if (!Auth::check() or Auth::user()->business != 1 or $Job->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);


        if (!is_null($Job->user_job)) {
            $WorksWith = WorksWith::where('user_id', $Job->user_job)->where('business_id', $Job->user_id)->first();

            if (is_null($WorksWith)) {
                $WorksWith = new WorksWith;
                $WorksWith->user_id = $Job->user_job;
                $WorksWith->business_id = $Job->user_id;
                $WorksWith->save();
            }
        }

        $Job->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function get(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('id'));

        if ($Job == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        return response()->json([
            'status' => 'success',
            'job' => $Job
        ]);
    }

    public function add(Request $request)
    {
        if (!in_array(User::isType(), ['business']))
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'JobTitle' => 'required|max:255',
            'Description' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = new Job;
        $Job->user_id = Auth::user()->id;
        if ($request->input('file_type') != NULL)
            $Job->file_type = $request->input('file_type');
        if ($request->input('file_name') != NULL)
            $Job->file_name = $request->input('file_name');
        if ($request->input('file_preview') != NULL)
            $Job->file_preview = $request->input('file_preview');
        $Job->title = strip_tags($request->input('JobTitle'));
        $Job->text = strip_tags($request->input('Description'));
        if ($request->input('responsibilities') != NULL and is_array($request->input('responsibilities')))
            $Job->responsibilities = json_encode($request->input('responsibilities'));
        if ($request->input('fields') != NULL and is_array($request->input('fields')))
            $Job->fields = json_encode($request->input('fields'));
        $Job->save();

        $fields = (is_null($request->input('fields'))) ? array() : $request->input('fields');

        $html = '
                <div class="tab_job_block job_box" data-id="' . $Job->id . '">
                    <div class="tab_job_block_top">
                        <div class="tab_job_left">
                            <div class="job_tittle">' . $Job->title . '</div>';
        if (isset($fields['Location']))
            $html .= '<div class="job_location">Location: ' . $fields['Location'] . '</div>';

        $html .= '
                            <div class="job_data">' . \App\DateConvert::Convert($Job->created_at) . '</div>
                        </div>
                        <div class="tab_job_right ">
                            <div class="job_right_text job_right_textDetails">
                                <a href="#" class="delete_job" data-id="' . $Job->id . '">Delete</a>
                                <a href="#" class="edit_job" data-id="' . $Job->id . '">Edit</a>
                                <p class="open_job_profile">View Details</p>   <!-- Close Details -->
                            </div>
                        </div>
                    </div>
                    <div class="tab_job_block_bottom" style="display: none;">
                        <div class="tab_job_block_wrap">
                            <p>
                                 ' . $Job->text . '
                            </p>';
        if ($Job->responsibilities != NULL) {
            $responsibilities = json_decode($Job->responsibilities, true);

            if (is_array($responsibilities)) {
                $html .= '<h6>Responsibilities</h6><ul>';

                foreach ($responsibilities as $responsibility)
                    $html .= '<li>' . $responsibility . '</li>';

                $html .= '</ul>';
            }
        }
        if (is_array($fields))
            foreach ($fields as $key => $value)
                $html .= '<div class="job_info"><span>' . $key . ': </span>' . $value . '</div>';

        $html .= '
                        </div>
                    </div>
                </div>';

        return response()->json([
            'status' => 'success',
            'html' => $html
        ]);
    }

    public function update(Request $request)
    {
        if (!in_array(User::isType(), ['business']))
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'JobTitle' => 'required|max:255',
            'Description' => 'required',
            'update_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Job = Job::find($request->input('update_id'));

        if (is_null($Job))
            return response()->json([
                'status' => 'error',
                'message' => 'Job not found.'
            ]);

        if ($Job->user_id != \Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $Job->user_id = Auth::user()->id;
        if ($request->input('file_type') != NULL)
            $Job->file_type = $request->input('file_type');
        if ($request->input('file_name') != NULL)
            $Job->file_name = $request->input('file_name');
        if ($request->input('file_preview') != NULL)
            $Job->file_preview = $request->input('file_preview');

        $Job->title = $request->input('JobTitle');
        $Job->text = $request->input('Description');
        if ($request->input('responsibilities') != NULL and is_array($request->input('responsibilities')))
            $Job->responsibilities = json_encode($request->input('responsibilities'));
        if ($request->input('fields') != NULL and is_array($request->input('fields')))
            $Job->fields = json_encode($request->input('fields'));
        $Job->save();

        $fields = (is_null($request->input('fields'))) ? array() : $request->input('fields');

        $html = '
                <div class="tab_job_block job_box">
                    <div class="tab_job_block_top">
                        <div class="tab_job_left">
                            <div class="job_tittle">' . $Job->title . '</div>';
        if (isset($fields['Location']))
            $html .= '<div class="job_location">Location: ' . $fields['Location'] . '</div>';

        if(!is_null($Job->user_job)) {
            $UserJob = \App\User::find($Job->user_job);

            if (!is_null($UserJob))
                $html .= '<div class="job_location"><a href="'.route('id_profile', ['id' => $UserJob->id]) .'">'. $UserJob->name .'</a></div>';
        }

        $html .= '
                            <div class="job_data">' . \App\DateConvert::Convert($Job->created_at) . '</div>
                        </div>
                        <div class="tab_job_right ">
                            <div class="job_right_text job_right_textDetails">
                                <a href="#" class="delete_job" data-id="' . $Job->id . '">Delete</a>
                                <p class="open_job_profile">View Details</p>   <!-- Close Details -->
                            </div>
                        </div>
                    </div>
                    <div class="tab_job_block_bottom" style="display: none;">
                        <div class="tab_job_block_wrap">
                            <p>
                                 ' . $Job->text . '
                            </p>';
        if ($Job->responsibilities != NULL) {
            $responsibilities = json_decode($Job->responsibilities, true);

            if (is_array($responsibilities)) {
                $html .= '<h6>Responsibilities</h6><ul>';

                foreach ($responsibilities as $responsibility)
                    $html .= '<li>' . $responsibility . '</li>';

                $html .= '</ul>';
            }
        }
        if (is_array($fields))
            foreach ($fields as $key => $value)
                $html .= '<div class="job_info"><span>' . $key . ': </span>' . $value . '</div>';

        $html .= '
                        </div>
                    </div>
                </div>';

        return response()->json([
            'status' => 'success',
            'html' => $html
        ]);
    }
}