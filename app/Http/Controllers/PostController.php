<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Group;
use App\GroupFollower;
use App\Notification;
use App\Post;
use App\PostLike;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class PostController extends Controller
{

    public function single_post(Post $post)
    {
        $Group = Group::find($post->group_id);

        return view('user.post', [
            'user' => Auth::user(),
            'Post' => $post,
            'Group' => $Group,
            'RecentPosts' => is_null($Group)
                ? Post::where('user_id', $post->user_id)->whereNull('group_id')->where('id', '!=', $post->id)->orderBy('id', 'desc')->limit(2)->get()
                : Post::where('group_id', $post->group_id)->where('id', '!=', $post->id)->orderBy('id', 'desc')->limit(2)->get(),
            'js' => []
        ]);
    }

    public function going_post()
    {
        if (!\Auth::check())
            abort(404);

        return view('user.going', [
            'user' => Auth::user(),
            'Posts' => \App\Going::where('going.user_id', \Auth::user()->id)->join('posts','posts.id','=','going.post_id')->get(),
            'js' => []
        ]);
    }

    public function interested_post()
    {
        if (!\Auth::check())
            abort(404);

        return view('user.interested', [
            'user' => Auth::user(),
            'Posts' => \App\Interested::where('interested.user_id', \Auth::user()->id)->join('posts','posts.id','=','interested.post_id')->get(),
            'js' => []
        ]);
    }

    public function get_page(Request $request)
    {
        switch ($request->input('load_page_type')) {
            case "load_index":
                $Posts = Post::whereNull('deleted_at')
                    ->whereIn('post_type', ['event', 'news', 'online'])
                    ->where('posts.status', 'post')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                if (count($Posts)) {
                    foreach ($Posts as $Post)
                        $PostsArray[] = view('includes.post', ['Post' => $Post])->render();

                    return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                } else
                    return response()->json(['status' => 'error']);
                break;
            case "load_draft":
                $Posts = Post::whereNull('deleted_at')
                    ->whereIn('post_type', ['event', 'news', 'online'])
                    ->where('posts.status', 'draft')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                if (count($Posts)) {
                    foreach ($Posts as $Post)
                        $PostsArray[] = view('includes.draft-post', ['Post' => $Post])->render();

                    return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                } else
                    return response()->json(['status' => 'error']);
                break;
            case "load_tag":
                $Posts = Post::whereNull('posts.deleted_at')
                    ->rightjoin('tags', 'posts.id', '=', 'tags.post_id')
                    ->where('tags.tag', $request->input('tag'))
                    ->where('posts.status', 'post')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                    ->orderBy('posts.id', 'desc')
                    ->paginate(10);

                if (count($Posts)) {
                    foreach ($Posts as $Post)
                        $PostsArray[] = view('includes.post', ['Post' => $Post])->render();

                    return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                } else
                    return response()->json(['status' => 'error']);
                break;
            case "load_search":
                if ($request->input('tab') == 'posts') {
                    $Posts = Post::where('title', 'like', '%' . $request->input('load_search') . '%')
                        ->where('post_type', '!=', 'group_post')
                        ->where('posts.status', 'post')
                        ->orWhere('description', 'like', '%' . $request->input('load_search') . '%')
                        ->where('post_type', '!=', 'group_post')
                        ->where('posts.status', 'post')
                        ->join('users', 'posts.user_id', '=', 'users.id')
                        ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                        ->orderBy('id', 'desc')
                        ->paginate(10);

                    if (count($Posts)) {
                        foreach ($Posts as $Post)
                            $PostsArray[] = view('includes.post', ['Post' => $Post])->render();

                        return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                    } else
                        return response()->json(['status' => 'error']);
                } elseif ($request->input('tab') == 'photos') {
                    $Photos = Gallery::where('title', 'like', '%' . $request->input('load_search') . '%')
                        ->whereNull('group_id')
                        ->orWhere('description', 'like', '%' . $request->input('load_search') . '%')
                        ->whereNull('group_id')
                        ->join('users', 'gallery.user_id', '=', 'users.id')
                        ->select('gallery.*', 'users.name as user_name', 'users.avatar as user_avatar')
                        ->orderBy('id', 'desc')
                        ->paginate(10);

                    if (count($Photos)) {
                        foreach ($Photos as $Photo)
                            $PostsArray[] = view('includes.post-photo', ['Photo' => $Photo])->render();

                        return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                    } else
                        return response()->json(['status' => 'error']);
                } elseif ($request->input('tab') == 'videos') {
                    $Videos = Video::where('title', 'like', '%' . $request->input('load_search') . '%')
                        ->join('users', 'videos.user_id', '=', 'users.id')
                        ->select('videos.*', 'users.name as user_name', 'users.avatar as user_avatar')
                        ->orderBy('id', 'desc')
                        ->paginate(10);

                    if (count($Videos)) {
                        foreach ($Videos as $Video)
                            $PostsArray[] = view('includes.post-video', ['Video' => $Video])->render();

                        return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                    } else
                        return response()->json(['status' => 'error']);
                } elseif ($request->input('tab') == 'business') {
                    $Category = \App\UserCategory::where('name', 'Business')->first();
                    $Business = User::where('business', 1)
                        ->where('name', 'like', '%' . $request->input('load_search') . '%')
                        ->where('user_category_id', $Category->id)
                        ->orderBy('id', 'desc')
                        ->paginate(40);

                    if (count($Business)) {
                        foreach ($Business as $business_user)
                            $PostsArray[] = view('includes.post-business', ['business_user' => $business_user])->render();

                        return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                    } else
                        return response()->json(['status' => 'error']);
                } elseif ($request->input('tab') == 'professionals') {
                    $Category = \App\UserCategory::where('name', 'Business')->first();
                    $Business = User::where('business', 1)
                        ->where('name', 'like', '%' . $request->input('load_search') . '%')
                        ->where('user_category_id', '!=', $Category->id)
                        ->orderBy('id', 'desc')
                        ->paginate(40);

                    if (count($Business)) {
                        foreach ($Business as $business_user)
                            $PostsArray[] = view('includes.post-business', ['business_user' => $business_user])->render();

                        return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                    } else
                        return response()->json(['status' => 'error']);
                }
                break;
            case "load_group":
                $Posts = Post::whereNull('posts.deleted_at')
                    ->where('posts.group_id', $request->input('load_page_group'))
                    ->where('posts.post_type', 'group_post')
                    ->join('groups', 'posts.group_id', '=', 'groups.id')
                    ->select('posts.*', 'groups.name as group_name', 'groups.avatar as group_avatar')
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                if (count($Posts)) {
                    foreach ($Posts as $Post)
                        $PostsArray[] = view('includes.post-group', ['Post' => $Post])->render();

                    return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                } else
                    return response()->json(['status' => 'error']);
                break;
            case "load_user":
                $Posts = Post::whereNull('deleted_at')
                    ->whereIn('post_type', ['event', 'news', 'online'])
                    ->where('user_id', $request->input('load_page_user'))
                    ->where('posts.status', 'post')
                    ->join('users', 'posts.user_id', '=', 'users.id')
                    ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                if (count($Posts)) {
                    foreach ($Posts as $Post)
                        $PostsArray[] = view('includes.post-profile', ['Post' => $Post])->render();

                    return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                } else
                    return response()->json(['status' => 'error']);
                break;
            case "load_notifications":
                $Notifications = Notification::whereNull('deleted_at')
                    ->where('user_id', Auth::user()->id)
                    ->orderBy('id', 'desc')
                    ->paginate(10);

                if (count($Notifications)) {
                    Notification::readNotifications($Notifications);

                    foreach ($Notifications as $Notification)
                        $PostsArray[] = view('includes.notification', ['Notification' => $Notification])->render();

                    return response()->json(['Posts' => $PostsArray, 'status' => 'success']);
                } else
                    return response()->json(['status' => 'error']);
                break;
            default:
                return response()->json(['status' => 'error']);
                break;
        }
    }

    public function new_post(Request $request)
    {
        $UserType = User::isType();

        if (!$UserType or $UserType == 'user' or $UserType == 'public_user')
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'PostType' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        switch ($request->input('PostType')) {
            case 'News':
                $validator = Validator::make($request->all(), [
                    'PostTitle' => 'required|max:255',
                    'PostSubtitle' => 'max:255',
                    'PostCategory' => 'required',
                    'Description' => 'max:1000',
                    'Keywords' => 'max:255',
                ]);
                break;
            case 'Online':
                $validator = Validator::make($request->all(), [
                    'PostTitle' => 'required|max:255',
                    'PostSubtitle' => 'max:255',
                    'Start' => 'required',
                    'StartTime' => 'required',
                    'End' => 'required',
                    'EndTime' => 'required',
                    'Description' => 'max:1000',
                    'Keywords' => 'max:255',
                ]);
                break;
            case 'group_post':
                $validator = Validator::make($request->all(), [
                    'PostTitle' => 'required|max:255',
                    'PostSubtitle' => 'max:255',
                    'groupId' => 'required',
                    'Description' => 'max:1000',
                    'Keywords' => 'max:255',
                ]);
                break;
            default:
                $validator = Validator::make($request->all(), [
                    'PostTitle' => 'required|max:255',
                    'PostSubtitle' => 'max:255',
                    'Location' => 'max:255',
                    'Frequency' => 'max:255',
                    'PostCategory' => 'required',
                    'StartEvent' => 'required',
                    'EndEvent' => 'required',
                    'Description' => 'max:1000',
                    'Keywords' => 'max:255',
                ]);
                break;
        }

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        if (!is_null($request->input('draft_id'))) {
            $Post = Post::find($request->input('draft_id'));

            if (is_null($Post))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Post not found.'
                ]);

            if ($request->input('file_type') != NULL)
                $Post->file_type = $request->input('file_type');
            if ($request->input('file_name') != NULL)
                $Post->file_name = $request->input('file_name');

            if ($request->input('preview_video') != NULL and $request->input('file_type') != NULL and $request->input('file_type') == 'video')
                $Post->video_preview = $request->input('preview_video');
            else
                $Post->video_preview = NULL;

            $Post->title = strip_tags($request->input('PostTitle'));
            $Post->subtitle = strip_tags($request->input('PostSubtitle'));
            $Post->location = $request->input('Location');
            $Post->frequency = $request->input('Frequency');
            if (!is_null($request->input('StartEvent')))
                $Post->start = date('Y-m-d', strtotime($request->input('StartEvent')));
            else
                $Post->start = date('Y-m-d', strtotime($request->input('Start')));
            $Post->start_time = date('H:i', strtotime($request->input('StartTime')));
            if (!is_null($request->input('EndEvent')))
                $Post->end = date('Y-m-d', strtotime($request->input('EndEvent')));
            else
                $Post->end = date('Y-m-d', strtotime($request->input('End')));
            $Post->end_time = date('H:i', strtotime($request->input('EndTime')));
            $Post->category_id = $request->input('PostCategory');
            $Post->description = strip_tags($request->input('Description'));
            $Post->keywords = strip_tags($request->input('Keywords'));
            $Post->post_type = strtolower($request->input('PostType'));
            $Post->save();

            if (is_null($request->input('status'))) {
                $NewPost = new Post;

                $NewPost->user_id = $Post->user_id;
                $NewPost->post_type = $Post->post_type;
                $NewPost->file_type = $Post->file_type;
                $NewPost->file_name = $Post->file_name;
                $NewPost->video_preview = $Post->video_preview;
                $NewPost->title = $Post->title;
                $NewPost->location = $Post->location;
                $NewPost->frequency = $Post->frequency;
                $NewPost->start = $Post->start;
                $NewPost->start_time = $Post->start_time;
                $NewPost->end = $Post->end;
                $NewPost->end_time = $Post->end_time;
                $NewPost->category_id = $Post->category_id;
                $NewPost->description = $Post->description;
                $NewPost->keywords = $Post->keywords;

                $NewPost->save();
                $Post->delete();

                $Post = $NewPost;
            }
        } elseif (!is_null($request->input('post_id'))) {
            $Post = Post::find($request->input('post_id'));

            if (is_null($Post))
                return response()->json([
                    'status' => 'error',
                    'message' => 'Post not found.'
                ]);

            if ($request->input('file_type') != NULL)
                $Post->file_type = $request->input('file_type');
            if ($request->input('file_name') != NULL)
                $Post->file_name = $request->input('file_name');

            if ($request->input('preview_video') != NULL and $request->input('file_type') != NULL and $request->input('file_type') == 'video')
                $Post->video_preview = $request->input('preview_video');
            else
                $Post->video_preview = NULL;

            $Post->status = is_null($request->input('status')) ? 'post' : 'draft';
            $Post->title = strip_tags($request->input('PostTitle'));
            $Post->location = $request->input('Location');
            $Post->frequency = $request->input('Frequency');
            if (!is_null($request->input('StartEvent')))
                $Post->start = date('Y-m-d', strtotime($request->input('StartEvent')));
            else
                $Post->start = date('Y-m-d', strtotime($request->input('Start')));
            $Post->start_time = date('H:i', strtotime($request->input('StartTime')));
            if (!is_null($request->input('EndEvent')))
                $Post->end = date('Y-m-d', strtotime($request->input('EndEvent')));
            else
                $Post->end = date('Y-m-d', strtotime($request->input('End')));
            $Post->end_time = date('H:i', strtotime($request->input('EndTime')));
            $Post->category_id = $request->input('PostCategory');
            $Post->description = strip_tags($request->input('Description'));
            $Post->keywords = strip_tags($request->input('Keywords'));
            $Post->post_type = strtolower($request->input('PostType'));
            $Post->save();
        } else {
            $Post = new Post;

            $Post->user_id = Auth::user()->id;
            if ($request->input('file_type') != NULL)
                $Post->file_type = $request->input('file_type');
            if ($request->input('file_name') != NULL)
                $Post->file_name = $request->input('file_name');
            if ($request->input('groupId') != NULL)
                $Post->group_id = $request->input('groupId');

            if ($request->input('preview_video') != NULL and $request->input('file_type') != NULL and $request->input('file_type') == 'video')
                $Post->video_preview = $request->input('preview_video');

            $Post->status = is_null($request->input('status')) ? 'post' : 'draft';
            $Post->title = strip_tags($request->input('PostTitle'));
            $Post->location = $request->input('Location');
            $Post->frequency = $request->input('Frequency');
            if (!is_null($request->input('StartEvent')))
                $Post->start = date('Y-m-d', strtotime($request->input('StartEvent')));
            else
                $Post->start = date('Y-m-d', strtotime($request->input('Start')));
            $Post->start_time = date('H:i', strtotime($request->input('StartTime')));
            if (!is_null($request->input('EndEvent')))
                $Post->end = date('Y-m-d', strtotime($request->input('EndEvent')));
            else
                $Post->end = date('Y-m-d', strtotime($request->input('End')));
            $Post->end_time = date('H:i', strtotime($request->input('EndTime')));
            $Post->category_id = $request->input('PostCategory');
            $Post->description = strip_tags($request->input('Description'));
            $Post->keywords = strip_tags($request->input('Keywords'));
            $Post->post_type = strtolower($request->input('PostType'));
            $Post->save();
        }

        $Post = \App\Tag::checkAndCreateTag($Post);

        if (!is_null($Post->group_id)) {
            $Post = Post::whereNull('posts.deleted_at')
                ->where('posts.id', $Post->id)
                ->where('posts.post_type', 'group_post')
                ->whereNotNull('posts.group_id')
                ->join('groups', 'posts.group_id', '=', 'groups.id')
                ->select('posts.*', 'groups.name as group_name', 'groups.avatar as group_avatar')
                ->orderBy('id', 'desc')
                ->first();

            $view = 'post-group';

            $GroupFollowers = GroupFollower::where('group_id', $Post->group_id)->get();

            $param = [
                'group_id' => $Post->group_id,
                'text' => 'Added new post.',
                'group_check' => 1
            ];

            foreach ($GroupFollowers as $GroupFollower)
                Notification::checkAndCreateNotification($GroupFollower->user_id, 'group_post', $param);
        } else {
            $Post = Post::whereNull('deleted_at')
                ->where('posts.id', $Post->id)
                ->join('users', 'posts.user_id', '=', 'users.id')
                ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                ->orderBy('id', 'desc')
                ->first();

            $view = 'post';
        }

        if ($Post->status == 'draft') {
            $view = 'draft-post';
        }

        return response()->json([
            'Post' => view('includes.' . $view, ['Post' => $Post])->render(),
            'status' => 'success'
        ]);
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('id'));

        if ($Post == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        if ($Post->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        if ($Post->post_type == 'event') {
            $followers = \App\ConnectionRequest::getFollowers();

            $param = [
                'user_id' => $Post->user_id,
                'text' => 'Event "' . $Post->title . '" deleted.'
            ];

            foreach ($followers as $follower) {
                $user_id = $follower->user_id_1 == \Auth::user()->id ? $follower->user_id_2 : $follower->user_id_1;
                Notification::checkAndCreateNotification($user_id, 'deleting_event', $param);
            }
        }

        $Post->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }


    public function get(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('id'));

        if ($Post == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        if ($Post->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        return response()->json([
            'status' => 'success',
            'Post' => $Post
        ]);
    }

    public function publish(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('id'));

        if ($Post == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        if ($Post->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $NewPost = new Post;

        $NewPost->user_id = $Post->user_id;
        $NewPost->post_type = $Post->post_type;
        $NewPost->file_type = $Post->file_type;
        $NewPost->file_name = $Post->file_name;
        $NewPost->video_preview = $Post->video_preview;
        $NewPost->title = $Post->title;
        $NewPost->location = $Post->location;
        $NewPost->frequency = $Post->frequency;
        $NewPost->start = $Post->start;
        $NewPost->start_time = $Post->start_time;
        $NewPost->end = $Post->end;
        $NewPost->end_time = $Post->end_time;
        $NewPost->category_id = $Post->category_id;
        $NewPost->description = $Post->description;
        $NewPost->keywords = $Post->keywords;

        $NewPost->save();
        $Post->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function like(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Post = Post::find($request->input('id'));

        if ($Post == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Post not found.'
            ]);

        $PostLike = PostLike::where('post_id', $request->input('id'))->where('user_id', Auth::user()->id)->first();

        if ($PostLike != NULL) {
            $PostLike->delete();
        } else {
            $PostLike = new PostLike;
            $PostLike->post_id = $request->input('id');
            $PostLike->user_id = Auth::user()->id;
            $PostLike->save();

            $param = [
                'user_id' => $PostLike->user_id,
                'text' => 'Liked your post "' . $Post->title . '"'
            ];

            if ($Post->post_type == 'group_post') {
                $Group = Group::find($Post->group_id);

                if (!is_null($Group))
                    $param['text'] .= ', in the group&nbsp;<a href="' . route('group', ['id' => $Group->id]) . '">' . $Group->name . '</a>.';
                else
                    $param['text'] .= '.';
            } else
                $param['text'] .= '.';

            Notification::checkAndCreateNotification($Post->user_id, 'like_post', $param);
        }

        return response()->json([
            'status' => 'success',
            'count_like' => PostLike::where('post_id', $request->input('id'))->count()
        ]);
    }

    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Posts = Post::getSearchPostsByUser($request->input('user_id'), $request->input('search'), 10);
        $posts = [];

        $user = User::find($request->input('user_id'));

        foreach ($Posts as $Post)
            $posts[] = view('includes/news_block', ['Post' => $Post, 'user' => $user])->render();

        return response()->json([
            'status' => 'success',
            'posts' => $posts
        ]);
    }
}