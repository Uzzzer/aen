<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Tag;
use App\Post;

class TagController extends BaseController {

    public function tag($tag)
    {
        return view('guest.index', [
            'title' => 'Ladies',
            'Posts' => Post::whereNull('posts.deleted_at')
                ->rightjoin('tags', 'posts.id', '=', 'tags.post_id')
                ->where('tags.tag', $tag)
                ->where('posts.status', 'post')
                ->join('users', 'posts.user_id', '=', 'users.id')
                ->select('posts.*', 'users.name as user_name', 'users.avatar as user_avatar')
                ->orderBy('posts.id', 'desc')
                ->paginate(10),
            'tag' => $tag,
            'user' => Auth::user()
        ]);
    }

}