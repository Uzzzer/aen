<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Validator;
use App\User;
use App\Post;
use App\Group;
use App\Gallery;
use App\GroupFollower;
use App\GroupRequest;
use App\VideoGallery;
use App\Notification;
use App\NotificationOffGroup;
use App\NotificationType;

class GroupController extends Controller
{

    public function group($id)
    {
        $UserType = User::isType();

        if (!$UserType or $UserType == 'public_user')
            return redirect(route('profile'));

        $Group = Group::find($id);

        if (is_null($Group))
            return redirect(route('groups'));

        Group::UpdateFeed($Group->id);

        return view('guest.group', [
            'Group' => $Group,
            'Posts' => Post::whereNull('posts.deleted_at')
                            ->where('posts.group_id', $Group->id)
                            ->where('posts.post_type', 'group_post')
                            ->join('groups', 'posts.group_id', '=', 'groups.id')
                            ->select('posts.*', 'groups.name as group_name', 'groups.avatar as group_avatar')
                            ->limit(10)
                            ->orderBy('id', 'desc')
                            ->get(),
            'user' => Auth::user(),
            'css' => [asset('/css/newpage.css')]
        ]);
    }

    public function followers_group($id) {
        $Group = Group::find($id);

        if (is_null($Group) or $Group->status == 'blocked')
            return redirect(route('groups'));

        if (Group::checkPrivateForUser($Group->id))
            return redirect(route('groups'));

        return view('guest.followers_group', [
            'Group' => $Group,
            'Followers' => GroupFollower::getFollowers($Group->id),
            'user' => Auth::user()
        ]);
    }

    public function gallery_group($id) {
        $Group = Group::find($id);

        if (is_null($Group) or $Group->status == 'blocked')
            return redirect(route('groups'));

        if (Group::checkPrivateForUser($Group->id))
            return redirect(route('groups'));

        return view('guest.gallery_group', [
            'Group' => $Group,
            'Gallery' => Gallery::where('group_id', $Group->id)->orderBy('id', 'desc')->get(),
            'user' => Auth::user(),
            'js' => [asset('/js/gallery.js')]
        ]);
    }

    public function videos_group($id) {
        $Group = Group::find($id);

        if (is_null($Group) or $Group->status == 'blocked')
            return redirect(route('groups'));

        if (Group::checkPrivateForUser($Group->id))
            return redirect(route('groups'));

        return view('guest.videos_group', [
            'Group' => $Group,
            'VideoGallery' => VideoGallery::where('group_id', $Group->id)->orderBy('id', 'desc')->get(),
            'user' => Auth::user(),
            'js' => [asset('/js/videos_group.js')]
        ]);
    }

    public function video_group($id, $vid) {
        $Group = Group::find($id);
        $VideoGallery = VideoGallery::find($vid);

        if (is_null($Group) or is_null($VideoGallery) or $VideoGallery->group_id != $Group->id or $Group->status == 'blocked')
            return redirect(route('groups'));

        if (Group::checkPrivateForUser($Group->id))
            return redirect(route('groups'));

        \App\MediaView::viewVideo($VideoGallery->id);

        return view('guest.video_group', [
            'Group' => $Group,
            'VideoGallery' => $VideoGallery,
            'user' => Auth::user(),
            'js' => [asset('/js/videos_group.js')]
        ]);
    }

    public function photo_group($id, $pid) {
        $Group = Group::find($id);
        $Gallery = Gallery::find($pid);

        if (is_null($Group) or is_null($Gallery) or $Gallery->group_id != $Group->id or $Group->status == 'blocked')
            return redirect(route('groups'));

        if (Group::checkPrivateForUser($Group->id))
            return redirect(route('groups'));

        \App\MediaView::viewPhoto($Gallery->id);

        return view('guest.photo_group', [
            'Group' => $Group,
            'Gallery' => $Gallery,
            'user' => Auth::user(),
            'js' => [asset('/js/gallery.js')]
        ]);
    }

    public function settings_group($id) {
        $Group = Group::find($id);

        if (is_null($Group) or !Auth::check() or Auth::user()->id != $Group->user_id or $Group->status == 'blocked')
            return redirect(route('groups'));

        return view('user.settings_group', [
            'Group' => $Group,
            'Gallery' => Gallery::where('group_id', $Group->id)->orderBy('id', 'desc')->get(),
            'VideoGallery' => VideoGallery::where('group_id', $Group->id)->orderBy('id', 'desc')->get(),
            'GroupRequest' => GroupRequest::where('group_requests.group_id', $Group->id)->join('users', 'group_requests.user_id', '=', 'users.id')->select('group_requests.*', 'users.avatar', 'users.name')->orderBy('group_requests.id', 'desc')->get(),
            'user' => Auth::user(),
            'js' => [
                asset('js/group-settings.js')
            ]
        ]);
    }

    public function update(Request $request)
    {
        $Group = Group::find($request->input('group_id'));

        if (is_null($Group) or $Group->status == 'blocked')
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        if (!Auth::check() or $Group->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'GroupName' => 'required|string|max:255',
            'GroupCategories' => 'required',
            'Keywords' => 'max:255',
            'GroupType' => 'required|max:255',
            'Description' => 'max:1000',
            'avatar' => 'required|max:255',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);


        $Group->avatar = $request->input('avatar');
        $Group->name = $request->input('GroupName');
        $Group->description = $request->input('Description');
        $Group->keywords = $request->input('Keywords');
        $Group->type = $request->input('GroupType');
        $Group->category_id = $request->input('GroupCategories');
        $Group->save();

        if ($Group->type != 'private') {
            $GroupRequest = GroupRequest::where('group_id', $Group->id)->get();

            foreach ($GroupRequest as $req) {
                $GroupFollower = new GroupFollower;
                $GroupFollower->group_id = $Group->id;
                $GroupFollower->user_id = $req->user_id;
                $GroupFollower->save();
            }

            GroupRequest::where('group_id', $Group->id)->delete();
        }

        return response()->json(['status' => 'success']);
    }

    public function groups()
    {
        return view('guest.groups', [
            'Groups' => Group::orderBy('id', 'desc')->get(),
            'user' => Auth::user()
        ]);
    }

    public function subscribed_groups()
    {
        return view('user.subscribed-groups', [
            'Groups' => GroupFollower::where('group_followers.user_id', Auth::user()->id)
                            ->join('groups', 'group_followers.group_id', '=', 'groups.id')
                            ->select('group_followers.*', 'groups.*', 'groups.user_id as owner_id')
                            ->orderBy('group_followers.id', 'desc')
                            ->get(),
            'user' => Auth::user()
        ]);
    }

    public function my_groups()
    {
        return view('user.my-groups', [
            'Groups' => Group::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get(),
            'user' => Auth::user()
        ]);
    }

    public function new_group(Request $request) {
        $UserType = User::isType();

        if (!$UserType or $UserType == 'user' or $UserType == 'public_user')
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'GroupName' => 'required|max:255',
            'GroupType' => 'required|max:255',
            'avatar' => 'max:255',
            'Description' => 'max:1000',
            'Keywords' => 'max:255',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Group = new Group;
        $Group->user_id = Auth::user()->id;
        $Group->avatar = ($request->input('file_name') != NULL) ? $request->input('file_name') : 'img/no-avatar.png';
        $Group->name = $request->input('GroupName');
        $Group->description = $request->input('Description');
        $Group->keywords = $request->input('Keywords');
        $Group->category_id = $request->input('GroupCategories');
        $Group->type = $request->input('GroupType');
        $Group->save();

        $GroupFollower = new GroupFollower;
        $GroupFollower->group_id = $Group->id;
        $GroupFollower->user_id = $Group->user_id;
        $GroupFollower->save();

        $html = '
                        <div class="groups-item" data-id="'.$Group->id.'">
                            <img src="'.asset($Group->avatar).'" alt="">
                            <div class="groups-item-content">
                                <a href="'.route('group', ['id' => $Group->id]).'" class="groups-item-title">
                                    <b>'.$Group->name.'</b>
                                    <span>10+ unread post</span>
                                </a>
                                <a href="#" class="groups-item-wheel"></a>
                                <ul class="groups-item-options">
                                    <li><a href="#">Edit notification settings</a></li>
                                    <li><a href="#" class="leave_group" data-id="'.$Group->id.'">Leave group</a></li>
                                    <li><a href="#">Pin to shortcuts</a></li>
                                </ul>
                            </div>
                        </div>
        ';

        return response()->json([
            'status' => 'success',
            'html' => $html
        ]);
    }

    public function notifications_group($id) {
        $Group = Group::find($id);

        if (is_null($Group) or $Group->status == 'blocked')
            return redirect(route('groups'));

        if (Group::checkPrivateForUser($Group->id))
            return redirect(route('groups'));

        $NotificationOffGroup = NotificationOffGroup::where('user_id', Auth::user()->id)->where('group_id', $Group->id)->get();

        $NotificationOff = [];
        foreach ($NotificationOffGroup as $off)
            $NotificationOff[] = $off->type_id;

        return view('user.notifications_group', [
            'Group' => $Group,
            'user' => Auth::user(),
            'NotificationOff' => $NotificationOff,
            'NotificationTypes' => NotificationType::whereIn('name', ['group_post', 'group_photo', 'group_video'])->get(),
            'css' => [asset('/css/newpage.css')]
        ]);
    }

    public function approve(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $GroupRequest = GroupRequest::find($request->input('id'));

        if ($GroupRequest == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Request not found.'
            ]);

        $Group = Group::find($GroupRequest->group_id);

        if (is_null($Group))
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        if (!Auth::check() or $Group->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $GroupFollower = GroupFollower::where('group_id', $Group->id)->where('user_id', $GroupRequest->user_id)->first();

        if ($GroupFollower != NULL) {
            $GroupRequest->delete();

            return response()->json([
                'status' => 'error',
                'message' => 'User has already subscribed.'
            ]);
        }

        $GroupFollower = new GroupFollower;
        $GroupFollower->group_id = $GroupRequest->group_id;
        $GroupFollower->user_id = $GroupRequest->user_id;
        $GroupFollower->save();

        $param = [
            'group_id' => $GroupFollower->group_id,
            'text' => 'Your request has been approved.'
        ];

        Notification::checkAndCreateNotification($GroupFollower->user_id, 'group_request', $param);

        $GroupRequest->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function cancel(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $GroupRequest = GroupRequest::find($request->input('id'));

        if ($GroupRequest == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Request not found.'
            ]);

        $Group = Group::find($GroupRequest->group_id);

        if (is_null($Group))
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        if (!Auth::check() or $Group->user_id != Auth::user()->id)
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $param = [
            'group_id' => $Group->id,
            'text' => 'Your request has been canceled.'
        ];

        Notification::checkAndCreateNotification($GroupRequest->user_id, 'group_request', $param);

        $GroupRequest->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function join(Request $request) {
        $UserType = User::isType();

        if (!$UserType or $UserType == 'public_user')
            return response()->json([
                'status' => 'error',
                'message' => 'No access.'
            ]);

        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Group = Group::find($request->input('id'));

        if ($Group == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        $GroupFollower = GroupFollower::where('group_id', $Group->id)->where('user_id', Auth::user()->id)->first();

        if ($GroupFollower != NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'You are already subscribed to a group.'
            ]);

        if ($Group->type == 'private' and $Group->user_id != Auth::user()->id) {

            $GroupRequest = GroupRequest::where('group_id', $Group->id)->where('user_id', Auth::user()->id)->first();

            if ($GroupRequest != NULL)
                return response()->json([
                    'status' => 'error',
                    'message' => 'You have already sent a request.'
                ]);

            $GroupRequest = new GroupRequest;
            $GroupRequest->group_id = $Group->id;
            $GroupRequest->user_id = Auth::user()->id;
            $GroupRequest->save();
        } else {
            $GroupFollower = new GroupFollower;
            $GroupFollower->group_id = $Group->id;
            $GroupFollower->user_id = Auth::user()->id;
            $GroupFollower->save();
        }

        return response()->json([
            'status' => 'success',
            'message' => ($Group->type != 'private' or $Group->user_id == Auth::user()->id) ? 'You have subscribed to the group.' : 'Request has been sent.'
        ]);
    }

    public function leave(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()[0]
            ]);

        $Group = Group::find($request->input('id'));

        if ($Group == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'Group not found.'
            ]);

        $GroupFollower = GroupFollower::where('group_id', $Group->id)->where('user_id', Auth::user()->id)->first();

        if ($GroupFollower == NULL)
            return response()->json([
                'status' => 'error',
                'message' => 'You are not subscribed to this group.'
            ]);

        $GroupFollower->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

}