<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model {

	protected $table = 'post_categories';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}
