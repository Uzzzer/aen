<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CategoriesAvailable extends Model {

	protected $table = 'categories_available';
	protected $primaryKey = 'id';

    protected $fillable = ['name'];

}
