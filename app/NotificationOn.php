<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class NotificationOn extends Model {

	protected $table = 'notifications_user_on';
	protected $primaryKey = 'id';

    protected $fillable = ['type_id', 'user_id'];

}
