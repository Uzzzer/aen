<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentContent extends Model{
	protected $table = 'payment_content';
	protected $primaryKey = 'id';

    protected $fillable = ['type', 'obj_id', 'token', 'download_count'];


}
