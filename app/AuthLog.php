<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class AuthLog extends Model {

	protected $table = 'auth_logs';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}
