<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveStream extends Model {

    protected $table = 'live_streams';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'status', 'conversation_id'];


    /**
     * Get user from stream.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Start stream
     *
     * @param $userId
     * @return mixed
     */
    public static function startStream($userId, $conversation_id) {
        self::stopAllStreamForUser($userId);

        return self::create([
            'user_id' => $userId,
            'conversation_id' => $conversation_id,
            'status' => 'stream'
        ]);
    }

    /**
     * Stop stream.
     *
     * @param $streamId
     * @param string $status
     * @return mixed
     */
    public static function stopStream($streamId, $status = 'stop') {
        return self::find($streamId)->update([
            'status' => $status
        ]);
    }

    /**
     * Stop all stream for user.
     *
     * @param $userId
     * @param $ignoreId
     * @return mixed
     */
    public static function stopAllStreamForUser($userId, $ignoreId = false) {
        $streams = self::where('user_id', $userId);

        if ($ignoreId) {
            $streams->where('id', '!=', $ignoreId);
        }

        return $streams->update([
            'status' => 'stop'
        ]);
    }
}