<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Stream extends Model {

    protected $table = 'streams';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id'];

}