<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class JobRequest extends Model {

	protected $table = 'job_requests';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}
