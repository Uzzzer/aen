<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class GroupCategory extends Model {

	protected $table = 'group_categories';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}