<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use MetzWeb\Instagram\Instagram;
use Illuminate\Support\Facades\Auth;

class InstagramPost extends Model {

	protected $table = 'instagram_posts';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function check($user_id = false) {
        if (!$user_id and Auth::check())
            $user_id = Auth::user()->id;

        $InstagramPost = InstagramPost::where('user_id', $user_id)->first();

        return (!is_null($InstagramPost)) ? true : false;
    }

    public static function getPost($user_id = false) {
        if (!$user_id and Auth::check())
            $user_id = Auth::user()->id;

        return InstagramPost::where('user_id', $user_id)->first();
    }

    public static function getRecentPost($instagram, $user_id = false) {

        if (!$user_id and Auth::check())
            $user_id = Auth::user()->id;

        $recent_photo = $instagram->getUserMedia('self', 1);

        if (isset($recent_photo->data[0]) and isset($recent_photo->data[0]->id)) {
            $InstagramPost = InstagramPost::where('user_id', $user_id)->first();

            if (!is_null($InstagramPost)) {
                if ($InstagramPost->post_id != $recent_photo->data[0]->id) {
                    InstagramPost::where('user_id', $user_id)->delete();
                    $InstagramPost = new InstagramPost;
                    $InstagramPost->user_id = $user_id;
                    $InstagramPost->filename = $recent_photo->data[0]->images->standard_resolution->url;
                    $InstagramPost->post_id = $recent_photo->data[0]->id;
                    $InstagramPost->user_name = $recent_photo->data[0]->user->username;
                    $InstagramPost->profile_picture = $recent_photo->data[0]->user->profile_picture;
                    $InstagramPost->save();
                }
            } else {
                $InstagramPost = new InstagramPost;
                $InstagramPost->user_id = $user_id;
                $InstagramPost->filename = $recent_photo->data[0]->images->standard_resolution->url;
                $InstagramPost->post_id = $recent_photo->data[0]->id;
                $InstagramPost->user_name = $recent_photo->data[0]->user->username;
                $InstagramPost->profile_picture = $recent_photo->data[0]->user->profile_picture;
                $InstagramPost->save();
            }

        } elseif (isset($recent_photo->meta->error_message)) {

            SocialAuth::where('user_id', $user_id)->where('type', 'instagram')->delete();

        }
    }

}