<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class NotificationOffGroup extends Model {

	protected $table = 'notifications_group_off';
	protected $primaryKey = 'id';

    protected $fillable = ['type_id', 'user_id', 'group_id'];

}
