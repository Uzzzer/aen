<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MediaCommentLike extends Model {

	protected $table = 'media_comment_likes';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}