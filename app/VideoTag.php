<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class VideoTag extends Model {

	protected $table = 'video_tags';
	protected $primaryKey = 'id';

    //protected $fillable = [];

}