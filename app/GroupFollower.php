<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupFollower extends Model {

	protected $table = 'group_followers';
	protected $primaryKey = 'id';

    //protected $fillable = [];

    public static function CheckFollowGroup($group_id, $user_id = false) {
        if ($user_id == false and !Auth::check())
            return false;
        elseif ($user_id == false and Auth::check())
            $user_id = Auth::user()->id;

        if (GroupFollower::where('user_id', $user_id)->where('group_id', $group_id)->first() == NULL)
            return false;
        else
            return true;
    }

    public static function getCountFollow($user_id = false) {
        if ($user_id == false and !Auth::check())
            return false;
        elseif ($user_id == false and Auth::check())
            $user_id = Auth::user()->id;

        return GroupFollower::where('user_id', $user_id)->count();
    }

    public static function getCountFollowersGroup($group_id) {
        return GroupFollower::where('group_id', $group_id)->count();
    }

    public static function getRandomMembers($group_id, $limit) {
        return GroupFollower::where('group_id', $group_id)->join('users', 'group_followers.user_id', '=', 'users.id')->inRandomOrder()->limit($limit)->get();
    }

    public static function getFollowers($group_id) {
        return GroupFollower::where('group_id', $group_id)->join('users', 'group_followers.user_id', '=', 'users.id')->orderBy('group_followers.id', 'desc')->get();
    }

}